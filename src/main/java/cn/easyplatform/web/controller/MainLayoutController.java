/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.vos.LoginUserVo;
import org.zkoss.util.resource.Labels;
import org.zkoss.xel.VariableResolver;
import org.zkoss.xel.util.SimpleResolver;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zk.ui.util.ComposerExt;
import org.zkoss.zul.A;
import org.zkoss.zul.Label;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MainLayoutController implements Composer<Component>,ComposerExt<Component> {

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component parent,
                                         ComponentInfo compInfo) throws Exception {
        Map<String, Object> variables = new HashMap<String, Object>();
        VariableResolver variableResolver = new SimpleResolver(variables);
        variables.put("$easyplatform", this);
        page.addVariableResolver(variableResolver);
        return compInfo;
    }

    @Override
    public void doBeforeComposeChildren(Component component) throws Exception {

    }

    @Override
    public boolean doCatch(Throwable throwable) throws Exception {
        return false;
    }

    @Override
    public void doFinally() throws Exception {

    }

    @Override
    public void doAfterCompose(Component comp) throws Exception {
    }

    public void logout() {

    }

    public void menu(Component comp) {

    }

    public void socket(Component comp) {

    }

    public void go(Component c, String taskId) {

    }

    public void go(Component c, String taskId, int openMode) {

    }

    public boolean isRole(String roles) {
        return true;
    }

    public void user(Component c) {
        if (c instanceof Label) {
            Label header = (Label) c;
            if (Strings.isBlank(header.getValue()))
                header.setValue(Labels.getLabel("app.welcome") + ","
                        + Contexts.getUser().getName());
        } else if (c instanceof A) {
            A a = (A) c;
            if (Strings.isBlank(a.getLabel()))
                a.setLabel(Labels.getLabel("app.welcome") + ","
                        + Contexts.getUser().getName());
        }
    }

    public void title(Component c) {
        if (c instanceof Label) {
            Label header = (Label) c;
            header.setValue(Contexts.getProject().getName());
        } else if (c instanceof A) {
            A a = (A) c;
            a.setLabel(Contexts.getProject().getName());
        }
    }

    public LoginUserVo getUser() {
        return Contexts.getUser();
    }

    public String getOrgId() {
        return Contexts.getProject().getId();
    }

    public String getOrgName() {
        return Contexts.getProject().getName();
    }

    public Object getOrgs() {
        return new ArrayList<>(0);
    }

    public void task(String taskId) {

    }

    public void task(Component comp, String taskId) {

    }

    public void task(Component comp, String taskId, boolean title) {

    }

    public void task(Component comp, String taskId, String roles) {

    }

    public void task(final Component comp, final String taskId,
                     final boolean title, final String roles) {

    }

    public void setting(Component c) {

    }

    public void changepwd(Component c) {

    }

    public void selectOrg(Component c) {

    }
}
