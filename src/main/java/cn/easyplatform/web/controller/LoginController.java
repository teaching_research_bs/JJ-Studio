/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.web.controller;

import cn.easyplatform.studio.context.Contexts;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zul.Label;
import org.zkoss.zul.Panel;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LoginController implements Composer<Component> {
    @Override
    public void doAfterCompose(Component comp) throws Exception {
        Component c = comp.getFellowIfAny("headerTitle");
        if (c != null)
            ((Label) c).setValue(Contexts.getProject().getName());
        else {
            Panel panel = (Panel) comp.query("panel");
            if (panel != null)
                panel.setTitle(Contexts.getProject().getName());
        }
    }
}
