/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dos;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SerialDo {

	private long maxKey;

	private long nextKey;

	private int poolSize;

	private String keyName;

	private int incVal;

	/**
	 * @param keyName
	 * @param poolSize
	 */
	public SerialDo(String keyName, int poolSize) {
		this(keyName, poolSize, 1);
	}

	/**
	 * @param keyName
	 * @param poolSize
	 * @param incVal
	 */
	public SerialDo(String keyName, int poolSize, int incVal) {
		this.poolSize = poolSize;
		this.keyName = keyName;
		this.incVal = incVal;
		this.nextKey = incVal;
	}

	/**
	 * @return the maxKey
	 */
	public long getMaxKey() {
		return maxKey;
	}

	/**
	 * @param maxKey
	 *            the maxKey to set
	 */
	public void setMaxKey(long maxKey) {
		this.maxKey = maxKey;
	}

	/**
	 * @return the incVal
	 */
	public int getIncVal() {
		return incVal;
	}

	/**
	 * @param incVal
	 *            the incVal to set
	 */
	public void setIncVal(int incVal) {
		this.incVal = incVal;
	}

	/**
	 * @return the nextKey
	 */
	public long getNextKey() {
		return nextKey;
	}

	/**
	 * @param nextKey
	 *            the nextKey to set
	 */
	public void setNextKey(long nextKey) {
		this.nextKey = nextKey;
	}

	/**
	 * @return the poolSize
	 */
	public int getPoolSize() {
		return poolSize;
	}

	/**
	 * @return the keyName
	 */
	public String getKeyName() {
		return keyName;
	}

}
