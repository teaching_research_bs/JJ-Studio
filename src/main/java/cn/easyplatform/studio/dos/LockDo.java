/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dos;

import java.io.Serializable;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LockDo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String entityId;
	
	private String projectId;
	
	private String userId;

	/**
	 * @param entityId
	 * @param projectId
	 * @param userId
	 */
	public LockDo(String entityId, String projectId, String userId) {
		this.entityId = entityId;
		this.projectId = projectId;
		this.userId = userId;
	}

	public String getEntityId() {
		return entityId;
	}

	public String getProjectId() {
		return projectId;
	}

	public String getUserId() {
		return userId;
	}
	
	
}
