package cn.easyplatform.studio.vos;

import cn.easyplatform.entities.beans.table.TableBean;

import java.util.ArrayList;
import java.util.List;

public class SqlWizardVo {
    private List<SqlFieldVo> sqlFieldVoList = new ArrayList<>();

    private List<SqlConditionVo> sqlConditionVoList = new ArrayList<>();

    private List<SqlOrderVo> sqlOrderVoList = new ArrayList<>();

    private List<TableBean> sqlTableBeanList = new ArrayList<>();

    private List<SqlConditionVo> sqlHavingVoList = new ArrayList<>();

    private SqlLimitVo limitVo = new SqlLimitVo();


    public List<SqlFieldVo> getSqlFieldVoList() {
        return sqlFieldVoList;
    }

    public void setSqlFieldVoList(List<SqlFieldVo> sqlFieldVoList) {
        this.sqlFieldVoList = sqlFieldVoList;
    }


    public List<SqlConditionVo> getSqlConditionVoList() {
        return sqlConditionVoList;
    }

    public void setSqlConditionVoList(List<SqlConditionVo> sqlConditionVoList) {
        this.sqlConditionVoList = sqlConditionVoList;
    }

    public List<SqlOrderVo> getSqlOrderVoList() {
        return sqlOrderVoList;
    }

    public void setSqlOrderVoList(List<SqlOrderVo> sqlOrderVoList) {
        this.sqlOrderVoList = sqlOrderVoList;
    }

    public List<TableBean> getSqlTableBeanList() {
        return sqlTableBeanList;
    }

    public void setSqlTableBeanList(List<TableBean> sqlTableBeanList) {
        this.sqlTableBeanList = sqlTableBeanList;
    }

    public List<SqlConditionVo> getSqlHavingVoList() {
        return sqlHavingVoList;
    }

    public void setSqlHavingVoList(List<SqlConditionVo> sqlHavingVoList) {
        this.sqlHavingVoList = sqlHavingVoList;
    }

    public SqlLimitVo getLimitVo() {
        return limitVo;
    }

    public void setLimitVo(SqlLimitVo limitVo) {
        this.limitVo = limitVo;
    }
}
