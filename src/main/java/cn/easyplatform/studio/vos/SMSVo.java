package cn.easyplatform.studio.vos;

import java.io.Serializable;
import java.util.Date;

public class SMSVo implements Serializable,Cloneable {

    public enum SMSType {
        SMSRegister("register"),
        SMSRetrieve("retrieve");

        private String name;
        private SMSType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String phoneNO;
    private String content;
    private Date createDate;
    private String type;

    public SMSVo(){}

    public SMSVo(String phoneNO, String content, Date createDate, String type) {
        this.phoneNO = phoneNO;
        this.content = content;
        this.createDate = createDate;
        this.type = type;
    }
    public Object clone() {
        SMSVo o = null;
        try {
            o = (SMSVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        return o;
    }
    public String getPhoneNO() {
        return phoneNO;
    }

    public void setPhoneNO(String phoneNO) {
        this.phoneNO = phoneNO;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
