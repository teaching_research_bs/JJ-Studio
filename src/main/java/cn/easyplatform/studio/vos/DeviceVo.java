package cn.easyplatform.studio.vos;


import cn.easyplatform.entities.beans.page.BindVariable;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(
        name = "device"
)
@XmlType(
        propOrder = {"style", "javascript", "bindVariables"}
)
@XmlAccessorType(XmlAccessType.NONE)
public class DeviceVo {
    @XmlElement
    private String style;
    @XmlElement
    private String javascript;
    @XmlElement(
            name = "bindVariable"
    )
    private List<BindVariable> bindVariables;

    public DeviceVo(){}

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getJavascript() {
        return javascript;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public List<BindVariable> getBindVariables() {
        return bindVariables;
    }

    public void setBindVariables(List<BindVariable> bindVariables) {
        this.bindVariables = bindVariables;
    }
}
