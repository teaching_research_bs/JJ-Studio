package cn.easyplatform.studio.vos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuInfoVo implements Cloneable, Serializable {
    private static final long serialVersionUID = 1L;
    private String menuId;
    private String name;
    private String image;
    private String orderNo;
    private String parentMenuId;
    private String tasks;
    private String desp;
    private List<MenuInfoVo> childrenList = new ArrayList<>();


    public Object clone() {
        MenuInfoVo o = null;
        try {
            o = (MenuInfoVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        o.childrenList = new ArrayList<>();
        return o;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getParentMenuId() {
        return parentMenuId;
    }

    public void setParentMenuId(String parentMenuId) {
        this.parentMenuId = parentMenuId;
    }

    public String getTasks() {
        return tasks;
    }

    public void setTasks(String tasks) {
        this.tasks = tasks;
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    public List<MenuInfoVo> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<MenuInfoVo> childrenList) {
        this.childrenList = childrenList;
    }
}
