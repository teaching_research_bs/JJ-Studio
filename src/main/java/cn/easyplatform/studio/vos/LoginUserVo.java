/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import cn.easyplatform.studio.context.Contexts;
import org.zkoss.zk.ui.Session;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LoginUserVo implements Serializable {

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public List<ProjectRoleVo> getRoleVoList() {
        return roleVoList;
    }

    public void setRoleVoList(List<ProjectRoleVo> roleVoList) {
        this.roleVoList = roleVoList;
    }

    public enum UserType {
        DEV("DEV"),
        ADMIN("admin");

        String name;

        private UserType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return this.name;
        }
    }

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户状态：未激活
     */
    public final static int STATE_NOT_ACTIVATED = 0x00;

    /**
     * 用户状态：已激活
     */
    public final static int STATE_ACTIVATED = 0x01;

    /**
     * 用户状态：被锁定
     */
    public final static int STATE_LOCK = 0x02;

    /**
     * 用户在线状态：不在线
     */
    public final static int ONLINE_NONE = 0x00;

    /**
     * 用户在线状态：本身在线
     */
    public final static int ONLINE_SELF = 0x01;

    /**
     * 用户在线状态：用户在其它设备上
     */
    public final static int ONLINE_OTHER = 0x02;

    /**
     * 项目id
     */
    private String projectId;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 密码
     */
    private String password;

    /**
     * 名称
     */
    private String name;


    /**
     * 客户端ip
     */
    private String ip;

    /**
     * 登陆时间
     */
    private Date loginDate;
    /**
     * 有效开始日期
     */
    private Date validDate;

    /**
     * 有效天数
     */
    private int validDays;

    /**
     * 最后操作时间
     */
    private Date lastAccessTime;

    /**
     * 超时时间，单位分
     */
    private int timeout;

    /**
     * 用户状态
     */
    private int state;

    /**
     * 密码附加值
     */
    private String salt;

    /**
     * 密码错误次数
     */
    private int loginFailedTimes;

    /**
     * 密码过期还剩几天
     */
    private int remainingDays;

    /**
     * 对应的会话
     */
    private Session session;

    /**
     * 页面风格
     */
    private String theme;

    /**
     * 编辑器风格
     */
    private String editorTheme;

    /**
     * 是否在执行长时操作
     */
    private boolean busying;

    /**
     * 权限控制
     */
    private Map<String, Integer> authorization;

    /**
     * 默认启动项目
     */
    private String defaultProject;

    /**
     * 当前用户权限
     */
    private String roleId;

    /**
     * 当前用户拥有的所有项目的授权
     */
    private List<ProjectRoleVo> roleVoList;

    /**
     * @return the busying
     */
    public boolean isBusying() {
        return busying;
    }

    /**
     * @param busying the busying to set
     */
    public void setBusying(boolean busying) {
        this.busying = busying;
    }

    /**
     * @return the theme
     */
    public String getTheme() {
        return theme;
    }

    /**
     * @param theme the theme to set
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * @return the editorTheme
     */
    public String getEditorTheme() {
        return editorTheme;
    }

    /**
     * @param editorTheme the editorTheme to set
     */
    public void setEditorTheme(String editorTheme) {
        this.editorTheme = editorTheme;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public int getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(int remainingDays) {
        this.remainingDays = remainingDays;
    }

    public int getLoginFailedTimes() {
        return loginFailedTimes;
    }

    public void setLoginFailedTimes(int loginFailedTimes) {
        this.loginFailedTimes = loginFailedTimes;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getValidDate() {
        return validDate;
    }

    public void setValidDate(Date validDate) {
        this.validDate = validDate;
    }

    public int getValidDays() {
        return validDays;
    }

    public void setValidDays(int validDays) {
        this.validDays = validDays;
    }

    public Date getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(Date lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public void setAuthorization(Map<String, Integer> authorization) {
        this.authorization = authorization;
    }

    public boolean isAuthorized(String name) {
        if (this.authorization == null)
            return true;
        else return this.authorization.containsKey(name);
    }

    public boolean validate() {
        if (timeout > 0 && !busying) {
            long expireTimeMillis = System.currentTimeMillis() - timeout * 1000
                    * 60;
            Date expireTime = new Date(expireTimeMillis);
            boolean isValid = lastAccessTime.after(expireTime);
            if (!isValid) {
                session.removeAttribute(Contexts.EP_USER);
                session.removeAttribute(Contexts.EP_PROJECT);
                session.invalidate();
            }
            return isValid;
        }
        return true;
    }

    public String getDefaultProject() {
        return defaultProject;
    }

    public void setDefaultProject(String defaultProject) {
        this.defaultProject = defaultProject;
    }
}
