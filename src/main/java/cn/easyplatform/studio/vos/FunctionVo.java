/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FunctionVo {

	private String name;

	//中文名称
	private String cName;
	//拖拽表达式
	private String exp;

	private List<ArgsVo> args;

	private List<String> examples;

	/**
	 * @param name
	 */
	public FunctionVo(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void addArg(String name, String desc) {
		if (args == null)
			args = new ArrayList<ArgsVo>();
		args.add(new ArgsVo(name, desc));
	}

	/**
	 * @return the args
	 */
	public List<ArgsVo> getArgs() {
		return args;
	}

	public void addExample(String example) {
		if (examples == null)
			examples = new ArrayList<String>();
		examples.add(example);
	}

	/**
	 * @return the examples
	 */
	public List<String> getExamples() {
		return examples;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}
}
