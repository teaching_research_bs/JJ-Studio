package cn.easyplatform.studio.vos;

import java.util.Date;

public class ModuleVo {
    private String moduleId;
    private String moduleName;
    private String price;
    private String desp;
    private String pics;
    private String entityIds;
    private String chooseType;
    private String rootEntityIds;
    private String dicDetailID;
    private Date createDate;
    private Date updateDate;
    private String createUser;
    private String updateUser;

    public ModuleVo(){}

    public ModuleVo(String moduleId, String moduleName, String price, String desp, String pics,
                    String entityIds, String chooseType, String rootEntityIds, String dicDetailID,
                    Date createDate, Date updateDate, String createUser, String updateUser) {

        this.moduleId = moduleId;
        this.moduleName = moduleName;
        this.price = price;
        this.desp = desp;
        this.pics = pics;
        this.entityIds = entityIds;
        this.chooseType = chooseType;
        this.rootEntityIds = rootEntityIds;
        this.dicDetailID = dicDetailID;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.createUser = createUser;
        this.updateUser = updateUser;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    public String getPics() {
        return pics;
    }

    public void setPics(String pics) {
        this.pics = pics;
    }

    public String getEntityIds() {
        return entityIds;
    }

    public void setEntityIds(String entityIds) {
        this.entityIds = entityIds;
    }

    public String getChooseType() {
        return chooseType;
    }

    public void setChooseType(String chooseType) {
        this.chooseType = chooseType;
    }

    public String getRootEntityIds() {
        return rootEntityIds;
    }

    public void setRootEntityIds(String rootEntityIds) {
        this.rootEntityIds = rootEntityIds;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getDicDetailID() {
        return dicDetailID;
    }

    public void setDicDetailID(String dicDetailID) {
        this.dicDetailID = dicDetailID;
    }
}
