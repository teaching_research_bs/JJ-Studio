/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ResultVo {

	private List<String> tableNames;

	private List<String> columns;

	private List<Object[]> data;

	/**
	 * @return the tableNames
	 */
	public List<String> getTableNames() {
		return tableNames;
	}

	/**
	 * @param tableNames the columns to set
	 */
	public void setTableNames(List<String> tableNames) {
		this.tableNames = tableNames;
	}

	/**
	 * @return the columns
	 */
	public List<String> getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	/**
	 * @return the data
	 */
	public List<Object[]> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<Object[]> data) {
		this.data = data;
	}

}
