package cn.easyplatform.studio.vos;

import java.io.Serializable;
import java.sql.Timestamp;

public class UploadHistVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String fileName;
    private String uploadData;//转成base64编码存
    private String packName;
    private String type;
    private String contentType;
    private String projectName;
    private String uploadUser;
    private Timestamp createDate;

    public UploadHistVo() {

    }

    public UploadHistVo(String fileName, String uploadData,String type, String contentType, String packName, String projectName,String uploadUser) {
        this.fileName = fileName;
        this.uploadData = uploadData;
        this.type = type;
        this.contentType = contentType;
        this.packName = packName;
        this.projectName = projectName;
        this.uploadUser = uploadUser;
    }

    public UploadHistVo(int id, String fileName, String uploadData, String packName,String type, String contentType, String projectName,String uploadUser, Timestamp createDate) {
        this.id = id;
        this.fileName = fileName;
        this.uploadData = uploadData;
        this.packName = packName;
        this.type = type;
        this.contentType = contentType;
        this.projectName = projectName;
        this.uploadUser = uploadUser;
        this.createDate = createDate;
    }

    public String getUploadUser() {
        return uploadUser;
    }

    public void setUploadUser(String uploadUser) {
        this.uploadUser = uploadUser;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUploadData() {
        return uploadData;
    }

    public void setUploadData(String uploadData) {
        this.uploadData = uploadData;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
}
