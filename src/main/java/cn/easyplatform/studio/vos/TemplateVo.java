package cn.easyplatform.studio.vos;

import cn.easyplatform.studio.utils.TemplateUtil;

import java.util.List;

public class TemplateVo {
    private String productIdentifier;

    private String productDisplayName;

    private TemplateUtil.TemplateType productType;

    private List<String> ZULFiles;

    private List<String> imageFiles;

    private List<String> jsFiles;

    private List<String> videoFiles;

    private List<String> audioFiles;

    private List<String> CSSFiles;

    private List<String> productPreview;

    public String getProductIdentifier() {
        return productIdentifier;
    }

    public void setProductIdentifier(String productIdentifier) {
        this.productIdentifier = productIdentifier;
    }

    public String getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductDisplayName(String productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public TemplateUtil.TemplateType getProductType() {
        return productType;
    }

    public void setProductType(TemplateUtil.TemplateType productType) {
        this.productType = productType;
    }

    public List<String> getZULFiles() {
        return ZULFiles;
    }

    public void setZULFiles(List<String> ZULFiles) {
        this.ZULFiles = ZULFiles;
    }

    public List<String> getImageFiles() {
        return imageFiles;
    }

    public void setImageFiles(List<String> imageFiles) {
        this.imageFiles = imageFiles;
    }

    public List<String> getJsFiles() {
        return jsFiles;
    }

    public void setJsFiles(List<String> jsFiles) {
        this.jsFiles = jsFiles;
    }

    public List<String> getVideoFiles() {
        return videoFiles;
    }

    public void setVideoFiles(List<String> videoFiles) {
        this.videoFiles = videoFiles;
    }

    public List<String> getAudioFiles() {
        return audioFiles;
    }

    public void setAudioFiles(List<String> audioFiles) {
        this.audioFiles = audioFiles;
    }

    public List<String> getCSSFiles() {
        return CSSFiles;
    }

    public void setCSSFiles(List<String> CSSFiles) {
        this.CSSFiles = CSSFiles;
    }

    public List<String> getProductPreview() {
        return productPreview;
    }

    public void setProductPreview(List<String> productPreview) {
        this.productPreview = productPreview;
    }
}
