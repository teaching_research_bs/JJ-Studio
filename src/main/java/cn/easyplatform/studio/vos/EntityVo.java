/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;


/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EntityVo {

	private String id;

	private String name;

	private String desp;

	private String type;

	private String subType;

	/**
	 * @param id
	 * @param name
	 * @param desp
	 */
	public EntityVo(String id, String name, String desp) {
		this.id = id;
		this.name = name;
		this.desp = desp;
	}

	/**
	 * @param id
	 * @param name
	 * @param desp
	 */
	public EntityVo(String id, String name, String desp, String type,
			String subType) {
		this.id = id;
		this.name = name;
		this.desp = desp;
		this.type = type;
		this.subType = subType;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDesp() {
		return desp;
	}

	public String getType() {
		return type;
	}

	public String getSubType() {
		return subType;
	}
}
