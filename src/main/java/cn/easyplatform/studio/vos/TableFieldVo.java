package cn.easyplatform.studio.vos;

public class TableFieldVo implements Cloneable {

    private String tableId;

    private String tableName;

    private String tableDes;

    private String fieldName;

    private String fieldType;

    private String fieldLength;

    private String fieldDecimal;

    private String fieldDes;

    private String fieldNotNull;

    private Integer fieldOrderNo;

    public TableFieldVo(String tableId, String tableName, String tableDes, String fieldName, String fieldType,
                        String fieldLength, String fieldDecimal, String fieldDes, String fieldNotNull, Integer fieldOrderNo) {
        this.tableId = tableId;
        this.tableName = tableName;
        this.tableDes = tableDes;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.fieldLength = fieldLength;
        this.fieldDecimal = fieldDecimal;
        this.fieldDes = fieldDes;
        this.fieldNotNull = fieldNotNull;
        this.fieldOrderNo = fieldOrderNo;
    }

    public Object clone() {
        TableFieldVo o = null;
        try {
            o = (TableFieldVo) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.toString());
        }
        return o;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableDes() {
        return tableDes;
    }

    public void setTableDes(String tableDes) {
        this.tableDes = tableDes;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldLength() {
        return fieldLength;
    }

    public void setFieldLength(String fieldLength) {
        this.fieldLength = fieldLength;
    }

    public String getFieldDecimal() {
        return fieldDecimal;
    }

    public void setFieldDecimal(String fieldDecimal) {
        this.fieldDecimal = fieldDecimal;
    }

    public String getFieldDes() {
        return fieldDes;
    }

    public void setFieldDes(String fieldDes) {
        this.fieldDes = fieldDes;
    }

    public String getFieldNotNull() {
        return fieldNotNull;
    }

    public void setFieldNotNull(String fieldNotNull) {
        this.fieldNotNull = fieldNotNull;
    }

    public Integer getFieldOrderNo() {
        return fieldOrderNo;
    }

    public void setFieldOrderNo(Integer fieldOrderNo) {
        this.fieldOrderNo = fieldOrderNo;
    }
}
