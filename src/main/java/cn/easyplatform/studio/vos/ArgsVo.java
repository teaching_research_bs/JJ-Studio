/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ArgsVo {

	private String name;
	
	private String desc;

	/**
	 * @param name
	 * @param desc
	 */
	public ArgsVo(String name, String desc) {
		this.name = name;
		this.desc = desc;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}
	
}
