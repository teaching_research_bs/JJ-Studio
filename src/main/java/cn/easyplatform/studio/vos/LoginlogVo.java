package cn.easyplatform.studio.vos;


import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class LoginlogVo {

    private int loginlogId;

    private String userName;

    private Date loginTime;

    private String ipAddress;

    private String loginOS;

    private String loginTN;

    private String type;

    private String userId;

    private String userType;

    private String roleName;

    private HttpSession session;

    public LoginlogVo() {
    }

    public LoginlogVo( int loginlogId,String userName, Date loginTime, String ipAddress, String loginOS, String loginTN, String type) {

        this.loginlogId=loginlogId;
        this.userName = userName;
        this.loginTime = loginTime;
        this.ipAddress = ipAddress;
        this.loginOS = loginOS;
        this.loginTN = loginTN;
        this.type = type;
    }

    public int getLoginlogId() {
        return loginlogId;
    }

    public void setLoginlogId(int loginlogId) {
        this.loginlogId = loginlogId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLoginOS() {
        return loginOS;
    }

    public void setLoginOS(String loginOS) {
        this.loginOS = loginOS;
    }

    public String getLoginTN() {
        return loginTN;
    }

    public void setLoginTN(String loginTN) {
        this.loginTN = loginTN;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "LoginlogVo{" +
                "loginlogId=" + loginlogId +
                ", userName='" + userName + '\'' +
                ", loginTime=" + loginTime +
                ", ipAddress='" + ipAddress + '\'' +
                ", loginOS='" + loginOS + '\'' +
                ", loginTN='" + loginTN + '\'' +
                ", type='" + type + '\'' +
                ", userId='" + userId + '\'' +
                ", userType='" + userType + '\'' +
                ", rolename='" + roleName + '\'' +
                ", session=" + session +
                '}';
    }
}
