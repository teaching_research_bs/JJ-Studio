package cn.easyplatform.studio.vos;


import java.util.Date;

public class CustomWidgetVo {
    private int widgetId;
    private String widgetName;
    private String widgetDesp;
    private String widgetPic;
    private String content;
    private Date createDate;
    private Date updateDate;
    private String createUser;
    private String updateUser;
    private DeviceVo device;

    public CustomWidgetVo(){}

    public CustomWidgetVo(int widgetId, String widgetName, String widgetDesp, String widgetPic, String content, Date createDate,
                          Date updateDate, String createUser, String updateUser, DeviceVo device) {
        this.widgetId = widgetId;
        this.widgetName = widgetName;
        this.widgetDesp = widgetDesp;
        this.widgetPic = widgetPic;
        this.content = content;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.createUser = createUser;
        this.updateUser = updateUser;
        this.device = device;
    }

    public String getWidgetName() {
        return widgetName;
    }

    public void setWidgetName(String widgetName) {
        this.widgetName = widgetName;
    }

    public String getWidgetDesp() {
        return widgetDesp;
    }

    public void setWidgetDesp(String widgetDesp) {
        this.widgetDesp = widgetDesp;
    }

    public String getWidgetPic() {
        return widgetPic;
    }

    public void setWidgetPic(String widgetPic) {
        this.widgetPic = widgetPic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public DeviceVo getDevice() {
        return device;
    }

    public void setDevice(DeviceVo device) {
        this.device = device;
    }

    public int getWidgetId() {
        return widgetId;
    }

    public void setWidgetId(int widgetId) {
        this.widgetId = widgetId;
    }
}
