package cn.easyplatform.studio.vos;

public class ThemeVo {
    private String themeId;
    private String themeName;
    private String themePic;

    public ThemeVo(String themeId, String themeName, String themePic) {
        this.themeId = themeId;
        this.themeName = themeName;
        this.themePic = themePic;
    }

    public String getThemePic() {
        return themePic;
    }

    public void setThemePic(String themePic) {
        this.themePic = themePic;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public String getThemeId() {
        return themeId;
    }

    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }
}
