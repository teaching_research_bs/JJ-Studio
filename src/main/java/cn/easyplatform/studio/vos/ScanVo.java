/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ScanVo {

	private List<TableVo> data;

	private List<TableVo> sourceNew;

	private List<TableVo> targetNew;

	private int totalSize;

	/**
	 * @param data
	 * @param sourceNew
	 * @param
	 */
	public ScanVo(List<TableVo> data, List<TableVo> sourceNew,
			List<TableVo> targetNew) {
		this.data = data;
		this.sourceNew = sourceNew;
		this.targetNew = targetNew;
	}

	/**
	 * @param data
	 * @param sourceNew
	 * @param
	 */
	public ScanVo(List<TableVo> data, List<TableVo> sourceNew, int totalSize) {
		this.data = data;
		this.sourceNew = sourceNew;
		this.totalSize = totalSize;
	}

	/**
	 * @return the data
	 */
	public List<TableVo> getData() {
		return data;
	}

	/**
	 * @return the sourceNew
	 */
	public List<TableVo> getSourceNew() {
		return sourceNew;
	}

	/**
	 * @return the totalSize
	 */
	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @return the targetNew
	 */
	public List<TableVo> getTargetNew() {
		return targetNew;
	}

}
