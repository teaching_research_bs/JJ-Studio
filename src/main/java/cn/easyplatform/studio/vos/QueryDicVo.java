package cn.easyplatform.studio.vos;

import java.util.List;

public class QueryDicVo {
    private List<DicVo> dicVoList;

    private int totalSize = 1;

    public QueryDicVo(int totalSize,List<DicVo> dicVoList)
    {
        this.totalSize = totalSize;
        this.dicVoList = dicVoList;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public List<DicVo> getLinkVoList() {
        return dicVoList;
    }

    public void setLinkVoList(List<DicVo> dicVoList) {
        this.dicVoList = dicVoList;
    }
}
