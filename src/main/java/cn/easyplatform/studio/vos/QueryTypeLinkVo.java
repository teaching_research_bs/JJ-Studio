package cn.easyplatform.studio.vos;

import java.util.List;

public class QueryTypeLinkVo {

    private List<TypeLinkVo> linkVoList;

    private int totalSize = 1;

    public QueryTypeLinkVo(int totalSize,List<TypeLinkVo> linkVoList)
    {
        this.totalSize = totalSize;
        this.linkVoList = linkVoList;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public List<TypeLinkVo> getLinkVoList() {
        return linkVoList;
    }

    public void setLinkVoList(List<TypeLinkVo> linkVoList) {
        this.linkVoList = linkVoList;
    }
}
