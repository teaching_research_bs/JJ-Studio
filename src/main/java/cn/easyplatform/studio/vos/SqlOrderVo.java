package cn.easyplatform.studio.vos;

public class SqlOrderVo {
    public enum OrderType {
        ASC("Asc"),
        DESC("Desc");

        String name;

        private OrderType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return this.name;
        }
    }
    private String keyContent;
    private String orderType;

    public String getKeyContent() {
        return keyContent;
    }

    public void setKeyContent(String keyContent) {
        this.keyContent = keyContent;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}
