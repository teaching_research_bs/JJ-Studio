package cn.easyplatform.studio.vos;

import java.io.Serializable;


public class ProjectRoleVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String projectId;

    private String roleIDs;

    private String projectName;

    private String projectDesp;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getRoleIDs() {
        return roleIDs;
    }

    public void setRoleIDs(String roleIDs) {
        this.roleIDs = roleIDs;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDesp() {
        return projectDesp;
    }

    public void setProjectDesp(String projectDesp) {
        this.projectDesp = projectDesp;
    }
}
