/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.vos;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RoleVo {

	private String id;

	private String name;

	private String image;

	private String desp;

	private String type;

	private char code;

	private List<String> deskTopMenus;

	private List<String> mobileMenus;

	private List<RoleAccessVo> roleAccessVos;

	public char getCode() {
		return code;
	}

	public void setCode(char code) {
		this.code = code;
	}

	public String getDesp() {
		return desp;
	}

	public void setDesp(String desp) {
		this.desp = desp;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<String> getDeskTopMenus() {
		return deskTopMenus;
	}

	public void setDeskTopMenus(List<String> deskTopMenus) {
		this.deskTopMenus = deskTopMenus;
	}

	public List<String> getMobileMenus() {
		return mobileMenus;
	}

	public void setMobileMenus(List<String> mobileMenus) {
		this.mobileMenus = mobileMenus;
	}

	public List<RoleAccessVo> getRoleAccessVos() {
		return roleAccessVos;
	}

	public void setRoleAccessVos(List<RoleAccessVo> roleAccessVos) {
		this.roleAccessVos = roleAccessVos;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
