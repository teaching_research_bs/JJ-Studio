/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.context;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.InviteVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import org.zkoss.zk.ui.Sessions;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Contexts {

	private static final ThreadLocal<Map<Class<?>, Object>> resources = new ThreadLocal<Map<Class<?>, Object>>() {
		@Override
		protected Map<Class<?>, Object> initialValue() {
			return new HashMap<Class<?>, Object>();
		}
	};

	public static CommandContext getCommandContext() {
		return (CommandContext) resources.get().get(CommandContext.class);
	}

	public static void set(Class<?> clazz, Object inst) {
		resources.get().put(clazz, inst);
	}

	public final static void clear() {
		JdbcTransactions.clear();
		resources.remove();
	}

	public final static void setUser(LoginUserVo user) {
		if (user == null)
			Sessions.getCurrent().removeAttribute(EP_USER);
		else
			Sessions.getCurrent().setAttribute(EP_USER, user);
	}

	public final static LoginUserVo getUser() {
		return (LoginUserVo) Sessions.getCurrent().getAttribute(EP_USER);
	}

	public final static void setProject(ProjectBean project) {
		if (project == null)
			Sessions.getCurrent().removeAttribute(EP_PROJECT);
		else
			Sessions.getCurrent().setAttribute(EP_PROJECT, project);
	}

	public final static ProjectBean getProject() {
		return (ProjectBean) Sessions.getCurrent().getAttribute(EP_PROJECT);
	}

	public final static InviteVo getInvite() {
		return (InviteVo) Sessions.getCurrent().getAttribute(EP_INVITE);
	}

	public final static void setInvite(InviteVo invite) {
		if (invite == null)
			Sessions.getCurrent().removeAttribute(EP_INVITE);
		else
			Sessions.getCurrent().setAttribute(EP_INVITE, invite);
	}

	public final static Boolean getIntoProjectCenter() {
		return (Boolean) Sessions.getCurrent().getAttribute(EP_PROJECT_CENTER);
	}

	public final static void setIntoProjectCenter(Boolean isInto) {
		if (isInto == null)
			Sessions.getCurrent().removeAttribute(EP_PROJECT_CENTER);
		else
			Sessions.getCurrent().setAttribute(EP_PROJECT_CENTER, isInto);
	}

	public final static String EP_USER = "ep_user";

	public final static String EP_PROJECT = "ep_project";

	public final static String EP_INVITE = "ep_invite";

	public final static String EP_PROJECT_CENTER = "ep_project_center";
}
