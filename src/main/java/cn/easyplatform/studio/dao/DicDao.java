package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.DicVo;

import java.util.List;

public interface DicDao {
    List<DicVo> selectAllList();
    DicVo selectOne(String dicID);
    Boolean setDicList(List<DicVo> dicVos);
    Boolean updateDicList(List<DicVo> dicVos);
}
