package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.List;

public interface TaskLinkDao {

    List<TypeLinkVo> getAllTaskLink(String linkTable, String taskLinkTable);

    TypeLinkVo getTaskLink(String linkTable, String taskLinkTable, String entityId);

    List<TypeLinkVo> getLinkWithChildrenEntityId(String linkTable, String taskLinkTable, String entityId);

    boolean deleteTaskLink(String taskLinkTable, String entityId);

    boolean addTaskLink(String taskLinkTable, TypeLinkVo vo);

    boolean updateTaskLink(String taskLinkTable, TypeLinkVo vo);

    boolean updateTaskLinks(String taskLinkTable, List<TypeLinkVo> voList);
}
