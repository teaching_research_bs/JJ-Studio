package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.MenuInfoVo;

import java.util.List;

public interface MenuInfoDao {
    List<MenuInfoVo> selectAllList();
    Boolean setMenuList(List<MenuInfoVo> dicVos);
    Boolean updateMenuList(List<MenuInfoVo> dicVos);
}
