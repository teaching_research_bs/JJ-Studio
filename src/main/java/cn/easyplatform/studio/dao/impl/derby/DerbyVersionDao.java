/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractVersionDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DerbyVersionDao extends AbstractVersionDao {

	/**
	 * @param ds
	 */
	public DerbyVersionDao(DataSource ds) {
		super(ds);
	}

	@Override
	protected Dialect getDialect() {
		return new DerbyDialect();
	}


}
