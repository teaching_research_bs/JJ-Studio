package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractCustomWidgetDao;

import javax.sql.DataSource;

public class DerbyCustomWidgetDao extends AbstractCustomWidgetDao {
    /**
     * @param ds
     */
    public DerbyCustomWidgetDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
