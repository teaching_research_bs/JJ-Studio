package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDetailDao;

import javax.sql.DataSource;

public class SybaseDicDetailDao extends AbstractDicDetailDao {
    /**
     * @param ds
     */
    public SybaseDicDetailDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
