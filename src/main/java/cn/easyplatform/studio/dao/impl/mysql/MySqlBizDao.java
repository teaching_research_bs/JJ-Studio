/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractBizDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.type.FieldType;
import org.apache.commons.lang3.RandomStringUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MySqlBizDao extends AbstractBizDao {

	/**
	 * @param dataSource
	 */
	public MySqlBizDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected Dialect getDialect() {
		return new MySqlDialect();
	}

	@Override
	protected String dropForeignKey(String table, String name) {
		StringBuilder sb = new StringBuilder();
		sb.append("ALTER TABLE ").append(table).append(" DROP FOREIGN KEY ")
				.append(name);
		return sb.toString();
	}

	@Override
	protected String dropIndex(String table, String name) {
		StringBuilder sb = new StringBuilder();
		sb.append("DROP INDEX ").append(name).append(" ON ").append(table);
		return sb.toString();
	}

	@Override
	protected String backup(String table/*, List<TableField> fields*/) {
		Statement stmt = null;
		try {
			Connection conn = JdbcTransactions.getConnection(ds);
			String tmpTable = table + "_"
					+ RandomStringUtils.randomAlphanumeric(10);
			StringBuilder sb = new StringBuilder();
			sb.append("CREATE TABLE ").append(tmpTable).append(" LIKE ").append(table);
			System.out.println("backup:" + sb.toString());
			stmt = conn.createStatement();
			stmt.execute(sb.toString());
			stmt.close();
			stmt = null;
			//插入表
			stmt = conn.createStatement();
			stmt.execute("insert into " + tmpTable + " select * from " + table);
			stmt.close();
			stmt = null;
			return tmpTable;
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			DaoUtils.closeQuietly(stmt);
		}
	}

	@Override
	protected void restore(String source, String target) {
		Statement stmt = null;
		try {
			Connection conn = JdbcTransactions.getConnection(ds);
			stmt = conn.createStatement();
			stmt.execute("drop table if exists " + target);
			stmt.close();
			stmt = null;
			//拷贝表
			StringBuilder sb = new StringBuilder();
			sb.append("CREATE TABLE ").append(target)
					.append(" LIKE ").append(source);
			stmt = conn.createStatement();
			stmt.execute(sb.toString());
			stmt.close();
			stmt = null;
			//插入表
			stmt = conn.createStatement();
			stmt.execute("insert into " + target + " select * from " + source);
			stmt.close();
			stmt = null;
		} catch (SQLException ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			DaoUtils.closeQuietly(stmt);
		}
	}

	protected String evalFieldType(TableField field) {
		if (field.getType() == FieldType.INT) {
			int width = field.getLength();
			if (width <= 0) {
				return "INT(32)";
			} else if (width <= 2) {
				return "TINYINT(" + (width * 4) + ")";
			} else if (width <= 4) {
				return "MEDIUMINT(" + (width * 4) + ")";
			} else if (width <= 8) {
				return "INT(" + (width * 4) + ")";
			}
			return "INT(32)";
		} else if (field.getType() == FieldType.LONG)
			return "BIGINT";
		else if (field.getType() == FieldType.BLOB) {
			return "MediumBlob"; // 默认用16M
		} else if (field.getType() == FieldType.BOOLEAN)
			return "TINYINT(1)";
		return super.evalFieldType(field);
	}
}
