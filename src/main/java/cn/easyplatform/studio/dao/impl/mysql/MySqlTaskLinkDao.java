package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractTaskLinkDao;

import javax.sql.DataSource;

public class MySqlTaskLinkDao extends AbstractTaskLinkDao {
    public MySqlTaskLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
