package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractTaskLinkDao;

import javax.sql.DataSource;

public class SqlServerTaskLinkDao extends AbstractTaskLinkDao {
    public SqlServerTaskLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
