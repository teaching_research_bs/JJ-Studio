/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractVersionDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import org.apache.commons.lang3.RandomStringUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MySqlVersionDao extends AbstractVersionDao {

	/**
	 * @param ds
	 */
	public MySqlVersionDao(DataSource ds) {
		super(ds);
	}

	@Override
	protected Dialect getDialect() {
		return new MySqlDialect();
	}
}
