package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDatabaseUpdateDao;

import javax.sql.DataSource;

public class DerbyDatabaseUpdateDao extends AbstractDatabaseUpdateDao {
    /**
     * @param ds
     */
    public DerbyDatabaseUpdateDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
