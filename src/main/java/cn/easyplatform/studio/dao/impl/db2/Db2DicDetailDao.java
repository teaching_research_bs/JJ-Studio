package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDetailDao;

import javax.sql.DataSource;

public class Db2DicDetailDao extends AbstractDicDetailDao {
    /**
     * @param ds
     */
    public Db2DicDetailDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
