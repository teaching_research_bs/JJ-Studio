package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.GuideConfigDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGuideConfigDao implements GuideConfigDao {

    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractGuideConfigDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();

    @Override
    public List<GuideConfigVo> getGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String tableStr = guideConfigType == GuideConfigEditor.GuideConfigType.GuideConfigStudio ? "ep_guide_info": "sys_guide_info";
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select guideID,guideName,guideStep,guideNextID,isRoot,createDate,updateDate," +
                    "createUser,updateUser from " + tableStr  + " where 1=1 ");
            rs = pstmt.executeQuery();
            List<GuideConfigVo> list = new ArrayList<>();
            while (rs.next()) {
                GuideConfigVo vo = new GuideConfigVo(rs.getInt(1),rs.getString(2),
                        rs.getString(3),rs.getString(4),rs.getString(5), rs.getDate(6),
                        rs.getDate(7), rs.getString(8), rs.getString(9));
                list.add(vo);
            }
            return list;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public boolean addGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType, List<GuideConfigVo> addList) {
        if (addList == null || addList.size() == 0)
        {
            return false;
        }
        PreparedStatement pstmt = null;
        Connection conn = null;
        String tableStr = guideConfigType == GuideConfigEditor.GuideConfigType.GuideConfigStudio ? "ep_guide_info": "sys_guide_info";
        try {
            conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("INSERT INTO `" + tableStr + "`" +
                            "(`guideName`, `guideStep`, `guideNextID`, `isRoot`, `createUser`) VALUES " +
                            "(?, ?, ?, ?, ?)");
            for (GuideConfigVo guideConfigVo : addList) {
                pstmt.setString(1, guideConfigVo.getGuideName());
                pstmt.setString(2, guideConfigVo.getGuideStep());
                pstmt.setString(3, guideConfigVo.getGuideNextID());
                pstmt.setString(4, guideConfigVo.getIsRoot());
                pstmt.setString(5, guideConfigVo.getCreateUser());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
        return true;
    }
    @Override
    public boolean updateGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType, List<GuideConfigVo> updateList) {
        if (updateList == null || updateList.size() == 0)
        {
            return false;
        }
        PreparedStatement pstmt = null;
        Connection conn = null;
        String tableStr = guideConfigType == GuideConfigEditor.GuideConfigType.GuideConfigStudio ? "ep_guide_info": "sys_guide_info";
        try {
            conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("UPDATE `" + tableStr + "` SET " +
                    "`guideName` = ?, `guideStep` = ?, `guideNextID` = ?, `isRoot` = ?, `updateUser` = ?, `updateDate` = ? WHERE `guideID` = ?");
            for (GuideConfigVo guideConfigVo : updateList) {
                pstmt.setString(1, guideConfigVo.getGuideName());
                pstmt.setString(2, guideConfigVo.getGuideStep());
                pstmt.setString(3, guideConfigVo.getGuideNextID());
                pstmt.setString(4, guideConfigVo.getIsRoot());
                pstmt.setString(5, guideConfigVo.getUpdateUser());
                pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
                pstmt.setInt(7, guideConfigVo.getGuideID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
        return true;
    }
    @Override
    public boolean deleteGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType, List<GuideConfigVo> updateList) {
        if (updateList == null || updateList.size() == 0)
        {
            return false;
        }
        PreparedStatement pstmt = null;
        Connection conn = null;
        String tableStr = guideConfigType == GuideConfigEditor.GuideConfigType.GuideConfigStudio ? "ep_guide_info": "sys_guide_info";
        try {
            conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("delete from `" + tableStr + "`  where 1=1 and guideID=?");
            for (GuideConfigVo guideConfigVo : updateList) {
                pstmt.setInt(1, guideConfigVo.getGuideID());
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
        return true;
    }
}
