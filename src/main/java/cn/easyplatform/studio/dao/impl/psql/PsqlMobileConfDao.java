package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMobileConfDao;

import javax.sql.DataSource;

public class PsqlMobileConfDao extends AbstractMobileConfDao {

    public PsqlMobileConfDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
