package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractCustomWidgetDao;

import javax.sql.DataSource;

public class MySqlCustomWidgetDao extends AbstractCustomWidgetDao {
    /**
     * @param ds
     */
    public MySqlCustomWidgetDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
