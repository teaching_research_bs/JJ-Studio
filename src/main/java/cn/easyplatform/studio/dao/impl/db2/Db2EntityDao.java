/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractEntityDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Db2EntityDao extends AbstractEntityDao {

	/**
	 * @param ds
	 */
	public Db2EntityDao(DataSource ds) {
		super(ds);
	}

	@Override
	protected Dialect getDialect() {
		return new Db2Dialect();
	}

}
