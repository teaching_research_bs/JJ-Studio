package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.entities.beans.page.DeviceBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.dao.CustomWidgetDao;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.utils.apkrepackage.util.XMLUtil;
import cn.easyplatform.studio.vos.CustomWidgetVo;
import cn.easyplatform.studio.vos.DeviceVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCustomWidgetDao implements CustomWidgetDao {
    protected final static Logger log = LoggerFactory.getLogger(AbstractEntityDao.class);

    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractCustomWidgetDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();

    @Override
    public List<CustomWidgetVo> selectAllPublicList(){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select widgetId,`name`,desp,picture,content,device," +
                    "createDate,updateDate,createUser,updateUser from ep_widget_info where 1=1");
            rs = pstmt.executeQuery();
            List<CustomWidgetVo> customWidgetVos = new ArrayList<>();
            while (rs.next()) {
                CustomWidgetVo vo = new CustomWidgetVo();
                vo.setWidgetId(rs.getInt(1));
                vo.setWidgetName(rs.getString(2));
                vo.setWidgetDesp(rs.getString(3));
                vo.setWidgetPic(rs.getString(4));
                vo.setContent(rs.getString(5));
                if (Strings.isBlank(rs.getString(6)) == false) {
                    vo.setDevice(XMLUtil.xml2bean(rs.getString(6), DeviceVo.class));
                }
                vo.setCreateDate(rs.getDate(7));
                vo.setUpdateDate(rs.getDate(8));
                vo.setCreateUser(rs.getString(9));
                vo.setUpdateUser(rs.getString(10));
                customWidgetVos.add(vo);
            }
            return customWidgetVos;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }

    @Override
    public List<CustomWidgetVo> selectAllProductList(String tableName, String userId) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            boolean isPrivate =  Strings.isBlank(userId) == false;
            String sb = "select widgetId,`name`,desp,picture,content,device,createDate,updateDate" +
                    ",createUser,updateUser from " + tableName + " where 1=1";
            if (isPrivate)
                sb = sb + " and createUser=?";
            pstmt = conn.prepareStatement(sb);
            if (isPrivate)
                pstmt.setString(1, userId);
            rs = pstmt.executeQuery();
            List<CustomWidgetVo> customWidgetVos = new ArrayList<>();
            while (rs.next()) {
                CustomWidgetVo vo = new CustomWidgetVo();
                vo.setWidgetId(rs.getInt(1));
                vo.setWidgetName(rs.getString(2));
                vo.setWidgetDesp(rs.getString(3));
                vo.setWidgetPic(rs.getString(4));
                vo.setContent(rs.getString(5));
                if (Strings.isBlank(rs.getString(6)) == false) {
                    vo.setDevice(XMLUtil.xml2bean(rs.getString(6), DeviceVo.class));
                }
                vo.setCreateDate(rs.getDate(7));
                vo.setUpdateDate(rs.getDate(8));
                vo.setCreateUser(rs.getString(9));
                vo.setUpdateUser(rs.getString(10));
                customWidgetVos.add(vo);
            }
            return customWidgetVos;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }
    @Override
    public CustomWidgetVo selectOneProduct(String tableName, String name) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            String sb = "select widgetId,`name`,desp,picture,content,device,createDate,updateDate,createUser,updateUser from "
                    + tableName +" where 1=1 and `name`=?";
            pstmt = conn.prepareStatement(sb);
            pstmt.setString(1, name);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                CustomWidgetVo vo = new CustomWidgetVo();
                vo.setWidgetId(rs.getInt(1));
                vo.setWidgetName(rs.getString(2));
                vo.setWidgetDesp(rs.getString(3));
                vo.setWidgetPic(rs.getString(4));
                vo.setContent(rs.getString(5));
                if (Strings.isBlank(rs.getString(6)) == false) {
                    vo.setDevice(XMLUtil.xml2bean(rs.getString(6), DeviceVo.class));
                }
                vo.setCreateDate(rs.getDate(7));
                vo.setUpdateDate(rs.getDate(8));
                vo.setCreateUser(rs.getString(9));
                vo.setUpdateUser(rs.getString(10));
                return vo;
            }
            return null;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }

    @Override
    public Boolean addWidget(String tableName, CustomWidgetVo widget){
        PreparedStatement pstmt = null;
        Connection conn = null;
        try {
            conn = ds.getConnection();
            String sb = "INSERT INTO " + tableName + "(`name`,desp,picture,content,device,createUser" +
            ") VALUES(?, ?, ?, ?,?, ?)";
            pstmt = conn
                    .prepareStatement(sb);
            pstmt.setString(1, widget.getWidgetName());
            pstmt.setString(2, widget.getWidgetDesp());
            pstmt.setString(3, widget.getWidgetPic());
            pstmt.setString(4, widget.getContent());
            String bean2xml = XMLUtil.bean2xml(widget.getDevice(), Charset.forName("UTF-8"));
            pstmt.setString(5, bean2xml);
            pstmt.setString(6, widget.getCreateUser());
            pstmt.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }
}
