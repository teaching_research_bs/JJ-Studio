package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDao;

import javax.sql.DataSource;

public class MySqlDicDao extends AbstractDicDao {
    /**
     * @param ds
     */
    public MySqlDicDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
