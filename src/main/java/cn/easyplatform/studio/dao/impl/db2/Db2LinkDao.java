package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLinkDao;

import javax.sql.DataSource;

public class Db2LinkDao extends AbstractLinkDao {

    public Db2LinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
