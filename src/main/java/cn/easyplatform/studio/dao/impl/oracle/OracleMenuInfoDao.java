package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMenuInfoDao;

import javax.sql.DataSource;

public class OracleMenuInfoDao extends AbstractMenuInfoDao {
    /**
     * @param ds
     */
    public OracleMenuInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
