package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.lang.Nums;
import cn.easyplatform.studio.dao.*;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.MessageVo;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractMessageInfoDao implements MessageInfoDao {
    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractMessageInfoDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();
    @Override
    public List<MessageVo> selectList(String statement, Page page) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            StringBuilder sb = new StringBuilder();
            if (page != null && page.isGetTotal()) {
                sb.append("select count(1) from (").append(statement)
                        .append(") c");
                pstmt = conn.prepareStatement(sb.toString());

                rs = pstmt.executeQuery();
                rs.next();
                page.setTotalCount(Nums.toInt(rs.getObject(1).toString(), 0));
                DaoUtils.closeQuietly(pstmt, rs);
                sb.setLength(0);
            }
            sb.append(statement);
            if (page != null) {
                if (page.isGetTotal() && page.getTotalCount() == 0)
                    return Collections.emptyList();
                if (page.getOrderBy() != null)
                    sb.append(" order by ").append(page.getOrderBy());
                statement = getDialect().getPageSql(sb.toString(), page);
            }

            pstmt = conn.prepareStatement(statement);
            rs = pstmt.executeQuery();
            List<MessageVo> loadedObjects = new ArrayList<MessageVo>();
            while (rs.next()) {
                MessageVo e = new MessageVo();
                e.setCode(rs.getString(1));
                e.setZh_cn(rs.getString(2));
                e.setZh_tw(rs.getString(3));
                e.setEn_us(rs.getString(4));
                e.setJa_jp(rs.getString(5));
                e.setKo_kr(rs.getString(6));
                e.setDe_de(rs.getString(7));
                e.setFr_fr(rs.getString(8));
                e.setIt_it(rs.getString(9));
                e.setStatus(rs.getString(10));
                e.setMSG_TYPE(rs.getString(11));
                loadedObjects.add(e);
            }
            return loadedObjects;
        } catch (SQLException ex) {

            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public Boolean setMessageInfoList(List<MessageVo> messageVos) {
        if (messageVos == null || messageVos.size() == 0)
        {
            return false;
        }
        synchronized (messageVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("INSERT INTO `sys_message_info`" +
                                "(`code`, `zh_cn`, `zh_tw`, `en_us`, `ja_jp`, `ko_kr`, `de_de`, `fr_fr`, `it_it`, `status`, `MSG_TYPE`) " +
                                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                for (MessageVo messageVo : messageVos) {
                    pstmt.setString(1, messageVo.getCode());
                    pstmt.setString(2, messageVo.getZh_cn());
                    pstmt.setString(3, messageVo.getZh_tw());
                    pstmt.setString(4, messageVo.getEn_us());
                    pstmt.setString(5, messageVo.getJa_jp());
                    pstmt.setString(6, messageVo.getKo_kr());
                    pstmt.setString(7, messageVo.getDe_de());
                    pstmt.setString(8, messageVo.getFr_fr());
                    pstmt.setString(9, messageVo.getIt_it());
                    pstmt.setString(10, messageVo.getStatus());
                    pstmt.setString(11, messageVo.getMSG_TYPE());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public Boolean updateMessageInfoList(List<MessageVo> messageVos) {
        if (messageVos == null || messageVos.size() == 0)
        {
            return false;
        }
        synchronized (messageVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("UPDATE `sys_message_info` SET " +
                                "`ZH_CN` = ?, `ZH_TW` = ?, `EN_US` = ?, `JA_JP` = ?, `KO_KR` = ?, `DE_DE` = ?, `FR_FR` = ?, " +
                                "`IT_IT` = ?, `status` = ?, `MSG_TYPE` = ? " +
                                "WHERE `code` = ?");
                for (MessageVo messageVo:messageVos) {
                    pstmt.setString(1, messageVo.getZh_cn());
                    pstmt.setString(2, messageVo.getZh_tw());
                    pstmt.setString(3, messageVo.getEn_us());
                    pstmt.setString(4, messageVo.getJa_jp());
                    pstmt.setString(5, messageVo.getKo_kr());
                    pstmt.setString(6, messageVo.getDe_de());
                    pstmt.setString(7, messageVo.getFr_fr());
                    pstmt.setString(8, messageVo.getIt_it());
                    pstmt.setString(9, messageVo.getStatus());
                    pstmt.setString(10, messageVo.getMSG_TYPE());
                    pstmt.setString(11, messageVo.getCode());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }
}
