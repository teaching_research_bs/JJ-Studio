package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMessageInfoDao;

import javax.sql.DataSource;

public class Db2MessageInfoDao extends AbstractMessageInfoDao {
    /**
     * @param ds
     */
    public Db2MessageInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
