package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDetailDao;

import javax.sql.DataSource;

public class SqlServerDicDetailDao extends AbstractDicDetailDao {
    /**
     * @param ds
     */
    public SqlServerDicDetailDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
