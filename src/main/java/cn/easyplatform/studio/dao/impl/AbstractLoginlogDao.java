package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.dao.*;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.LoginlogVo;

import javax.sql.DataSource;
import java.sql.*;
import java.util.*;

/**
 * @Author Zeta
 * @Version 1.0
 */
public abstract class AbstractLoginlogDao implements LoginlogDao {
    private DataSource ds;

    public AbstractLoginlogDao(DataSource dataSource) {
        this.ds = dataSource;
    }

    protected abstract Dialect getDialect();

    @Override
    public Map<String,Object> getLoginlogByTermpage(String table, LoginlogVo loginlogvo, Page page) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer queryct = new StringBuffer();
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            if (!Strings.isBlank(loginlogvo.getUserName())) {
                queryct.append(" and username = '" + loginlogvo.getUserName() + "'");
            }
            if (null != loginlogvo.getLoginTime() && !("").equals(loginlogvo.getLoginTime())) {
                Calendar dayover = Calendar.getInstance();
                dayover.setTime(loginlogvo.getLoginTime());
                dayover.set(Calendar.HOUR, dayover.get(Calendar.HOUR) + 24);
                queryct.append(" and logintime >= ('" + new Timestamp(loginlogvo.getLoginTime().getTime())
                        + "') and logintime < ( '" + new Timestamp(dayover.getTime().getTime())+ " ')" );
            }
            if (!Strings.isBlank(loginlogvo.getLoginTN())) {
                queryct.append(" and logintn like '%" + loginlogvo.getLoginTN() + "%'");
            }
            if (!Strings.isBlank(loginlogvo.getType())) {
                queryct.append(" and logintype = '" + loginlogvo.getType() + "'");
            }
            pstmt = conn.prepareStatement("select logId,username,logintime,ipaddress,loginos,logintn,logintype " + "from " + table
                    + " where 1=1" + queryct + " order by logId desc limit " +(page.getPageNo() - 1) * page.getPageSize()+","+page.getPageSize());
            rs = pstmt.executeQuery();
            List<LoginlogVo> loginloglist = new ArrayList<LoginlogVo>();
            while (rs.next()) {
                LoginlogVo vo = new LoginlogVo(rs.getInt(1), rs.getString(2),
                        rs.getTimestamp(3), rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7));
                loginloglist.add(vo);
            }
            pstmt.close();
            pstmt=null;
            pstmt = conn.prepareStatement("select count(*) from " + table
                    + " where 1=1" + queryct );
            rs = pstmt.executeQuery();
            rs.next();
            int loginlogcout=rs.getInt(1);
            Map<String,Object> querymap= new HashMap<String,Object>();
            querymap.put("loginloglist",loginloglist);
            querymap.put("loginlogcout",loginlogcout);
            return querymap;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public Map<String,Object> getLoginlogAll(String table, Page page) {

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select logId,username,logintime,ipaddress,loginos,logintn,logintype " +
                    "from " + table + " where 1=1 order by logId desc limit " +(page.getPageNo() - 1) * page.getPageSize()+","+page.getPageSize());
            rs = pstmt.executeQuery();
            List<LoginlogVo> loginloglist = new ArrayList<LoginlogVo>();
            while (rs.next()) {
                LoginlogVo vo = new LoginlogVo(rs.getInt(1), rs.getString(2),
                        rs.getTimestamp(3), rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7));
                loginloglist.add(vo);
            }
            pstmt.close();
            pstmt=null;
            pstmt = conn.prepareStatement("select count(*) from " + table
                    + " where 1=1" );
            rs = pstmt.executeQuery();
            rs.next();
            int loginlogcout=rs.getInt(1);
            Map<String,Object> queryallmap= new HashMap<String,Object>();
            queryallmap.put("loginloglist",loginloglist);
            queryallmap.put("loginlogcout",loginlogcout);
            return queryallmap;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public boolean addLoginlog(String table, LoginlogVo loginlogvo) {
        if (loginlogvo == null) {
            return false;
        }
        synchronized (loginlogvo) {
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn.prepareStatement("insert into " + table +
                        " (`username`, `logintime`, `ipaddress`,`loginos`,`logintn`,`logintype`) " +
                        "values(?,?,?,?,?,?)");
                pstmt.setString(1, loginlogvo.getUserName());
                pstmt.setTimestamp(2, new Timestamp(loginlogvo.getLoginTime().getTime()));
                pstmt.setString(3, loginlogvo.getIpAddress());
                pstmt.setString(4, loginlogvo.getLoginOS());
                pstmt.setString(5, loginlogvo.getLoginTN());
                pstmt.setString(6, loginlogvo.getType());
                pstmt.executeUpdate();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public boolean deleteLoginlogById(String table, String str) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            if (!Strings.isBlank(str)) {
                String[] strs = str.split("/");
                for (String s : strs) {
                    pstmt = conn.prepareStatement("delete from " + table + " where 1=1 and logId = " + s);
                    pstmt.executeUpdate();
                }
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }


    @Override
    public boolean deleteLoginlogAll(String table) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("delete from " + table + " where 1=1  ");
            pstmt.executeUpdate();
            return true;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }
}
