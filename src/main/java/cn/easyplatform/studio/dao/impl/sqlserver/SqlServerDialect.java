/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.Page;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class SqlServerDialect extends Dialect {

	@Override
	protected String getPageBefore(Page page) {
		return "";
	}

	@Override
	protected String getPageAfter(Page page) {
		long start = (page.getPageNo() - 1) * page.getPageSize();
		StringBuilder sb = new StringBuilder(" OFFSET ").append(start)
				.append(" ROWS FETCH NEXT ").append(page.getPageSize())
				.append(" ROWS ONLY");
		return sb.toString();
	}

	@Override
	public String evalFieldType(TableField mf) {
		switch (mf.getType()) {
		case BOOLEAN:
			return "BIT";

		case DATETIME:
		case DATE:
		case TIME:
			return "DATETIME";
		case LONG:
		case INT:
			// 用户自定义了宽度
			if (mf.getLength() > 0)
				return "NUMERIC(" + mf.getLength() + ")";
			// 用数据库的默认宽度
			return "INT";

		case NUMERIC:
			// 用户自定义了精度
			if (mf.getLength() > 0 && mf.getDecimal() > 0) {
				return "decimal(" + mf.getLength() + "," + mf.getDecimal()
						+ ")";
			}
			return "NUMBER(15,10)";
		case BLOB:
			return "BINARY";
		default:
			break;
		}
		return super.evalFieldType(mf);
	}
}
