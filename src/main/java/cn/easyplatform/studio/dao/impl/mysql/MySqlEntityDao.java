/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractEntityDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MySqlEntityDao extends AbstractEntityDao {

	/**
	 * @param ds
	 */
	public MySqlEntityDao(DataSource ds) {
		super(ds);
	}

	@Override
	protected Dialect getDialect() {
		return new MySqlDialect();
	}
}
