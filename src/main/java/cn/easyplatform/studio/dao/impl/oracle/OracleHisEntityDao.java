/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractEntityDao;
import cn.easyplatform.studio.dao.impl.AbstractHisEntityDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class OracleHisEntityDao extends AbstractHisEntityDao {

	/**
	 * @param ds
	 */
	public OracleHisEntityDao(DataSource ds) {
		super(ds);
	}

	@Override
	protected Dialect getDialect() {
		return new OracleDialect();
	}
}
