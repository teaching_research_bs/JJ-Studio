package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractModuleDao;

import javax.sql.DataSource;

public class SybaseModuleDao extends AbstractModuleDao {
    public SybaseModuleDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
