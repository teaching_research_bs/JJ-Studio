package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMobileConfDao;

import javax.sql.DataSource;

public class DerbyMobileConfDao extends AbstractMobileConfDao {

    public DerbyMobileConfDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
