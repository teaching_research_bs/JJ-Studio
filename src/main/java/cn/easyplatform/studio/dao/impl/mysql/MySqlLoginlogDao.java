package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;

import javax.sql.DataSource;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class MySqlLoginlogDao extends AbstractLoginlogDao {
    public MySqlLoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
