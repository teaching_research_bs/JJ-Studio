package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLinkDao;

import javax.sql.DataSource;

public class OracleLinkDao extends AbstractLinkDao {
    public OracleLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
