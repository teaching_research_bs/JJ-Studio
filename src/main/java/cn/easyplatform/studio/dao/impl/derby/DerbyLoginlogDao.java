package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;

import javax.sql.DataSource;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class DerbyLoginlogDao extends AbstractLoginlogDao {
    public DerbyLoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
