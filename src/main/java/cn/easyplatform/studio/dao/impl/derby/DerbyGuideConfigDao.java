package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractGuideConfigDao;

import javax.sql.DataSource;

public class DerbyGuideConfigDao extends AbstractGuideConfigDao {
    /**
     * @param ds
     */
    public DerbyGuideConfigDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
