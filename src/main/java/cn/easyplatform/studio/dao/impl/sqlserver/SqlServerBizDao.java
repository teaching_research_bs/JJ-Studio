/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractBizDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SqlServerBizDao extends AbstractBizDao{

	/**
	 * @param dataSource
	 */
	public SqlServerBizDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected Dialect getDialect() {
		return new SqlServerDialect();
	}


	@Override
	protected String dropIndex(String table, String name) {
		StringBuilder sb = new StringBuilder();
		sb.append("DROP INDEX ").append(table).append(".").append(name);
		return sb.toString();
	}

	@Override
	protected String evalFieldType(TableField field) {
		switch (field.getType()) {
		case BOOLEAN:
			return "BIT";
		case DATETIME:
		case DATE:
		case TIME:
			return "DATETIME";
		case LONG:
			return "NUMERIC(23)";
		case INT:
			// 用户自定义了宽度
			if (field.getLength() > 0)
				return "NUMERIC(" + field.getLength() + ")";
			// 用数据库的默认宽度
			return "INT";
		case BLOB:
			return "BINARY";
		default:
			return super.evalFieldType(field);
		}
	}
}
