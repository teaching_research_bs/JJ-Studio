package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractModuleDao;

import javax.sql.DataSource;

public class SqlServerModuleDao extends AbstractModuleDao {
    public SqlServerModuleDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
