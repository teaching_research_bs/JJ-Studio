package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractCustomWidgetDao;

import javax.sql.DataSource;

public class OracleCustomWidgetDao extends AbstractCustomWidgetDao {
    /**
     * @param ds
     */
    public OracleCustomWidgetDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
