package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMobileConfDao;

import javax.sql.DataSource;

public class SqlServerMobileConfDao extends AbstractMobileConfDao {
    public SqlServerMobileConfDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
