package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.MenuInfoDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.MenuInfoVo;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMenuInfoDao implements MenuInfoDao {
    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractMenuInfoDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();

    @Override
    public List<MenuInfoVo> selectAllList() {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select menuId,name,image,orderNo,parentMenuId,tasks,desp from sys_menu_info where 1=1");
            rs = pstmt.executeQuery();
            List<MenuInfoVo> loadedObjects = new ArrayList<>();
            while (rs.next()) {
                MenuInfoVo e = new MenuInfoVo();
                e.setMenuId(rs.getString(1));
                e.setName(rs.getString(2));
                e.setImage(rs.getString(3));
                e.setOrderNo(rs.getString(4));
                e.setParentMenuId(rs.getString(5));
                e.setTasks(rs.getString(6));
                e.setDesp(rs.getString(7));
                loadedObjects.add(e);
            }
            return loadedObjects;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public Boolean setMenuList(List<MenuInfoVo> dicVos)
    {
        if (dicVos == null || dicVos.size() == 0)
        {
            return false;
        }
        synchronized (dicVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("INSERT INTO `sys_menu_info`" +
                                "(`menuId`, `name`, `image`, `orderNo`, `parentMenuId`, `tasks`, `desp`) VALUES " +
                                "(?, ?, ?, ?, ?, ?, ?)");
                for (MenuInfoVo dicVo : dicVos) {
                    pstmt.setString(1, dicVo.getMenuId());
                    pstmt.setString(2, dicVo.getName());
                    pstmt.setString(3, dicVo.getImage());
                    pstmt.setString(4, dicVo.getOrderNo());
                    pstmt.setString(5, dicVo.getParentMenuId());
                    pstmt.setString(6, dicVo.getTasks());
                    pstmt.setString(7, dicVo.getDesp());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }

    @Override
    public Boolean updateMenuList(List<MenuInfoVo> dicVos)
    {
        if (dicVos == null || dicVos.size() == 0)
        {
            return false;
        }
        synchronized (dicVos){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn.prepareStatement("UPDATE `sys_menu_info` SET " +
                        "`name` = ?, `image` = ?, `orderNo` = ?, `parentMenuId` = ?, `tasks` = ?, `desp` = ? " +
                        " WHERE `menuId` = ?");
                for (MenuInfoVo dicVo : dicVos) {
                    pstmt.setString(1, dicVo.getName());
                    pstmt.setString(2, dicVo.getImage());
                    pstmt.setString(3, dicVo.getOrderNo());
                    pstmt.setString(4, dicVo.getParentMenuId());
                    pstmt.setString(5, dicVo.getTasks());
                    pstmt.setString(6, dicVo.getDesp());
                    pstmt.setString(7, dicVo.getMenuId());
                    pstmt.addBatch();
                }
                pstmt.executeBatch();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
        }
        return true;
    }
}
