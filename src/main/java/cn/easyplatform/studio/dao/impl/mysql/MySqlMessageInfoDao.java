package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMessageInfoDao;

import javax.sql.DataSource;

public class MySqlMessageInfoDao extends AbstractMessageInfoDao {
    /**
     * @param ds
     */
    public MySqlMessageInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
