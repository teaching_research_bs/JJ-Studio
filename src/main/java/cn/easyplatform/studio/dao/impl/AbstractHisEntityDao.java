package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.dao.*;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.HistoryVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author Zeta
 * @Version 1.0
 */
public abstract class AbstractHisEntityDao implements HisEntityDao {

    protected final static Logger log = LoggerFactory.getLogger(AbstractEntityDao.class);

    protected DataSource ds;

    /**
     * @param ds
     */
    public AbstractHisEntityDao(DataSource ds) {
        this.ds = ds;
    }

    protected abstract Dialect getDialect();

    @Override
    public EntityInfo getMaxVsEntry(String table, String id){
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("select versionNo,name,desp,type,subType,content,updateDate,status from " + table + "_repo a where versionNo = " +
                            "(SELECT MAX(versionNo) FROM "+ table + "_repo where entityId = a.entityId ) and entityId =?");
            pstmt.setString(1, id);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                EntityInfo entity = new EntityInfo();
                entity.setId(id);
                entity.setVersion(rs.getInt(1));
                entity.setName(rs.getString(2));
                entity.setDescription(rs.getString(3));
                entity.setType(rs.getString(4));
                entity.setSubType(rs.getString(5));
                entity.setContent(rs.getString(6));
                entity.setUpdateDate(rs.getTimestamp(7));
                entity.setStatus((rs.getString(8).charAt(0)));
                return entity;
            }
            return null;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public EntityInfo getHisEntry(String table, int versionNo) {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn
                    .prepareStatement("SELECT entityId,`name`,desp,`type`,subType,content,updateDate,updateUser,status FROM "
                            + table + "_repo WHERE versionNo=? ");
            pstmt.setInt(1, versionNo);
            rs = pstmt.executeQuery();
            if(rs.next()){
                EntityInfo HisEntry = new EntityInfo();
                HisEntry.setId(rs.getString(1));
                HisEntry.setName(rs.getString(2));
                HisEntry.setDescription(rs.getString(3));
                HisEntry.setType(rs.getString(4));
                HisEntry.setSubType(rs.getString(5));
                HisEntry.setContent(rs.getString(6));
                HisEntry.setUpdateDate(rs.getTimestamp(7));
                HisEntry.setUpdateUser(rs.getString(8));
                HisEntry.setStatus(rs.getString(9).charAt(0));
                return HisEntry;
            }
            return null;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }
}
