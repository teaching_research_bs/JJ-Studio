/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.Page;
import cn.easyplatform.type.FieldType;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class MySqlDialect extends Dialect {

	@Override
	protected String getPageBefore(Page page) {
		return "";
	}

	@Override
	protected String getPageAfter(Page page) {
		int start = (page.getPageNo() - 1) * page.getPageSize();
		StringBuilder sb = new StringBuilder();
		sb.append(" LIMIT ").append(start).append(",")
				.append(page.getPageSize());
		return sb.toString();
	}

	@Override
	public String evalFieldType(TableField mf) {
		if (mf.getType() == FieldType.INT) {
			int width = mf.getLength();
			if (width <= 0) {
				return "INT(32)";
			} else if (width <= 2) {
				return "TINYINT(" + (width * 4) + ")";
			} else if (width <= 4) {
				return "MEDIUMINT(" + (width * 4) + ")";
			} else if (width <= 8) {
				return "INT(" + (width * 4) + ")";
			}
			return "BIGINT(" + (width * 4) + ")";
		} else if (mf.getType() == FieldType.LONG)
			return "BIGINT";

		if (mf.getType() == FieldType.BLOB)
			return "MediumBlob"; // 默认用16M
		else if (mf.getType() == FieldType.CLOB)
			return "MediumText";
		return super.evalFieldType(mf);
	}

}
