/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractBizDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SybaseBizDao extends AbstractBizDao {

	/**
	 * @param dataSource
	 */
	public SybaseBizDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected Dialect getDialect() {
		return new SybaseDialect();
	}

	@Override
	protected String evalFieldType(TableField field) {
		switch (field.getType()) {
		case BOOLEAN:
			return "TINYINT";

		case DATETIME:
		case DATE:
		case TIME:
			return "DATETIME";
		case LONG:
			return "NUMERIC(23)";
		case INT:
			// 用户自定义了宽度
			if (field.getLength() > 0)
				return "NUMERIC(" + field.getLength() + ")";
			// 用数据库的默认宽度
			return "INT";
		case BLOB:
			return "IMAGE";
		default:
			return super.evalFieldType(field);
		}
	}
}
