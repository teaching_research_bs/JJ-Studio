package cn.easyplatform.studio.dao.impl.db2;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDatabaseUpdateDao;

import javax.sql.DataSource;

public class Db2DatabaseUpdateDao extends AbstractDatabaseUpdateDao {
    /**
     * @param ds
     */
    public Db2DatabaseUpdateDao(DataSource ds) {
        super(ds);
    }
    @Override
    protected Dialect getDialect() {
        return new Db2Dialect();
    }
}
