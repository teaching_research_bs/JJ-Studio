/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractIdentityDao;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SybaseIdentityDao extends AbstractIdentityDao{

	/**
	 * @param dataSource
	 */
	public SybaseIdentityDao(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected Dialect getDialect() {
		return new SybaseDialect();
	}

}
