package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractTaskLinkDao;

import javax.sql.DataSource;

public class DerbyTaskLinkDao extends AbstractTaskLinkDao {
    public DerbyTaskLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
