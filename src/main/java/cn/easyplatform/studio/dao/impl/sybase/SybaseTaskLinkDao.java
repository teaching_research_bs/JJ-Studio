package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractTaskLinkDao;

import javax.sql.DataSource;

public class SybaseTaskLinkDao extends AbstractTaskLinkDao {
    public SybaseTaskLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
