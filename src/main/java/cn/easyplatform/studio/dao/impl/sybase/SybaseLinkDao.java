package cn.easyplatform.studio.dao.impl.sybase;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLinkDao;

import javax.sql.DataSource;

public class SybaseLinkDao extends AbstractLinkDao {
    public SybaseLinkDao(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    protected Dialect getDialect() {
        return new SybaseDialect();
    }
}
