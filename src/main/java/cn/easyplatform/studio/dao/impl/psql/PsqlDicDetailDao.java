package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDetailDao;

import javax.sql.DataSource;

public class PsqlDicDetailDao extends AbstractDicDetailDao {
    /**
     * @param ds
     */
    public PsqlDicDetailDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
