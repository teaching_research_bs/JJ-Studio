package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMessageInfoDao;

import javax.sql.DataSource;

public class PsqlMessageInfoDao extends AbstractMessageInfoDao {
    /**
     * @param ds
     */
    public PsqlMessageInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
