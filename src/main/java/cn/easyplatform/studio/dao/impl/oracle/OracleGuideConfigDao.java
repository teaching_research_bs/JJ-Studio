package cn.easyplatform.studio.dao.impl.oracle;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractGuideConfigDao;

import javax.sql.DataSource;

public class OracleGuideConfigDao extends AbstractGuideConfigDao {
    /**
     * @param ds
     */
    public OracleGuideConfigDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new OracleDialect();
    }
}
