package cn.easyplatform.studio.dao.impl.derby;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMenuInfoDao;

import javax.sql.DataSource;

public class DerbyMenuInfoDao extends AbstractMenuInfoDao {
    /**
     * @param ds
     */
    public DerbyMenuInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new DerbyDialect();
    }
}
