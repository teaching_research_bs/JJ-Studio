package cn.easyplatform.studio.dao.impl.psql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractDicDao;

import javax.sql.DataSource;

public class PsqlDicDao extends AbstractDicDao {
    /**
     * @param ds
     */
    public PsqlDicDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new PsqlDialect();
    }
}
