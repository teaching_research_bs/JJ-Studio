package cn.easyplatform.studio.dao.impl;

import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.dao.DaoUtils;
import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.MobileConfDao;
import cn.easyplatform.studio.dao.transaction.JdbcTransactions;
import cn.easyplatform.studio.vos.MobileConfVo;

import javax.sql.DataSource;
import java.sql.*;

public abstract class AbstractMobileConfDao implements MobileConfDao {
    private DataSource ds;

    public AbstractMobileConfDao(DataSource dataSource) {
        this.ds = dataSource;
    }

    protected abstract Dialect getDialect();
    @Override
    public Boolean addNewMobileConf(MobileConfVo vo) {
        if (vo == null)
        {
            return false;
        }
        synchronized (vo){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn
                        .prepareStatement("insert into ep_mobile_conf_info (projectId, confType,bundleId,displayName,version,icon,launch,url,createUserId) " +
                                "values(?,?,?,?,?,?,?,?,?)");
                pstmt.setString(1, vo.getProjectId());
                pstmt.setString(2, vo.getConfType());
                pstmt.setString(3, vo.getBundleId());
                pstmt.setString(4, vo.getDisplayName());
                pstmt.setString(5, vo.getMobileVersion());
                pstmt.setString(6, vo.getIcon());
                pstmt.setString(7, vo.getLaunch());
                pstmt.setString(8, vo.getUrl());
                pstmt.setString(9, vo.getCreateUserId());
                pstmt.executeUpdate();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
            return true;
        }
    }

    @Override
    public MobileConfVo getConf(String confStr, String projectId)
    {
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            Connection conn = JdbcTransactions.getConnection(ds);
            pstmt = conn.prepareStatement("select confId,bundleId,displayName,version,icon,launch,url " +
                    "from ep_mobile_conf_info where confType=? AND projectId=?  ORDER BY createDate DESC LIMIT 1");
            pstmt.setString(1, confStr);
            pstmt.setString(2, projectId);
            rs = pstmt.executeQuery();
            MobileConfVo vo = null;
            while (rs.next()) {
                vo = new MobileConfVo(rs.getInt(1), projectId, confStr, rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getString(7), null, null,
                        null, null);
            }
            return vo;
        } catch (SQLException ex) {
            throw new DaoException(ex.getMessage());
        } finally {
            DaoUtils.closeQuietly(pstmt, rs);
        }
    }

    @Override
    public Boolean updateMobileConf(MobileConfVo vo) {
        if (vo == null)
        {
            return false;
        }
        synchronized (vo){
            PreparedStatement pstmt = null;
            Connection conn = null;
            try {
                conn = JdbcTransactions.getConnection(ds);
                pstmt = conn.prepareStatement("UPDATE `ep_mobile_conf_info` SET " +
                        "`bundleId` = ?, `displayName` = ?, `version` = ?, `icon` = ?, `launch` = ?, `url` = ?, " +
                        "`updateUserId` = ?, `updateDate` = ? WHERE `confId` = ?");
                pstmt.setString(1, vo.getBundleId());
                pstmt.setString(2, vo.getDisplayName());
                pstmt.setString(3, vo.getMobileVersion());
                pstmt.setString(4, vo.getIcon());
                pstmt.setString(5, vo.getLaunch());
                pstmt.setString(6, vo.getUrl());
                pstmt.setString(7, vo.getUpdateUserId());
                pstmt.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
                pstmt.setInt(9, vo.getConfId());
                pstmt.executeUpdate();
            } catch (SQLException ex) {
                throw new DaoException(ex.getMessage());
            } finally {
                DaoUtils.closeQuietly(pstmt);
            }
            return true;
        }
    }
}
