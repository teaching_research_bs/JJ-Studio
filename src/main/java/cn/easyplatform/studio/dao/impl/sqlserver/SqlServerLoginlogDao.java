package cn.easyplatform.studio.dao.impl.sqlserver;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractLoginlogDao;

import javax.sql.DataSource;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class SqlServerLoginlogDao extends AbstractLoginlogDao {

    public SqlServerLoginlogDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new SqlServerDialect();
    }
}
