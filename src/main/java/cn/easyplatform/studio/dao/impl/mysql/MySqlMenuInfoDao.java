package cn.easyplatform.studio.dao.impl.mysql;

import cn.easyplatform.studio.dao.Dialect;
import cn.easyplatform.studio.dao.impl.AbstractMenuInfoDao;

import javax.sql.DataSource;

public class MySqlMenuInfoDao extends AbstractMenuInfoDao {
    /**
     * @param ds
     */
    public MySqlMenuInfoDao(DataSource ds) {
        super(ds);
    }

    @Override
    protected Dialect getDialect() {
        return new MySqlDialect();
    }
}
