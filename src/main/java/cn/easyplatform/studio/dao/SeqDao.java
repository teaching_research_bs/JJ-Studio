/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.dos.SerialDo;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface SeqDao {

	void create(SerialDo sn);
	
	void delete(String name);
	
	Long nextval(SerialDo sn);
	
	void reset(SerialDo name);
}
