/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.studio.dao.impl.DefaultSeqDao;
import cn.easyplatform.studio.dao.impl.db2.*;
import cn.easyplatform.studio.dao.impl.derby.*;
import cn.easyplatform.studio.dao.impl.mysql.*;
import cn.easyplatform.studio.dao.impl.oracle.*;
import cn.easyplatform.studio.dao.impl.psql.*;
import cn.easyplatform.studio.dao.impl.sqlserver.*;
import cn.easyplatform.studio.dao.impl.sybase.*;

import javax.sql.DataSource;

import static cn.easyplatform.studio.dao.DaoUtils.*;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class DaoFactory {

	private DaoFactory() {
	}

	public static EntityDao createEntityDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
		case ORACLE:
			return new OracleEntityDao(ds);
		case DB2:
			return new Db2EntityDao(ds);
		case MYSQL:
			return new MySqlEntityDao(ds);
		case SQLSERVER:
			return new SqlServerEntityDao(ds);
		case PSQL:
			return new PsqlEntityDao(ds);
		case DERBY:
			return new DerbyEntityDao(ds);
		case SYBASE:
			return new SybaseEntityDao(ds);
		default:
			String msg = ds.toString().substring(5);
			msg = msg.substring(0, msg.indexOf(":"));
			throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
					msg);
		}
	}

	public static VersionDao createVersionDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleVersionDao(ds);
			case DB2:
				return new Db2VersionDao(ds);
			case MYSQL:
				return new MySqlVersionDao(ds);
			case SQLSERVER:
				return new SqlServerVersionDao(ds);
			case PSQL:
				return new PsqlVersionDao(ds);
			case DERBY:
				return new DerbyVersionDao(ds);
			case SYBASE:
				return new SybaseVersionDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static BizDao createBizDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
		case ORACLE:
			return new OracleBizDao(ds);
		case DB2:
			return new Db2BizDao(ds);
		case MYSQL:
			return new MySqlBizDao(ds);
		case SQLSERVER:
			return new SqlServerBizDao(ds);
		case PSQL:
			return new PsqlBizDao(ds);
		case DERBY:
			return new DerbyBizDao(ds);
		case SYBASE:
			return new SybaseBizDao(ds);
		default:
			String msg = ds.toString().substring(5);
			msg = msg.substring(0, msg.indexOf(":"));
			throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
					msg);
		}
	}

	public static IdentityDao createIdentityDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
		case ORACLE:
			return new OracleIdentityDao(ds);
		case DB2:
			return new Db2IdentityDao(ds);
		case MYSQL:
			return new MySqlIdentityDao(ds);
		case SQLSERVER:
			return new SqlServerIdentityDao(ds);
		case PSQL:
			return new PsqlIdentityDao(ds);
		case DERBY:
			return new DerbyIdentityDao(ds);
		case SYBASE:
			return new SybaseIdentityDao(ds);
		default:
			String msg = ds.toString().substring(5);
			msg = msg.substring(0, msg.indexOf(":"));
			throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
					msg);
		}
	}

	public static SeqDao createSeqDao(DataSource ds) {
		return new DefaultSeqDao(ds);
	}

	public static MobileConfDao createMobileConfDao(DataSource ds)
	{
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleMobileConfDao(ds);
			case DB2:
				return new Db2MobileConfDao(ds);
			case MYSQL:
				return new MySqlMobileConfDao(ds);
			case SQLSERVER:
				return new SqlServerMobileConfDao(ds);
			case PSQL:
				return new PsqlMobileConfDao(ds);
			case DERBY:
				return new DerbyMobileConfDao(ds);
			case SYBASE:
				return new SybaseMobileConfDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static UploadHistDao createUploadHistDao(DataSource ds)
	{
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleUploadHistDao(ds);
			case DB2:
				return new Db2UploadHistDao(ds);
			case MYSQL:
				return new MySqlUploadHistDao(ds);
			case SQLSERVER:
				return new SqlServerUploadHistDao(ds);
			case PSQL:
				return new PsqlUploadHistDao(ds);
			case DERBY:
				return new DerbyUploadHistDao(ds);
			case SYBASE:
				return new SybaseUploadHistDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static TaskLinkDao createTaskLinkDao(DataSource ds)
	{
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleTaskLinkDao(ds);
			case DB2:
				return new Db2TaskLinkDao(ds);
			case MYSQL:
				return new MySqlTaskLinkDao(ds);
			case SQLSERVER:
				return new SqlServerTaskLinkDao(ds);
			case PSQL:
				return new PsqlTaskLinkDao(ds);
			case DERBY:
				return new DerbyTaskLinkDao(ds);
			case SYBASE:
				return new SybaseTaskLinkDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static LinkDao createLinkDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleLinkDao(ds);
			case DB2:
				return new Db2LinkDao(ds);
			case MYSQL:
				return new MySqlLinkDao(ds);
			case SQLSERVER:
				return new SqlServerLinkDao(ds);
			case PSQL:
				return new PsqlLinkDao(ds);
			case DERBY:
				return new DerbyLinkDao(ds);
			case SYBASE:
				return new SybaseLinkDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static DicDao createDicDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleDicDao(ds);
			case DB2:
				return new Db2DicDao(ds);
			case MYSQL:
				return new MySqlDicDao(ds);
			case SQLSERVER:
				return new SqlServerDicDao(ds);
			case PSQL:
				return new PsqlDicDao(ds);
			case DERBY:
				return new DerbyDicDao(ds);
			case SYBASE:
				return new SybaseDicDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static DicDetailDao createDicDetailDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleDicDetailDao(ds);
			case DB2:
				return new Db2DicDetailDao(ds);
			case MYSQL:
				return new MySqlDicDetailDao(ds);
			case SQLSERVER:
				return new SqlServerDicDetailDao(ds);
			case PSQL:
				return new PsqlDicDetailDao(ds);
			case DERBY:
				return new DerbyDicDetailDao(ds);
			case SYBASE:
				return new SybaseDicDetailDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static MessageInfoDao createMessageInfoDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleMessageInfoDao(ds);
			case DB2:
				return new Db2MessageInfoDao(ds);
			case MYSQL:
				return new MySqlMessageInfoDao(ds);
			case SQLSERVER:
				return new SqlServerMessageInfoDao(ds);
			case PSQL:
				return new PsqlMessageInfoDao(ds);
			case DERBY:
				return new DerbyMessageInfoDao(ds);
			case SYBASE:
				return new SybaseMessageInfoDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static MenuInfoDao createMenuInfoDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleMenuInfoDao(ds);
			case DB2:
				return new Db2MenuInfoDao(ds);
			case MYSQL:
				return new MySqlMenuInfoDao(ds);
			case SQLSERVER:
				return new SqlServerMenuInfoDao(ds);
			case PSQL:
				return new PsqlMenuInfoDao(ds);
			case DERBY:
				return new DerbyMenuInfoDao(ds);
			case SYBASE:
				return new SybaseMenuInfoDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static GuideConfigDao createGuideConfigDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleGuideConfigDao(ds);
			case DB2:
				return new Db2GuideConfigDao(ds);
			case MYSQL:
				return new MySqlGuideConfigDao(ds);
			case SQLSERVER:
				return new SqlServerGuideConfigDao(ds);
			case PSQL:
				return new PsqlGuideConfigDao(ds);
			case DERBY:
				return new DerbyGuideConfigDao(ds);
			case SYBASE:
				return new SybaseGuideConfigDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static ModuleDao createModuleDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleModuleDao(ds);
			case DB2:
				return new Db2ModuleDao(ds);
			case MYSQL:
				return new MySqlModuleDao(ds);
			case SQLSERVER:
				return new SqlServerModuleDao(ds);
			case PSQL:
				return new PsqlModuleDao(ds);
			case DERBY:
				return new DerbyModuleDao(ds);
			case SYBASE:
				return new SybaseModuleDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static LoginlogDao createLoginlogDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleLoginlogDao(ds);
			case DB2:
				return new Db2LoginlogDao(ds);
			case MYSQL:
				return new MySqlLoginlogDao(ds);
			case SQLSERVER:
				return new SqlServerLoginlogDao(ds);
			case PSQL:
				return new PsqlLoginlogDao(ds);
			case DERBY:
				return new DerbyLoginlogDao(ds);
			case SYBASE:
				return new SybaseLoginlogDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static HisEntityDao createHisEntityDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleHisEntityDao(ds);
			case DB2:
				return new Db2HisEntityDao(ds);
			case MYSQL:
				return new MySqlHisEntityDao(ds);
			case SQLSERVER:
				return new SqlServerHisEntityDao(ds);
			case PSQL:
				return new PsqlHisEntityDao(ds);
			case DERBY:
				return new DerbyHisEntityDao(ds);
			case SYBASE:
				return new SybaseHisEntityDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static DatabaseUpdateDao createDatabaseUpdateDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleDatabaseUpdateDao(ds);
			case DB2:
				return new Db2DatabaseUpdateDao(ds);
			case MYSQL:
				return new MySqlDatabaseUpdateDao(ds);
			case SQLSERVER:
				return new SqlServerDatabaseUpdateDao(ds);
			case PSQL:
				return new PsqlDatabaseUpdateDao(ds);
			case DERBY:
				return new DerbyDatabaseUpdateDao(ds);
			case SYBASE:
				return new SybaseDatabaseUpdateDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}

	public static CustomWidgetDao createWidgetDao(DataSource ds) {
		switch (DaoUtils.getDbType(ds.toString())) {
			case ORACLE:
				return new OracleCustomWidgetDao(ds);
			case DB2:
				return new Db2CustomWidgetDao(ds);
			case MYSQL:
				return new MySqlCustomWidgetDao(ds);
			case SQLSERVER:
				return new SqlServerCustomWidgetDao(ds);
			case PSQL:
				return new PsqlCustomWidgetDao(ds);
			case DERBY:
				return new DerbyCustomWidgetDao(ds);
			case SYBASE:
				return new SybaseCustomWidgetDao(ds);
			default:
				String msg = ds.toString().substring(5);
				msg = msg.substring(0, msg.indexOf(":"));
				throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
						msg);
		}
	}
}
