package cn.easyplatform.studio.dao;

import javax.sql.DataSource;
import java.util.List;

public interface DatabaseUpdateDao {
    /**
     * 第二版本更新，添加_upload_file_hist，_link_info，_mobile_conf_info，_task_link_info，sys_param_info
     * @param table
     */
    void createSecondVersion(String table);

    /**
     * 第2.1版本更新，更换数据表名称ep_cj_link_info为ep_cj_link,ep_cj_task_link_info为ep_cj_task_link,
     * ep_ cj _mobile_conf_info为ep _mobile_conf_info,ep_guide_info
     * @param table
     * @param projectId
     * @param isContainNewMobileExport
     * @param isContainGuideInfo
     */
    void createSecondOneVersion(String table, String projectId, boolean isContainNewMobileExport, boolean isContainGuideInfo);

    /**
     * 第2.2版本更新,添加sys_guide_info
     * @param dataSource
     * @param isContainSysGuideInfo
     */
    void createSecondSecondVersion(DataSource dataSource, boolean isContainSysGuideInfo);

    /**
     * 第2.3版本更新,添加_module
     * @param table
     * @param isContainModule
     */
    void createSecondThirdVersion(String table, boolean isContainModule);

    /**
     * 第2.7版本更新,修改_module，添加sys_dic_info
     * @param dataSource
     * @param table
     * @param table
     */
    void createSecondSevenVersion(DataSource dataSource, String table, String userID);

    /**
     * 第2.17版本更新,修改sys_role_info，sys_menu_info
     * @param dataSource
     */
    void createSecondSeventeenVersion(DataSource dataSource);

    /**
     * 第3.0版本更新,添加sys_table_field_info数据
     * @param dataSource
     */
    void createThreeVersion(DataSource dataSource, List<String> fieldNameList, List<String> fieldTypeList,
                            List<String> fieldLengthList, List<String> fieldDesList, List<String> fieldNotNUllList);

    /**
     * 第3.1版本更新,添加ep_import_detail_info字段
     */
    void createThreeOneVersion();

    /**
     * 第3.2版本更新,判断字典表结构是否一致，添加ep_import_detail_info字段
     */
    void createThreeTwoVersion(DataSource dataSource, List<String> fieldNameList, List<String> fieldTypeList,
                               List<String> fieldLengthList, List<String> fieldDesList, List<String> fieldNotNUllList);
    /**
     * 第2.18版本更新,添加_login_log表
     * @param table
     */
    void createSecondEighteenVersion(String table);

    /**
     * 第3.3版本更新,删除sys_role_info的type栏位，增加sys_role_menu_info栏位,添加ep_model_ext_info表
     */
    void createThreeThreeVersion(DataSource dataSource);

    /**
     * 第3.4版本更新,添加ep_model_ext_info表
     */
    void createThreeFourVersion();

    /**
     * 第3.5版本更新,添加ep_项目表ext0和ext1
     */
    void createThreeFiveVersion(String table);

    /**
     * 第3.6版本更新,添加sys_role_access_info表
     * @param dataSource
     */
    void createThreeSixVersion(DataSource dataSource);

    /**
     * 第3.7版本更新,添加sys_access_info表
     * @param dataSource
     */
    void createThreeSevenVersion(DataSource dataSource);

    /**
     * 第3.8版本更新,ep_user_info表:删除type字段,删除roleId字段。ep_user_project_info表：roleId字段。新增ep_user_project_info数据：admin和dev类型
     */
    void createThreeEightVersion();

    /**
     * 第3.9版本更新,所有用户添加系统的默认授权
     */
    void createThreeNineVersion();

    /**
     * 第4.1版本更新,系统自定义控件表生成
     */
    void createFourOneVersion();

    /**
     * 第4.2版本更新,项目自定义控件表生成
     */
    void createFourTwoVersion(String table);
    /**
     * 第4.3版本更新,项目自动生成增删改参数模板表
     */
    void createFourThreeVersion();
}
