package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.MobileConfVo;

public interface MobileConfDao {

    Boolean addNewMobileConf(MobileConfVo vo);

    MobileConfVo getConf(String confStr, String projectId);

    Boolean updateMobileConf(MobileConfVo vo);

}
