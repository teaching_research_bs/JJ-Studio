package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.MessageVo;

import java.util.List;

public interface MessageInfoDao {
    List<MessageVo> selectList(String statement, Page page);
    Boolean setMessageInfoList(List<MessageVo> messageVos);
    Boolean updateMessageInfoList(List<MessageVo> messageVos);
}
