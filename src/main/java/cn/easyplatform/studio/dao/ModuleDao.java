package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.ModuleVo;

import java.util.List;

public interface ModuleDao {
    List<ModuleVo> getModule(String table, String searchStr);

    boolean addModule(String table, ModuleVo moduleVo);

    boolean updateModule(String table, ModuleVo moduleVo);

    boolean deleteModule(String table, ModuleVo moduleVo);

    void backup(String dir, String templateId, String... modules);
}
