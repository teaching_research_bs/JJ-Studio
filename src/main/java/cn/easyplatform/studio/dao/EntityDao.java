/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.vos.MessageVo;

import java.util.HashMap;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface EntityDao {

    List<EntityVo> selectList(String statement, Page page, String... parameters);

    <T extends BaseEntity> List<T> selectEntity(String query, Page page,
                                                String... parameters);

    Object selectObject(String query, Object... parameters);

    HashMap<String, Object> selectCommonOne(String query, Object... parameters);

    List<EntityVo> selectModel(String statement, String... parameters);

    List<MessageVo> selectMessage(String statement, String... parameters);

    ResourceBean getDatasource(String id);

    List<ResourceBean> getDatasources(String projectId);

    <T extends BaseEntity> T getEntity(String table, String id);

    <T extends BaseEntity> List<T> getEntityList(String table, List<String> idList);

    List<EntityVo> selectListWithID(String table, List<String> idList);

    EntityInfo getEntry(String table, String id);

    boolean exists(String table, String id);

    boolean exists(String table);

    void save(String projectId, String table, EntityInfo entity);

    List<String> getColumnNameList(String table);
}
