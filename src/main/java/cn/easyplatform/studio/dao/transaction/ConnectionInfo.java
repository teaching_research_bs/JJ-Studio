/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.dao.transaction;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ConnectionInfo {

	public ConnectionInfo(DataSource ds, Connection conn) {
		this.ds = ds;
		this.conn = conn;
	}

	public ConnectionInfo(DataSource ds, Connection conn, int level)
			throws SQLException {
		this.ds = ds;
		this.conn = conn;
		this.oldLevel = conn.getTransactionIsolation();
		if (this.oldLevel != level)
			conn.setTransactionIsolation(level);
	}

	public DataSource getDs() {
		return ds;
	}

	public void setDs(DataSource ds) {
		this.ds = ds;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public int getOldLevel() {
		return oldLevel;
	}

	public void setOldLevel(int oldLevel) {
		this.oldLevel = oldLevel;
	}

	private DataSource ds;
	
	private Connection conn;
	
	private int oldLevel;
}
