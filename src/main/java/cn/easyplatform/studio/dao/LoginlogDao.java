package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.LoginlogVo;

import java.util.Map;

/**
 * @Author Zeta
 * @Version 1.0
 */
public interface LoginlogDao {
    public Map<String,Object> getLoginlogByTermpage(String table, LoginlogVo loginlogvo, Page page);//查询指定条件日志
    public Map<String,Object> getLoginlogAll(String table,Page page);//查询所有
    public boolean addLoginlog(String table,LoginlogVo loginlogvo);//添加记录
    public boolean deleteLoginlogById(String table,String loginlogId);//删除指定记录
    public boolean deleteLoginlogAll(String table);//删除所有记录

}
