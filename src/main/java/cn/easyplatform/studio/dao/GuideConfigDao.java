package cn.easyplatform.studio.dao;

import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;

import java.util.List;

public interface GuideConfigDao {

    List<GuideConfigVo> getGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType);

    boolean addGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType, List<GuideConfigVo> addList);

    boolean updateGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType, List<GuideConfigVo> updateList);

    boolean deleteGuideConfig(GuideConfigEditor.GuideConfigType guideConfigType, List<GuideConfigVo> updateList);
}
