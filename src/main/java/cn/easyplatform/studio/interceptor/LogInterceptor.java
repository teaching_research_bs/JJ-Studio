/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.interceptor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LogInterceptor extends AbstractCommandInterceptor {

	private static Logger log = LoggerFactory.getLogger(LogInterceptor.class);

	public <T> T execute(Command<T> command) {
		if (!log.isDebugEnabled())
			return next.execute(command);
		log.debug(
				"--- starting {%s} --------------------------------------------------------",
				command.getClass().getSimpleName());
		try {
			return next.execute(command);
		} finally {
			log.debug(
					"--- {%s} finished --------------------------------------------------------",
					command.getClass().getSimpleName());
		}
	}
}
