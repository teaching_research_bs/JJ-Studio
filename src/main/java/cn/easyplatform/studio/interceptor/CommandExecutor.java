/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.interceptor;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface CommandExecutor {

	/**
	 * @param command
	 * @return
	 */
	<T> T execute(Command<T> command);

}
