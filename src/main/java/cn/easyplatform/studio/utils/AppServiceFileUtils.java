package cn.easyplatform.studio.utils;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;

import java.io.File;

public class AppServiceFileUtils extends ServiceFileUtil {

    public String imgDirPath_app = getEngineProjectPath() +File.separator + getAppTemplatePath() +
            File.separator + projectName + File.separator + "public" + File.separator + "img";

    public String cssDirPath_app = getEngineProjectPath() +File.separator + getAppTemplatePath() +
            File.separator + projectName + File.separator + "public" + File.separator + "css";

    public String jsDirPath_app = getEngineProjectPath() +File.separator + getAppTemplatePath() +
            File.separator + projectName + File.separator + "public" + File.separator + "js";

    public String audioDirPath_app = getEngineProjectPath() +File.separator + getAppTemplatePath() +
            File.separator + projectName + File.separator + "public" + File.separator + "audio";

    public String videoDirPath_app = getEngineProjectPath() +File.separator + getAppTemplatePath() +
            File.separator + projectName + File.separator + "public" + File.separator + "video";

    public String templatePath_app = getEngineProjectPath() + File.separator + getAppTemplatePath()+
            File.separator + projectName + File.separator;

    public boolean webContainerIsLegal() {
        if (Strings.isBlank(StudioApp.getAppPath())) {
            return false;
        }
        boolean isExists;
        try {
            isExists = new File(getEngineProjectPath()).exists();
        } catch (Exception e) {
            isExists = false;
        }
        return isExists;
    }

    private String getEngineProjectPath() {
        String relativePath = StudioApp.getAppPath();
        relativePath = getRealPath(relativePath);
        return System.getProperty("catalina.home") + File.separator + "webapps" + File.separator + relativePath;
    }

    private String getAppTemplatePath() {
        String relativePath = StudioApp.getAppTemplatePath();
        if (Strings.isBlank(relativePath))
            relativePath = "/WEB-INF/template";
        relativePath = getRealPath(relativePath);
        return relativePath;
    }

    public String getFilePath(String pathName,TemplateUtil.TemplateType templateType) {
        String path = null;
        if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_DESKTOP_LOGIN.getName()))
            path = templatePath_app + pathName + "d0";
        else if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_DESKTOP_MAIN.getName()))
            path = templatePath_app + pathName + "d1";
        else if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_MOBILE_LOGIN.getName()))
            path = templatePath_app + pathName + "m0";
        else if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_MOBILE_MAIN.getName()))
            path = templatePath_app + pathName + "m1";
        return path;
    }

    public String getHisFilePath(String pathName,TemplateUtil.TemplateType templateType) {
        String path = null;
        if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_DESKTOP_LOGIN.getName()))
            path = templatePath_app + "/his/" +pathName + "d0";
        else if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_DESKTOP_MAIN.getName()))
            path = templatePath_app + "/his/" + pathName + "d1";
        else if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_MOBILE_LOGIN.getName()))
            path = templatePath_app + "/his/" + pathName + "m0";
        else if (templateType.getName().equals(TemplateUtil.TemplateType.TEMPLATE_TYPE_MOBILE_MAIN.getName()))
            path = templatePath_app + "/his/" + pathName + "m1";
        return path;
    }

    public String getJarFilePath(){
        return templatePath_app + "templates";
    }


    private String getRealPath(String path) {
        if (path.startsWith("/") || path.startsWith("\\")) {
            path = path.substring(1, path.length());
        }
        if (path.endsWith("/") || path.endsWith("\\")) {
            path = path.substring(0, path.length() - 1);
        }
        if (path.indexOf("/") != -1 || path.indexOf("\\") != -1) {
            if (path.indexOf("/") != -1) {
                if (File.separator.equals("\\"))
                    path = path.replaceAll("/", "\\\\");
                else
                    path = path.replaceAll("/", File.separator);
            }
            if (path.indexOf("\\") != -1) {
                if (File.separator.equals("\\"))
                    path = path.replaceAll("\\\\", "\\\\");
                else
                    path = path.replaceAll("\\\\", File.separator);
            }
        }
        return path;
    }
}
