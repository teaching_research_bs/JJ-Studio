package cn.easyplatform.studio.utils;

import cn.easyplatform.lang.Files;
import cn.easyplatform.lang.Streams;

import java.io.IOException;
import java.util.Properties;

public class AttributeCnUtil {
    private static Properties properties = new Properties();


    public static void init(){
        try {
            properties.load(Streams.utf8r(Files
                    .findFileAsStream("web/support/attribute_cn.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getAttrubuleName(String key){
       return properties.getProperty(key);

    }
}
