package cn.easyplatform.studio.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImageUtil {
    /**
     * 改变图片的大小到宽为size，然后高随着宽等比例变化
     * @param is        上传的图片的输入流
     * @param fileStr   改变了图片的大小后，把图片的流输出到目标文件
     * @param newWidth  新图片的宽
     * @param newHeight 新图片的高
     * @param format    新图片的格式
     * @return
     * @throws IOException
     */
    public static void resizeAndWriteImage(BufferedImage is, String fileStr, int newWidth, int newHeight, String format) throws IOException {
        FileOutputStream os = new FileOutputStream(fileStr);
        BufferedImage image = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = image.createGraphics();
        graphics.drawImage(is, 0, 0, newWidth, newHeight, null);
        ImageIO.write(image, format, os);
        os.flush();
        os.close();
    }
}
