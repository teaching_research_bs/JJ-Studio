package cn.easyplatform.studio.utils;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.LogicBean;
import cn.easyplatform.entities.beans.list.Group;
import cn.easyplatform.entities.beans.list.Header;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.entities.beans.task.TaskBean;
import cn.easyplatform.entities.beans.task.TransitionBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.EntityType;

import java.util.ArrayList;
import java.util.List;

public class GrammarUtil<T extends BaseEntity> {

    boolean beginQuotes = false;
    boolean beginDbQuotes = false;

    public  List<List<String>> getNextLinks(List<T> baseEntityList) {
        List<List<String>> allList = new ArrayList<>();
        for (T baseEntity : baseEntityList) {
            if (baseEntity.getType().equals(EntityType.TASK.getName())) {
                allList.add(getAllLinkWithTask((TaskBean) baseEntity));
            } else if (baseEntity.getType().equals(EntityType.PAGE.getName())) {
                allList.add(getAllLinkWithPage((PageBean) baseEntity));
            } else if (baseEntity.getType().equals(EntityType.LOGIC.getName())) {
                allList.add(getAllLinkWithLogic((LogicBean) baseEntity));
            } else if (baseEntity.getType().equals(EntityType.DATALIST.getName())) {
                allList.add(getAllLinkWithDatalist((ListBean) baseEntity));
            } else {
                allList.add(new ArrayList<String>());
            }
        }
        return allList;
    }

    private List<String> getAllLinkWithTask(TaskBean taskBean) {
        List<String> idList = new ArrayList<>();
        //RefId
        equalAdd(idList, taskBean.getRefId());
        //table
        equalAdd(idList, taskBean.getTable());
        //Decision->getTo,Decision->getLogicId
        if (taskBean.getDecision() != null && taskBean.getDecision().getTransitions().size() > 0) {
            for (TransitionBean transitionBean:
                    taskBean.getDecision().getTransitions()) {
                equalAdd(idList, transitionBean.getTo());
                equalAdd(idList, transitionBean.getLogicId());
            }
        }
        //OnInit
        if (taskBean.getOnInit() != null)
            logicGrammarAdd(idList, taskBean.getOnInit().getContent());
        //OnClose
        if (taskBean.getOnClose() != null)
            logicGrammarAdd(idList, taskBean.getOnClose().getContent());
        //OnPreCommit
        if (taskBean.getOnPreCommit() != null)
            logicGrammarAdd(idList, taskBean.getOnPreCommit().getContent());
        //OnPreCommitRollback
        if (taskBean.getOnPreCommitRollback() != null)
            logicGrammarAdd(idList, taskBean.getOnPreCommitRollback().getContent());
        //OnBeforeCommit
        if (taskBean.getOnBeforeCommit() != null)
            logicGrammarAdd(idList, taskBean.getOnBeforeCommit().getContent());
        //OnAfterCommit
        if (taskBean.getOnAfterCommit() != null)
            logicGrammarAdd(idList, taskBean.getOnAfterCommit().getContent());
        //OnRollback
        if (taskBean.getOnRollback() != null)
            logicGrammarAdd(idList, taskBean.getOnRollback().getContent());
        //onCommitted
        if (taskBean.getOnCommitted() != null)
            logicGrammarAdd(idList, taskBean.getOnCommitted().getContent());
        //onCommittedRollback
        if (taskBean.getOnCommittedRollback() != null)
            logicGrammarAdd(idList, taskBean.getOnCommittedRollback().getContent());
        return idList;
    }

    private List<String> getAllLinkWithPage(PageBean pageBean) {
        List<String> idList = new ArrayList<>();
        //table
        equalAdd(idList, pageBean.getTable());
        //onLoad
        if (pageBean.getOnLoad() != null)
            logicGrammarAdd(idList, pageBean.getOnLoad().getContent());
        //onBack
        if (pageBean.getOnBack() != null)
            logicGrammarAdd(idList, pageBean.getOnBack().getContent());
        //onOk
        if (pageBean.getOnOk() != null)
            logicGrammarAdd(idList, pageBean.getOnOk().getContent());
        //onRefresh
        if (pageBean.getOnRefresh() != null)
            logicGrammarAdd(idList, pageBean.getOnRefresh().getContent());
        //onVisible
        logicGrammarAdd(idList, pageBean.getOnVisible());
        //script
        logicGrammarAdd(idList, pageBean.getScript());
        //ajax
        labelGrammarAdd(idList, pageBean.getAjax());
        //mil
        labelGrammarAdd(idList, pageBean.getMil());
        //onMessage
        logicGrammarAdd(idList, pageBean.getOnMessage());
        return idList;
    }

    private List<String> getAllLinkWithLogic(LogicBean logicBean) {
        List<String> idList = new ArrayList<>();
        //content
        logicGrammarAdd(idList, logicBean.getContent());
        //equalAdd(idList, logicBean.getContent());
        return idList;
    }

    private List<String> getAllLinkWithDatalist(ListBean listBean) {
        List<String> idList = new ArrayList<>();
        //table
        equalAdd(idList, listBean.getTable());
        //pageId
        equalAdd(idList, listBean.getPageId());
        //query
        sqlGrammarAdd(idList, listBean.getQuery());
        //condition
        sqlGrammarAdd(idList, listBean.getCondition());
        //onInit
        if (listBean.getOnInit() != null)
            logicGrammarAdd(idList, listBean.getOnInit().getContent());
        //onBefore
        if (listBean.getOnBefore() != null)
            logicGrammarAdd(idList, listBean.getOnBefore().getContent());
        //onAfter
        if (listBean.getOnAfter() != null)
            logicGrammarAdd(idList, listBean.getOnAfter().getContent());
        //onRow
        if (listBean.getOnRow() != null)
            logicGrammarAdd(idList, listBean.getOnRow().getContent());
        //onHeaderScript
        logicGrammarAdd(idList, listBean.getOnHeaderScript());
        //onRowScript
        logicGrammarAdd(idList, listBean.getOnRowScript());
        //onGroupScript
        logicGrammarAdd(idList, listBean.getOnGroupScript());
        //onFooterScript
        logicGrammarAdd(idList, listBean.getOnFooterScript());
        //header->event,header->sql
        if (listBean.getHeaders() != null && listBean.getHeaders().size() > 0) {
            for (Header header:
                    listBean.getHeaders()) {
                logicGrammarAdd(idList, header.getEvent());
                sqlGrammarAdd(idList, header.getSql());
            }
        }
        //group->orderBy
        if (listBean.getGroups() != null && listBean.getGroups().size() > 0) {
            for (Group group:
                    listBean.getGroups()) {
                sqlGrammarAdd(idList, group.getOrderBy());
            }
        }
        return idList;
    }

    private void equalAdd (List<String> list, String value) {
        if (Strings.isBlank(value) == false) {
            if (list.contains(value) == false)
                list.add(value);
        }
    }

    private void logicGrammarAdd (List<String> list, String value) {
        if (Strings.isBlank(value) == false) {
            List<String> addList = RegularUtils.getLogicGrammar(value);
            for (String idString: addList) {
                if (list.contains(idString) == false)
                    list.addAll(addList);
            }
        }
    }

    private void sqlGrammarAdd (List<String> list, String value) {
        if (Strings.isBlank(value) == false) {
            List<String> addList = RegularUtils.getSQLGrammar(value);
            for (String idString: addList) {
                if (list.contains(idString) == false)
                    list.addAll(addList);
            }
        }
    }

    private void labelGrammarAdd (List<String> list, String value) {
        if (Strings.isBlank(value) == false) {
            List<String> addList = RegularUtils.getLabelGrammar(value);
            for (String idString: addList) {
                if (list.contains(idString) == false)
                    list.addAll(addList);
            }
        }
    }
}
