package cn.easyplatform.studio.utils;

import cn.easyplatform.studio.context.Contexts;

public class ServiceFileUtil {
    public enum MediaType {
        MEDIA_TYPE_IMG("img"),
        MEDIA_TYPE_VIDEO("video"),
        MEDIA_TYPE_AUDIO("audio"),
        MEDIA_TYPE_CSS("css"),
        MEDIA_TYPE_JS("js");

        String name;

        private MediaType(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return this.name;
        }
    }

    protected String projectName = Contexts.getProject().getId();

}
