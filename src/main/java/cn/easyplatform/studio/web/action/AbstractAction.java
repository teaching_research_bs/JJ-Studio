/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.action;

import cn.easyplatform.studio.web.layout.WorkbenchController;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractAction implements Action {

	protected WorkbenchController workbench;

	/**
	 * @param workbench
	 */
	public AbstractAction(WorkbenchController workbench) {
		this.workbench = workbench;
	}

}
