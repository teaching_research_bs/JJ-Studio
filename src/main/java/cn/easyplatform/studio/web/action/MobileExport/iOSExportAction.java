package cn.easyplatform.studio.web.action.MobileExport;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.utils.ImageUtil;
import cn.easyplatform.studio.utils.ZipUtil;
import cn.easyplatform.studio.vos.MobileConfVo;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class iOSExportAction extends MobileAction implements EventListener<Event> {
    /**
     * @param workbench
     */
    public iOSExportAction(WorkbenchController workbench) {
        super(workbench);
    }

    @Override
    public void on() {
        Component c = Executions.createComponents(
                "~./include/mobileExport/iOSExport.zul", null, null);
        window = (Window)c.query("window");
        bundleTextbox = (Textbox)c.getFellow("bundleid");
        bundleTextbox.addEventListener(Events.ON_CHANGE,this);
        displayNameTextbox = (Textbox)c.getFellow("displayname");
        displayNameTextbox.addEventListener(Events.ON_CHANGE,this);
        versionTextbox = (Textbox)c.getFellow("versionnum");
        versionTextbox.addEventListener(Events.ON_CHANGE,this);
        urlTextbox = (Textbox)c.getFellow("urlTextbox");
        urlTextbox.addEventListener(Events.ON_CHANGE,this);

        iconDiv = (Div)c.getFellow("iconDiv");
        iconImg = (Image)c.getFellow("iconShow");
        iconBtn = (Button)c.getFellow("iconBtn");
        iconBtn.addEventListener(Events.ON_UPLOAD,this);

        smallPortraitDiv = (Div)c.getFellow("smallPortraitDiv");
        launchSmallPortraitImg = (Image)c.getFellow("launchSmallPortraitShow");
        launchSmallPortraitBtn = (Button)c.getFellow("launchSmallPortraitBtn");
        launchSmallPortraitBtn.addEventListener(Events.ON_UPLOAD,this);

        smallLandscapeDiv = (Div)c.getFellow("smallLandscapeDiv");
        launchSmallLandscapeImg = (Image)c.getFellow("launchSmallLandscapeShow");
        launchSmallLandscapeBtn = (Button)c.getFellow("launchSmallLandscapeBtn");
        launchSmallLandscapeBtn.addEventListener(Events.ON_UPLOAD,this);

        bigPortraitDiv = (Div)c.getFellow("bigPortraitDiv");
        launchBigPortraitImg = (Image)c.getFellow("launchBigPortraitShow");
        launchBigPortraitBtn = (Button)c.getFellow("launchBigPortraitBtn");
        launchBigPortraitBtn.addEventListener(Events.ON_UPLOAD,this);

        bigLandscapeDiv = (Div)c.getFellow("bigLandscapeDiv");
        launchBigLandscapeImg = (Image)c.getFellow("launchBigLandscapeShow");
        launchBigLandscapeBtn = (Button)c.getFellow("launchBigLandscapeBtn");
        launchBigLandscapeBtn.addEventListener(Events.ON_UPLOAD,this);

        finishBtn = (Button)c.getFellow("finishBtn");
        finishBtn.addEventListener(Events.ON_CLICK,this);
        setDefaultConf(MobileConfVo.iOSCONF);

        helpBtn = (Button)c.getFellow("helpBtn");
        helpBtn.addEventListener(Events.ON_CLICK,this);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Component c = event.getTarget();
        if (c == launchSmallLandscapeBtn || c == launchBigLandscapeBtn || c == iconBtn || c == launchSmallPortraitBtn || c == launchBigPortraitBtn)
        {
            isChangeContent = true;
            verifyImage(event);
        }
        else if (c == displayNameTextbox || c == bundleTextbox || c == versionTextbox || c == urlTextbox)
        {
            isChangeContent = true;
        }
        else if (c == finishBtn)
        {
            if (uploaToDatabase(MobileConfVo.iOSCONF))
                exportProduct();
        }
        else if (c == helpBtn)
        {
            //下载
            try {
                String path = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/iOS_help.docx");
                Filedownload.save(new File(path),null);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void exportProduct()
    {
        String path = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/InfoModel.plist");
        String result = null;
        try {
            result = FileUtil.getStreamWithFile(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //包名
        int bundleStartIndex = result.indexOf("CFBundleIdentifier-");
        int bundleEndIndex = result.indexOf("-CFBundleIdentifier");
        String oldBundleIdStr = result.substring(bundleStartIndex,bundleEndIndex+"-CFBundleIdentifier".length());
        result = result.replace(oldBundleIdStr,vo.getBundleId());
        //版本号
        int versionStartIndex = result.indexOf("CFBundleShortVersionString-");
        int versionEndIndex = result.indexOf("-CFBundleShortVersionString");
        String oldVersionStr = result.substring(versionStartIndex,versionEndIndex+"-CFBundleShortVersionString".length());
        result = result.replace(oldVersionStr,vo.getMobileVersion());
        //应用名
        int nameStartIndex = result.indexOf("CFBundleDisplayName-");
        int nameEndIndex = result.indexOf("-CFBundleDisplayName");
        String oldNameStr = result.substring(nameStartIndex,nameEndIndex+"-CFBundleDisplayName".length());
        result = result.replace(oldNameStr,vo.getDisplayName());
        //修改plist
        String infoPath = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/epCloud/epCloud/Info.plist");
        FileUtil.setStreamWithFile(result,infoPath);
        //修改图片
        String mainDir = "/WEB-INF/mobileProduct/epCloud/epCloud/Assets.xcassets";
        try {
            ImageUtil.resizeAndWriteImage(iconImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/40.png"),
                    40, 40, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/58.png"),
                    58, 58, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage, StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/60.png"),
                    60, 60, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage, StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/80.png"),
                    80, 80, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/87.png"),
                    87, 87, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage, StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/120.png"),
                    120, 120, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage, StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/180.png"),
                    180, 180, iconImageFormat);
            ImageUtil.resizeAndWriteImage(iconImage, StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/1024.png"),
                    1024, 1024, iconImageFormat);
            ImageUtil.resizeAndWriteImage(smallPortraitImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/12422208.png"),
                    1242, 2208, smallPortraitImageFormat);
            ImageUtil.resizeAndWriteImage(smallPortraitImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/7501334.png"),
                    750, 1334, smallPortraitImageFormat);
            ImageUtil.resizeAndWriteImage(smallLandscapeImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/22081242.png"),
                    2208, 1242, smallLandscapeImageFormat);
            ImageUtil.resizeAndWriteImage(bigPortraitImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/12422688.png"),
                    1242, 2688, bigPortraitImageFormat);
            ImageUtil.resizeAndWriteImage(bigPortraitImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/8281792.png"),
                    828, 1792, bigPortraitImageFormat);
            ImageUtil.resizeAndWriteImage(bigPortraitImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/11252436.png"),
                    1125, 2436, bigPortraitImageFormat);
            ImageUtil.resizeAndWriteImage(bigLandscapeImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/26881242.png"),
                    2688, 1242, bigLandscapeImageFormat);
            ImageUtil.resizeAndWriteImage(bigLandscapeImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/1792828.png"),
                    1792, 828, bigLandscapeImageFormat);
            ImageUtil.resizeAndWriteImage(bigLandscapeImage,
                    StudioApp.getServletContext().getRealPath(mainDir + "/LaunchImage.launchimage/24361125.png"),
                    2436, 1125, bigLandscapeImageFormat);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //拷贝图片
        try {
            FileUtil.copyFileUsingFileChannels(
                    new File(StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/120.png")),
                    new File(StudioApp.getServletContext().getRealPath(mainDir + "/AppIcon.appiconset/120-1.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String jsonPath = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/epCloud/epCloud/AppSetting.json");
        String jsonResult = null;
        try {
            jsonResult = FileUtil.getStreamWithFile(jsonPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        JSONObject jsonObject = JSON.parseObject(jsonResult);
        jsonObject.put("baseURL",vo.getUrl());
        jsonResult = jsonObject.toJSONString();
        FileUtil.setStreamWithFile(jsonResult,jsonPath);
        //zip
        String productPath = StudioApp.getServletContext().getRealPath("/WEB-INF/mobileProduct/epCloud");
        String zipPath = StudioApp.getServletContext().getRealPath(FileUtil.getCachePath() + "epCloud.zip");
        try {
            ZipUtil.compress(productPath,zipPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //下载
        try {
            Filedownload.save(new File(zipPath), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        window.onClose();
    }
}
