package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.studio.vos.PageTemplateVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.wizards.PageWizard;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Cardlayout;
import org.zkoss.zkmax.zul.Scrollview;
import org.zkoss.zul.*;

import java.util.*;

public class PageTemplateView extends AbstractView<PageBean> {
    public enum PageTemplateType {
        TEMPLATE_TYPE_PAGE_PC,
        TEMPLATE_TYPE_PAGE_MOBILE
    }
    private PageTemplateType pageTemplateType;
    private Cardlayout cardlayout;
    private List<PageTemplateVo> pageTemplateVoList;
    private Scrollview secondScrollview;
    private Vlayout currentLayout;
    private Button nextBtn;

    private PageBean pageBean;
    protected PageTemplateView(Component component, EditorCallback<PageBean> cb, PageTemplateType templateType, PageBean pageBean) {
        super(component, cb);
        pageTemplateType = templateType;
        this.pageBean = pageBean;
        pageTemplateVoList = new ArrayList<>();
        List<String> ids = null;
        List<String> imagePath = null;
        List<String> labelString = null;
        List<Boolean> isSelected = null;
        if (pageTemplateType == PageTemplateType.TEMPLATE_TYPE_PAGE_PC) {
            ids = Arrays.asList(new String[]{"template_vlayout_empty", "template_vlayout_page0",
                    "template_vlayout_page1", "template_vlayout_page2", "template_vlayout_page3",
                    "template_vlayout_page4", "template_vlayout_page5"});
            imagePath = Arrays.asList(new String[]{"~./images/pc_template_empty.png", "~./images/pc_template_one.png",
                    "~./images/pc_template_two.png", "~./images/pc_template_three.png", "~./images/pc_template_four.png",
                    "~./images/pc_template_five.png", "~./images/pc_template_six.png"});
            labelString = Arrays.asList(new String[]{"create.template.empty", "create.template.one",
                    "create.template.two", "create.template.three", "create.template.four", "create.template.five",
                    "create.template.six"});
            isSelected = Arrays.asList(new Boolean[]{true, false, false, false, false, false, false});
        } else if (pageTemplateType == PageTemplateType.TEMPLATE_TYPE_PAGE_MOBILE) {
            ids = Arrays.asList(new String[]{"template_vlayout_empty", "template_vlayout_mobile0",
                    "template_vlayout_mobile1", "template_vlayout_mobile2", "template_vlayout_mobile3"});
            imagePath = Arrays.asList(new String[]{"~./images/app_template_empty.png", "~./images/app_template_one.png",
                    "~./images/app_template_two.png", "~./images/app_template_three.png", "~./images/app_template_four.png"});
            labelString = Arrays.asList(new String[]{"create.template.empty", "create.template.one",
                    "create.template.senven", "create.template.two", "create.template.eight"});
            isSelected = Arrays.asList(new Boolean[]{true, false, false, false, false});
        }

        if (imagePath!= null && imagePath.size()> 0) {
            for (int index = 0; index < imagePath.size(); index++) {
                PageTemplateVo templateVo = new PageTemplateVo();
                templateVo.setIdString(ids.get(index));
                templateVo.setImagePath(imagePath.get(index));
                templateVo.setLabelString(labelString.get(index));
                templateVo.setSelected(isSelected.get(index));
                pageTemplateVoList.add(templateVo);
            }
        }
    }

    @Override
    protected void createContent(Component parent) {
        createPageTemplate(parent);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget().equals(nextBtn)) {
            if (currentLayout != null) {
                if (currentLayout.getId().contains("empty")) {
                    if (cb != null) {
                        PageBean bean = new PageBean();
                        if (pageTemplateType == PageTemplateType.TEMPLATE_TYPE_PAGE_PC)
                            bean.setAjax("");
                        else
                            bean.setMil("");
                        cb.setValue(bean);
                    }
                    cardlayout.getRoot().detach();
                    onCloseClick(win);
                } else {
                    Map<String, String> map = new HashMap<>();
                    map.put("page", currentLayout.getId());
                    secondScrollview.getChildren().clear();
                    Executions.createComponents("~./include/wizards/page.zul", secondScrollview, map);
                    cardlayout.next();
                }
            }
        } else if (event.getTarget().getId().equals("template_button_finish")) {
            String comp = "borderlayout";
            PageWizard w = (PageWizard) secondScrollview.query(comp).getAttribute("");
            PageBean bean = new PageBean();
            PageBean settingBean = w.getEntity();
            if (pageTemplateType == PageTemplateType.TEMPLATE_TYPE_PAGE_PC) {
                bean.setAjax(settingBean.getAjax());
            } else if (pageTemplateType == PageTemplateType.TEMPLATE_TYPE_PAGE_MOBILE) {
                bean.setMil(settingBean.getAjax());
            }
            if (cb != null) {
                bean.setTable(settingBean.getTable());
                cb.setValue(bean);
            }
            cardlayout.getRoot().detach();
            onCloseClick(win);
        } else if (event.getTarget().getId().equals("template_button_close")) {
            cardlayout.getRoot().detach();
            onCloseClick(win);
        } else if (event.getTarget().getId().equals("template_button_prev")) {
            cardlayout.previous();
        }
    }

    //ui
    private void createPageTemplate(Component parent) {
        cardlayout = new Cardlayout();
        cardlayout.setHflex("1");
        cardlayout.setVflex("1");
        parent.appendChild(cardlayout);
        //第一个页面
        Vlayout layout = new Vlayout();
        layout.setHflex("1");
        layout.setVflex("1");
        layout.setSpacing("0");
        layout.setStyle("background: #FAFAFA;");

        Scrollview centerScrollview= new Scrollview();
        centerScrollview.setHflex("1");
        centerScrollview.setVflex("1");
        layout.appendChild(centerScrollview);
        if (pageTemplateVoList != null || pageTemplateVoList.size() > 0) {
            //每一行最外层控件
            Div contentDiv = new Div();
            contentDiv.setHflex("1");
            contentDiv.setStyle("display: flex;flex-direction: row;flex-wrap: wrap;min-width:164px;justify-content: center;");
            centerScrollview.appendChild(contentDiv);
            for (int index = 0; index < pageTemplateVoList.size(); index++) {
                new TemplateCell(pageTemplateVoList.get(index), contentDiv, pageTemplateType);
                //默认选中空内容
                if (index == 0) {
                    currentLayout = (Vlayout) contentDiv.getChildren().get(0);
                    currentLayout.setSclass("ui-div-selected");
                }
            }
        }

        Div buttonDiv = new Div();
        buttonDiv.setHflex("1");
        buttonDiv.setHeight("50px");
        buttonDiv.setStyle("background: white;");
        layout.appendChild(buttonDiv);

        Button closeBtn = new Button(Labels.getLabel("button.close"));
        closeBtn.setStyle("left: 20px;position: absolute;margin-top: 13px;");
        closeBtn.setId("template_button_close");
        closeBtn.addEventListener(Events.ON_CLICK, this);
        buttonDiv.appendChild(closeBtn);

        nextBtn = new Button(Labels.getLabel("button.ok"));
        nextBtn.setStyle("right: 20px;position: absolute;margin-top: 13px;");
        nextBtn.addEventListener(Events.ON_CLICK, this);
        buttonDiv.appendChild(nextBtn);

        layout.setParent(cardlayout);

        //第二个页面
        Vlayout secondLayout = new Vlayout();
        secondLayout.setHflex("1");
        secondLayout.setVflex("1");
        secondLayout.setSpacing("0");
        secondLayout.setStyle("background: #E8EEEB;");

        secondScrollview= new Scrollview();
        secondScrollview.setHflex("1");
        secondScrollview.setVflex("1");
        secondLayout.appendChild(secondScrollview);

        Div secondButtonDiv = new Div();
        secondButtonDiv.setHflex("1");
        secondButtonDiv.setHeight("50px");
        secondButtonDiv.setStyle("background: white;");
        secondLayout.appendChild(secondButtonDiv);

        Button prevBtn = new Button(Labels.getLabel("button.prev"));
        prevBtn.setStyle("left: 20px;position: absolute;margin-top: 13px;");
        prevBtn.setId("template_button_prev");
        prevBtn.addEventListener(Events.ON_CLICK, this);
        secondButtonDiv.appendChild(prevBtn);

        Button finishBtn  = new Button(Labels.getLabel("button.ok"));
        finishBtn.setStyle("right: 20px;position: absolute;margin-top: 13px;");
        finishBtn.setId("template_button_finish");
        finishBtn.addEventListener(Events.ON_CLICK, this);
        secondButtonDiv.appendChild(finishBtn);

        secondLayout.setParent(cardlayout);

        //设置window的ID属性，用来产生DataList的ID
        parent.getRoot().setAttribute("id", pageBean.getId());
        parent.getRoot().setAttribute("name", pageBean.getName());
        ((Window)parent.getRoot()).setWidth("966px");
    }

    private class TemplateCell implements EventListener<Event> {

        public TemplateCell(PageTemplateVo templateVo, Component parentLayout, PageTemplateView.PageTemplateType templateType) {
            if (templateVo == null) {
                Div div = new Div();
                div.setHflex("1");
                parentLayout.appendChild(div);
            } else  {
                Vlayout contentLayout = new Vlayout();
                contentLayout.setSpacing("0");
                contentLayout.setHeight("200px");
                contentLayout.setWidth("164px");
                contentLayout.setStyle("text-align: center;#195B40 solid 2px;");
                contentLayout.setSclass("ui-div-normal");
                contentLayout.addEventListener(Events.ON_CLICK, this);
                contentLayout.setId(templateVo.getIdString());
                parentLayout.appendChild(contentLayout);

                Div oneDiv = new Div();
                oneDiv.setHeight("15px");
                contentLayout.appendChild(oneDiv);

                Image contentImage = new Image();
                contentImage.setSrc(templateVo.getImagePath());
                if (pageTemplateType == PageTemplateType.TEMPLATE_TYPE_PAGE_PC) {
                    contentImage.setHeight("137px");
                    contentImage.setWidth("137px");
                } else {
                    contentImage.setHeight("150px");
                    contentImage.setWidth("110px");
                }
                contentLayout.appendChild(contentImage);

                Div secondDiv = new Div();
                secondDiv.setHeight("8px");
                contentLayout.appendChild(secondDiv);

                Label contentLabel = new Label();
                contentLabel.setValue(Labels.getLabel(templateVo.getLabelString()));
                contentLabel.setStyle("font-size:12px;border-radius: 14px;padding: 2px 8px;");
                contentLayout.appendChild(contentLabel);

                Div lastDiv = new Div();
                lastDiv.setHeight("8px");
                contentLayout.appendChild(lastDiv);
            }
        }
        @Override
        public void onEvent(Event event) throws Exception {
            if (event.getTarget() instanceof Vlayout) {
                if (currentLayout != event.getTarget()) {
                    ((Vlayout) event.getTarget()).setSclass("ui-div-selected");
                    currentLayout.setSclass("ui-div-normal");
                    currentLayout = (Vlayout) event.getTarget();
                    if (currentLayout.getId().contains("empty")) {
                        nextBtn.setLabel(Labels.getLabel("button.ok"));
                    } else
                        nextBtn.setLabel(Labels.getLabel("button.next"));
                }
            }
        }
    }
}
