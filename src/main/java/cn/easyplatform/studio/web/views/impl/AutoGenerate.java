package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.entities.beans.task.TaskBean;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.*;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.web.bos.TableFieldExt;
import cn.easyplatform.studio.web.editors.entity.TableEntityEditor;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.web.ext.zul.BandboxExt;
import cn.easyplatform.web.ext.zul.SelectboxExt;
import org.apache.commons.lang3.StringUtils;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.*;
import org.zkoss.zul.event.ListDataListener;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.PagingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AutoGenerate {

    /**
     * 主表名称/功能id/功能名称
     */
    private String table;
    private String funcCode;
    private String funcName;

    /**
     * 页面id 列表id
     */
    private String pageCode;
    private String pageName;
    private String listCode;
    private String listName;
    //是否关联次表，次列表id，次列表名称
    private boolean genList2 = false;
    private String listCode2;
    private String listName2;
    //左列表全选，表单全选，次列表全选
    private Boolean leftAll = false;
    private Boolean northAll = false;
    private Boolean eastAll = false;
    private Boolean editAll = false;
    //次表名称
    private String relateTable = "";
    //表单按钮组类型，表单列数
    private String buttonType = "";
    int colNum = 2;
    //主表字段，次表字段
    List<TableFieldExt> tfList = null;
    List<TableFieldExt> subList = null;
    //所有次表列表，所有次表查询结果
    private ListModelList<EntityVo> relTableList = null;
    private List<EntityVo> allTableList = null;
    //主表字段/次表字段
    private ListModel mainTabCol1;
    private ListModel subTabCol1;
    private ListModel mainTabCol2;
    private ListModel subTabCol2;
    //上级页面
    private TableEntityEditor tes = null;
    private Component view = null;
    private Group relGroup = null;
    private East relEast = null;
    private BandboxExt relBandBox = null;
    private Grid relGrid = null;


    private String buttonTempCode = "<row spans=\"##colNum##\" ##align##>\n" +
            "\t<div>\n" +
            "\t\t<button label=\"新增\" event=\"create()\" iconSclass=\"z-icon-plus\"/>\n" +
            "\t\t<button label=\"删除\" iconSclass=\"z-icon-minus\">\n" +
            "\t\t\t<attribute name=\"event\">remove();pageList.reload();showInfo('删除成功！')\n" +
            "\t\t\t</attribute>\n" +
            "\t\t</button>\n" +
            "\t\t<button label=\"保存\" iconSclass=\"z-icon-save\">\n" +
            "\t\t\t<attribute name=\"event\">save();pageList.reload();showInfo('保存成功！')\n" +
            "\t\t\t</attribute>\n" +
            "\t\t</button>\n" +
            "\t</div>\n" +
            "</row>";

    public AutoGenerate() {
    }

    @AfterCompose
    public void afterCompose(@ContextParam(ContextType.VIEW) Component view) {
        this.view = view;
        Selectors.wireComponents(view, this, false);

        relGroup = (Group)this.view.getFellow("rel-group");
        relEast = (East)this.view.getFellow("rel-east");
        relBandBox = (BandboxExt)this.view.getFellow("rel-bandbox");
        relGrid = (Grid)this.view.getFellow("group-grid");
        this.isGenList(null, "false");
    }

    @Init
    public void init(@BindingParam("arg2") TableEntityEditor tee) {
        this.table = tee.getEntity().getId();
//        tee.getEntity().getId();
//        tee.getWorkbench().openEntity("");
        TableBean tableBean = (TableBean) StudioApp.execute(new GetEntityCmd(table));
        tfList = new ArrayList<>();
        tableBean.getFields().forEach(i -> {
            tfList.add(new TableFieldExt(i.getName(), i.getDescription(), i.getType().name(), i.getOptionType(), this.leftAll, this.northAll));
        });
        ProjectBean pb = Contexts.getProject();
        allTableList = StudioApp.execute(new QueryTableCmd(pb.getBizDb()));
        this.relTableList = new ListModelList<EntityVo>(allTableList);
        relTableList.setPageSize(5);
        /**
         * 初始化主表字段id
         */
        List tmp = tfList.stream().map(TableFieldExt::getName).collect(Collectors.toList());
        this.mainTabCol1 = new ListModelList(tmp);
        this.mainTabCol2 = new ListModelList(tmp);

//        relTableList.addPagingEventListener(pl);
        List<String> keys = tableBean.getKey();
        if(keys != null && keys.size()>0) {
            keys.forEach(i -> {
                for(TableFieldExt tfe:tfList) {
                    if(i.equalsIgnoreCase(tfe.getName())) {
                        tfe.setKey(true);
                        break;
                    }
                }
            });
        }
        this.tes = tee;
    }

    private void initOtherCode() {
        pageCode = funcCode + "PY01";
        pageName = funcName + "页面";
        listCode = funcCode + "QY01";
        listName = funcName + "主列表";
        listCode2 = funcCode + "QY02";
        listName2 = funcName + "主子列表";
    }

    private boolean checkCode() {
        /**
         * 查询
         */
        Long num = 0L;
        HashMap<String, Object> dbData = null;
        if(genList2) {
            dbData = (HashMap<String, Object>) StudioApp.execute(
                    new GetCommonOneCmd("select count(*) as num from ep_demo where entityId in(?, ?, ?)"
                            , new String[]{funcCode, pageCode, listCode}));
            num = (Long)dbData.get("num");
        } else {
            dbData = (HashMap<String, Object>) StudioApp.execute(
                    new GetCommonOneCmd("select count(*) as num from ep_demo where entityId in(?, ?, ?, ?)"
                            , new String[]{funcCode, pageCode, listCode, listCode2}));
            num = (Long)dbData.get("num");
        }
        return num == 0L;
    }
    private String list_header_temp = null;
    private String list_temp = null;

    private String getListHeaderTemp() {
        if(list_header_temp == null) {
            HashMap<String, Object> dbData = (HashMap<String, Object>) StudioApp.execute(new GetCommonOneCmd("select paramvalue from ep_config_info where paramcode = ? and paramgroupid = ?", new String[]{"list-header", "auto-template"}));
            list_header_temp = (String)dbData.get("paramvalue");
        }
        return list_header_temp;
    }
    private String getListTemp() {
        if(list_temp == null) {
            HashMap<String, Object> dbData = (HashMap<String, Object>) StudioApp.execute(new GetCommonOneCmd("select paramvalue from ep_config_info where paramcode = ? and paramgroupid = ?", new String[]{"list", "auto-template"}));
            list_temp = (String)dbData.get("paramvalue");
        }
        return list_temp;
    }

    private void generateLeftList(String table, List<TableFieldExt> tfList, String listCode, String listName, String condition) {
        String header = getListHeaderTemp();
        StringBuffer headerBuffer = new StringBuffer();
        tfList.forEach(i -> {
            if(i.isSelected()) {
                headerBuffer.append(header.replace("##name##", i.getName())
                        .replace("##cnName##", i.getDesc())
                        .replace("##type##", i.getType().toString().toLowerCase()));
            }
        });
        String list = getListTemp();
        list = list.replace("##tableName##", table).replace("##headers##", headerBuffer.toString())
                .replace("##condition##", condition);
        EntityInfo saveEntity = new EntityInfo();
        saveEntity.setId(listCode);
        saveEntity.setName(listName);
        saveEntity.setType("Datalist");
        saveEntity.setContent(list);
        saveEntity.setStatus('C');
        saveEntity.setDescription("");
        saveEntity.setSubType("");
        StudioApp.execute(new SaveCmd(saveEntity));
    }
    @Command
    @NotifyChange({"subTabCol1","subTabCol2", "relateTable", "subList"})
    public void initSubCol(BindContext ctx, @BindingParam("selecteVal")String selecteVal) {
//        InputEvent event = (InputEvent)ctx.getTriggerEvent();
        if(!StringUtils.isEmpty(selecteVal)) {
            this.relateTable = selecteVal;
            TableBean tableBean = (TableBean) StudioApp.execute(new GetEntityCmd(selecteVal));
            if(tableBean!= null) {
                List tmp = tableBean.getFields().stream().map(TableField::getName).collect(Collectors.toList());
                this.subTabCol1 = new ListModelList(tmp);
                this.subTabCol2 = new ListModelList(tmp);
                this.subList = new ArrayList<>();
                tableBean.getFields().forEach(i -> {
                    subList.add(new TableFieldExt(i.getName(), i.getDescription(), i.getType().name(), i.getOptionType(), this.leftAll, this.northAll));
                });
            }
        }
        this.relBandBox.close();
    }
    @Command
    @NotifyChange({"relTableList"})
    public void isGenList(BindContext ctx, @BindingParam("val") String value) {
        this.genList2 = Boolean.valueOf(value);
        if(this.genList2) {
            this.relBandBox.setDisabled(false);
//            this.relGroup.setVisible(true);
            this.relGrid.setVisible(true);
            this.relGroup.setOpen(true);
            this.relEast.setVisible(true);
        } else {
            this.relBandBox.setDisabled(true);
            this.relGroup.setOpen(false);
            this.relGrid.setVisible(false);
//            this.relGroup.setVisible(false);
            this.relEast.setVisible(false);
        }
    }


    @Command
    @NotifyChange({"relTableList"})
    public void listChange(BindContext ctx) {
        Event event = ctx.getTriggerEvent();

        if(event instanceof InputEvent) {
            InputEvent ie = (InputEvent) event;
            String relTableName = ie.getValue();

            relTableList.clear();
            relTableList.addAll(
                allTableList.stream().filter(i->{
                    String tmp = i.getId();
                    if(tmp != null && tmp.contains(relTableName)) {
                        return true;
                    }
                    tmp = i.getName();
                    if(tmp != null && tmp.contains(relTableName)) {
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList()));
        } else if(event instanceof PagingEvent) {

        }
    }
    @Command
    public void clickItems() {
        leftAll = !leftAll;
        tfList.forEach(i -> {
            i.setSelectedWithoutAll(leftAll);
        });
    }
    @Command
    @NotifyChange({"tfList"})
    public void clickRightItem() {
        northAll = !northAll;
        tfList.forEach(i -> {
            i.setRightSelectedWithoutAll(northAll);
        });
    }

//    @Command
//    public void test(BindContext ctx) {
//        Component c = this.view.getFellow("rel-groupfoot");
//        Rows parent = (Rows)this.view.getFellow("form-rows");
//        Row tempRow = (Row)this.view.getFellow("temp-row").clone();
//        tempRow.setId("");
//        parent.insertBefore(initRelRow(), c);
//        System.out.println("");
////        c.insertBefore(c, c);
//    }
//    private Component initRelRow() {
//        Row r = new Row();
////        selectbox
//        Selectbox sb = new SelectboxExt();
//        sb.setModel(this.mainTabCol);
//        Selectbox sb1 = new SelectboxExt();
//        Map anno = new HashMap();
//        anno.put("value", "vm.subTabCol");
//        sb1.addAnnotation("model", "bind", anno);
////        sb1.setModel(this.subTabCol);
//
//        r.appendChild(sb);
//        r.appendChild(new Label());
//        r.appendChild(sb1);
//        return r;
//    }
    @Command
    public void generateFunc() throws Exception {
        initOtherCode();
        if(StringUtils.isEmpty(this.funcCode) || StringUtils.isEmpty(this.funcName)) {
            Messagebox.show("功能id/功能名称不能为空！", "Warning", Messagebox.OK, Messagebox.INFORMATION);
            return;
        }
        if(!checkCode()) {
            Messagebox.show("功能id存在重复情况，请修改！", "Warning", Messagebox.OK, Messagebox.INFORMATION);
            return;
        }
        generateLeftList(this.table, this.tfList, this.listCode, this.listName, "");
        generatePage();
        HashMap<String, Object> tableBean1 = (HashMap<String, Object>) StudioApp.execute(new GetCommonOneCmd("select paramvalue from ep_config_info where paramcode = ? and paramgroupid = ?", new String[]{"func", "auto-template"}));
        String funcCodeTemp = (String)tableBean1.get("paramvalue");
        funcCodeTemp = funcCodeTemp.replace("##page##", this.pageCode);
        EntityInfo saveEntity = new EntityInfo();
        saveEntity.setId(funcCode);
        saveEntity.setName(funcName);
        saveEntity.setType("Task");
        saveEntity.setContent(funcCodeTemp);
        saveEntity.setStatus('C');
        saveEntity.setDescription("");
        saveEntity.setSubType("Main");
        StudioApp.execute(new SaveCmd(saveEntity));
        this.tes.getWorkbench().openEntity(funcCode);
        this.view.detach();
    }

    private void generatePage() {
        /**
         * 获取list模板代码
         * 拼接代码
         */
        HashMap<String, Object> dbData = null;
        String borderCode = null;
        String relCos = null;
        if(this.genList2) {
            relCos = initRelCols();
            dbData = (HashMap<String, Object>) StudioApp.execute(new GetCommonOneCmd("select paramvalue from ep_config_info where paramcode = ? and paramgroupid = ?", new String[]{"page-sub-border", "auto-template"}));
            borderCode = (String)dbData.get("paramvalue");
            this.generateLeftList(this.relateTable, this.subList, this.listCode2, this.listName2, initSubCondition());
        } else {
            dbData = (HashMap<String, Object>) StudioApp.execute(new GetCommonOneCmd("select paramvalue from ep_config_info where paramcode = ? and paramgroupid = ?", new String[]{"page-border", "auto-template"}));
            borderCode = (String)dbData.get("paramvalue");
        }
        dbData = (HashMap<String, Object>) StudioApp.execute(new GetCommonOneCmd("select paramvalue from ep_config_info where paramcode = ? and paramgroupid = ?", new String[]{"page", "auto-template"}));
        String pageCodeTemp = (String)dbData.get("paramvalue");
        StringBuffer columnBuf = new StringBuffer();
        IntStream.rangeClosed(1, colNum).forEach(i -> {
            columnBuf.append("<column/>");
        });
        //<label value="~~分类代码~~"/><textbox id="~~#{diccode}"~~ hflex="1"/>
        StringBuffer rowBuf = new StringBuffer();
        AtomicInteger ai = new AtomicInteger(0);
        String buttonCode = "";
        if(this.buttonType.startsWith("top")) {
            buttonCode = getTopButtonCode(this.buttonType);
        }
        StringBuffer keyBuf = new StringBuffer();
        rowBuf.append(buttonCode);
        tfList.forEach(i -> {
            if(i.isRightSelected()) {
                boolean res = ai.getAndAdd(2) % colNum == 0;
                if(res) {
                    rowBuf.append("<row>");
                }
                rowBuf.append(getFieldCode(i));
                if(ai.get() % colNum == 0) {
                    rowBuf.append("</row>");
                }
                if(i.isKey()) {
                    keyBuf.append("####.readonly=$814!='C';".replace("####", i.getName()));
                }
            }
        });
        if(ai.get() % colNum != 0) {
            rowBuf.append("</row>");
        }
        if(this.buttonType.startsWith("bottom")) {
            rowBuf.append(getBottomButtonCode(this.buttonType));
        }
        borderCode = borderCode.replace("##columns##", columnBuf.toString())
                .replace("##rows##", rowBuf.toString())
                .replace("##list-west##", this.listCode)
                .replace("##list-south##", this.listCode2);
        if(this.genList2) {
            borderCode = borderCode.replace("##sub-list##", this.listCode2)
                    .replace("##rel-cols##", relCos).replace("##editableColumns##", initSubEditCol());
        }

        pageCodeTemp = pageCodeTemp.replace("##table##", this.table)
                .replace("##onVisible##", keyBuf.toString())
                .replace("##border##", borderCode);
        EntityInfo saveEntity = new EntityInfo();
        saveEntity.setId(pageCode);
        saveEntity.setName(pageName);
        saveEntity.setType("Page");
        saveEntity.setContent(pageCodeTemp);
        saveEntity.setStatus('C');
        saveEntity.setDescription("");
        saveEntity.setSubType("");
        StudioApp.execute(new SaveCmd(saveEntity));
    }

    private String initSubEditCol() {



        Selectbox subTabCol = (Selectbox)this.view.getFellow("subTabCol1");
        Selectbox subTabCol2 = (Selectbox)this.view.getFellow("subTabCol2");
        int subSel = subTabCol.getSelectedIndex();
        int subSel2 = subTabCol2.getSelectedIndex();
        final String subCol = subSel >= 0 ? this.subList.get(subSel).getName():null;
        final String subCol2 = subSel2 >= 0 ? this.subList.get(subSel2).getName():null;
        List result = this.subList.stream().filter(i -> {
            if(i.isSelected() && i.isRightSelected()) {
                return true;
            }
            return false;
        }).map(TableFieldExt::getName).collect(Collectors.toList());
        return String.join(",", result);
    }

    private String initSubCondition() {
        Selectbox mainTabCol = (Selectbox)this.view.getFellow("mainTabCol1");
        Selectbox subTabCol = (Selectbox)this.view.getFellow("subTabCol1");
        int mainSel = mainTabCol.getSelectedIndex();
        int subSel = subTabCol.getSelectedIndex();
        StringBuffer sb = new StringBuffer();
        boolean sub1 = false;
        if(mainSel >= 0 && subSel >= 0) {
            sub1 = true;
            sb.append(this.subList.get(subSel).getName()).append("=$").append(this.tfList.get(mainSel).getName());
        }
        mainTabCol = (Selectbox)this.view.getFellow("mainTabCol2");
        subTabCol = (Selectbox)this.view.getFellow("subTabCol2");
        mainSel = mainTabCol.getSelectedIndex();
        subSel = subTabCol.getSelectedIndex();
        if(mainSel >= 0 && subSel >= 0) {
            if(sub1) {
                sb.append(" and ");
            }
            sb.append(this.tfList.get(subSel).getName()).append("=$").append(this.subList.get(mainSel).getName());
        }
        return sb.toString();
    }

    public String initRelCols() {
        //$diccode=@diccode
        Selectbox mainTabCol = (Selectbox)this.view.getFellow("mainTabCol1");
        Selectbox subTabCol = (Selectbox)this.view.getFellow("subTabCol1");
        int mainSel = mainTabCol.getSelectedIndex();
        int subSel = subTabCol.getSelectedIndex();
        StringBuffer sb = new StringBuffer();
        if(mainSel >= 0 && subSel >= 0) {
            this.tfList.get(mainSel).setRelSelectted(true);
            sb.append("$").append(this.subList.get(subSel).getName()).append("=@").append(this.tfList.get(mainSel).getName()).append(";");
        }

        mainTabCol = (Selectbox)this.view.getFellow("mainTabCol2");
        subTabCol = (Selectbox)this.view.getFellow("subTabCol2");
        mainSel = mainTabCol.getSelectedIndex();
        subSel = subTabCol.getSelectedIndex();
        if(mainSel >= 0 && subSel >= 0) {
            this.tfList.get(mainSel).setRelSelectted(true);
            sb.append("$").append(this.tfList.get(subSel).getName()).append("=@").append(this.subList.get(mainSel).getName()).append(";");
        }
//
//        mainTabCol = (Selectbox)this.view.getFellow("mainTabCol3");
//        subTabCol = (Selectbox)this.view.getFellow("subTabCol3");
//        mainSel = mainTabCol.getSelectedIndex();
//        subSel = subTabCol.getSelectedIndex();
//        if(mainSel >= 0 && subSel >= 0) {
//            sb.append("$").append(this.tfList.get(mainSel).getName()).append("=@").append(this.subList.get(subSel).getName()).append(";");
//        }
        return sb.toString();
    }
    private String getBottomButtonCode(String buttonType) {
        String buttonCode = "";
        switch (buttonType) {
            case "bottom,left":
                buttonCode = this.buttonTempCode.replace("##colNum##", String.valueOf(this.colNum)).replace("##align##", "align=\"left\"");
                break;
            case "bottom,center":
                buttonCode = this.buttonTempCode.replace("##colNum##", String.valueOf(this.colNum)).replace("##align##", "align=\"center\"");
                break;
            case "bottom,right":
                buttonCode = this.buttonTempCode.replace("##colNum##", String.valueOf(this.colNum)).replace("##align##", "align=\"right\"");
                break;
            default:
        }
        return buttonCode;
    }
    private String getTopButtonCode(String buttonType) {
        String buttonCode = "";
        switch (buttonType) {
            case "top,left":
                buttonCode = this.buttonTempCode.replace("##colNum##", String.valueOf(this.colNum)).replace("##align##", "align=\"left\"");
                break;
            case "top,center":
                buttonCode = this.buttonTempCode.replace("##colNum##", String.valueOf(this.colNum)).replace("##align##", "align=\"center\"");
                break;
            case "top,right":
                buttonCode = this.buttonTempCode.replace("##colNum##", String.valueOf(this.colNum)).replace("##align##", "align=\"right\"");
                break;
            default:
        }
        return buttonCode;
    }
    private static String TEXTBOX = "<textbox id=\"#{##name##}\" hflex=\"1\" ##constraint## />";
    private static String TEXTBOX_EVENT = "<textbox id=\"#{##name##}\" hflex=\"1\" ##constraint## event=\"$##name##=self.value\" />";
    private static String DATETIME = "<datebox id=\"#{##name##}\" hflex=\"1\" format=\"yyyy-MM-dd HH:mm:ss\" />";
    private static String DATETIME_EVENT = "<datebox id=\"#{##name##}\" hflex=\"1\" format=\"yyyy-MM-dd HH:mm:ss\" event=\"$##name##=self.value\" />";
    private static String DATE = "<datebox id=\"#{##name##}\" hflex=\"1\" format=\"yyyy-MM-dd\" />";
    private static String DATE_EVENT = "<datebox id=\"#{##name##}\" hflex=\"1\" format=\"yyyy-MM-dd\" event=\"$##name##=self.value\" />";
    private static String TIME = "<datebox id=\"#{##name##}\" hflex=\"1\" format=\"HH:mm:ss\" />";
    private static String TIME_EVENT =  "<datebox id=\"#{##name##}\" hflex=\"1\" format=\"HH:mm:ss\" event=\"$##name##=self.value\" />";
    private static String INT = "<intbox id=\"#{##name##}\" hflex=\"1\" ##constraint## />";
    private static String INT_EVENT = "<intbox id=\"#{##name##}\" hflex=\"1\" ##constraint##  event=\"$##name##=self.value\" />";
    private static String LONG = "<longbox id=\"#{##name##}\" hflex=\"1\" ##constraint## />";
    private static String LONG_EVENT = "<longbox id=\"#{##name##}\" hflex=\"1\" ##constraint##  event=\"$##name##=self.value\" />";
    private static String NUMERIC = "<intbox id=\"#{##name##}\" hflex=\"1\" format=\"#,##0.00\" />";
    private static String NUMERIC_EVENT = "<intbox id=\"#{##name##}\" hflex=\"1\" format=\"#,##0.00\" event=\"$##name##=self.value\" />";
    private static String LABEL = "<label value=\"##cnName##\" />";
    private static String LABEL_EVENT = "<label value=\"##cnName##\" event=\"$##name##=self.value\" />";
    private static String COMBOBOX = "<combobox id=\"#{##name##}\" hflex=\"1\" readonly=\"true\" />";
    private static String COMBOBOX_EVENT = "<combobox id=\"#{##name##}\" hflex=\"1\" readonly=\"true\" event=\"$##name##=self.selectedItem.value\" />";
    private static String BLOB = "<image id=\"#{##name##}\" />";
    private static String BLOB_EVENT = "<image id=\"#{##name##}\" event=\"$##name##=self.value\" />";
    private static String BOOLEAN = "<checkbox id=\"#{##name##}\" />";
    private static String BOOLEAN_EVENT = "<checkbox id=\"#{##name##}\"  event=\"$##name##=self.isChecked()\" />";

    private static String CLOB = "<ckeditor id=\"#{##name##}\" hflex=\"1\" height=\"300px\" />";
    private static String CLOB_EVENT = "<ckeditor id=\"#{##name##}\" hflex=\"1\" height=\"300px\" event=\"$##name##=self.value\" />";

    private String getFieldCode(TableFieldExt tableFieldExt) {
        StringBuffer field = new StringBuffer(LABEL.replace("##cnName##", tableFieldExt.getDesc()));
        String tmp = null;
        /**
         * 是否配置了数据字典/表参数
         */
        if(tableFieldExt.getOptionType() != null) {
            field.append(COMBOBOX.replace("##name##", tableFieldExt.getName()));
            return field.toString();
        }
        /**
         * 按类型/是否主键生成代码
         */
        switch(tableFieldExt.getType()) {
            case "BOOLEAN":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(BOOLEAN_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(BOOLEAN.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "CLOB":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(CLOB_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(CLOB.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "DATETIME":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(DATETIME_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(DATETIME.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "DATE":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(DATE_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(DATE.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "TIME":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(TIME_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(TIME.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "INT":
                if(tableFieldExt.isRelSelectted()) {
                    tmp = INT_EVENT.replace("##name##", tableFieldExt.getName());
                } else {
                    tmp = INT.replace("##name##", tableFieldExt.getName());
                }
                if(tableFieldExt.isKey()) {
                    tmp = tmp.replace("##constraint##", " constraint=\"no zero,no negative\"");
                } else {
                    tmp = tmp.replace("##constraint##", "");
                }
                field.append(tmp);
                break;
            case "LONG":
                if(tableFieldExt.isRelSelectted()) {
                    tmp = LONG_EVENT.replace("##name##", tableFieldExt.getName());
                } else {
                    tmp = LONG.replace("##name##", tableFieldExt.getName());
                }
                if(tableFieldExt.isKey()) {
                    tmp = tmp.replace("##constraint##", " constraint=\"no zero,no negative\"");
                } else {
                    tmp = tmp.replace("##constraint##", "");
                }
                field.append(tmp);
                break;
            case "NUMERIC":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(NUMERIC_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(NUMERIC.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "BLOB":
            case "OBJECT":
                if(tableFieldExt.isRelSelectted()) {
                    field.append(BLOB_EVENT.replace("##name##", tableFieldExt.getName()));
                } else {
                    field.append(BLOB.replace("##name##", tableFieldExt.getName()));
                }
                break;
            case "CHAR":
            case "VARCHAR":
            default:
                if(tableFieldExt.isRelSelectted()) {
                    tmp = TEXTBOX_EVENT.replace("##name##", tableFieldExt.getName());
                } else {
                    tmp = TEXTBOX.replace("##name##", tableFieldExt.getName());
                }
                if(tableFieldExt.isKey()) {
                    tmp = tmp.replace("##constraint##", " constraint=\"no empty\"");
                } else {
                    tmp = tmp.replace("##constraint##", "");
                }
                field.append(tmp);
                break;
        }
        return field.toString();
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getFuncName() {
        return funcName;
    }

    public void setFuncName(String funcName) {
        this.funcName = funcName;
    }

    public String getPageCode() {
        return pageCode;
    }

    public void setPageCode(String pageCode) {
        this.pageCode = pageCode;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getListCode() {
        return listCode;
    }

    public void setListCode(String listCode) {
        this.listCode = listCode;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListCode2() {
        return listCode2;
    }

    public void setListCode2(String listCode2) {
        this.listCode2 = listCode2;
    }

    public String getListName2() {
        return listName2;
    }

    public void setListName2(String listName2) {
        this.listName2 = listName2;
    }

    public int getColNum() {
        return colNum;
    }

    public void setColNum(int colNum) {
        this.colNum = colNum;
    }

    public List<TableFieldExt> getTfList() {
        return tfList;
    }

    public void setTfList(List<TableFieldExt> tfList) {
        this.tfList = tfList;
    }

    public boolean isGenList2() {
        return genList2;
    }

    public void setGenList2(boolean genList2) {
        this.genList2 = genList2;
    }

    public String getButtonType() {
        return buttonType;
    }

    public void setButtonType(String buttonType) {
        this.buttonType = buttonType;
    }

    public String getColNumStr() {
        return String.valueOf(this.colNum);
    }
    @NotifyChange("northAll")
    public void setColNumStr(String colNumStr) {
        this.colNum = Integer.valueOf(colNumStr);
    }


    public Boolean getLeftAll() {
        return leftAll;
    }

    @NotifyChange({"tfList"})
    public void setLeftAll(Boolean leftAll) {
        this.leftAll = leftAll;
        tfList.forEach(i -> {
            i.setSelectedWithoutAll(leftAll);
        });
    }

    public Boolean getNorthAll() {
        return northAll;
    }

    @NotifyChange({"tfList"})
    public void setNorthAll(Boolean northAll) {
        this.northAll = northAll;
        tfList.forEach(i -> {
            i.setRightSelectedWithoutAll(northAll);
        });
    }

    public String getRelateTable() {
        return relateTable;
    }

    public void setRelateTable(String relateTable) {
        this.relateTable = relateTable;
    }

    public ListModelList getRelTableList() {
        return relTableList;
    }

    public void setRelTableList(ListModelList relTableList) {
        this.relTableList = relTableList;
    }

    public ListModel getMainTabCol1() {
        return mainTabCol1;
    }

    public void setMainTabCol1(ListModel mainTabCol) {
        this.mainTabCol1 = mainTabCol;
    }

    public ListModel getSubTabCol1() {
        return subTabCol1;
    }

    public void setSubTabCol1(ListModel subTabCol1) {
        this.subTabCol1 = subTabCol1;
    }

    public ListModel getMainTabCol2() {
        return mainTabCol2;
    }

    public void setMainTabCol2(ListModel mainTabCol2) {
        this.mainTabCol2 = mainTabCol2;
    }

    public ListModel getSubTabCol2() {
        return subTabCol2;
    }

    public void setSubTabCol2(ListModel subTabCol2) {
        this.subTabCol2 = subTabCol2;
    }

    public Boolean getEastAll() {
        return eastAll;
    }

    @NotifyChange({"subList"})
    public void setEastAll(Boolean eastAll) {
        this.eastAll = eastAll;
        subList.forEach(i -> {
            i.setSelectedWithoutAll(eastAll);
        });
    }

    public Boolean getEditAll() {
        return editAll;
    }
    @NotifyChange({"subList"})
    public void setEditAll(Boolean editAll) {
        this.editAll = editAll;
        subList.forEach(i -> {
            i.setRightSelected(editAll);
        });
    }




    public List<TableFieldExt> getSubList() {
        return subList;
    }

    public void setSubList(List<TableFieldExt> subList) {
        this.subList = subList;
    }
}
