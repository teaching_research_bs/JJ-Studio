/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.event.Event;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class VersionView extends AbstractView<String> {

    private CMeditor editor;

    private String orig;

    /**
     * @param cb
     */
    protected VersionView(EditorCallback<String> cb, String orig) {
        super(cb);
        this.orig = orig;
    }

    @Override
    protected void createContent(Component parent) {
        editor = new CMeditor();
        editor.setId("versionView_cmeditor_content");
        editor.setTheme(Contexts.getUser().getEditorTheme());
        editor.setHflex("1");
        editor.setVflex("1");
        editor.setType("diff");
        editor.setHighlightSelection(true);
        editor.setValue(cb.getValue());
        editor.setOrig(orig);
        Components.replace(parent.getParent(),editor);
    }

    @Override
    public void onEvent(Event event) throws Exception {
    }

}
