/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.PagingCmd;
import cn.easyplatform.studio.cmd.entity.QueryCmd;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.vos.QueryResultVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.ZulEvents;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EntityView extends AbstractView<String> {

    private String selector;

    private Grid grid;

    private Paging paging;

    private Bandbox searchbox;

    private OnMsgListener onMsgListener;

    public interface OnMsgListener {
        void onMsg(String msg);
    }

    /**
     * @param cb
     */
    protected EntityView(EditorCallback<String> cb, String selector, OnMsgListener onMsgListener, Component component) {
        super(component, cb);
        this.selector = selector;
        this.onMsgListener = onMsgListener;
    }

    protected EntityView(EditorCallback<String> cb, String selector, Component component) {
        super(component, cb);
        this.selector = selector;
    }

    @Override
    protected void createHeader(Component parent) {
        // 搜索栏
        North searchbar = new North();
        searchbar.setBorder("none");
        searchbar.setSize("30px");
        searchbar.setHflex("1");
        Div bar = new Div();
        Label label = new Label(Labels.getLabel("entity.search.key") + ":");
        label.setParent(bar);
        searchbox = new Bandbox();
        searchbox.setId("entityView_bandbox_searchbox");
        searchbox.setPlaceholder(Labels.getLabel("navi.search.placeholder"));
        searchbox.addEventListener(Events.ON_OK, this);
        searchbox.addEventListener(Events.ON_OPEN, this);
        searchbox.setParent(bar);
        bar.setParent(searchbar);
        searchbar.setParent(parent);
    }

    @Override
    protected void createContent(Component parent) {
        // 内容
        grid = new Grid();
        grid.setId("entityView_grid_result");
        grid.setHflex("1");
        grid.setVflex("1");
        Columns columns = new Columns();
        columns.setSizable(true);
        Column column = new Column(Labels.getLabel("entity.id"));
        column.setId("entityView_column_entityId");
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.name"));
        column.setId("entityView_column_entityName");
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.type"));
        column.setId("entityView_column_entityType");
        column.setWidth("60px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.desp"));
        column.setId("entityView_column_entityDesp");
        column.setHflex("1");
        column.setParent(columns);

        columns.setParent(grid);
        grid.appendChild(new Rows());
        grid.setParent(parent);

        Window window = (Window) parent.getParent().getParent();
        window.setId("entityView_window_main");
    }

    private void redraw(List<EntityVo> data) {
        for (EntityVo vo : data) {
            Row row = new Row();
            row.setSclass("epstudio-link");
            row.setValue(vo);
            row.appendChild(new Label(vo.getId()));
            row.appendChild(new Label(vo.getName()));
            row.appendChild(new Label(vo.getType()));
            row.appendChild(new Label(vo.getDesp()));
            row.setParent(grid.getRows());
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    @Override
    protected void createFooter(Component parent) {
        paging = new Paging();
        paging.setPageSize(20);
        paging.setDetailed(true);
        paging.addEventListener(ZulEvents.ON_PAGING, this);
        South south = new South();
        south.setBorder("none");
        south.setHflex("1");
        south.setSize("30px");
        south.appendChild(paging);
        south.setParent(parent);
        QueryResultVo result;
        if (!Strings.isBlank(cb.getValue())) {
            searchbox.setValue(cb.getValue());
            result = StudioApp.execute(grid, new QueryCmd(
                    selector, "*", cb.getValue().trim(), paging.getPageSize()));
        } else {
            result = StudioApp.execute(grid, new QueryCmd(selector,
                    paging.getPageSize()));
        }
        paging.setTotalSize(result.getTotalSize());
        redraw(result.getEntities());
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getName().equals(ZulEvents.ON_PAGING)) {
            PagingEvent pe = (PagingEvent) event;
            int pageNo = pe.getActivePage() + 1;
            List<EntityVo> data = StudioApp.execute(paging,
                    new PagingCmd(selector, "*", searchbox.getValue().trim(),
                            paging.getPageSize(), pageNo));
            grid.getRows().getChildren().clear();
            redraw(data);
        } else if (event.getTarget() == searchbox) {
            String val = null;
            if (event instanceof OpenEvent) {
                OpenEvent evt = (OpenEvent) event;
                val = (String) evt.getValue();
                searchbox.setValue(val);
            } else
                val = searchbox.getValue();
            QueryResultVo result = StudioApp.execute(grid, new QueryCmd(
                    selector, "*", val.trim(), paging.getPageSize()));
            paging.setTotalSize(result.getTotalSize());
            grid.getRows().getChildren().clear();
            redraw(result.getEntities());
        } else if (event.getTarget() instanceof Row) {
            Row row = (Row) event.getTarget();
            EntityVo vo = row.getValue();
            grid.getRoot().detach();
            onCloseClick(win);
            if (null != onMsgListener) {
                onMsgListener.onMsg(vo.getId());
            } else {
                cb.setValue(vo.getId());
            }
        }
    }

}
