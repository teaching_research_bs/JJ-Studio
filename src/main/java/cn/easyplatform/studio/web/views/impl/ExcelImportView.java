package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelImportView extends AbstractView<List<Object>> {
    private Vlayout vlayout;
    private Listbox sourcesBox;
    private Listbox targetsBox;
    private Object data;
    private String type;

    /**
     * @param cb
     */
    protected ExcelImportView(EditorCallback<List<Object>> cb, Object data, String type, Component component) {
        super(component, cb);
        this.data = data;
        this.type = type;
    }

    @Override
    protected void createContent(Component parent) {

        vlayout = new Vlayout();
        vlayout.setVflex("1");

        Hlayout centerHlayout = new Hlayout();
        centerHlayout.setHflex("1");
        centerHlayout.setVflex("1");
        centerHlayout.setStyle("padding-top:6px");

        sourcesBox = new Listbox();
        sourcesBox.setId("excelImportView_listbox_sources");
        sourcesBox.setHflex("1");
        sourcesBox.setVflex("1");
        sourcesBox.setCheckmark(true);
        sourcesBox.setMultiple(true);
        sourcesBox.setMold("paging");
        sourcesBox.setPageSize(20);
        sourcesBox.setParent(centerHlayout);

        Listhead listhead = new Listhead();
        listhead.setSizable(true);
        listhead.setParent(sourcesBox);

        Listheader idListheader = new Listheader();
        idListheader.setLabel(Labels.getLabel("entity.name"));
        idListheader.setWidth("200px");
        idListheader.setParent(listhead);

        Listheader nameListheader = new Listheader();
        nameListheader.setLabel(Labels.getLabel("entity.desp"));
        nameListheader.setWidth("200px");
        nameListheader.setParent(listhead);

        Vlayout middleLayout = new Vlayout();
        middleLayout.setSpacing("10px");
        middleLayout.setWidth("24px");
        middleLayout.setParent(centerHlayout);

        Image createAllImg = new Image();
        createAllImg.setId("excelImportView_image_createAll");
        createAllImg.setSrc("~./images/rightrightarrow_g.png");
        createAllImg.setStyle("cursor:pointer");
        createAllImg.setTooltip(Labels.getLabel("button.createAll"));
        createAllImg.setParent(middleLayout);
        createAllImg.addEventListener(Events.ON_CLICK, this);

        Image createImg = new Image();
        createImg.setId("excelImportView_image_create");
        createImg.setSrc("~./images/rightarrow_g.png");
        createImg.setStyle("cursor:pointer");
        createImg.setTooltip(Labels.getLabel("button.create"));
        createImg.setParent(middleLayout);
        createImg.addEventListener(Events.ON_CLICK, this);

        Image removeImg = new Image();
        removeImg.setId("excelImportView_image_remove");
        removeImg.setSrc("~./images/leftarrow_g.png");
        removeImg.setStyle("cursor:pointer");
        removeImg.setTooltip(Labels.getLabel("button.remove"));
        removeImg.setParent(middleLayout);
        removeImg.addEventListener(Events.ON_CLICK, this);

        Image removeAllImg = new Image();
        removeAllImg.setId("excelImportView_image_removeAll");
        removeAllImg.setSrc("~./images/leftleftarrow_g.png");
        removeAllImg.setStyle("cursor:pointer");
        removeAllImg.setTooltip(Labels.getLabel("button.removeAll"));
        removeAllImg.setParent(middleLayout);
        removeAllImg.addEventListener(Events.ON_CLICK, this);

        middleLayout.setParent(centerHlayout);

        targetsBox = new Listbox();
        targetsBox.setId("excelImportView_listbox_targets");
        targetsBox.setHflex("1");
        targetsBox.setVflex("1");
        targetsBox.setCheckmark(true);
        targetsBox.setMultiple(true);
        targetsBox.setMold("paging");
        targetsBox.setPageSize(20);
        targetsBox.setParent(centerHlayout);
        Listhead newListhead = (Listhead) listhead.clone();
        newListhead.setParent(targetsBox);

        centerHlayout.setParent(vlayout);
        vlayout.setParent(parent);

        Hlayout bottomHlayout = new Hlayout();
        bottomHlayout.setHflex("1");

        Hlayout h = new Hlayout();
        h.setHflex("1");
        h.setParent(bottomHlayout);
        Button btn = new Button(Labels.getLabel("button.ok"));
        btn.setId("excelImportView_button_chooseBtn");
        btn.setSclass("pull-right");
        btn.setIconSclass("z-icon-check");
        btn.addEventListener(Events.ON_CLICK, this);
        btn.setParent(bottomHlayout);

        bottomHlayout.setParent(vlayout);

        redraw();
    }

    private void redraw() {
        createTables();
    }

    private void createTables() {
        if (type.equals("DB")) {
            Map<String, Object> map = (Map<String, Object>) data;
            List<List<Map<TableField, String>>> datas = (List<List<Map<TableField, String>>>) map.get("datas");
            List<TableBean> tableBeans = (List<TableBean>) map.get("tableBeans");
            List<Map<String, Object>> itemData = new ArrayList<>();
            for (int i = 0; i < datas.size(); i++) {
                List<Map<TableField, String>> data = datas.get(i);
                TableBean tableBean = tableBeans.get(i);
                Map<String, Object> m = new HashMap<>();
                m.put("data", data);
                m.put("tableBean", tableBean);
                itemData.add(m);
            }
            List<Listitem> items = sourcesBox.getItems();
            for (int i = 0; i < itemData.size(); i++) {
                Map<String, Object> m = itemData.get(i);
                Listitem ti = new Listitem();
                ti.appendChild(new Listcell(((TableBean) m.get("tableBean")).getId()));
                ti.appendChild(new Listcell(((TableBean) m.get("tableBean")).getName()));
                ti.setDraggable("table");
                ti.setValue(m);
                ti.addEventListener(Events.ON_DOUBLE_CLICK, this);
                items.add(ti);
            }
        }
        if (type.equals("TABLE")) {
            List<TableBean> tableBeans = (List<TableBean>) data;
            List<Listitem> items = sourcesBox.getItems();
            for (int i = 0; i < tableBeans.size(); i++) {
                TableBean m = tableBeans.get(i);
                Listitem ti = new Listitem();
                ti.appendChild(new Listcell(m.getId()));
                ti.appendChild(new Listcell(m.getName()));
                ti.setDraggable("table");
                ti.setValue(m);
                ti.addEventListener(Events.ON_DOUBLE_CLICK, this);
                items.add(ti);
            }
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {

        if (event.getTarget().getId().equals("excelImportView_image_createAll"))
            select(false);
        else if (event.getTarget().getId().equals("excelImportView_image_create"))
            select(true);
        else if (event.getTarget().getId().equals("excelImportView_image_remove"))
            unselect(true);
        else if (event.getTarget().getId().equals("excelImportView_image_removeAll"))
            unselect(false);
        else if (event.getTarget().getId().equals("excelImportView_button_chooseBtn"))
            choose();
    }

    private void select(boolean selectFlag) {
        if (type.equals("DB")) {
            for (Listitem sel : sourcesBox.getItems()) {
                if (selectFlag && !sel.isSelected())
                    continue;
                if (sel.getValue() != null) {
                    boolean exists = false;
                    Map<String, Object> s = sel.getValue();
                    for (Listitem li : targetsBox.getItems()) {
                        Map<String, Object> t = li.getValue();
                        if (((TableBean) s.get("tableBean")).getId().equals(((TableBean) t.get("tableBean")).getId())) {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists) {
                        Listitem li = new Listitem();
                        li.appendChild(new Listcell(((TableBean) s.get("tableBean")).getId()));
                        li.appendChild(new Listcell(((TableBean) s.get("tableBean")).getName()));
                        li.setValue(s);
                        targetsBox.appendChild(li);
                    }
                }
            }
        }
        if (type.equals("TABLE")) {
            for (Listitem sel : sourcesBox.getItems()) {
                if (selectFlag && !sel.isSelected())
                    continue;
                if (sel.getValue() != null) {
                    boolean exists = false;
                    TableBean s = sel.getValue();
                    for (Listitem li : targetsBox.getItems()) {
                        TableBean t = li.getValue();
                        if (s.getId().equals(t.getId())) {
                            exists = true;
                            break;
                        }
                    }
                    if (!exists) {
                        Listitem li = new Listitem();
                        li.appendChild(new Listcell(s.getId()));
                        li.appendChild(new Listcell(s.getName()));
                        li.setValue(s);
                        targetsBox.appendChild(li);
                    }
                }
            }
        }
    }

    private void unselect(boolean selectedFlag) {
        if (selectedFlag) {
            if (targetsBox.getSelectedCount() > 0) {
                List<Listitem> items = new ArrayList<Listitem>();
                for (Listitem li : targetsBox.getSelectedItems())
                    items.add(li);
                for (Listitem li : items)
                    li.detach();
            }
        } else
            targetsBox.getItems().clear();
    }

    private void choose() {
        List<Object> dicVos = new ArrayList<>();
        if (targetsBox.getItems() != null && targetsBox.getItems().size() > 0) {
            for (Listitem li : targetsBox.getItems()) {
                dicVos.add(li.getValue());
            }
        }
        if (cb != null)
            cb.setValue(dicVos);
        vlayout.getRoot().detach();
        onCloseClick(win);
    }
}
