package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

public class SqlFieldView extends AbstractView<String> {

    private Borderlayout layout;

    private Listbox listbox;

    private List<TableBean> tableBeanList;

    protected SqlFieldView(EditorCallback<String> cb, List<TableBean> tableBeanList) {
        super(cb);
        this.tableBeanList = tableBeanList;
    }

    @Override
    protected void createContent(Component parent) {
        layout = new Borderlayout();
        layout.setHflex("1");
        layout.setVflex("1");
        layout.setParent(parent);

        North north = new North();
        north.setSize("30px");
        north.setBorder("none");
        north.setParent(layout);

        Bandbox searchBox = new Bandbox();
        searchBox.setPlaceholder(Labels.getLabel("sqlwizard.search.table.placeholder"));
        searchBox.addEventListener(Events.ON_OK, this);
        searchBox.addEventListener(Events.ON_CHANGE, this);
        searchBox.addEventListener(Events.ON_OPEN, this);
        searchBox.setId("searchBox");
        searchBox.setParent(north);

        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");
        center.setParent(layout);

        listbox = new Listbox();
        listbox.setVflex("1");
        listbox.setHflex("1");
        listbox.setMold("paging");
        listbox.setPageSize(20);

        Listhead listhead = new Listhead();
        listhead.setSizable(true);

        String[] labels = new String[]{Labels.getLabel("entity.table"),
                Labels.getLabel("sqlwizard.field")};
        String[] widths = new String[]{"100px", "100px"};
        for (int index = 0; index < labels.length; index++) {
            Listheader listheader = new Listheader();
            listheader.setLabel(labels[index]);
            listheader.setWidth(widths[index]);
            listheader.setParent(listhead);
        }

        listhead.setParent(listbox);
        listbox.setParent(center);

        Window window = (Window) parent.getParent().getParent();
        window.setWidth("210px");

        redraw(tableBeanList);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof Listitem) {
            Listitem row = (Listitem) event.getTarget();
            cb.setValue((String) row.getValue());
            listbox.getRoot().detach();
            onCloseClick(win);
        } else if (event.getTarget() instanceof Bandbox) {
            String val;
            if (event instanceof OpenEvent)
                val = ((OpenEvent) event).getValue().toString();
            else
                val = ((Bandbox) event.getTarget()).getValue();
            List<TableBean> currentGuideConfigVos = searchName(val.trim());
            redraw(currentGuideConfigVos);
        }
    }

    private void redraw(List<TableBean> tableBeans) {
        listbox.getItems().clear();
        for (TableBean table : tableBeans) {
            for (TableField tableField : table.getFields()) {
                Listitem listitem = new Listitem();
                listitem.setValue(table.getId() + "." +  tableField.getName());
                listitem.appendChild(new Listcell(table.getId()));
                listitem.appendChild(new Listcell(tableField.getName()));
                listitem.setParent(listbox);
                listitem.addEventListener(Events.ON_DOUBLE_CLICK, this);
            }
        }
    }

    private List<TableBean> searchName(String searchStr) {
        if (Strings.isBlank(searchStr)) {
            return tableBeanList;
        } else {
            List<TableBean> currentVos = new ArrayList<>();
            for (TableBean bean : tableBeanList) {
                for (TableField tableField : bean.getFields()) {
                    if (bean.getId().contains(searchStr))
                        currentVos.add(bean);
                    else if (tableField.getName().contains(searchStr))
                        currentVos.add(bean);
                }
            }
            return currentVos;
        }
    }
}
