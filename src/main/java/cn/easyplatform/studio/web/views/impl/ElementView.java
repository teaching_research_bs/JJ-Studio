package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.vos.ElementVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import com.alibaba.fastjson.JSON;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

interface ElementViewIml {
    List<ElementVo> searchName(String searchStr);
    void redraw(List<ElementVo> data);
}
public class ElementView extends AbstractView<ElementVo> implements ElementViewIml {
    protected Borderlayout layout;
    protected Listbox listbox;

    private static List<ElementVo> elementVos = new ArrayList<>();

    static {
        String file = StudioApp.getServletContext().getRealPath("/WEB-INF/element/element.json");
        if (new File(file).exists()) {
            try {
                String content = FileUtil.getStreamWithFile(file);
                elementVos = JSON.parseArray(content, ElementVo.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * @param cb
     */
    protected ElementView(EditorCallback cb, Component component) {
        super(component, cb);
    }

    @Override
    protected void createContent(Component parent) {
        setUpView(parent);
        redraw(elementVos);

        Window window = (Window) parent.getParent().getParent();
        window.setWidth("510px");
    }

    protected void setUpView(Component parent) {
        layout = new Borderlayout();
        layout.setHflex("1");
        layout.setVflex("1");
        layout.setParent(parent);

        North north = new North();
        north.setSize("30px");
        north.setBorder("none");
        north.setParent(layout);

        Bandbox searchBox = new Bandbox();
        searchBox.setPlaceholder(Labels.getLabel("editor.widget.entity.placeholder"));
        searchBox.addEventListener(Events.ON_OK, this);
        searchBox.addEventListener(Events.ON_CHANGE, this);
        searchBox.addEventListener(Events.ON_OPEN, this);
        searchBox.setId("searchBox");
        searchBox.setParent(north);

        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");
        center.setParent(layout);

        listbox = new Listbox();
        listbox.setVflex("1");
        listbox.setHflex("1");
        listbox.setMold("paging");
        listbox.setPageSize(20);

        Listhead listhead = new Listhead();
        listhead.setSizable(true);

        String[] labels = new String[]{Labels.getLabel("guide.step.element"),
                Labels.getLabel("guide.element.path")};
        String[] widths = new String[]{"250px", "250px"};
        for (int index = 0; index < labels.length; index++) {
            Listheader listheader = new Listheader();
            listheader.setLabel(labels[index]);
            listheader.setWidth(widths[index]);
            listheader.setHeight("38px");
            listheader.setParent(listhead);
        }

        listhead.setParent(listbox);
        listbox.setParent(center);

        Window window = (Window) parent.getParent().getParent();
        window.setHeight("644px");
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof Listitem) {
            Listitem row = (Listitem) event.getTarget();
            cb.setValue((ElementVo)row.getValue());
            listbox.getRoot().detach();
            onCloseClick(win);
        } else if (event.getTarget() instanceof Bandbox) {
            String val;
            if (event instanceof OpenEvent)
                val = ((OpenEvent) event).getValue().toString();
            else
                val = ((Bandbox) event.getTarget()).getValue();
            List<ElementVo> currentGuideConfigVos = searchName(val.trim());
            redraw(currentGuideConfigVos);
        }
    }

    @Override
    public void redraw(List<ElementVo> data) {
        listbox.getItems().clear();
        for (ElementVo vo : data) {
            Listitem row = new Listitem();
            row.setValue(vo);
            row.appendChild(new Listcell(vo.getElementId()));
            row.appendChild(new Listcell(vo.getElementPath()));
            row.appendChild(new Listcell(vo.getAction()));
            row.setParent(listbox);
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    @Override
    public List<ElementVo> searchName(String searchStr) {
        if (Strings.isBlank(searchStr)) {
            return elementVos;
        } else {
            List<ElementVo> currentGuideConfigVos = new ArrayList<>();
            for (ElementVo vo : elementVos) {
                if (vo.getElementId().contains(searchStr))
                    currentGuideConfigVos.add(vo);
                else if (vo.getElementPath().contains(searchStr))
                    currentGuideConfigVos.add(vo);
                else if (Strings.isBlank(vo.getEntityId()) == false && vo.getEntityId().contains(searchStr))
                    currentGuideConfigVos.add(vo);
            }
            return currentGuideConfigVos;
        }
    }
}
