package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.List;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class AppPageProductView extends AbstractView<String> {

    private List<EntityVo> entityVos;

    private Listbox targetsListbox;

    private Button close;

    protected AppPageProductView(EditorCallback<String> cb, List<EntityVo> entityVos,
                                 Component component) {
        super(component, cb);
        this.entityVos = entityVos;
    }

    @Override
    protected void createContent(Component parent) {

        Window window = (Window) parent.getParent().getParent();
        window.setTitle(Labels.getLabel("admin.project.product.module.detail"));
        window.setBorder("normal");
        window.setPosition("right,bottom");
        window.setWidth("950px");
        window.setHeight("650px");
        window.setMaximizable(true);
        window.setSizable(true);
        window.setClosable(true);
        window.setMaximizable(true);
        window.setPosition("center,top");

        Borderlayout layout = new Borderlayout();
        Center container = new Center();
        container.setHflex("1");
        container.setVflex("1");
        targetsListbox = new Listbox();
        targetsListbox.setMold("paging");
        targetsListbox.setPageSize(15);
        targetsListbox.setDraggable("true");
        Listhead lh = new Listhead();
        Listheader lhID = new Listheader();
        lhID.setLabel(Labels.getLabel("entity.id"));
        Listheader lhName = new Listheader();
        lhName.setLabel(Labels.getLabel("entity.name"));
        Listheader lhType = new Listheader();
        lhType.setLabel(Labels.getLabel("entity.type"));
        Listheader lhDesp = new Listheader();
        lhDesp.setLabel(Labels.getLabel("entity.desp"));
        lh.appendChild(lhID);
        lh.appendChild(lhName);
        lh.appendChild(lhType);
        lh.appendChild(lhDesp);
        targetsListbox.appendChild(lh);
        for (EntityVo vo : entityVos) {
            Listitem row = new Listitem();
            row.setValue(vo);
            row.appendChild(new Listcell(vo.getId()));
            row.appendChild(new Listcell(vo.getName()));
            row.appendChild(new Listcell(vo.getType()));
            row.appendChild(new Listcell(vo.getDesp()));
            targetsListbox.appendChild(row);
        }
        targetsListbox.setParent(container);
        container.setParent(layout);

        South south = new South();
        Div divs = new Div();
        divs.setZclass("text-center mt-2");
        divs.setParent(south);
        close = new Button(Labels.getLabel("button.close"));
        close.setSclass("ml-2");
        close.addEventListener(Events.ON_CLICK, this);
        close.setParent(divs);
        south.setParent(layout);
        layout.setParent(parent);
    }

    @Override
    public void onEvent(Event evt) throws Exception {
        if(evt.getTarget() == close){
            targetsListbox.getRoot().detach();
            onCloseClick(win);
        }
    }

}


