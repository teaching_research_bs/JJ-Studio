package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.vos.DicDetailVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.*;

import java.util.List;

public class DicContentView extends AbstractView<String> {
    private Grid grid;

    private List<DicDetailVo> dicDetailVoList;
    /**
     * @param cb
     */
    protected DicContentView(EditorCallback<String> cb, List<DicDetailVo> dicDetailVoList) {
        super(cb);
        this.dicDetailVoList = dicDetailVoList;
    }

    @Override
    protected void createContent(Component parent) {
        // 内容
        grid = new Grid();
        grid.setHflex("1");
        grid.setVflex("1");
        Columns columns = new Columns();
        columns.setSizable(true);
        Column column = new Column(Labels.getLabel("dic.upcode"));
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("dic.detail.no"));
        column.setWidth("200px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.name"));
        column.setHflex("1");
        column.setParent(columns);

        columns.setParent(grid);
        grid.appendChild(new Rows());
        grid.setParent(parent);
        if (dicDetailVoList != null)
            redraw(dicDetailVoList);
    }

    private void redraw(List<DicDetailVo> data) {
        for (DicDetailVo vo : data) {
            Row row = new Row();
            row.setSclass("epstudio-link");
            row.setValue(vo);
            row.appendChild(new Label(vo.getDicCode()));
            row.appendChild(new Label(vo.getDetailNO()));
            row.appendChild(new Label(vo.getDesc1()));
            row.setParent(grid.getRows());
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {
    }
}
