package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.FileUtil;
import cn.easyplatform.studio.utils.ServiceFileUtil;
import cn.easyplatform.studio.vos.MediaFileVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.image.AImage;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Image;
import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class FileContentView extends AbstractView<String> {
    private MediaFileVo fileVo;
    /**
     * @param cb
     */
    public FileContentView(EditorCallback<String> cb, MediaFileVo fileVo) {
        super(cb);
        this.fileVo = fileVo;
    }
    @Override
    protected void createContent(Component parent) {
        if (ServiceFileUtil.MediaType.MEDIA_TYPE_IMG.getName().equals(fileVo.getType())) {
            Div bgDiv = new Div();
            bgDiv.setStyle("background-color: #DCDCDC;display: flex;justify-content: center;align-items: Center;");
            bgDiv.setHflex("1");
            bgDiv.setVflex("1");
            Image image = new Image();
            image.setStyle("background-color: white;");
            image.setTooltiptext(fileVo.getName());
            if (Strings.isBlank(fileVo.getPath()) == false || Strings.isBlank(fileVo.getBase64Data()) == false) {
                try {
                    AImage aImage = null;
                    if (Strings.isBlank(fileVo.getPath()) == false)
                        aImage = new AImage(fileVo.getName(), new FileInputStream(fileVo.getPath()));
                    else {
                        BASE64Decoder decoder = new BASE64Decoder();
                        aImage = new AImage(fileVo.getName(), decoder.decodeBuffer(fileVo.getBase64Data()));
                    }
                    image.setHeight(aImage.getHeight()+"px");
                    image.setWidth(aImage.getWidth()+"px");
                    image.setContent(aImage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            image.setParent(bgDiv);
            bgDiv.setParent(parent);
        } else if (ServiceFileUtil.MediaType.MEDIA_TYPE_VIDEO.getName().equals(fileVo.getType()) ||
                ServiceFileUtil.MediaType.MEDIA_TYPE_AUDIO.getName().equals(fileVo.getType())) {
            Iframe iframe_audio = new Iframe();
            iframe_audio.setHflex("1");
            iframe_audio.setVflex("1");
            iframe_audio.setStyle("border: 1px solid gray");
            if (Strings.isBlank(fileVo.getPath()) == false || Strings.isBlank(fileVo.getBase64Data()) == false) {
                try {
                    if (Strings.isBlank(fileVo.getPath()) == false) {
                        iframe_audio.setContent(new AMedia(new File(fileVo.getPath()), null, null));
                    }
                    else {
                        BASE64Decoder decoder = new BASE64Decoder();
                        iframe_audio.setContent(new AMedia(fileVo.getName(), null, null, decoder.decodeBuffer(fileVo.getBase64Data())));
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            iframe_audio.setParent(parent);
        } else if (ServiceFileUtil.MediaType.MEDIA_TYPE_CSS.getName().equals(fileVo.getType()) ||
                ServiceFileUtil.MediaType.MEDIA_TYPE_JS.getName().equals(fileVo.getType())) {
            CMeditor editor = new CMeditor();
            editor.setId("editor");
            editor.setHflex("1");
            editor.setVflex("1");
            editor.setTheme(Contexts.getUser().getEditorTheme());
            editor.setAutocomplete(true);
            editor.setFoldGutter(true);
            if (Strings.isBlank(fileVo.getPath()) == false || Strings.isBlank(fileVo.getBase64Data()) == false) {
                try {
                    String content = null;
                    if (Strings.isBlank(fileVo.getPath()) == false)
                        content = FileUtil.getStreamWithFile(fileVo.getPath());
                    else {
                        BASE64Decoder decoder = new BASE64Decoder();
                        content = new String(decoder.decodeBuffer(fileVo.getBase64Data()));
                    }
                    editor.setValue(content);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            editor.setReadonly(true);
            editor.setParent(parent);
            editor.setFocus(false);
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {

    }
}
