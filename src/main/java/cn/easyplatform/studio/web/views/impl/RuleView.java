package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Cardlayout;
import org.zkoss.zul.*;

import java.util.Arrays;
import java.util.List;


public class RuleView extends AbstractView<String> {
    private enum PageType {
        MenuPage,
        RolePage,
        UserPage
    }
    private enum InputType {
        TextInputType,
        DateInputType,
        ComboboxType
    }

    private Cardlayout cardlayout;

    private PageType pageType;

    protected RuleView(Component component, EditorCallback<String> cb) {
        super(component, cb);
    }

    @Override
    protected void createContent(Component parent) {
        cardlayout = new Cardlayout();
        cardlayout.setHflex("1");
        cardlayout.setVflex("1");
        cardlayout.setParent(parent);
        setFirstView();
        setSecondView();
    }

    private void setFirstView() {
        //第一个页面
        Hlayout hlayout = new Hlayout();
        hlayout.setVflex("1");
        hlayout.setHflex("1");
        hlayout.setSpacing("0");
        hlayout.setParent(cardlayout);
        Div leftEmpty = new Div();
        leftEmpty.setHflex("1");
        leftEmpty.setVflex("1");
        hlayout.appendChild(leftEmpty);
        Vlayout vlayout = new Vlayout();
        vlayout.setHflex("2");
        vlayout.setVflex("1");
        hlayout.appendChild(vlayout);
        List<String> nameList = Arrays.asList(new String[]{"menu.menu", "menu.role", "menu.user"});
        for (String name: nameList) {
            Div topEmpty = new Div();
            topEmpty.setVflex("1");
            topEmpty.setHflex("1");
            vlayout.appendChild(topEmpty);
            ChooseButton(name, vlayout);
        }
        Div bottomEmpty = new Div();
        bottomEmpty.setVflex("1");
        bottomEmpty.setHflex("1");
        vlayout.appendChild(bottomEmpty);
        Div rightEmpty = new Div();
        rightEmpty.setHflex("1");
        rightEmpty.setVflex("1");
        hlayout.appendChild(rightEmpty);
    }
    private void ChooseButton(String name, Component parent) {
        Div div = new Div();
        div.setId(name);
        div.setHflex("1");
        div.setVflex("5");
        div.setSclass("ui-choose-div ui-div-center-parent");
        Label label = new Label(Labels.getLabel(name));
        label.setStyle("font-size:28px;");
        div.appendChild(label);
        div.addEventListener(Events.ON_CLICK, this);
        div.setParent(parent);
    }

    private void setSecondView() {
        Vlayout vlayout = new Vlayout();
        vlayout.setHflex("1");
        vlayout.setVflex("1");
        cardlayout.appendChild(vlayout);

        Div topDiv = new Div();
        topDiv.setHeight("32px");
        vlayout.appendChild(topDiv);

        Grid grid = new Grid();
        grid.setHflex("1");
        grid.setVflex("1");
        vlayout.appendChild(grid);
        grid.setOddRowSclass("none");
        Columns columns = new Columns();
        columns.setVisible(false);
        Column leftColumn = new Column();
        leftColumn.setHflex("1");
        columns.appendChild(leftColumn);
        Column centerColumn = new Column();
        centerColumn.setHflex("2");
        columns.appendChild(centerColumn);
        Column rightColumn = new Column();
        rightColumn.setHflex("1");
        columns.appendChild(rightColumn);
        grid.appendChild(columns);

        Rows rows = new Rows();
        List<String> nameList = Arrays.asList(new String[]{"ID", Labels.getLabel("editor.user.password"),
                Labels.getLabel("editor.user.validDate"), Labels.getLabel("editor.user.state"),
                Labels.getLabel("editor.file.name"), Labels.getLabel("entity.type"),
                Labels.getLabel("editor.user.validDate")});
        for (String name: nameList) {
            Row row = new Row();
            row.setHeight("46px");
            InputDiv(name, row, InputType.TextInputType, null);
            Div div = new Div();
            row.appendChild(div);
            rows.appendChild(row);
        }

        Div buttonDiv = new Div();
        buttonDiv.setHeight("116px");
        buttonDiv.setHflex("1");
        vlayout.appendChild(buttonDiv);
    }
    private void getLabel(String name, Component parent) {

    }
    private void InputDiv(String name, Component parent, InputType inputType, List<String> dataList) {
        Label idLabel = new Label(name + ":");
        idLabel.setSclass("ui-div-hend-parent");
        idLabel.setStyle("color:rgba(51,51,51,1);font-size:14px;");
        idLabel.setParent(parent);
        if (inputType == InputType.TextInputType){
            Textbox textbox = new Textbox();
            textbox.setId(name);
            textbox.setParent(parent);
        } else if (inputType == InputType.DateInputType){
            Datebox datebox = new Datebox();
            datebox.setId(name);
            datebox.setParent(parent);
        } else if (inputType == InputType.ComboboxType) {
            Combobox combobox = new Combobox();
            combobox.setReadonly(true);
            combobox.setId(name);
            combobox.setParent(parent);
            for (String data : dataList) {
                Comboitem comboitem = new Comboitem(Labels.getLabel(data));
                comboitem.setId(data);
                combobox.appendChild(comboitem);
            }
        }
        Div div = new Div();
        div.setParent(parent);
    }

    private void setThirdView() {

    }

    private void setFourthView() {

    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget().getId().equals("menu.menu") || event.getTarget().getId().equals("menu.role")
                ||event.getTarget().getId().equals("menu.user")) {
            if (event.getTarget().getId().equals("menu.menu"))
                pageType = PageType.MenuPage;
            else if (event.getTarget().getId().equals("menu.role"))
                pageType = PageType.RolePage;
            else
                pageType = PageType.UserPage;
            cardlayout.next();
        }
    }

    private class InputDiv implements EventListener<Event> {

        public InputDiv(String name, Component parent, InputType inputType) {
            Div div = new Div();
            div.setHflex("1");
            div.setVflex("5");
            div.setSclass("ui-choose-div ui-div-center-parent");
            Label label = new Label(name);
            label.setStyle("font-size:28px;");
            div.appendChild(label);
            div.addEventListener(Events.ON_CLICK, this);
            div.setParent(parent);
        }

        @Override
        public void onEvent(Event event) throws Exception {
            cardlayout.next();
        }
    }
}
