package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.image.AImage;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.io.FileInputStream;
import java.io.IOException;

public class CodeThemeContentView extends AbstractView<String> {
    private Grid grid;
    protected CodeThemeContentView(EditorCallback<String> cb) {
        super(cb);
    }

    @Override
    protected void createContent(Component parent) {
        grid = new Grid();
        grid.setId("CodeThemeContentView_grid_result");
        grid.setHflex("1");
        grid.setVflex("1");
        String[] voList = new String[]{"default", "3024-night", "ambiance",
                "night", "cobalt", "eclipse", "zenburn"};

        Rows rows = new Rows();
        String theme = Contexts.getUser().getEditorTheme();
        if (Strings.isBlank(theme)) {
            theme = "default";
        }
        for (int i = 0; i < voList.length; i += 2) {
            Row row = new Row();
            row.setHeight("50%");
            int maxJ = i + 1 < voList.length? 2: 1;
            for (int j = 0; j < maxJ; j++) {
                String themeVo = voList[i + j];
                Vlayout vlayout = new Vlayout();
                if (theme.equals(themeVo))
                    vlayout.setStyle("text-align: center;border: 3px solid #ffb529;");
                else
                    vlayout.setStyle("text-align: center;");
                Image themePic = new Image();
                themePic.setHflex("1");
                try {
                    themePic.setContent(new AImage(themeVo+".jpg", new FileInputStream(
                            StudioApp.getServletContext().getRealPath("/WEB-INF/themePic/" + themeVo+".jpg"))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                themePic.setParent(vlayout);
                Label themeNameLabel = new Label();
                if (themeVo.equals("default"))
                    themeNameLabel.setValue(Labels.getLabel("button.default"));
                else
                    themeNameLabel.setValue(themeVo);
                themeNameLabel.setParent(vlayout);
                vlayout.setParent(row);
                vlayout.setAttribute("tag",themeVo);
                vlayout.addEventListener(Events.ON_CLICK, this);
            }
            row.setParent(rows);
        }
        grid.appendChild(rows);
        grid.setParent(parent);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        Vlayout vlayout = (Vlayout) event.getTarget();
        String theme = (String) vlayout.getAttribute("tag");
        String currentTheme = Contexts.getUser().getEditorTheme();
        if (!theme.equals(currentTheme)) {
            if (cb != null)
                cb.setValue(theme);
            grid.getRoot().detach();
        }
    }
}
