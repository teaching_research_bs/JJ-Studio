/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.editors.support.NodeFactory;
import cn.easyplatform.web.ext.cmez.CMeditor;
import nu.xom.Builder;
import nu.xom.Document;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.South;
import org.zkoss.zul.Window;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
class CodeView extends AbstractView<String> {

    private String mode;

    /**
     * @param cb
     */
    protected CodeView(EditorCallback<String> cb, String mode, Component component) {
        super(component, cb);
        this.mode = mode;
    }

    @Override
    protected void createContent(Component parent) {
        CMeditor editor = new CMeditor();
        editor.setId("codeView_cmeditor_code");
        editor.setMode(mode);
        editor.setHflex("1");
        editor.setVflex("1");
        editor.setTheme(Contexts.getUser().getEditorTheme());
        editor.setAutocomplete(true);
        if (mode.equals("eppage") || mode.equals("xml")) {
            editor.setAutoCloseTags(true);
            editor.setFoldGutter(true);
            editor.setMatchTags(true);
        } else if (mode.equals("epscript") || mode.equals("javascript")) {
            editor.setFoldGutter(true);
        }
        editor.setValue(cb.getValue());
        editor.setParent(parent);
        editor.setFocus(true);
        editor.addEventListener(Events.ON_CHANGING, this);
        ((Window) parent.getRoot()).setTitle(cb.getTitle() + ":" + mode);
    }

    @Override
    protected void createFooter(Component parent) {
        South south = new South();
        south.setBorder("none");
        south.setSize("35px");
        south.setHflex("1");
        Hlayout toolbar = new Hlayout();
        toolbar.setSpacing("10px");
        toolbar.setVflex("1");
        toolbar.setSclass("epstudio-top pull-right");
        Button pretty = new Button(Labels.getLabel("button.beautify"));
        pretty.setIconSclass("z-icon-magic");
        pretty.addEventListener(Events.ON_CLICK, this);
        pretty.setMold("bs");
        pretty.setId("codeView_button_beautify");
        pretty.setSclass("btn-light btn-sm");
        pretty.setParent(toolbar);
        Button save = new Button(Labels.getLabel("button.save"));
        save.setIconSclass("z-icon-save");
        save.setId("codeView_button_save");
        save.setMold("bs");
        save.setSclass("btn-primary btn-sm");
        save.setParent(toolbar);
        save.addEventListener(Events.ON_CLICK, this);
        toolbar.setParent(south);
        south.setParent(parent);
    }

    @Override
    public void onEvent(Event event) throws Exception {
        CMeditor editor = (CMeditor) event.getTarget().getFellow("codeView_cmeditor_code");
        String value = (String) editor.getValue();
        if ("codeView_button_beautify".equals(event.getTarget().getId())) {
            if (!Strings.isBlank(value))
                editor.exeCmd("formatAll");
        } else if (Events.ON_CLICK.equals(event.getName())) {
            if (!Strings.isBlank(value)) {
                if (mode.equals("eppage") || mode.equals("xml")) {
                    try {
                        Document doc = new Builder(false, new NodeFactory())
                                .build(value, null);
                        doc.detach();
                        doc = null;
                    } catch (Exception ex) {
                        WebUtils.showError(Labels.getLabel(
                                "editor.parse.error",
                                new Object[]{cb.getTitle(), ex.getMessage()}));
                        return;
                    }
                }
            }
            event.getTarget().getRoot().detach();
            onCloseClick(win);
            cb.setValue(value);
        }
    }
}
