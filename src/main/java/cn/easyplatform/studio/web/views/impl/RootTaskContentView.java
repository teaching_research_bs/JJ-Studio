package cn.easyplatform.studio.web.views.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.link.GetLinkCmd;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.vos.LinkVo;
import cn.easyplatform.studio.web.editors.EditorCallback;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.*;

import java.util.List;


public class RootTaskContentView extends AbstractView<String> {
    private Grid grid;

    private String taskID;
    /**
     * @param cb
     */
    protected RootTaskContentView(EditorCallback<String> cb, String taskID) {
        super(cb);
        this.taskID = taskID;
    }

    @Override
    protected void createContent(Component parent) {
        // 内容
        grid = new Grid();
        grid.setHflex("1");
        grid.setVflex("1");
        Columns columns = new Columns();
        columns.setSizable(true);
        Column column = new Column(Labels.getLabel("entity.id"));
        column.setWidth("180px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.name"));
        column.setWidth("180px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.desp"));
        column.setWidth("180px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("entity.type"));
        column.setWidth("90px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("editor.export.update.date"));
        column.setWidth("120px");
        column.setParent(columns);

        column = new Column(Labels.getLabel("editor.export.update.user"));
        column.setWidth("90px");
        column.setParent(columns);

        columns.setParent(grid);
        grid.appendChild(new Rows());
        grid.setParent(parent);
        if (Strings.isBlank(taskID) == false) {
            List<LinkVo> linkVos = StudioApp.execute(new GetLinkCmd(taskID, true));
            redraw(linkVos);
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {

    }

    private void redraw(List<LinkVo> data) {
        for (LinkVo vo : data) {
            Row row = new Row();
            row.setSclass("epstudio-link");
            row.setValue(vo);
            row.appendChild(new Label(vo.getEntityId()));
            row.appendChild(new Label(vo.getEntityName()));
            row.appendChild(new Label(vo.getEntityDesp()));
            row.appendChild(new Label(StudioUtil.getSringWithEntityType(vo.getEntityType(), vo.getEntitySubType())));
            row.appendChild(new Label(vo.getUpdateDate() == null ? null: vo.getUpdateDate().toString()));
            row.appendChild(new Label(vo.getUpdateUser()));
            row.setParent(grid.getRows());
        }
    }
}
