/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.wizards;

import cn.easyplatform.entities.beans.page.PageBean;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zul.A;

import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PageWizard implements Composer<Component>, IWizard<PageBean>,
		EventListener<Event> {

	private Component container;

	private IWizard<PageBean> delegate;

	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		for (Component c : comp.getSpaceOwner().getFellows()) {
			if (c.getId().equals("container"))
				container = c;
			else if (c instanceof A)
				c.addEventListener(Events.ON_CLICK, this);
		}
		String page = (String) Executions.getCurrent().getArg().get("page");
		container.getChildren().clear();
		List pageList = Arrays.asList(page.split("_"));
		Component c = Executions.createComponents(
				"~./include/wizards/template/"+pageList.get(pageList.size() - 1)+".zul", container, null);
		delegate = (IWizard<PageBean>) c.getAttribute("");
		comp.setAttribute("", this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEvent(Event event) throws Exception {

	}

	@Override
	public PageBean getEntity() {
		return delegate.getEntity();
	}

}
