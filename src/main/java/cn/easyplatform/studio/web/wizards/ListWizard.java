/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.wizards;

import cn.easyplatform.entities.beans.list.Header;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Nums;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.web.editors.GridEditorCallback;
import cn.easyplatform.studio.web.editors.TableInfo;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListWizard implements Composer<Component>, IWizard<ListBean>,
		EventListener<Event> {

	private Grid canvas;

	private TableInfo selectedTable;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		A choose = (A) comp.getFirstChild().getFirstChild();
		choose.setId("listWizard_a_choose");
		choose.addEventListener(Events.ON_CLICK, this);
		canvas = (Grid) comp.getLastChild();
		canvas.setId("listWizard_grid_canvas");
		canvas.addEventListener(Events.ON_CHANGE, this);
		comp.setAttribute("", this);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		Component c = event.getTarget();
		if (c == canvas) {
			selectedTable = (TableInfo) event.getData();
			redraw();
		} else if (c instanceof A) {
			AbstractView.createTableView(
					new GridEditorCallback(canvas,
							Labels.getLabel("wizard.table")), true, c)
					.doOverlapped("right,center");
		} else if (c.getParent() instanceof Column) {
			setColumnProperty(c);
		}
	}

	private void setColumnProperty(Component c) {
		Column col = (Column) c.getParent();
		if (c instanceof Combobox) {
			col.setAlign(((Combobox) c).getSelectedItem().getLabel());
		} else {
			Textbox tbx = (Textbox) c;
			if (!Strings.isBlank(tbx.getValue())) {
				int width = Nums.toInt(tbx.getValue(), 0);
				if (width > 0)
					col.setWidth(width + "px");
				else
					col.setWidth(tbx.getValue());
			} else {
				col.setWidth(null);
			}
		}
		col.invalidate();
	}

	private void redraw() {
		canvas.getColumns().getChildren().clear();
		for (TableField tf : selectedTable.getFields()) {
			Column col = new Column();
			col.setTooltiptext(tf.getName());
			col.appendChild(new Label(tf.getDescription()));
			canvas.getColumns().appendChild(col);
		}
		canvas.setVisible(true);
	}

	@Override
	public ListBean getEntity() {
		if (selectedTable == null)
			return null;
		ListBean bean = new ListBean();
		bean.setTable(selectedTable.getTable());
		List<Header> headers = new ArrayList<Header>();
		for (TableField tf : selectedTable.getFields()) {
			Header header = new Header();
			header.setField(tf.getName());
			header.setName(tf.getName());
			header.setTitle(tf.getDescription());
			header.setType(tf.getType());
			headers.add(header);
		}
		bean.setHeaders(headers);
		return bean;
	}
}
