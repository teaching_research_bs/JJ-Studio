/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.wizards.template;

import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.studio.web.wizards.IWizard;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Composer;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractTemplate implements Composer<Component>,
		EventListener<Event>, IWizard<PageBean> {

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		initComponents(comp);
		comp.setAttribute("", this);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		dispatch(event);
	}

	protected abstract void dispatch(Event evt);

	protected abstract void initComponents(Component root);
}
