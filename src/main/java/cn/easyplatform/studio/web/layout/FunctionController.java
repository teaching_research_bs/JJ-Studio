package cn.easyplatform.studio.web.layout;

public interface FunctionController {
    void refreshGuide();

    void setWorkbench(WorkbenchController workbench);

    void setMenuitem(int state);

}
