/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.layout;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.studio.web.action.CreateAction;
import cn.easyplatform.studio.web.controller.MainLayoutController;
import cn.easyplatform.studio.web.editors.Editor;
import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface WorkbenchController {

	/**
	 * 快捷键
	 * alt^1表示新增2表示保存3表示保存所有4打开表窗口
	 * @param keyCode
	 */
	void onCtrlKeys(int keyCode);
	
	/**
	 * 创建新的参数编辑器
	 * @param entity
	 * @param code
	 * @param args
	 */
	<T extends BaseEntity> void createEditor(T entity, char code,
			Object... args);

	/**
	 * 创建新的参数编辑器
	 * @param entity
	 * @param code
	 * @param listener
	 * @param args
	 */
	<T extends BaseEntity> void createEditor(T entity, char code,
											 CreateAction.OnSuccessListener listener, Object... args);

	/**
	 * 校验并打开新的参数编辑器
	 * @param entityId
	 */
	void openEntity(String entityId);
	/**
	 * 添加到工作区
	 * @param tab
	 * @param tabpanel
	 */
	void addEditor(Tab tab, Tabpanel tabpanel);

	/**
	 * 检查编辑器是否存在
	 * @param id
	 * @return
	 */
	Editor checkEditor(String id);
		
	/**
	 * 设置工具栏菜单的状态
	 * @param name
	 * @param state
	 */
	void setDisabled(String name, boolean state);
	
	/**
	 * 获取侧边栏
	 * @return
	 */
	NavbarController getEntityBar();

	/**
	 * 获取window侧边栏
	 * @return
	 */
	NavbarController getEntityWindowBar();
	
	/**
	 * 获取导航栏
	 * @return
	 */
	HeaderController getHeader();
	
	/**
	 * 返回当前zk 页面对象
	 * @return
	 */
	Page getPage();

	/**
	 * 返回主页面
	 * @return
	 */
	MainLayoutController getMainLayout();

	/**
	 * 获取快捷栏
	 * @return
	 */
	HeaderController getShortcutKeyBar();

	/**
	 * 获取功能区
	 * @return
	 */
	FunctionController getFunctionBar();
}
