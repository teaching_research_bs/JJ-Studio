package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.web.action.CreateAction;
import cn.easyplatform.studio.web.layout.HeaderController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ShortcutKeyControllerImpl implements HeaderController,
        EventListener<Event> {
    private WorkbenchController workbench;

    private Map<String, Div> actionComponents;

    public ShortcutKeyControllerImpl() {
        actionComponents = new HashMap<>();
    }

    public void createEditor() {
        workbench.getShortcutKeyBar().setDisabled("shortcutKey_div_create",true);
        new CreateAction(workbench, new CreateAction.OnCreateListener() {

            @Override
            public void closeWindow() {
                workbench.getShortcutKeyBar().setDisabled("shortcutKey_div_create",false);
            }
        }).on();
    }

    public Map<String, Div> getMenuitem() {
        return actionComponents;
    }

    public void bindComponents(Component root) {
        HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
        String userAgent = request.getHeader("User-Agent").toLowerCase();

        String[] idStrings = new String[]{"shortcutKey_label_create", "shortcutKey_label_save",
                "shortcutKey_label_saveAll", "shortcutKey_label_table", "shortcutKey_label_task",
                "shortcutKey_label_widget", "shortcutKey_label_code", "shortcutKey_label_eChart"};
        String[] winStrings = new String[]{"Alt+1", "Alt+2", "Alt+3", "Alt+4", "Alt+5", "Alt+6", "Alt+W", "Alt+Q"};
        String[] macStrings = new String[]{"^+1", "^+2", "^+3", "^+4", "^+5", "^+6", "^+W", "^+Q"};

        for (int i = 0; i < idStrings.length; i++) {
            String value = winStrings[i];
            if (userAgent.contains("mac" )) {
                value = macStrings[i];
            }
            ((Label) root.getSpaceOwner().getFellow(idStrings[i])).setValue(value);
        }


        for (Component c : root.getSpaceOwner().getFellows()) {
            if (c instanceof Div && Strings.isBlank(c.getId()) == false) {
                actionComponents.put(c.getId(), (Div) c);
                c.addEventListener(Events.ON_CLICK, this);
                if (c.getId().equals("shortcutKey_div_create")) {
                    setDivDisabled((Div) c, !Contexts.getUser().isAuthorized("create"));
                } else if (c.getId().equals("shortcutKey_div_save") || c.getId().equals("shortcutKey_div_saveAll"))
                    setDivDisabled((Div) c, true);
                else
                    setDivDisabled((Div) c, false);
            }
        }
        setMenuitem(0);
    }
    @Override
    public void setDisabled(String name, boolean disabled) {
        Div c = actionComponents.get(name);
        if (c != null)
            setDivDisabled(c, disabled);
    }

    @Override
    public void setMenuitem(int state) {
        setDivDisabled(actionComponents.get("shortcutKey_div_code"), state < 2);
        setDivDisabled(actionComponents.get("shortcutKey_div_widget"), state < 2);
        setDivDisabled(actionComponents.get("shortcutKey_div_eChart"), state < 2);
    }

    @Override
    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        A iconA = (A) event.getTarget().getChildren().get(0);
        boolean isDisabled = iconA.isDisabled();
        if (event.getTarget().getId().equals("shortcutKey_div_create") && isDisabled == false) {
            ((ShortcutKeyControllerImpl)workbench.getShortcutKeyBar()).createEditor();
        } else if (event.getTarget().getId().equals("shortcutKey_div_save") && isDisabled == false) {
            ((WorkbenchControllerImpl)workbench).save();
        } else if (event.getTarget().getId().equals("shortcutKey_div_saveAll") && isDisabled == false) {
            ((WorkbenchControllerImpl)workbench).saveAll();
        } else if (event.getTarget().getId().equals("shortcutKey_div_dataBase") && isDisabled == false) {
            workbench.onCtrlKeys(52);
        } else if (event.getTarget().getId().equals("shortcutKey_div_task") && isDisabled == false) {
            workbench.onCtrlKeys(53);
        } else if (event.getTarget().getId().equals("shortcutKey_div_widget") && isDisabled == false) {
            ((WorkbenchControllerImpl)workbench).openwidgetWin();
        } else if (event.getTarget().getId().equals("shortcutKey_div_code") && isDisabled == false) {
            ((WorkbenchControllerImpl)workbench).formatAll();
        } else if (event.getTarget().getId().equals("shortcutKey_div_eChart") && isDisabled == false) {
            ((WorkbenchControllerImpl)workbench).openEchartWin();
        }
    }

    //UI
    private void setDivDisabled(Div div, Boolean disabled) {
        if (div != null) {
            A iconA = (A) div.getChildren().get(0);
            if (disabled == true) {
                div.setSclass("ui-shortcutKeyBar-layout-disable");
                //div.removeEventListener(Events.ON_CLICK, this);
                iconA.setDisabled(true);
            }
            else {
                div.setSclass("ui-shortcutKeyBar-layout");
                //div.addEventListener(Events.ON_CLICK, this);
                iconA.setDisabled(false);
            }
        }
    }
}
