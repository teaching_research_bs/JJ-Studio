/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.layout;

import cn.easyplatform.studio.vos.EntityVo;


/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface NavbarController {

	void setWorkbench(WorkbenchController workbench);
	
	EntityVo getSelectedEntity();

	void refreshAll();

	void refreshModule();

	void refreshEntity();
}
