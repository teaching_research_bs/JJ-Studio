/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.biz.UpdateCmd;
import cn.easyplatform.studio.cmd.entity.GetAndLockEntityCmd;
import cn.easyplatform.studio.cmd.entity.PagingCmd;
import cn.easyplatform.studio.cmd.entity.QueryCmd;
import cn.easyplatform.studio.cmd.entity.SaveCmd;
import cn.easyplatform.studio.cmd.link.DeleteLinkCmd;
import cn.easyplatform.studio.cmd.link.GetChildrenLinkCmd;
import cn.easyplatform.studio.cmd.link.UpdateLinkListCmd;
import cn.easyplatform.studio.cmd.taskLink.*;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.*;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EntityEditor;
import cn.easyplatform.studio.web.layout.NavbarController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.web.ext.zul.TreeExt;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.ZulEvents;

import java.util.ArrayList;
import java.util.List;

import static cn.easyplatform.studio.vos.TypeLinkVo.ROOTENTITYID;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class NavbarControllerImpl implements NavbarController,
        EventListener<Event> {

    private WorkbenchController workbench;

    private Listbox entityGrid;

    private Paging entityPaging;

    private Bandbox entitySearch;

    //private West navbar;

    private Menupopup epentitytypemenu;

    private Tree moduleTree;

    private Paging modulePaging;

    private Bandbox moduleSearch;

    private QueryTypeLinkVo result;

    /*private static List<LinkVo> linkVoList = new ArrayList<>();

    private static List<LinkVo> dataLinkVoList = new ArrayList<>();

    private static List<TypeLinkVo> dataTypeLinkVoList = new ArrayList<>();

    public static synchronized List<LinkVo> getLinkVoList(){
        return linkVoList;
    }

    public static synchronized void setLinkVoList(List<LinkVo> linkVoList)
    {
        NavbarControllerImpl.linkVoList = linkVoList;
    }

    public static synchronized List<LinkVo> getDataLinkVoList() {
        return dataLinkVoList;
    }

    public static synchronized void setDataLinkVoList(List<LinkVo> dataLinkVoList) {
        NavbarControllerImpl.dataLinkVoList = dataLinkVoList;
    }

    public static synchronized List<TypeLinkVo> getDataTypeLinkVoList() {
        return dataTypeLinkVoList;
    }

    public static synchronized void setDataTypeLinkVoList(List<TypeLinkVo> dataTypeLinkVoList) {
        NavbarControllerImpl.dataTypeLinkVoList = dataTypeLinkVoList;
    }*/

    public void bindComponents(Component root) {
        //createVersion();
        modulePaging = (Paging) root.getFellow("navbar_paging_modulePaging");
        moduleSearch = (Bandbox) root.getFellow("navbar_bandbox_moduleSearch");
        moduleTree = (Tree) root.getFellow("navbar_tree_moduleTree");
        for (Component c : root.getSpaceOwner().getFellows()) {
            if (c.getId().equals("navbar_bandbox_entitySearch")) {
                this.entitySearch = (Bandbox) c;
                this.entitySearch.addEventListener(Events.ON_OK, this);
                this.entitySearch.addEventListener(Events.ON_OPEN, this);
            } else if (c.getId().equals("navbar_paging_entityPaging")) {
                entityPaging = (Paging) c;
            } else if (c.getId().equals("navbar_listbox_entityGrid")) {
                entityGrid = (Listbox) c;
                QueryResultVo result = StudioApp.execute(entityGrid,
                        new QueryCmd(entityPaging.getPageSize()));
                this.entityPaging.setTotalSize(result.getTotalSize());
                this.entityPaging.addEventListener(ZulEvents.ON_PAGING, this);
                redraw(result.getEntities());
            } else if (c.getId().equals("navbar_menupopup_epentitymenu")) {
                epentitytypemenu = (Menupopup) c;
                for (Component child : c.getChildren()) {
                    if (child instanceof Menuitem)
                        child.addEventListener(Events.ON_CLICK, this);
                }
            } else if (c.getId().equals("navbar_bandbox_moduleSearch")) {
                moduleSearch.addEventListener(Events.ON_OK, this);
                moduleSearch.addEventListener(Events.ON_OPEN, this);
            } else if (c.getId().equals("navbar_tree_moduleTree")) {
                result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", null, modulePaging.getPageSize(),1));
                redrawList(result.getLinkVoList(), true, "");
                modulePaging.setTotalSize(result.getTotalSize());
                modulePaging.addEventListener(ZulEvents.ON_PAGING, this);
                moduleTree.addEventListener(Events.ON_DOUBLE_CLICK, this);
            }
        }
    }

    /*private void createVersion() {
        try {
            StudioApp.execute(new InitCmd());
        } catch (Exception ex) {
        }
    }

    public void setNavbar(West navbar) {
        this.navbar = navbar;
    }

    public void close() {
        navbar.setOpen(false);
    }*/

    private void redraw(List<EntityVo> data) {
        entityGrid.getItems().clear();
        for (EntityVo vo : data) {
            Listitem row = new Listitem();
            row.setValue(vo);
            row.appendChild(new Listcell(vo.getId()));
            row.appendChild(new Listcell(vo.getName()));
            row.appendChild(new Listcell(vo.getType()));
            row.appendChild(new Listcell(vo.getDesp()));
            row.setDraggable("treeItem");
            row.setParent(this.entityGrid);
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    public void refreshAll()
    {
        moduleSearch.setValue(null);
        result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", null, modulePaging.getPageSize(),1));
        redrawList(result.getLinkVoList(), true, "");
        modulePaging.setTotalSize(result.getTotalSize());
        modulePaging.setActivePage(0);
        refreshEntity();
    }

    public void refreshModule()
    {
        moduleSearch.setValue(null);
        result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", null, modulePaging.getPageSize(),1));
        redrawList(result.getLinkVoList(), true, "");
        modulePaging.setTotalSize(result.getTotalSize());
        modulePaging.setActivePage(0);
    }

    private void redrawList(List<TypeLinkVo> list, boolean isInit, String rootTypeLinkId) {
        moduleTree.clear();
        org.zkoss.zul.Treechildren tree;
        if (null == moduleTree.getTreechildren()) {
            tree = new org.zkoss.zul.Treechildren();
            moduleTree.appendChild(tree);
        } else {
            tree = moduleTree.getTreechildren();
        }
        List<String> children = new ArrayList<>();
        children.add(rootTypeLinkId);
        allTreeChildren(tree, list, isInit, children);
    }

    private org.zkoss.zul.Treechildren allTreeChildren(org.zkoss.zul.Treechildren tree, List<TypeLinkVo> list,
                                                       boolean isInit, List<String> childrenList) {
        for (TypeLinkVo vo : list) {
            org.zkoss.zul.Treeitem item = new org.zkoss.zul.Treeitem();
            item.setValue(vo);
            org.zkoss.zul.Treerow row = new org.zkoss.zul.Treerow();
            row.appendChild(new org.zkoss.zul.Treecell(vo.getEntityId()));
            row.appendChild(new org.zkoss.zul.Treecell(vo.getEntityName()));
            row.appendChild(new org.zkoss.zul.Treecell(vo.getEntityDesp()));
            item.appendChild(row);

            boolean isAddROOT = false;
            if (!isInit && null != vo.getChildrenList() && vo.getChildrenList().size() != 0) {
                if (childrenList.contains(vo.getEntityId())) {
                    List<String> children = new ArrayList<>();
                    for (TypeLinkVo typeLinkVo: vo.getChildrenList()) {
                        children.add(typeLinkVo.getEntityId());
                    }
                    org.zkoss.zul.Treechildren resultTree = allTreeChildren(new org.zkoss.zul.Treechildren(),
                            vo.getChildrenList(), isInit, children);
                    item.appendChild(resultTree);
                } else {
                    isAddROOT = true;
                }
            } else {
                isAddROOT = true;
            }

            if (isAddROOT == true) {
                if (vo.isRoot().equals(ROOTENTITYID)) {
                    org.zkoss.zul.Treechildren childrenTree = new org.zkoss.zul.Treechildren();
                    item.appendChild(childrenTree);
                    item.setOpen(false);
                    item.addEventListener(Events.ON_OPEN, this);
                }
            }
            tree.appendChild(item);
        }
        return tree;
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() == entityPaging) {// 分页
            PagingEvent pe = (PagingEvent) event;
            int pageNo = pe.getActivePage() + 1;
            String field = null;
            if (!Strings.isBlank(entitySearch.getValue()))
                field = "*";
            List<EntityVo> data = StudioApp.execute(entityPaging,
                    new PagingCmd(getTypes(), field, entitySearch.getValue()
                            .trim(), entityPaging.getPageSize(), pageNo));
            redraw(data);
        } else if (event.getTarget() instanceof Listitem) {
            Listitem selectedItem = (Listitem)event.getTarget();
            EntityVo vo = selectedItem.getValue();// 打开参数
            openEnitity(vo.getId());
        } else if (event.getTarget() == entitySearch) {// 查询
            String val = null;
            if (event instanceof OpenEvent) {
                OpenEvent evt = (OpenEvent) event;
                val = (String) evt.getValue();
                entitySearch.setValue(val);
            } else
                val = entitySearch.getValue();
            String type = getTypes();
            QueryResultVo result = null;
            if (Strings.isBlank(val)) {
                result = StudioApp.execute(entityGrid, new QueryCmd(type,
                        entityPaging.getPageSize()));
            } else {
                result = StudioApp.execute(entityGrid, new QueryCmd(type, "*",
                        val.trim(), entityPaging.getPageSize()));
            }
            this.entityPaging.setTotalSize(result.getTotalSize());
            this.entityPaging.setActivePage(0);
            redraw(result.getEntities());
        } else if (event.getName().equals(Messagebox.ON_YES)) {// 删除
            EntityVo vo = getSelectedEntity();
            GetEntityVo ge = StudioApp.execute(event.getTarget(),
                    new GetAndLockEntityCmd(vo.getId()));
            if (ge != null) {
                if (!Strings.isBlank(ge.getLockUser())) {
                    Messagebox.show(Labels.getLabel("enitiy.locked",
                            new String[]{ge.getLockUser()}), Labels
                                    .getLabel("message.title.error"),
                            Messagebox.OK, Messagebox.INFORMATION);
                } else {
                    EntityInfo ei = new EntityInfo();
                    ei.setId(getSelectedEntity().getId());
                    ei.setStatus('D');
                    StudioApp.execute(new SaveCmd(ei));
                    if ("Table".equals(getSelectedEntity().getType()))
                        StudioApp.execute(new UpdateCmd(getSelectedEntity().getSubType(), "delete from sys_table_field_info where table1id=" + "'" + getSelectedEntity().getId() + "'", null));
                    refreshEntity();
                    Editor editor = workbench.checkEditor(ei.getId());
                    if (editor != null && editor instanceof EntityEditor<?>)
                        ((EntityEditor<?>) editor).close();

                    //更新link
                    LinkVo deleteVo = new LinkVo();
                    deleteVo.setEntityId(ei.getId());
                    StudioApp.execute(new DeleteLinkCmd(deleteVo));
                    StudioApp.execute(new DeleteTaskLinkCmd(new TypeLinkVo(deleteVo.getEntityId(), null, null)));
                    //更新跟删除有关的关系
                    List<LinkVo> updateVos = StudioApp.execute(new GetChildrenLinkCmd(deleteVo.getEntityId()));
                    if (updateVos != null && updateVos.size() > 0) {
                        for (LinkVo updateVo: updateVos) {
                            String childrenStr = StringUtil.removeStringToListString(updateVo.getChildrenEntityId(),
                                    ",", deleteVo.getEntityId());
                            updateVo.setChildrenEntityId(childrenStr);
                        }
                        StudioApp.execute(new UpdateLinkListCmd(updateVos, deleteVo.getEntityId()));
                    }
                    List<TypeLinkVo> updateLinkVos = StudioApp.execute(new GetChildrenTaskLinkCmd(deleteVo.getEntityId()));
                    if (updateLinkVos != null && updateLinkVos.size() > 0) {
                        for (TypeLinkVo updateVo: updateLinkVos) {
                            String childrenStr = StringUtil.removeStringToListString(updateVo.getChildrenEntityId(),
                                    ",", deleteVo.getEntityId());
                            updateVo.setChildrenEntityId(childrenStr);
                        }
                        StudioApp.execute(new UpdateTaskLinkListCmd(updateLinkVos, deleteVo.getEntityId()));
                    }
                }
            }
        } else if (event.getTarget() instanceof Menuitem) {
            Menuitem mi = (Menuitem) event.getTarget();
            if (Strings.isBlank(mi.getTooltiptext())) {
                for (Component child : mi.getParent().getChildren()) {
                    if (child instanceof Menuitem)
                        ((Menuitem) child).setChecked(mi.isChecked());
                }
            }
            String type = getTypes();
            QueryResultVo result = StudioApp.execute(entityGrid,
                    new QueryCmd(type, "*", entitySearch.getValue().trim(),
                            entityPaging.getPageSize()));
            this.entityPaging.setTotalSize(result.getTotalSize());
            this.entityPaging.setActivePage(0);
            redraw(result.getEntities());
        } else if (event.getTarget() instanceof TreeExt) {
            Treeitem selectedItem = ((TreeExt) event.getTarget()).getSelectedItem();
            TypeLinkVo vo = selectedItem.getValue();
            openEnitity(vo.getEntityId());
        } else if (event.getTarget() == modulePaging) {
            PagingEvent pe = (PagingEvent) event;
            int pageNo = pe.getActivePage() + 1;
            String field = null;
            if (!Strings.isBlank(moduleSearch.getValue()))
                field = "*";
            result = StudioApp.execute(modulePaging,
                    new PagingTaskLinkCmd(field, moduleSearch.getValue()
                            .trim(), modulePaging.getPageSize(), pageNo));
            redrawList(result.getLinkVoList(), false, "");
        } else if (event.getTarget() == moduleSearch) {// 查询
            String val = null;
            if (event instanceof OpenEvent) {
                OpenEvent evt = (OpenEvent) event;
                val = (String) evt.getValue();
                moduleSearch.setValue(val);
            } else
                val = moduleSearch.getValue();
            if (Strings.isBlank(val)) {
                result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", null, modulePaging.getPageSize(), 1));
            } else {
                result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", val
                        .trim(), modulePaging.getPageSize(), 1));
            }
            modulePaging.setTotalSize(result.getTotalSize());
            modulePaging.setActivePage(0);
            redrawList(result.getLinkVoList(), true, "");
        } else if (event.getTarget() instanceof Treeitem) {
            Treeitem item = (Treeitem) event.getTarget();
            TypeLinkVo vo = item.getValue();
            result = StudioApp.execute(item, new RootTaskLinkCmd(result, vo));
            redrawList(result.getLinkVoList(), false, vo.getEntityId());
        }
    }

    private String getTypes() {
        StringBuilder sb = new StringBuilder();
        int size = epentitytypemenu.getChildren().size() - 2;
        for (int i = 0; i < size; i++) {
            Menuitem mi = (Menuitem) epentitytypemenu.getChildren().get(i);
            if (mi.isChecked())
                sb.append(mi.getTooltiptext()).append(",");
        }
        if (sb.length() > 0)
            sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
    private void openEnitity(String entityId)
    {
        if (workbench.checkEditor(entityId) == null) {
            GetEntityVo ge = StudioApp.execute(new GetAndLockEntityCmd(entityId));
            if (ge != null) {
                char code = 'U';
                if (!Strings.isBlank(ge.getLockUser())) {
                    code = 'R';
                    Messagebox.show(Labels.getLabel("enitiy.locked",
                            new String[]{ge.getLockUser()}), Labels
                                    .getLabel("message.title.error"),
                            Messagebox.OK, Messagebox.INFORMATION);
                }
                workbench.createEditor(ge.getEntity(), code);
            }
        }
    }
    @Override
    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }

    @Override
    public void refreshEntity() {
        String field = null;
        if (!Strings.isBlank(entitySearch.getValue()))
            field = "*";
        List<EntityVo> data = StudioApp.execute(
                entityPaging,
                new PagingCmd(getTypes(), field, entitySearch.getValue(),
                        entityPaging.getPageSize(), entityPaging
                        .getActivePage()));
        redraw(data);
    }

    @Override
    public EntityVo getSelectedEntity() {
        if (entityGrid.getSelectedItem() == null)
            return null;
        EntityVo ev = entityGrid.getSelectedItem().getValue();
        return ev;
    }

    // //////////////////////右键菜单事件//////////////////////////
    public void copy() {
        EntityVo copyObj = getSelectedEntity();
        if (copyObj == null)
            return;
        //判断是否有权限
        if (!StudioUtil.isAddPermission(copyObj)) {
            WebUtils.showError(Labels.getLabel("access.add.error",
                    new String[]{copyObj.getType()}));
            return;
        }
        StudioUtil.showInputDialog(workbench, entityGrid.getPage(), copyObj.getId(), true);
    }

    public void export() {
        EntityVo copyObj = getSelectedEntity();
        if (copyObj == null)
            return;
        //判断是否有权限
        if (!Contexts.getUser().isAuthorized("repoExport")) {
            WebUtils.showError(Labels.getLabel("access.export.error",
                    new String[]{copyObj.getType()}));
            return;
        }
        StudioUtil.showInputDialog(workbench, entityGrid.getPage(), copyObj.getId(), false);
    }

    public void delete() {
        EntityVo ev = getSelectedEntity();
        if (ev == null)
            return;
        //判断是否有权限
        if (!StudioUtil.isDeletePermission(ev)) {
            WebUtils.showError(Labels.getLabel("access.delete.error", new String[]{ev.getType()}));
            return;
        }
        Messagebox.show(
                Labels.getLabel("message.confirm",
                        new String[]{Labels.getLabel("button.delete") + "["
                                + ev.getId() + "]"}),
                Labels.getLabel("message.title.confirm"), Messagebox.YES
                        | Messagebox.NO, Messagebox.QUESTION, this);
    }

    public void showHistory() {
        EntityVo vo = entityGrid.getSelectedItem().getValue();
        StudioUtil.showHistory(workbench, vo);
    }
}