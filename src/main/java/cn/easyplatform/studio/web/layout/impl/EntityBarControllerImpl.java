package cn.easyplatform.studio.web.layout.impl;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.biz.UpdateCmd;
import cn.easyplatform.studio.cmd.entity.GetAndLockEntityCmd;
import cn.easyplatform.studio.cmd.entity.PagingCmd;
import cn.easyplatform.studio.cmd.entity.QueryCmd;
import cn.easyplatform.studio.cmd.entity.SaveCmd;
import cn.easyplatform.studio.cmd.link.DeleteLinkCmd;
import cn.easyplatform.studio.cmd.link.GetChildrenLinkCmd;
import cn.easyplatform.studio.cmd.link.UpdateLinkListCmd;
import cn.easyplatform.studio.cmd.taskLink.*;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.*;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EntityEditor;
import cn.easyplatform.studio.web.layout.NavbarController;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.web.ext.zul.TreeExt;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.IdSpace;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;
import org.zkoss.zul.event.PagingEvent;
import org.zkoss.zul.event.ZulEvents;

import java.util.ArrayList;
import java.util.List;

import static cn.easyplatform.studio.vos.TypeLinkVo.ROOTENTITYID;

public class EntityBarControllerImpl implements NavbarController,
        EventListener<Event> {

    private Hlayout tabHlayout;
    private Div entityTab;
    private Div moduleTab;
    private Div dateTableTab;
    private Borderlayout entityBorderlayout;
    private Borderlayout moduleBorderlayout;
    private Borderlayout dateTableBorderlayout;
    private Tree moduleTree;
    private Listbox entityListbox;
    private Listbox dateTableListbox;
    //private Button moduleSearchButton;
    private Paging paging;
    private Paging modulePaging;
    private Paging dateTablePaging;
    private Textbox searchTextbox;
    private Textbox searchModuleTextbox;
    private Textbox searchdateTableTextbox;
    private Combobox typeCombobox;
    private Menupopup epEntityTypeMenu;
    private Div pegDiv;
    //private Div modulePegDiv;
    private WorkbenchController workbench;
    private Div closeDiv;

    private boolean isWindow;
    //private FunctionType functionType;
    private FunctionType showFunctionType;
    private QueryTypeLinkVo result;
    private String searchStr;
    private boolean isNeedRefreshEntityData = true;
    private boolean isNeedRefreshModuleData = true;
    private boolean isNeedRefreshDateTableData = true;

    public OnClickListener clickListener;

    public void setSearchStr(String searchStr) {
        this.searchStr = searchStr;
        if (showFunctionType == FunctionType.FunctionEntity && searchTextbox != null) {
            searchTextbox.setValue(searchStr);
            search("*");
        } else if (showFunctionType == FunctionType.FunctionModule && searchModuleTextbox != null) {
            searchModuleTextbox.setValue(searchStr);
            search("*");
        }else if(showFunctionType == FunctionType.FunctionDateTable && searchdateTableTextbox != null){
            searchdateTableTextbox.setValue(searchStr);
            search("*");
        }
    }

    public interface OnClickListener {
        void onShowEntityBar();
        void onVisible();
        void onShowNewEntity();
    }

    public enum FunctionType {
        FunctionEntity, FunctionModule,FunctionDateTable, FunctionAll, FunctionNone
    }

    public EntityBarControllerImpl (FunctionType functionType, boolean isWindow) {
        this.isWindow = isWindow;
        showFunctionType(functionType);
    }
    @Override
    public void setWorkbench(WorkbenchController workbench) {
        this.workbench = workbench;
    }

    @Override
    public EntityVo getSelectedEntity() {
        if(showFunctionType == FunctionType.FunctionEntity && entityListbox.getSelectedItem() != null){
            EntityVo ev = entityListbox.getSelectedItem().getValue();
            return ev;
        } else if((showFunctionType == FunctionType.FunctionDateTable&& dateTableListbox.getSelectedItem() != null)) {
            EntityVo evd = dateTableListbox.getSelectedItem().getValue();
            return evd;
        } else {
            return null;
        }
    }

    @Override
    public void refreshAll() {
        refreshModule();
        refreshEntity();
    }

    @Override
    public void refreshEntity() {
        if (entityListbox != null) {
            String field = null;
            if (!Strings.isBlank(searchTextbox.getValue()))
                field = "*";
            List<EntityVo> data = StudioApp.execute(paging,
                    new PagingCmd(getTypes(), field, searchTextbox.getValue()
                            .trim(), paging.getPageSize(), paging.getActivePage() + 1));
            redrawEntity(data);
        }
        if (dateTableListbox != null) {
            String field = null;
            if (!Strings.isBlank(searchdateTableTextbox.getValue()))
                field = "*";
            List<EntityVo> list = StudioApp.execute(dateTablePaging,
                    new PagingCmd("table", field, searchTextbox.getValue()
                            .trim(), paging.getPageSize(), paging.getActivePage() + 1));
            redrawDateTable(list);
        }
    }

    @Override
    public void refreshModule() {
        if (moduleTree != null) {
            result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", searchModuleTextbox.getValue().trim(),
                    modulePaging.getPageSize(),modulePaging.getActivePage() + 1));
            redrawModule(result.getLinkVoList(), true, "");
        }
    }

    @Override
    public void onEvent(Event event) throws Exception {
        if (event.getTarget() instanceof Listitem) {
            Listitem selectedItem = (Listitem)event.getTarget();
            EntityVo vo = selectedItem.getValue();// 打开参数
            openEntity(vo.getId());
            if (clickListener != null)
                clickListener.onShowNewEntity();
        } else if (event.getTarget().equals(entityTab)) {
            showFunctionType(FunctionType.FunctionEntity);
            //isNeedRefreshEntityData = false;
            createContent();
        } else if (event.getTarget().equals(moduleTab)) {
            showFunctionType(FunctionType.FunctionModule);
            //isNeedRefreshModuleData = false;
            createContent();
        }else if (event.getTarget().equals(dateTableTab)) {
            showFunctionType(FunctionType.FunctionDateTable);
            //isNeedRefreshModuleData = false;
            createContent();
        } else if (event.getTarget().equals(paging)|| event.getTarget().equals(modulePaging)||
                event.getTarget().equals(dateTablePaging)){// 分页
            PagingEvent pe = (PagingEvent) event;
            int pageNo = pe.getActivePage() + 1;
            String field = null;
            if (event.getTarget().equals(paging)) {
                if (!Strings.isBlank(searchTextbox.getValue()))
                    field = "*";
                List<EntityVo> data = StudioApp.execute(paging,
                        new PagingCmd(getTypes(), field, searchTextbox.getValue()
                                .trim(), paging.getPageSize(), pageNo));
                redrawEntity(data);
            } else if(event.getTarget().equals(modulePaging)){
                if (!Strings.isBlank(searchModuleTextbox.getValue()))
                    field = "*";
                result = StudioApp.execute(modulePaging,
                        new PagingTaskLinkCmd(field, searchModuleTextbox.getValue()
                                .trim(), modulePaging.getPageSize(), pageNo));
                redrawModule(result.getLinkVoList(), false, "");
            } else {
                if (!Strings.isBlank(searchdateTableTextbox.getValue()))
                    field = "*";
                List<EntityVo> list = StudioApp.execute(paging,
                        new PagingCmd("table", field, searchdateTableTextbox.getValue().trim(), dateTablePaging.getPageSize(), pageNo));
                redrawDateTable(list);
            }
        } else if ((event.getTarget().equals(searchTextbox) || event.getTarget().equals(searchModuleTextbox)||
                event.getTarget().equals(searchdateTableTextbox)) && event.getName().equals(Events.ON_OK)) {
            if (event instanceof OpenEvent) {
                OpenEvent evt = (OpenEvent) event;
                String val = (String) evt.getValue();
                ((Textbox) event.getTarget()).setValue(val);
            }
            search("*");
        } else if (event.getTarget() instanceof TreeExt) {
            Treeitem selectedItem = ((TreeExt) event.getTarget()).getSelectedItem();
            TypeLinkVo vo = selectedItem.getValue();
            openEntity(vo.getEntityId());
            if (clickListener != null)
                clickListener.onShowNewEntity();
        } else if (event.getTarget() instanceof Treeitem) {
            Treeitem item = (Treeitem) event.getTarget();
            TypeLinkVo vo = item.getValue();
            result = StudioApp.execute(item, new RootTaskLinkCmd(result, vo));
            redrawModule(result.getLinkVoList(), false, vo.getEntityId());
        } else if (event.getName().equals(Events.ON_CHANGE)) {
            search("*");
        } else if (event.getTarget().equals(pegDiv)/* || event.getTarget().equals(modulePegDiv)*/) {
            if (clickListener != null) {
                clickListener.onShowEntityBar();
            }
            changePegDiv();
        } else if (event.getTarget().equals(closeDiv)) {
            if (clickListener != null)
                clickListener.onVisible();
        }
    }

    //UI
    public void bindComponents(Component root) {
        IdSpace is = root.getSpaceOwner();
        tabHlayout = (Hlayout) is.getFellow(isWindow? "entityBarWindow_hlayout_tab": "entityBar_hlayout_tab");
        moduleTree = (Tree) is.getFellow(isWindow? "entityBarWindow_tree_moduleTree": "entityBar_tree_moduleTree");
        dateTableListbox = (Listbox) is.getFellow("entityBar_listbox_dateTable");
        entityBorderlayout = (Borderlayout) is.getFellow(isWindow? "entityBarWindow_borderlayout_entity": "entityBar_borderlayout_entity");
        moduleBorderlayout = (Borderlayout) is.getFellow(isWindow? "entityBarWindow_borderlayout_module": "entityBar_borderlayout_module");
        dateTableBorderlayout = (Borderlayout) is.getFellow("entityBar_borderlayout_dateTable");
        entityListbox = (Listbox) is.getFellow(isWindow? "entityBarWindow_listbox_entity": "entityBar_listbox_entity");
        paging = (Paging) is.getFellow(isWindow? "entityBarWindow_paging_paging": "entityBar_paging_paging");
        modulePaging = (Paging) is.getFellow(isWindow? "entityBarWindow_paging_modulePaging": "entityBar_paging_modulePaging");
        dateTablePaging = (Paging) is.getFellow("entityBar_paging_dateTablePaging");
        searchTextbox = (Textbox) is.getFellow(isWindow? "entityBarWindow_textbox_search": "entityBar_textbox_search");
        searchTextbox.addEventListener(Events.ON_OK, this);
        searchdateTableTextbox = (Textbox) is.getFellow("entityBar_textbox_dateTableSearch");
        searchdateTableTextbox.addEventListener(Events.ON_OK, this);
        typeCombobox = (Combobox) is.getFellow("entityBar_combobox_epentity");
        typeCombobox.setValue(Labels.getLabel("button.all"));
        typeCombobox.addEventListener(Events.ON_CHANGE, this);
        //allCheckbox = (Checkbox) is.getFellow(isWindow? "entityBarWindow_checkbox_all": "entityBar_checkbox_all");
        //allCheckbox.addEventListener(Events.ON_CHECK, this);
        //moduleSearchButton = (Button) is.getFellow(isWindow? "entityBarWindow_button_moduleSearch": "entityBar_button_moduleSearch");
        //moduleSearchButton.addEventListener(Events.ON_CLICK, this);
        pegDiv = (Div) is.getFellow(isWindow? "entityBarWindow_div_peg": "entityBar_div_peg");
        pegDiv.addEventListener(Events.ON_CLICK, this);
        /*modulePegDiv = (Div) is.getFellow(isWindow? "entityBarWindow_div_modulePeg": "entityBar_div_modulePeg");
        modulePegDiv.addEventListener(Events.ON_CLICK, this);*/
        searchModuleTextbox = (Textbox) is.getFellow(isWindow? "entityBarWindow_textbox_moduleSearch": "entityBar_textbox_moduleSearch");
        searchModuleTextbox.addEventListener(Events.ON_OK, this);
        epEntityTypeMenu = (Menupopup) is.getFellow(isWindow? "entityBarWindow_menupopup_epEntityMenu": "entityBar_menupopup_epentity");
        //closeDiv = (Div) is.getFellow("entityBar_div_close");
        for (Component child : epEntityTypeMenu.getChildren()) {
            if (child instanceof Menuitem)
                child.addEventListener(Events.ON_CLICK, this);
        }
        /*functionType = FunctionType.FunctionAll;
        showFunctionType = FunctionType.FunctionEntity;*/
        isNeedRefreshModuleData = true;
        isNeedRefreshEntityData = true;
        isNeedRefreshDateTableData = true;
        createTabs();
        if (Strings.isBlank(searchStr) == false) {
            if (showFunctionType == FunctionType.FunctionEntity) {
                searchTextbox.setValue(searchStr);
            } else if (showFunctionType == FunctionType.FunctionModule) {
                searchModuleTextbox.setValue(searchStr);
            } else if (showFunctionType == FunctionType.FunctionDateTable) {
                searchdateTableTextbox.setValue(searchStr);
            }
            search("*");
        }
    }
    private void createTabs() {
        tabHlayout.getChildren().clear();
        Div div = new Div();
        div.setWidth("8px");
        div.setVflex("1");
        tabHlayout.appendChild(div);
        //标签
        createTab(FunctionType.FunctionEntity);
        createTab(FunctionType.FunctionModule);
        createTab(FunctionType.FunctionDateTable);
        createContent();
    }
    private void createContent() {
        //内容
        if (showFunctionType == FunctionType.FunctionEntity) {
            entityBorderlayout.setVisible(true);
            moduleBorderlayout.setVisible(false);
            dateTableBorderlayout.setVisible(false);
            //如果result存在并且有两个tab，不需要重复创建
            if (isNeedRefreshEntityData)
                createEntityList();
            entityTab.setSclass("ui-entityBar-tab");
            moduleTab.setSclass("ui-entityBar-tab-disable");
            dateTableTab.setSclass("ui-entityBar-tab-disable");
        } else if (showFunctionType == FunctionType.FunctionModule) {
            entityBorderlayout.setVisible(false);
            moduleBorderlayout.setVisible(true);
            dateTableBorderlayout.setVisible(false);
            if (isNeedRefreshModuleData)
                createModuleList();
            entityTab.setSclass("ui-entityBar-tab-disable");
            moduleTab.setSclass("ui-entityBar-tab");
            dateTableTab.setSclass("ui-entityBar-tab-disable");
        } else if (showFunctionType == FunctionType.FunctionDateTable) {
            entityBorderlayout.setVisible(false);
            moduleBorderlayout.setVisible(false);
            dateTableBorderlayout.setVisible(true);
            if (isNeedRefreshDateTableData)
                createDateTableList();
            entityTab.setSclass("ui-entityBar-tab-disable");
            moduleTab.setSclass("ui-entityBar-tab-disable");
            dateTableTab.setSclass("ui-entityBar-tab");
        }
    }
    private void createTab(FunctionType type) {
        Div div = new Div();
        div.setWidth("64px");
        div.setHeight("38px");
        div.setSclass("ui-entityBar-tab");
        if (type == FunctionType.FunctionEntity) {
            div.setId(isWindow? "entityBarWindow_div_entity": "entityBar_div_entity");
            entityTab = div;
        } else if (type == FunctionType.FunctionModule) {
            div.setId(isWindow? "entityBarWindow_div_module": "entityBar_div_module");
            moduleTab = div;
        } else {
            div.setId("entityBar_div_DateTable");
            dateTableTab = div;
        }
        div.addEventListener(Events.ON_CLICK, this);
        tabHlayout.appendChild(div);

        A aIcon = new A();
        aIcon.setSclass("ui-entityBar-tab-a");
        aIcon.setStyle("font-size: 16px;position: relative;top: 9px;left: 7px;");
        if (type == FunctionType.FunctionEntity) {
            aIcon.setIconSclass("icon font_family icon-canshu");
        } else if(type == FunctionType.FunctionModule) {
            aIcon.setIconSclass("icon font_family icon-mokuai");
        } else {
            aIcon.setIconSclass("icon font_family icon-xinjianshujubiao-");
        }
        div.appendChild(aIcon);

        Label label = new Label();
        label.setStyle("position: relative;top: 9px;left: 11px;font-size: 15px;");
        label.setSclass("ui-entityBar-tab-label");
        if (type == FunctionType.FunctionEntity) {
            label.setValue(Labels.getLabel("navi.entity"));
        } else if(type == FunctionType.FunctionModule) {
            label.setValue(Labels.getLabel("navi.module"));
        } else {
            label.setValue(Labels.getLabel("entity.table"));
        }
        div.appendChild(label);

        Div line = new Div();
        line.setWidth("68px");
        line.setHeight("3px");
        line.setStyle("position: relative;left: 0px;top: 11px;");
        line.setSclass("ui-entityBar-tab-line");
        div.appendChild(line);
    }
    private void createEntityList() {
        QueryResultVo result = StudioApp.execute(entityListbox,
                new QueryCmd(paging.getPageSize()));
        searchTextbox.setText(null);
        paging.setTotalSize(result.getTotalSize());
        paging.setActivePage(0);
        paging.addEventListener(ZulEvents.ON_PAGING, this);
        redrawEntity(result.getEntities());
        isNeedRefreshEntityData = false;
    }
    private void createModuleList() {
        result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd("*", null, modulePaging.getPageSize(),1));
        redrawModule(result.getLinkVoList(), true, "");
        searchModuleTextbox.setText(null);
        modulePaging.setTotalSize(result.getTotalSize());
        modulePaging.setActivePage(0);
        modulePaging.addEventListener(ZulEvents.ON_PAGING, this);
        moduleTree.addEventListener(Events.ON_DOUBLE_CLICK, this);
        isNeedRefreshModuleData = false;
    }
    private void createDateTableList() {
        QueryResultVo result = StudioApp.execute(searchdateTableTextbox,
                new QueryCmd("Table",dateTablePaging.getPageSize()));
        searchdateTableTextbox.setText(null);
        dateTablePaging.setTotalSize(result.getTotalSize());
        dateTablePaging.setActivePage(0);
        dateTablePaging.addEventListener(ZulEvents.ON_PAGING, this);
        redrawDateTable(result.getEntities());
        isNeedRefreshDateTableData= false;
    }
    private void redrawEntity(List<EntityVo> data) {
        entityListbox.getItems().clear();
        for (EntityVo vo : data) {
            Listitem row = new Listitem();
            row.setValue(vo);
            row.appendChild(new Listcell(vo.getId()));
            row.appendChild(new Listcell(vo.getName()));
            row.appendChild(new Listcell(StudioUtil.getSringWithEntityType(vo.getType(), vo.getSubType())));
            row.appendChild(new Listcell(vo.getDesp()));
            row.setDraggable("treeItem");
            row.setParent(entityListbox);
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    private void redrawModule(List<TypeLinkVo> list, boolean isInit, String rootTypeLinkId) {
        moduleTree.clear();
        org.zkoss.zul.Treechildren tree;
        if (null == moduleTree.getTreechildren()) {
            tree = new org.zkoss.zul.Treechildren();
            moduleTree.appendChild(tree);
        } else {
            tree = moduleTree.getTreechildren();
        }
        List<String> children = new ArrayList<>();
        children.add(rootTypeLinkId);
        allTreeChildren(tree, list, isInit, children);
    }

    private void redrawDateTable(List<EntityVo> list) {
        dateTableListbox.getItems().clear();
        for (EntityVo vo : list) {
            Listitem row = new Listitem();
            row.setValue(vo);
            row.appendChild(new Listcell(vo.getId()));
            row.appendChild(new Listcell(vo.getName()));
            row.appendChild(new Listcell(StudioUtil.getSringWithEntityType(vo.getType(), vo.getSubType())));
            row.appendChild(new Listcell(vo.getDesp()));
            row.setDraggable("treeItem");
            row.setParent(dateTableListbox);
            row.addEventListener(Events.ON_DOUBLE_CLICK, this);
        }
    }

    private org.zkoss.zul.Treechildren allTreeChildren(org.zkoss.zul.Treechildren tree, List<TypeLinkVo> list,
                                                       boolean isInit, List<String> childrenList) {
        for (TypeLinkVo vo : list) {
            org.zkoss.zul.Treeitem item = new org.zkoss.zul.Treeitem();

            item.setValue(vo);
            org.zkoss.zul.Treerow row = new org.zkoss.zul.Treerow();
            row.setHeight("32px");
            row.appendChild(new org.zkoss.zul.Treecell(vo.getEntityId()));
            row.appendChild(new org.zkoss.zul.Treecell(vo.getEntityName()));
            row.appendChild(new org.zkoss.zul.Treecell(vo.getEntityDesp()));
            item.appendChild(row);

            boolean isAddROOT = false;
            if (!isInit && null != vo.getChildrenList() && vo.getChildrenList().size() != 0) {
                if (childrenList.contains(vo.getEntityId())) {
                    List<String> children = new ArrayList<>();
                    for (TypeLinkVo typeLinkVo: vo.getChildrenList()) {
                        children.add(typeLinkVo.getEntityId());
                    }
                    org.zkoss.zul.Treechildren resultTree = allTreeChildren(new org.zkoss.zul.Treechildren(),
                            vo.getChildrenList(), isInit, children);
                    item.appendChild(resultTree);
                } else {
                    isAddROOT = true;
                }
            } else {
                isAddROOT = true;
            }

            if (isAddROOT == true) {
                if (vo.isRoot().equals(ROOTENTITYID)) {
                    org.zkoss.zul.Treechildren childrenTree = new org.zkoss.zul.Treechildren();
                    item.appendChild(childrenTree);
                    item.setOpen(false);
                    item.addEventListener(Events.ON_OPEN, this);
                }
            }
            tree.appendChild(item);
        }
        return tree;
    }
    private String getTypes() {
        if (typeCombobox.getSelectedIndex() != typeCombobox.getChildren().size() - 1) {
            return typeCombobox.getSelectedItem().getValue();
        } else {
            StringBuilder sb = new StringBuilder();
            int size = typeCombobox.getChildren().size();
            for (int i = 0; i < size; i++) {
                Comboitem mi = typeCombobox.getItems().get(i);
                sb.append((String) mi.getValue()).append(",");
            }
            if (sb.length() > 0)
                sb.deleteCharAt(sb.length() - 1);
            return sb.toString();
        }
    }
    public void changePegDiv() {
        A iconA = (A) pegDiv.getChildren().get(0);
        String sclassString = iconA.getSclass();
        if (sclassString.contains("ui-button-a-switch"))
            iconA.setSclass(sclassString.replace("ui-button-a-switch", "ui-button-a"));
        else
            iconA.setSclass(sclassString.replace("ui-button-a", "ui-button-a-switch"));
    }

    //data
    public void showFunctionType(FunctionType functionType) {
        this.showFunctionType = functionType;
    }
    private void openEntity(String entityId)
    {
        workbench.openEntity(entityId);
//        if (workbench.checkEditor(entityId) == null) {
//            GetEntityVo ge = StudioApp.execute(new GetAndLockEntityCmd(entityId));
//            if (ge != null) {
//                char code = 'U';
//                if (!Strings.isBlank(ge.getLockUser())) {
//                    code = 'R';
//                    Messagebox.show(Labels.getLabel("enitiy.locked",
//                            new String[]{ge.getLockUser()}), Labels
//                                    .getLabel("message.title.error"),
//                            Messagebox.OK, Messagebox.INFORMATION);
//                }
//                workbench.createEditor(ge.getEntity(), code);
//            }
//        }
    }
    //public
    public void addFunctionEntity(FunctionType functionType) {
        showFunctionType(functionType);
        if (showFunctionType == FunctionType.FunctionNone) {
            isNeedRefreshEntityData = true;
            isNeedRefreshModuleData = true;
            isNeedRefreshDateTableData = true;
        }
        createTabs();
    }

    private void search(String field) {
        if (showFunctionType == FunctionType.FunctionEntity) {
            String type = getTypes();
            QueryResultVo result = null;
            if (Strings.isBlank(searchTextbox.getValue())) {
                result = StudioApp.execute(entityListbox, new QueryCmd(type,
                        paging.getPageSize()));
            } else {
                result = StudioApp.execute(entityListbox, new QueryCmd(type, field,
                        searchTextbox.getValue().trim(), paging.getPageSize()));
            }
            paging.setTotalSize(result.getTotalSize());
            paging.setActivePage(0);
            redrawEntity(result.getEntities());
        } else if (showFunctionType == FunctionType.FunctionModule) {
            if (Strings.isBlank(searchModuleTextbox.getValue())) {
                result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd(field, null, modulePaging.getPageSize(), 1));
            } else {
                result = StudioApp.execute(moduleTree, new PagingTaskLinkCmd(field, searchModuleTextbox.getValue()
                        .trim(), modulePaging.getPageSize(), 1));
            }
            modulePaging.setTotalSize(result.getTotalSize());
            modulePaging.setActivePage(0);
            redrawModule(result.getLinkVoList(), true, "");
        }else if(showFunctionType == FunctionType.FunctionDateTable){
            QueryResultVo result = null;
            if (Strings.isBlank(searchdateTableTextbox.getValue())) {
                result = StudioApp.execute(dateTableListbox, new QueryCmd("table",
                        dateTablePaging.getPageSize()));
            } else {
                result = StudioApp.execute(dateTableListbox, new QueryCmd("table", field,
                        searchdateTableTextbox.getValue().trim(), dateTablePaging.getPageSize()));
            }
            dateTablePaging.setTotalSize(result.getTotalSize());
            dateTablePaging.setActivePage(0);
            redrawDateTable(result.getEntities());
        }
    }
    // //////////////////////右键菜单事件//////////////////////////
    public void copy() {
        EntityVo copyObj = getSelectedEntity();
        if (copyObj == null)
            return;
        //判断是否有权限
        if (!StudioUtil.isAddPermission(copyObj)) {
            WebUtils.showError(Labels.getLabel("access.add.error",
                    new String[]{copyObj.getType()}));
            return;
        }
        StudioUtil.showInputDialog(workbench, entityListbox.getPage(), copyObj.getId(), true);
    }

    public void export() {
        EntityVo copyObj = getSelectedEntity();
        if (copyObj == null)
            return;
        //判断是否有权限
        if (!Contexts.getUser().isAuthorized("repoExport")) {
            WebUtils.showError(Labels.getLabel("access.export.error",
                    new String[]{copyObj.getType()}));
            return;
        }
        StudioUtil.showInputDialog(workbench, entityListbox.getPage(), copyObj.getId(), false);
    }

    public void delete() {
        final EntityVo ev = getSelectedEntity();
        if (ev == null)
            return;
        //判断是否有权限
        if (!StudioUtil.isDeletePermission(ev)) {
            WebUtils.showError(Labels.getLabel("access.delete.error", new String[]{ev.getType()}));
            return;
        }
        Messagebox.show(Labels.getLabel("message.confirm",
                new String[]{Labels.getLabel("button.delete") + "[" + ev.getId() + "]"}),
                Labels.getLabel("message.title.confirm"), Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
                new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        if (event.getName().equals(Messagebox.ON_YES)) {// 删除
                            //EntityVo vo = getSelectedEntity();
                            GetEntityVo ge = StudioApp.execute(event.getTarget(),
                                    new GetAndLockEntityCmd(ev.getId()));
                            if (ge != null) {
                                if (!Strings.isBlank(ge.getLockUser())) {
                                    Messagebox.show(Labels.getLabel("enitiy.locked",
                                            new String[]{ge.getLockUser()}), Labels
                                                    .getLabel("message.title.error"),
                                            Messagebox.OK, Messagebox.INFORMATION);
                                } else {
                                    EntityInfo ei = new EntityInfo();
                                    ei.setId(ev.getId());
                                    ei.setStatus('D');
                                    StudioApp.execute(new SaveCmd(ei));
                                    if ("Table".equals(ev.getType()))
                                        StudioApp.execute(new UpdateCmd(ev.getSubType(),
                                                "delete from sys_table_field_info where table1id=" + "'" + ev.getId() + "'", null));
                                    refreshEntity();
                                    Editor editor = workbench.checkEditor(ei.getId());
                                    if (editor != null && editor instanceof EntityEditor<?>)
                                        ((EntityEditor<?>) editor).close();

                                    //更新link
                                    LinkVo deleteVo = new LinkVo();
                                    deleteVo.setEntityId(ei.getId());
                                    StudioApp.execute(new DeleteLinkCmd(deleteVo));
                                    StudioApp.execute(new DeleteTaskLinkCmd(new TypeLinkVo(deleteVo.getEntityId(), null, null)));
                                    //更新跟删除有关的关系
                                    List<LinkVo> updateVos = StudioApp.execute(new GetChildrenLinkCmd(deleteVo.getEntityId()));
                                    if (updateVos != null && updateVos.size() > 0) {
                                        for (LinkVo updateVo : updateVos) {
                                            String childrenStr = StringUtil.removeStringToListString(updateVo.getChildrenEntityId(),
                                                    ",", deleteVo.getEntityId());
                                            updateVo.setChildrenEntityId(childrenStr);
                                        }
                                        StudioApp.execute(new UpdateLinkListCmd(updateVos, deleteVo.getEntityId()));
                                    }
                                    List<TypeLinkVo> updateLinkVos = StudioApp.execute(new GetChildrenTaskLinkCmd(deleteVo.getEntityId()));
                                    if (updateLinkVos != null && updateLinkVos.size() > 0) {
                                        for (TypeLinkVo updateVo : updateLinkVos) {
                                            String childrenStr = StringUtil.removeStringToListString(updateVo.getChildrenEntityId(),
                                                    ",", deleteVo.getEntityId());
                                            updateVo.setChildrenEntityId(childrenStr);
                                        }
                                        StudioApp.execute(new UpdateTaskLinkListCmd(updateLinkVos, deleteVo.getEntityId()));
                                    }
                                }
                            }
                        }
                    }
                });
    }

    public void showHistory() {
        epEntityTypeMenu.close();
        if(showFunctionType == FunctionType.FunctionEntity && entityListbox.getSelectedItem() != null) {
            EntityVo vo = entityListbox.getSelectedItem().getValue();
            if (vo != null)
                StudioUtil.showHistory(workbench, vo);
        }else if((showFunctionType == FunctionType.FunctionDateTable&& dateTableListbox.getSelectedItem() != null)){
            EntityVo vo = dateTableListbox.getSelectedItem().getValue();
            if (vo != null)
                StudioUtil.showHistory(workbench, vo);
        }
    }
}
