/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.layout;


/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface HeaderController {

	void setDisabled(String name, boolean disabled);

	void setMenuitem(int state);
	
	void setWorkbench(WorkbenchController workbench);

}
