/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Idspace;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ImportEditor extends AbstractPanelEditor {


    public ImportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("editor.import.title"), "~./images/import.gif", "~./include/editor/project/import.zul");
        for (Component comp : is.getFellows()) {

        }
        redraw();
    }

    private void redraw() {

    }

    @Override
    public void dispatch(Event event) {

    }

}
