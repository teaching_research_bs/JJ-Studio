package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Button;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;

public class InformImportEditor extends LabelImportEditor {
    public InformImportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }



    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.import.inform"), "~./images/information.png",
                "~./include/editor/project/informImport.zul");
        this.project = (Label) is.getFellow("informImport_label_project");
        this.operator = (Label) is.getFellow("informImport_label_operator");
        this.date = (Label) is.getFellow("informImport_label_date");
        this.comment = (Label) is.getFellow("informImport_label_comment");
        this.size = (Label) is.getFellow("informImport_label_size");
        this.sources = (Listbox) is.getFellow("informImport_listbox_sources");
        this.commit = (Button) is.getFellow("informImport_button_commit");
        this.commit.addEventListener(Events.ON_CLICK, this);
        is.getFellow("informImport_button_upload").addEventListener(Events.ON_UPLOAD, this);
    }
}
