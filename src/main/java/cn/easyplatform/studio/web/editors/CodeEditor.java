/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface CodeEditor {
	
	void insertTodo();
	
	void insertComments();
	
	void undo();

	void redo();

	void selectAll();

	void format();

	void formatAll();

	void deleteLine();

	void indentMore();

	void indentLess();

	void find();

	void findNext();

	void findPrev();

	void replace();

	void replaceAll();
}
