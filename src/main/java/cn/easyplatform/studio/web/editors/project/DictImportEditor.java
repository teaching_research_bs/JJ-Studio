package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.importAll.SaveDicCmd;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.utils.ZipUtil;
import cn.easyplatform.studio.vos.DicVo;
import cn.easyplatform.studio.vos.IXVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.editors.ComponentCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.*;

public class DictImportEditor extends AbstractPanelEditor {

    protected Label project;

    protected Label operator;

    protected Label date;

    protected Label size;

    protected Label comment;

    protected Listbox sources;

    protected Button commit;

    protected IXVo entity;

    public DictImportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getName().equals(Events.ON_UPLOAD)) {
            UploadEvent ue = (UploadEvent) evt;
            IXVo vo = (IXVo) ZipUtil.unpackZip(ue.getMedia().getByteData());
            redraw(vo);
        } else if (evt.getName().equals(Events.ON_DOUBLE_CLICK)) {
            Listitem listitem = (Listitem) evt.getTarget();
            DicVo dicVo = listitem.getValue();
            AbstractView.createDicDetailView(new ComponentCallback(this.project, Labels.getLabel("dic.detail.title")),
                    dicVo.getDicDetailVoList()).doOverlapped();
        } else if (evt.getTarget().getId().equals("dictImport_button_commit")) {
            if (StudioApp.execute(new SaveDicCmd(entity.getDicVos())) == true)
                WebUtils.showSuccess(Labels.getLabel("export.success"));
        }
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.import.dictionary"), "~./images/dictionary.png",
                "~./include/editor/project/dictImport.zul");
        this.project = (Label) is.getFellow("dictImport_label_project");
        this.operator = (Label) is.getFellow("dictImport_label_operator");
        this.date = (Label) is.getFellow("dictImport_label_date");
        this.comment = (Label) is.getFellow("dictImport_label_comment");
        this.size = (Label) is.getFellow("dictImport_label_size");
        this.sources = (Listbox) is.getFellow("dictImport_listbox_sources");
        this.commit = (Button) is.getFellow("dictImport_button_commit");
        this.commit.addEventListener(Events.ON_CLICK, this);
        is.getFellow("dictImport_button_upload").addEventListener(Events.ON_UPLOAD, this);
    }

    private void redraw(IXVo vo) {
        entity = vo;
        project.setValue(vo.getProjectId());
        operator.setValue(vo.getUser());
        date.setValue(DateFormatUtils.format(vo.getDate(), "yyyy-MM-dd hh:mm:ss"));
        size.setValue((vo.getDicVos() != null ? vo.getDicVos().size(): "0") + "");
        comment.setValue(vo.getDesp());
        redrawTree(entity);
        commit.setDisabled(false);
    }

    private void redrawTree(IXVo vo) {
        sources.getItems().clear();
        if (vo.getDicVos() != null) {
            for (DicVo dicVo : vo.getDicVos()) {
                Listitem li = new Listitem();
                li.appendChild(new Listcell(dicVo.getDicCode()));
                li.appendChild(new Listcell(dicVo.getDesc1()));
                li.appendChild(new Listcell(dicVo.getUpCode()));
                li.appendChild(new Listcell(dicVo.getCreateDate().toString()));
                li.appendChild(new Listcell(dicVo.getCreateUser()));
                li.setValue(dicVo);
                li.addEventListener(Events.ON_DOUBLE_CLICK, this);
                sources.appendChild(li);
            }
        }
    }
}
