/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.version.GetDetailCmd;
import cn.easyplatform.studio.cmd.version.GetHistCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.IXVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.utils.SerializationUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ImtHistEditor extends AbstractPanelEditor {
    private Listbox list;

    private Grid detail;

    private Button export;

    public ImtHistEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.import"), "~./images/svn/showhistory.gif", "~./include/editor/version/imthist.zul");
        for (Component comp : is.getFellows()) {
            String id = comp.getId();
            if (id.equals("imthist_listbox_list")) {
                this.list = (Listbox) comp;
                this.list.addEventListener(Events.ON_SELECT, this);
            } else if (id.equals("imthist_grid_detail")) {
                this.detail = (Grid) comp;
            } else if (id.equals("imthist_button_export")) {
                export = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
            }
        }
        redraw();
    }

    private void redraw() {
        List<IXVo> hist = StudioApp.execute(new GetHistCmd(false));
        for (IXVo vo : hist) {
            Listitem li = new Listitem();
            li.appendChild(new Listcell(DateFormatUtils.format(vo.getDate(), "yyyy-MM-dd hh:mm:ss")));
            li.appendChild(new Listcell(vo.getUser()));
            li.appendChild(new Listcell(vo.getFirstNo() + "-" + vo.getLastNo()));
            li.appendChild(new Listcell(vo.getDesp()));
            li.setValue(vo);
            list.appendChild(li);
        }
    }

    @Override
    public void dispatch(Event event) {
        if (event.getName().equals(Events.ON_SELECT))
            selectHist();
        else
            export();
    }

    private void selectHist() {
        IXVo vo = list.getSelectedItem().getValue();
        if (vo.getEntities() == null) {
            List<EntityInfo> entities = StudioApp.execute(list, new GetDetailCmd(false, vo.getId()));
            vo.setEntities(entities);
        }
        detail.getRows().getChildren().clear();
        for (EntityInfo e : vo.getEntities()) {
            Row li = new Row();
            li.appendChild(new Label(e.getId()));
            li.appendChild(new Label(e.getName()));
            li.appendChild(new Label(e.getDescription()));
            li.appendChild(new Label(e.getType()));
            li.appendChild(new Label(e.getUpdateDate().toString()));
            li.appendChild(new Label(e.getUpdateUser()));
            li.appendChild(new Label(e.getVersion() + ""));
            detail.getRows().appendChild(li);
        }
        export.setDisabled(false);
    }

    private void export() {
        if (list.getSelectedIndex() >= 0) {
            IXVo vo = list.getSelectedItem().getValue();
            vo.setProjectId(Contexts.getProject().getId());
            byte[] data = SerializationUtils.serialize(vo);
            ByteArrayOutputStream os = null;
            ZipOutputStream zos = null;
            try {
                os = new ByteArrayOutputStream(data.length);
                zos = new ZipOutputStream(os);
                zos.putNextEntry(new ZipEntry(vo.getProjectId() + ".dat"));
                zos.write(data);
                zos.finish();
                org.zkoss.zkmax.zul.Filedownload.save(os.toByteArray(), "application/zip", vo.getId() + ".zip");
            } catch (IOException e) {
                WebUtils.showError(Labels.getLabel("editor.export.title"));
            } finally {
                IOUtils.closeQuietly(os);
                IOUtils.closeQuietly(zos);
            }
        }
    }
}
