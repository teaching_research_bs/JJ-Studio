package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.exportAll.QueryMessageInfoCmd;
import cn.easyplatform.studio.utils.StudioUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.MessageVo;
import cn.easyplatform.studio.web.editors.version.AllExportEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LabelExportEditor extends AllExportEditor {

    protected Textbox code;
    protected Textbox chinese;
    protected Combobox status;
    protected Combobox msgType;
    protected Listbox from;

    public LabelExportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getName().equals(Events.ON_CLICK)) {
            Component comp = evt.getTarget();
            if (comp.getId().equals("labelExport_button_query")) {
                MessageVo searchVo = new MessageVo();
                searchVo.setCode(code.getValue().trim());
                searchVo.setZh_cn(chinese.getValue().trim());
                if (status.getSelectedItem() != null)
                    searchVo.setStatus((String) status.getSelectedItem().getValue());
                if (msgType.getSelectedItem() != null)
                    searchVo.setMSG_TYPE((String) msgType.getSelectedItem().getValue());
                List<MessageVo> vos = StudioApp.execute(new QueryMessageInfoCmd(searchVo, true));
                allListCheck.setChecked(false);
                pageListCheck.setChecked(false);
                revListCheck.setChecked(false);
                redrawTree(vos);
            } else if (comp.getId().equals("labelExport_image_chooseAllBtn")) {
                select(false);
            } else if (comp.getId().equals("labelExport_image_chooseBtn")) {
                select(true);
            } else if (comp.getId().equals("labelExport_image_removeBtn")) {
                unselect(true);
            } else if (comp.getId().equals("labelExport_image_removeAllBtn")) {
                unselect(false);
            } else if (comp.getId().equals("labelExport_toolbarbutton_export")) {
                export(comp);
            }
        } else if (evt.getName().equals(Events.ON_CHECK)) {
            Checkbox r = (Checkbox) evt.getTarget();
            doCheck(r);
        }
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.export.label"), "~./images/web.gif",
                "~./include/editor/project/labelExport.zul");
        code = (Textbox)is.getFellow("labelExport_textbox_code");
        chinese = (Textbox)is.getFellow("labelExport_textbox_chinese");
        from = (Listbox)is.getFellow("labelExport_listbox_sources");
        targets = (Listbox)is.getFellow("labelExport_listbox_targets");
        comment = (Textbox) is.getFellow("labelExport_textbox_comment");
        status = (Combobox) is.getFellow("labelExport_combobox_status");
        msgType = (Combobox) is.getFellow("labelExport_combobox_msg_type");
        Button searchBtn = (Button)is.getFellow("labelExport_button_query");
        searchBtn.addEventListener(Events.ON_CLICK, this);
        allListCheck = (Checkbox)is.getFellow("labelExport_checkbox_checkAll");
        allListCheck.addEventListener(Events.ON_CHECK, this);
        pageListCheck = (Checkbox)is.getFellow("labelExport_checkbox_checkPage");
        pageListCheck.addEventListener(Events.ON_CHECK, this);
        revListCheck = (Checkbox)is.getFellow("labelExport_checkbox_checkRev");
        revListCheck.addEventListener(Events.ON_CHECK, this);
        is.getFellow("labelExport_image_chooseAllBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("labelExport_image_chooseBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("labelExport_image_removeBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("labelExport_image_removeAllBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("labelExport_toolbarbutton_export").addEventListener(Events.ON_CLICK, this);

        List<MessageVo> vos = StudioApp.execute(new QueryMessageInfoCmd(null, true));
        redrawTree(vos);
    }

    protected void redrawTree(List<MessageVo> vos) {
        from.getItems().clear();
        if (vos != null) {
            for (MessageVo vo: vos) {
                Listitem row = new Listitem();
                row.appendChild(new Listcell(vo.getCode()));
                row.appendChild(new Listcell(vo.getZh_cn()));
                row.appendChild(new Listcell(vo.getMSG_TYPE()));
                row.appendChild(new Listcell(vo.getStatus()));
                row.setValue(vo);
                from.appendChild(row);
            }
        }
    }

    protected void select(boolean selectFlag) {
        for (Listitem sel : from.getItems()) {
            if (selectFlag && !sel.isSelected())
                continue;
            if (sel.getValue() != null) {
                boolean exists = false;
                MessageVo s = sel.getValue();
                for (Listitem li : targets.getItems()) {
                    MessageVo t = li.getValue();
                    if (s.getCode().equals(t.getCode())) {
                        exists = true;
                        break;
                    }
                }
                if (!exists) {
                    Listitem li = new Listitem();
                    li.appendChild(new Listcell(s.getCode()));
                    li.appendChild(new Listcell(s.getZh_cn()));
                    li.appendChild(new Listcell(s.getMSG_TYPE()));
                    li.appendChild(new Listcell(s.getStatus()));
                    li.setValue(s);
                    targets.appendChild(li);
                }
            }
        }
    }

    protected void unselect(boolean selectedFlag) {
        if (selectedFlag) {
            if (targets.getSelectedCount() > 0) {
                List<Listitem> items = new ArrayList<Listitem>();
                for (Listitem li : targets.getSelectedItems())
                    items.add(li);
                for (Listitem li : items)
                    li.detach();
            }
        } else
            targets.getItems().clear();
    }

    protected void doCheck(Checkbox cbx) {
        if (cbx.getValue().equals("1") || cbx.getValue().equals("3")) {
            Iterator<Component> itr = from.queryAll("Listitem").iterator();
            while (itr.hasNext()) {
                Listitem ti = (Listitem) itr.next();
                if (ti.getValue() != null) {

                    if (cbx.getValue().equals("1")) {
                        ti.setSelected(cbx.isChecked());
                    }
                    else {
                        ti.setSelected(!ti.isSelected());
                    }
                }
            }
        } else {
            int pos = from.getPagingChild().getActivePage() * from.getPagingChild().getPageSize();
            int to = (from.getPagingChild().getActivePage() + 1) * from.getPagingChild().getPageSize();
            List<Listitem> data = new ArrayList<>(from.getItems());
            for (; pos < to; pos++) {
                if (data.size()>pos) {
                    Listitem ti = data.get(pos);
                    ti.setSelected(cbx.isChecked());
                }
            }
        }
    }

    //导出
    protected void export(Component btn) {
        if (targets.getItems().isEmpty()) {
            WebUtils.showError(Labels.getLabel("editor.export.empty"));
            return;
        }
        if (Strings.isBlank(comment.getValue())) {
            WebUtils.notEmpty(Labels.getLabel("editor.export.comment"));
            return;
        }
        Clients.clearWrongValue(btn);
        //打包保存
        List<MessageVo> dicVos = new ArrayList<MessageVo>();
        for (Listitem li : targets.getItems()) {
            MessageVo e = li.getValue();
            dicVos.add(e);
        }
        StudioUtil.exportMessage(dicVos, comment.getValue());
    }
}
