package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.exportAll.QueryMessageInfoCmd;
import cn.easyplatform.studio.vos.MessageVo;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

import java.util.List;

public class InformExportEditor extends LabelExportEditor {
    public InformExportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getName().equals(Events.ON_CLICK)) {
            Component comp = evt.getTarget();
            if (comp.getId().equals("informExport_button_query")) {
                MessageVo searchVo = new MessageVo();
                searchVo.setCode(code.getValue());
                searchVo.setZh_cn(chinese.getValue());
                if (status.getSelectedItem() != null)
                    searchVo.setStatus((String) status.getSelectedItem().getValue());
                if (msgType.getSelectedItem() != null)
                    searchVo.setMSG_TYPE((String) msgType.getSelectedItem().getValue());
                List<MessageVo> vos = StudioApp.execute(new QueryMessageInfoCmd(searchVo, false));
                allListCheck.setChecked(false);
                pageListCheck.setChecked(false);
                revListCheck.setChecked(false);
                redrawTree(vos);
            } else if (comp.getId().equals("informExport_image_chooseAllBtn")) {
                select(false);
            } else if (comp.getId().equals("informExport_image_chooseBtn")) {
                select(true);
            } else if (comp.getId().equals("informExport_image_removeBtn")) {
                unselect(true);
            } else if (comp.getId().equals("informExport_image_removeAllBtn")) {
                unselect(false);
            } else if (comp.getId().equals("informExport_toolbarbutton_comment")) {
                export(comp);
            }
        } else if (evt.getName().equals(Events.ON_CHECK)) {
            Checkbox r = (Checkbox) evt.getTarget();
            doCheck(r);
        }
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.export.inform"), "~./images/information.png",
                "~./include/editor/project/informExport.zul");
        code = (Textbox)is.getFellow("informExport_textbox_code");
        chinese = (Textbox)is.getFellow("informExport_textbox_chinese");
        from = (Listbox)is.getFellow("informExport_listbox_sources");
        targets = (Listbox)is.getFellow("informExport_listbox_targets");
        comment = (Textbox) is.getFellow("informExport_textbox_comment");
        status = (Combobox) is.getFellow("informExport_combobox_status");
        msgType = (Combobox) is.getFellow("informExport_combobox_msg_type");
        Button searchBtn = (Button)is.getFellow("informExport_button_query");
        searchBtn.addEventListener(Events.ON_CLICK, this);
        allListCheck = (Checkbox)is.getFellow("informExport_checkbox_checkAll");
        allListCheck.addEventListener(Events.ON_CHECK, this);
        pageListCheck = (Checkbox)is.getFellow("informExport_checkbox_checkPage");
        pageListCheck.addEventListener(Events.ON_CHECK, this);
        revListCheck = (Checkbox)is.getFellow("informExport_checkbox_checkRev");
        revListCheck.addEventListener(Events.ON_CHECK, this);
        is.getFellow("informExport_image_chooseAllBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("informExport_image_chooseBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("informExport_image_removeBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("informExport_image_removeAllBtn").addEventListener(Events.ON_CLICK, this);
        is.getFellow("informExport_toolbarbutton_comment").addEventListener(Events.ON_CLICK, this);

        List<MessageVo> vos = StudioApp.execute(new QueryMessageInfoCmd(null, false));
        redrawTree(vos);
    }
}
