package cn.easyplatform.studio.web.editors.admin;

import cn.easyplatform.studio.web.editors.AbstractEditor;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.editors.version.AllExportEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zul.Idspace;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class OperationLogEditor extends AbstractPanelEditor {

    public OperationLogEditor(WorkbenchController workbench, String id) {
        super(workbench , id);
    }


    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.operation.log"), "~./images/dictionary.png",
                "~./include/admin/log/operationlog.zul");
    }

    @Override
    protected void dispatch(Event evt) {

    }

}
