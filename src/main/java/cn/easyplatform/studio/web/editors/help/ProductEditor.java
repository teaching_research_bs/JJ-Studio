/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.help;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.entity.GetEntityListCmd;
import cn.easyplatform.studio.cmd.help.GetGuideConfigCmd;
import cn.easyplatform.studio.cmd.module.GetModuleCmd;
import cn.easyplatform.studio.cmd.module.UpdateModuleCmd;
import cn.easyplatform.studio.utils.HttpUtils;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.vos.ModuleVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.FileUtils;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.*;
import org.zkoss.zul.impl.InputElement;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductEditor extends AbstractPanelEditor implements EventListener<Event> {

    private ModuleVo selectedModule = new ModuleVo();
    private Boolean saveFlag = true;

    private Grid product_properties_grid;
    private Rows rows;
    private Hlayout product_hlayout_block;
    private Hlayout product_hlayout_address;
    private Button product_button_add;
    private Textbox product_textbox_verName;
    private Combobox product_combobox_selectNum;
    private Listbox product_listbox_verlist;
    private Button product_btn_submit;
    private Textbox product_textbox_name;//产品名称
    private Textbox product_textbox_introduce;//产品介绍
    private Textbox product_textbox_address;//演示地址
    private Textbox product_textbox_memo_one;
    private Textbox product_textbox_memo_two;
    private Textbox product_textbox_memo_three;
    private Textbox product_textbox_memo_four;
    private Textbox product_textbox_memo_five;

    private Vlayout product_vlayout_import_list;
    private Vlayout product_vlayout_import_manual;
    private Label product_lable_module_parent;
    private Textbox product_textbox_module_name;
    private Textbox product_textbox_module_price;
    private Textbox product_textbox_module_desp;
    private Listbox product_listbox_moduleList;
    private Button product_button_module_save;

    private Radiogroup product_radiogroup_Import_type;
    private Radiogroup product_radiogroup_pro;
    private Radiogroup product_radiogroup_manual_pro;
    private Radiogroup product_radiogroup_type;

    private Row product_row_market;
    private Row product_row_lease;

    private Combobox product_combobox_partition;
    private Combobox product_combobox_block;

    private Hlayout product_vlayout_uploadImg_one;
    private Hlayout product_vlayout_uploadImg_two;
    private Hlayout product_vlayout_uploadImg_three;
    private Hlayout product_vlayout_uploadImg_four;
    private Hlayout product_vlayout_uploadImg_five;

    private Button product_btn_uploadImg_one;
    private Button product_btn_uploadImg_two;
    private Button product_btn_uploadImg_three;
    private Button product_btn_uploadImg_four;
    private Button product_btn_uploadImg_five;

    private Button product_btn_uploadMainImg_one;
    private Button product_btn_uploadMainImg_two;

    private Image product_image_uploadImg_one;
    private Image product_image_uploadImg_two;
    private Image product_image_uploadImg_three;
    private Image product_image_uploadImg_four;
    private Image product_image_uploadImg_five;

    private Image product_image_uploadMainImg_one;
    private Image product_image_uploadMainImg_two;

    private String userId;

    /**
     * @param workbench
     */
    public ProductEditor(WorkbenchController workbench, String id) {
        super(workbench, id);

    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.product"), "~./images/explorer.gif", "~./include/help/product.zul");
        for (Component comp : is.getFellows()) {
            if (comp instanceof Grid) {
                product_properties_grid = (Grid) comp;
                rows = product_properties_grid.getRows();
            }else if (comp.getId().equals("product_btn_submit")) {
                product_btn_submit = (Button) comp;
                product_btn_submit.addEventListener(Events.ON_CLICK, this);
            } else if (comp.getId().equals("product_textbox_introduce")) {
                product_textbox_introduce = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_name")) {
                product_textbox_name = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_address")) {
                product_textbox_address = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_memo_one")) {
                product_textbox_memo_one = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_memo_two")) {
                product_textbox_memo_two = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_memo_three")) {
                product_textbox_memo_three = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_memo_four")) {
                product_textbox_memo_four = (Textbox) comp;
            } else if (comp.getId().equals("product_textbox_memo_five")) {
                product_textbox_memo_five = (Textbox) comp;
            } else if (comp.getId().equals("product_listbox_verlist")) {
                product_listbox_verlist = (Listbox) comp;
            } else if (comp.getId().equals("product_combobox_selectNum")) {
                product_combobox_selectNum = (Combobox) comp;
            } else if (comp.getId().equals("product_textbox_verName")) {
                product_textbox_verName = (Textbox) comp;
            } else if (comp.getId().equals("product_button_add")) {
                product_button_add = (Button) comp;
                product_button_add.addEventListener(Events.ON_CLICK, this);
            } else if (comp.getId().equals("product_hlayout_block")) {
                product_hlayout_block = (Hlayout) comp;
            } else if (comp.getId().equals("product_hlayout_address")) {
                product_hlayout_address = (Hlayout) comp;
            } else if (comp.getId().equals("product_combobox_partition")) {
                product_combobox_partition = (Combobox) comp;
                product_combobox_partition.addEventListener(Events.ON_SELECT, this);
            } else if (comp.getId().equals("product_combobox_block")) {
                product_combobox_block = (Combobox) comp;
                product_combobox_block.addEventListener(Events.ON_SELECT, this);
            } else if (comp.getId().equals("product_radiogroup_Import_type")) {
                product_radiogroup_Import_type = (Radiogroup) comp;
                product_radiogroup_Import_type.addEventListener(Events.ON_CHECK, this);
            } else if (comp.getId().equals("product_radiogroup_pro")) {
                product_radiogroup_pro = (Radiogroup) comp;
                product_radiogroup_pro.addEventListener(Events.ON_CHECK, this);
            } else if (comp.getId().equals("product_radiogroup_manual_pro")) {
                product_radiogroup_manual_pro = (Radiogroup) comp;
                product_radiogroup_manual_pro.addEventListener(Events.ON_CHECK, this);
            }else if (comp.getId().equals("product_radiogroup_type")) {
                product_radiogroup_type = (Radiogroup) comp;
                product_radiogroup_type.addEventListener(Events.ON_CHECK, this);
            } else if (comp.getId().equals("product_row_market")) {
                product_row_market = (Row) comp;
                product_row_market.addEventListener(Events.ON_CHECK, this);
            } else if (comp.getId().equals("product_row_lease")) {
                product_row_lease = (Row) comp;
                product_row_lease.addEventListener(Events.ON_CHECK, this);
            } else if (comp.getId().equals("product_vlayout_uploadImg_one")) {
                product_vlayout_uploadImg_one = (Hlayout) comp;
            } else if (comp.getId().equals("product_vlayout_uploadImg_two")) {
                product_vlayout_uploadImg_two = (Hlayout) comp;
            } else if (comp.getId().equals("product_vlayout_uploadImg_three")) {
                product_vlayout_uploadImg_three = (Hlayout) comp;
            } else if (comp.getId().equals("product_vlayout_uploadImg_four")) {
                product_vlayout_uploadImg_four = (Hlayout) comp;
            } else if (comp.getId().equals("product_vlayout_uploadImg_five")) {
                product_vlayout_uploadImg_five = (Hlayout) comp;
            } else if (comp.getId().equals("product_image_uploadImg_one")) {
                product_image_uploadImg_one = (Image) comp;
            } else if (comp.getId().equals("product_image_uploadImg_two")) {
                product_image_uploadImg_two = (Image) comp;
            } else if (comp.getId().equals("product_image_uploadImg_three")) {
                product_image_uploadImg_three = (Image) comp;
            } else if (comp.getId().equals("product_image_uploadImg_four")) {
                product_image_uploadImg_four = (Image) comp;
            } else if (comp.getId().equals("product_image_uploadImg_five")) {
                product_image_uploadImg_five = (Image) comp;
            } else if (comp.getId().equals("product_image_uploadMainImg_one")) {
                product_image_uploadMainImg_one = (Image) comp;
            } else if (comp.getId().equals("product_image_uploadMainImg_two")) {
                product_image_uploadMainImg_two = (Image) comp;
            } else if (comp.getId().equals("product_btn_uploadImg_one")) {
                product_btn_uploadImg_one = (Button) comp;
                product_btn_uploadImg_one.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_btn_uploadImg_two")) {
                product_btn_uploadImg_two = (Button) comp;
                product_btn_uploadImg_two.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_btn_uploadImg_three")) {
                product_btn_uploadImg_three = (Button) comp;
                product_btn_uploadImg_three.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_btn_uploadImg_four")) {
                product_btn_uploadImg_four = (Button) comp;
                product_btn_uploadImg_four.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_btn_uploadImg_five")) {
                product_btn_uploadImg_five = (Button) comp;
                product_btn_uploadImg_five.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_btn_uploadMainImg_one")) {
                product_btn_uploadMainImg_one = (Button) comp;
                product_btn_uploadMainImg_one.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_btn_uploadMainImg_two")) {
                product_btn_uploadMainImg_two = (Button) comp;
                product_btn_uploadMainImg_two.addEventListener(Events.ON_UPLOAD, this);
            } else if (comp.getId().equals("product_listbox_moduleList")){
                product_listbox_moduleList = (Listbox) comp;
            } else if (comp.getId().equals("product_lable_module_parent")){
                product_lable_module_parent = (Label) comp;
            } else if (comp.getId().equals("product_textbox_module_name")){
                product_textbox_module_name = (Textbox) comp;
                product_textbox_module_name.addEventListener(Events.ON_CHANGE, this);
            } else if (comp.getId().equals("product_textbox_module_price")){
                product_textbox_module_price = (Textbox) comp;
                product_textbox_module_price.addEventListener(Events.ON_CHANGE, this);
            } else if (comp.getId().equals("product_textbox_module_desp")){
                product_textbox_module_desp = (Textbox) comp;
                product_textbox_module_desp.addEventListener(Events.ON_CHANGE, this);
            } else if (comp.getId().equals("product_button_module_save")){
                product_button_module_save = (Button) comp;
                product_button_module_save.addEventListener(Events.ON_CLICK,this);
            }else if (comp.getId().equals("product_vlayout_import_list")){
                product_vlayout_import_list = (Vlayout) comp;
            }else if (comp.getId().equals("product_vlayout_import_manual")){
                product_vlayout_import_manual = (Vlayout) comp;
            }
        }
        createModuleList();
        addProductPro();
        /*disableBtn(true);
        if (Strings.isBlank(getUserId())) {
            openLoginWin(is.getPage());
        } else {
            disableBtn(false);
        }*/
        initRequestApi();
        List<GuideConfigVo> configVos = StudioApp.execute(new GetGuideConfigCmd(GuideConfigEditor.GuideConfigType.GuideConfigApp));
        for (GuideConfigVo guideConfigVo : configVos) {
            Checkbox checkbox = new Checkbox();
            checkbox.setLabel(guideConfigVo.getGuideName());
            checkbox.setValue(guideConfigVo.getGuideID());
            product_hlayout_address.appendChild(checkbox);
        }
    }

    private void openLoginWin(Page page) {//打开登录窗口
        final Window window = new Window();
        window.setPage(page);
        window.setTitle("epmall登录");
        window.setBorder("normal");
        window.setClosable(true);
        window.setWidth("280px");
        window.setHeight("280px");
        window.setPosition("center");
        Div div = new Div();
        div.setVflex("1");
        div.setHflex("1");
        div.setParent(window);
        Vlayout vlayout = new Vlayout();
        vlayout.setHflex("1");
        vlayout.setVflex("1");
        vlayout.setParent(div);
        vlayout.setStyle("text-align:center");
        Label title = new Label("登录");
        title.setStyle("font-weight:bold;font-size:13px");
        title.setParent(vlayout);
        final Textbox textAcc = new Textbox();
        textAcc.setHflex("1");
        textAcc.setPlaceholder("手机号/邮箱");
        textAcc.setParent(vlayout);
        final Textbox textPass = new Textbox();
        textPass.setHflex("1");
        textPass.setPlaceholder("密码");
        textPass.setType("password");
        textPass.setParent(vlayout);

        Hlayout capHlayout = new Hlayout();
        capHlayout.setHflex("1");
        final Textbox textCap = new Textbox();
        textCap.setHflex("1");
        textCap.setPlaceholder("输入验证码");
        textCap.setParent(capHlayout);
        final Captcha captcha = new Captcha();
        captcha.setHflex("1");
        captcha.setLength(6);
        captcha.setHeight("35px");
        captcha.setParent(capHlayout);
        captcha.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
              Captcha captcha1 =  ((Captcha)event.getTarget());
              captcha1.randomValue();
            }
        });
        capHlayout.setParent(vlayout);

        Button loginBtn = new Button();
        loginBtn.setWidth("100px");
        loginBtn.setStyle("margin-top:5px");
        loginBtn.setLabel("登录");
        loginBtn.setParent(vlayout);
        loginBtn.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (!textCap.getText().toLowerCase().equals(captcha.getValue().toLowerCase())){
                    WebUtils.showError("验证码错误");
                    return;
                }
                Map<String, String> formData = new HashMap<>();
                formData.put("phone", textAcc.getValue());
                formData.put("password", textPass.getValue());
                formData.put("type", "account");
                HttpUtils.getInstance().requestPostForm(HttpUtils.LOGIN, formData, new HttpUtils.OnMessageListener() {
                    @Override
                    public void onMessage(String result) {
                        if (result == null) {
                            WebUtils.showError("登录失败");
                            return;
                        }
                        JSONObject jsonObject = JSON.parseObject(result);
                        if (!jsonObject.getString("code").equals("0000")) {
                            WebUtils.showError(jsonObject.getString("data"));
                            return;
                        }
                        setUserId(jsonObject.getJSONObject("data").getString("user_id"));
                        Clients.showBusy("提交申请中,请稍后");
                        if (!uploadFile(product_image_uploadMainImg_one,product_image_uploadMainImg_one.getContent(),null)
                                ||!uploadFile(product_image_uploadMainImg_two,product_image_uploadMainImg_two.getContent(),null)
                                ||!uploadFile(product_image_uploadImg_one,product_image_uploadImg_one.getContent(),null)
                                ||!uploadFile(product_image_uploadImg_two,product_image_uploadImg_two.getContent(),null)
                                ||!uploadFile(product_image_uploadImg_three,product_image_uploadImg_three.getContent(),null)
                                ||!uploadFile(product_image_uploadImg_four,product_image_uploadImg_four.getContent(),null)
                                ||!uploadFile(product_image_uploadImg_five,product_image_uploadImg_five.getContent(),null)){
                            WebUtils.showError("提交申请失败！");
                            return;
                        }
                        if (product_radiogroup_type.getSelectedItem().getLabel().equals("发布至软件超市")) {
                            List<Row> rowList = rows.getChildren();
                            for (Row row : rowList) {
                                if (!uploadFile(row, (Media) row.getAttribute("media"),row)){
                                    WebUtils.showError("提交申请失败！");
                                    return;
                                }
                            }
                        }
                        submit();
                        //disableBtn(false);
                        window.detach();
                    }
                });
            }
        });
        window.doHighlighted();
        /*window.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                tab.close();
            }
        });*/
    }

    public String getUserId() {
        //return (String) Sessions.getCurrent().getAttribute("userid");
        return userId;
    }

    public void setUserId(String userid) {
        /*if (Strings.isBlank(userid))
            Sessions.getCurrent().removeAttribute("userid");
        else
            Sessions.getCurrent().setAttribute("userid", userid);*/
        userId = userid;
    }

    @Override
    public void dispatch(Event event) {
        if (event instanceof CheckEvent) {//选中事件
            CheckEvent checkEvent = (CheckEvent) event;
            if (((Radio) checkEvent.getTarget()).getLabel().equals("发布至软件超市")) {
                product_row_market.setVisible(true);
                product_row_lease.setVisible(false);
            } else if (((Radio) checkEvent.getTarget()).getLabel().equals("发布至软件租赁")) {
                product_row_market.setVisible(false);
                product_row_lease.setVisible(true);
            }else if (((Radio) checkEvent.getTarget()).getLabel().equals(Labels.getLabel("admin.project.product.import.list"))) {
                product_vlayout_import_manual.setVisible(false);
                product_vlayout_import_list.setVisible(true);
            }else if (((Radio) checkEvent.getTarget()).getLabel().equals(Labels.getLabel("admin.project.product.import.manual"))) {
                product_vlayout_import_manual.setVisible(true);
                product_vlayout_import_list.setVisible(false);
            } else if(((Radio) checkEvent.getTarget()).getLabel().equals(Labels.getLabel("admin.project.product.free"))){
                if(null!=selectedModule.getModuleId()){
                    product_textbox_module_price.setValue("0");
                    product_textbox_module_price.setDisabled(true);
                    product_button_module_save.setDisabled(false);
                    saveFlag = false;
                }else{
                    WebUtils.showInfo(Labels.getLabel("admin.project.product.selected.error"));
                    product_radiogroup_pro.setSelectedIndex(1);
                }
            }else if(((Radio) checkEvent.getTarget()).getLabel().equals(Labels.getLabel("admin.project.product.negotiable"))) {
                if (null != selectedModule.getModuleId()) {
                    product_textbox_module_price.setValue("negotiable");
                    product_textbox_module_price.setDisabled(true);
                    product_button_module_save.setDisabled(false);
                    saveFlag = false;
                } else {
                    WebUtils.showInfo(Labels.getLabel("admin.project.product.selected.error"));
                    product_radiogroup_pro.setSelectedIndex(1);
                }
            }else if(((Radio) checkEvent.getTarget()).getLabel().equals(Labels.getLabel("admin.project.product.pay"))) {
                if (null != selectedModule.getModuleId()) {
                    product_textbox_module_price.setValue(selectedModule.getPrice());
                    product_textbox_module_price.setDisabled(false);
                    product_button_module_save.setDisabled(false);
                    saveFlag = false;
                } else {
                    WebUtils.showInfo(Labels.getLabel("admin.project.product.selected.error"));
                }
            } else if (((Radio) checkEvent.getTarget()).getLabel().equals("付费.")) {
                List<Row> rowList = rows.getChildren();
                for (Row row : rowList) {
                    product_properties_grid.getColumns().getChildren().get(1).setVisible(true);
                    row.getChildren().get(1).setVisible(true);
                }
            } else {
                List<Row> rowList = rows.getChildren();
                for (Row row : rowList) {
                    product_properties_grid.getColumns().getChildren().get(1).setVisible(false);
                    row.getChildren().get(1).setVisible(false);
                }
            }
        }
        if (event instanceof UploadEvent) {//上传事件
            UploadEvent uploadEvent = (UploadEvent) event;
            org.zkoss.util.media.Media media = uploadEvent.getMedia();
            if (media instanceof org.zkoss.image.Image) {
                org.zkoss.image.Image img = (org.zkoss.image.Image) media;
                /*------------------------主图部分---------------------------*/
                if (event.getTarget() == product_btn_uploadMainImg_one) {
                    //uploadFile(product_image_uploadMainImg_one, img, null);
                    product_image_uploadMainImg_one.setContent(img);
                }
                if (event.getTarget() == product_btn_uploadMainImg_two) {
                    //uploadFile(product_image_uploadMainImg_two, img, null);
                    product_image_uploadMainImg_two.setContent(img);
                }
                /*------------------------产品图片部分------------------------*/
                if (event.getTarget() == product_btn_uploadImg_one) {
                    /*if (uploadFile(product_image_uploadImg_one, img, null))
                        product_vlayout_uploadImg_two.setVisible(true);*/
                    product_image_uploadImg_one.setContent(img);
                    product_vlayout_uploadImg_two.setVisible(true);
                }
                if (event.getTarget() == product_btn_uploadImg_two) {
                    /*if (uploadFile(product_image_uploadImg_two, img, null))
                        product_vlayout_uploadImg_three.setVisible(true);*/
                    product_image_uploadImg_two.setContent(img);
                    product_vlayout_uploadImg_three.setVisible(true);
                }
                if (event.getTarget() == product_btn_uploadImg_three) {
                    /*if (uploadFile(product_image_uploadImg_three, img, null))
                        product_vlayout_uploadImg_four.setVisible(true);*/
                    product_image_uploadImg_three.setContent(img);
                    product_vlayout_uploadImg_four.setVisible(true);
                }
                if (event.getTarget() == product_btn_uploadImg_four) {
                    /*if (uploadFile(product_image_uploadImg_four, img, null))
                        product_vlayout_uploadImg_five.setVisible(true);*/
                    product_image_uploadImg_four.setContent(img);
                    product_vlayout_uploadImg_five.setVisible(true);
                }
                if (event.getTarget() == product_btn_uploadImg_five) {
                    /*uploadFile(product_image_uploadImg_five, img, null);*/
                    product_image_uploadImg_five.setContent(img);
                }
            } else {
                WebUtils.showError("Not an image: " + media);
            }
        }
        if (event instanceof SelectEvent) {//选择事件
            if (event.getTarget() == product_combobox_partition) {//选择分区
                Comboitem comboitem = product_combobox_partition.getSelectedItem();
                JSONArray jsonArray = (JSONArray) comboitem.getAttribute("block");
                product_combobox_block.getChildren().clear();
                for (int i = 0; i < jsonArray.size(); i++) {
                    Comboitem item = new Comboitem();
                    item.setLabel(jsonArray.getJSONObject(i).getString("desc1"));
                    item.setValue(jsonArray.getJSONObject(i).getString("detailno"));
                    product_combobox_block.appendChild(item);
                }
            }
            if (event.getTarget() == product_combobox_block) {//添加版块
                if (product_combobox_block.getSelectedItem() == null)
                    return;
                addItem(product_combobox_block.getSelectedItem());
            }
        }
        if (event.getName().equals(Events.ON_CLICK)) {//点击事件
            if (event.getTarget() == product_btn_submit) {//提交产品申请
                if (!validate())
                    return;
                openLoginWin(product_btn_submit.getPage());
            } else if (event.getTarget() == product_button_add) {//立即添加
                if (Strings.isBlank(product_textbox_verName.getValue())) {
                    WebUtils.showError("请填写版本名称");
                    return;
                }
                if (product_combobox_selectNum.getSelectedItem() == null) {
                    WebUtils.showError("请选择使用人数");
                    return;
                }
                if (product_listbox_verlist.getItems() != null || product_listbox_verlist.getItems().size() != 0) {
                    for (Listitem row : product_listbox_verlist.getItems()) {
                        ProductVersion productVersion = row.getValue();
                        if (productVersion.getVersion().equals(product_textbox_verName.getValue())) {
                            WebUtils.showError("该版本已添加");
                            return;
                        }
                    }
                }
                addProductVersion(product_textbox_verName.getValue(), product_combobox_selectNum.getSelectedItem().getValue() + "");
            } else if (event.getTarget() instanceof Listitem){
                if (event.getTarget().getParent().equals(product_listbox_moduleList)) {
                    Listitem sel = (Listitem) event.getTarget();
                    selectedModule = sel.getValue();
                    showValue();
                    product_button_module_save.setDisabled(true);
                }
            } else if (event.getTarget() instanceof Button) {
                if (((Button) event.getTarget()).getLabel().equals(Labels.getLabel("admin.project.product.module.detail"))) {
                    Listitem sel = (Listitem) event.getTarget().getParent().getParent();
                    selectedModule = sel.getValue();
                    sel.setSelected(true);
                    showValue();
                    List<String> entityIDs = Arrays.asList(selectedModule.getEntityIds().split(","));
                    List<EntityVo> entityVos = StudioApp.execute(new GetEntityListCmd(entityIDs));
                    AbstractView.createAppPageProductView(new EditorCallback<String>() {
                        @Override
                        public String getValue() {
                            return null;
                        }

                        @Override
                        public void setValue(String value) {
                        }

                        @Override
                        public String getTitle() {
                            return null;
                        }

                        @Override
                        public Page getPage() {
                            return product_listbox_moduleList.getPage();
                        }

                        @Override
                        public Editor getEditor() {
                            return null;
                        }
                    }, entityVos, event.getTarget()).doOverlapped();
                } else if (((Button) event.getTarget()).getLabel().equals(Labels.getLabel("button.save"))) {
                    if (Strings.isBlank(product_textbox_module_name.getValue())) {
                        product_textbox_module_name.setFocus(true);
                        throw new WrongValueException(product_textbox_module_name, Labels.getLabel(
                                "message.no.empty", new Object[]{Labels.getLabel("entity.name")}));
                    } else {
                        selectedModule.setModuleName(product_textbox_module_name.getValue());
                        selectedModule.setPrice(product_textbox_module_price.getValue().toString());
                        selectedModule.setDesp(product_textbox_module_desp.getValue());
                        Boolean saveSuccess = StudioApp.execute(new UpdateModuleCmd(selectedModule));
                        if (saveSuccess) {
                            WebUtils.showSuccess(Labels.getLabel("button.save"));
                        } else {
                            WebUtils.showError(Labels.getLabel("message.error", new String[]{Labels.getLabel("button.save")}));
                        }
                        product_button_module_save.setDisabled(true);
                        saveFlag = true;
                    }
                }
            }
        }
        if (event.getTarget() instanceof Textbox && event.getName().equals(Events.ON_CHANGE)) {
            if (null != selectedModule.getModuleId()) {
                product_button_module_save.setDisabled(false);
                saveFlag = false;
            }
        }
    }

    //提交申请方法
    private void submit() {
        String apiUrl = "";
        Map<String, String> formData = new HashMap<>();
        formData.put("buttonType", "C");
        formData.put("user_id", getUserId());//用户id
        formData.put("product_name", product_textbox_name.getValue());//产品名称
        formData.put("introduce", product_textbox_introduce.getValue().replaceAll("\n", "\\\\n"));//产品介绍
        String var = "";
        Iterator iterator = product_hlayout_block.queryAll("label").iterator();
        while (iterator.hasNext()) {
            var += ((Label) iterator.next()).getAttribute("detailno") + ",";
        }
        String belongStr = var.substring(0, var.length() - 1);
        formData.put("belong_to", belongStr);//产品介绍
        List<String> showPathList = new ArrayList<>();
        Iterator<Component> iteratorAddress = product_hlayout_address.queryAll("checkbox").iterator();
        while (iteratorAddress.hasNext()) {
            Checkbox checkbox = (Checkbox) iteratorAddress.next();
            if (checkbox.isChecked()) {
                showPathList.add(product_textbox_address.getValue() + "&wizard=" + checkbox.getValue());
            }
        }
        if (showPathList.size() > 1) {
            JSONArray jsonArray = new JSONArray();
            for (String val : showPathList) {
                jsonArray.add(val);
            }
            formData.put("show_path", jsonArray.toJSONString());//演示地址
        } else if (showPathList.size() == 1)
            formData.put("show_path", showPathList.get(0));//演示地址
        else
            formData.put("show_path", product_textbox_address.getValue());//演示地址
        formData.put("attribution", product_radiogroup_pro.getSelectedItem().getValue() + "");//演示地址
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", "3");
        jsonObject.put("memo", "");
        jsonObject.put("path", ((Image) product_image_uploadMainImg_one).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);
        jsonObject = new JSONObject();
        jsonObject.put("type", "3");
        jsonObject.put("memo", "");
        jsonObject.put("path", ((Image) product_image_uploadMainImg_two).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);

        jsonObject = new JSONObject();
        jsonObject.put("type", "2");
        jsonObject.put("memo", product_textbox_memo_one.getValue().replaceAll("\n", "\\\\n"));
        jsonObject.put("path", ((Image) product_image_uploadImg_one).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);
        jsonObject = new JSONObject();
        jsonObject.put("type", "2");
        jsonObject.put("memo", product_textbox_memo_two.getValue().replaceAll("\n", "\\\\n"));
        jsonObject.put("path", ((Image) product_image_uploadImg_two).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);
        jsonObject = new JSONObject();
        jsonObject.put("type", "2");
        jsonObject.put("memo", product_textbox_memo_three.getValue().replaceAll("\n", "\\\\n"));
        jsonObject.put("path", ((Image) product_image_uploadImg_three).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);
        jsonObject = new JSONObject();
        jsonObject.put("type", "2");
        jsonObject.put("memo", product_textbox_memo_four.getValue().replaceAll("\n", "\\\\n"));
        jsonObject.put("path", ((Image) product_image_uploadImg_four).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);
        jsonObject = new JSONObject();
        jsonObject.put("type", "2");
        jsonObject.put("memo", product_textbox_memo_five.getValue().replaceAll("\n", "\\\\n"));
        jsonObject.put("path", ((Image) product_image_uploadImg_five).getAttribute("fileUrl"));
        jsonArray.add(jsonObject);
        formData.put("product_image", jsonArray.toJSONString());//产品图片
        if (product_radiogroup_type.getSelectedItem().getLabel().equals("发布至软件超市")) {
            apiUrl = HttpUtils.PRODUCT;
            jsonArray = new JSONArray();
            List<Row> rowList = rows.getChildren();
            for (Row row : rowList) {
                Map<String, String> map = row.getValue();
                jsonObject = new JSONObject();
                jsonObject.put("name", map.get("model"));
                if (product_radiogroup_pro.getSelectedItem().getValue().equals("2"))
                    jsonObject.put("price", map.get("price"));
                else
                    jsonObject.put("price", "");
                jsonObject.put("attach_path", map.get("packPath"));
                jsonArray.add(jsonObject);
            }
            formData.put("product_price", jsonArray.toJSONString());// 产品模块
        } else if (product_radiogroup_type.getSelectedItem().getLabel().equals("发布至软件租赁")) {
            apiUrl = HttpUtils.RENT;
            jsonArray = new JSONArray();
            for (Listitem row : product_listbox_verlist.getItems()) {
                ProductVersion productVersion = row.getValue();
                for (Map<String, String> map : productVersion.getData()) {
                    jsonObject = new JSONObject();
                    jsonObject.put("name", productVersion.getVersion());
                    jsonObject.put("user_amount", productVersion.getUserNum());
                    jsonObject.put("description", productVersion.getVerDesp().replaceAll("\n", "\\\\n"));
                    jsonObject.put("rent_time", map.get("time"));
                    jsonObject.put("price", map.get("price"));
                    jsonArray.add(jsonObject);
                }
            }
            formData.put("product_price", jsonArray.toJSONString());// 产品模块
        }
        HttpUtils.getInstance().requestPostForm(apiUrl, formData, new HttpUtils.OnMessageListener() {
            @Override
            public void onMessage(String result) {
                Clients.clearBusy();
                if (result == null) {
                    WebUtils.showError("提交申请失败！");
                }
                JSONObject resultObj = JSON.parseObject(result);
                if (!resultObj.getString("code").equals("0000")) {
                    WebUtils.showError(resultObj.getString("data"));
                    return;
                }
                WebUtils.showSuccess("提交申请");
            }
        });
    }

    //文件上传方法
    /*private boolean uploadFile(final Object show, final org.zkoss.util.media.Media media, final Row row) {
        try {
            File tempFile = File.createTempFile(media.getName().split("\\.")[0], "." + media.getFormat());
            FileUtils.copyInputStreamToFile(media.getStreamData(), tempFile);
            Map<String, String> param = new HashMap<>();
            param.put("path_type", "public");
            param.put("fileName", media.getName());
            param.put("user_id", getUserId());
            param.put("type", "file");
            final boolean[] flag = {false};
            HttpUtils.getInstance().uploadFile(tempFile, "file", HttpUtils.UPLOAD_FILE, param, new HttpUtils.OnUploadProcessListener() {
                @Override
                public void onUploadDone(int responseCode, String message) {
                    if (responseCode != HttpUtils.UPLOAD_SUCCESS_CODE) {
                        WebUtils.showError(message);
                        return;
                    }
                    JSONObject jsonObject = JSON.parseObject(message);
                    if (!jsonObject.getString("code").equals("200")) {
                        WebUtils.showError("文件上传失败");
                        return;
                    }
                    if (show instanceof Image) {
                        ((Image) show).setContent((org.zkoss.image.Image) media);
                        ((Image) show).setAttribute("fileUrl", jsonObject.getString("data"));
                    } else if (show instanceof Button) {
                        ((Button) show).setLabel(media.getName());
                        if (row != null) {
                            Map<String, String> map = row.getValue();
                            map.put("packPath", jsonObject.getString("data"));
                        }
                    }
                    flag[0] = true;
                }

                @Override
                public void onUploadProcess(int uploadSize) {

                }

                @Override
                public void initUpload(int fileSize) {

                }
            });
            tempFile.deleteOnExit();
            return flag[0];
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }*/
    private boolean uploadFile(final Object show, final org.zkoss.util.media.Media media, final Row row) {
        try {
            File tempFile = File.createTempFile(media.getName().split("\\.")[0], "." + media.getFormat());
            InputStream inputStream = null;
            try {
                inputStream = media.getStreamData();
            }catch (Exception e){
                inputStream = new ByteArrayInputStream(media.getStringData().getBytes());
            }
            FileUtils.copyInputStreamToFile(media.getStreamData(), tempFile);
            Map<String, String> param = new HashMap<>();
            param.put("path_type", "public");
            param.put("fileName", media.getName());
            param.put("user_id", getUserId());
            param.put("type", "file");
            final boolean[] flag = {false};
            HttpUtils.getInstance().uploadFile(tempFile, "file", HttpUtils.UPLOAD_FILE, param, new HttpUtils.OnUploadProcessListener() {
                @Override
                public void onUploadDone(int responseCode, String message) {
                    if (responseCode != HttpUtils.UPLOAD_SUCCESS_CODE) {
                        WebUtils.showError(message);
                        return;
                    }
                    JSONObject jsonObject = JSON.parseObject(message);
                    if (!jsonObject.getString("code").equals("200")) {
                        //WebUtils.showError("文件上传失败");
                        return;
                    }
                    if (show instanceof Image) {
                        ((Image) show).setAttribute("fileUrl", jsonObject.getString("data"));
                    } else if (row != null) {
                        Map<String, String> map = row.getValue();
                        map.put("packPath", jsonObject.getString("data"));
                    }
                    flag[0] = true;
                }

                @Override
                public void onUploadProcess(int uploadSize) {

                }

                @Override
                public void initUpload(int fileSize) {

                }
            });
            tempFile.deleteOnExit();
            return flag[0];
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    //添加产品属性的添加版本行
    public void addProductVersion(String version, String userNum) {
        final ProductVersion productVersion = new ProductVersion();
        productVersion.setVersion(version);
        productVersion.setUserNum(userNum);

        Listitem row = new Listitem();
        row.setValue(productVersion);
        row.appendChild(new Listcell());
        row.appendChild(new Listcell(version));
        row.appendChild(new Listcell(userNum));

        final Listcell listcell_time = new Listcell();
        final Vlayout vlayout_time = new Vlayout();
        vlayout_time.setHflex("1");
        final Combobox combobox = new Combobox();
        combobox.setHflex("1");
        combobox.setPlaceholder("添加期限");
        Comboitem comboitem = new Comboitem();
        comboitem.setLabel("3个月");
        comboitem.setParent(combobox);
        comboitem = new Comboitem();
        comboitem.setLabel("6个月");
        comboitem.setParent(combobox);
        comboitem = new Comboitem();
        comboitem.setLabel("1年");
        comboitem.setParent(combobox);
        comboitem = new Comboitem();
        comboitem.setLabel("2年");
        comboitem.setParent(combobox);
        comboitem = new Comboitem();
        comboitem.setLabel("3年");
        comboitem.setParent(combobox);
        comboitem = new Comboitem();
        comboitem.setLabel("5年");
        comboitem.setParent(combobox);
        vlayout_time.appendChild(combobox);
        vlayout_time.setParent(listcell_time);
        row.appendChild(listcell_time);

        Listcell listcell_price = new Listcell();
        final Vlayout vlayout_price = new Vlayout();
        vlayout_price.setHflex("1");
        final Textbox textbox = new Textbox();
        textbox.setConstraint("/(^[1-9]\\d*(\\.\\d{1,2})?$)|(^0(\\.\\d{1,2})?$)/:格式不正确,请输入数字,仅能有两个小数位");
        textbox.setPlaceholder("请输入价格");
        textbox.setHflex("1");
        vlayout_price.appendChild(textbox);
        vlayout_price.setParent(listcell_price);
        row.appendChild(listcell_price);

        Listcell listcell_a = new Listcell();
        final Vlayout vlayout_a = new Vlayout();
        vlayout_a.setHflex("1");
        final A add = new A("添加");
        add.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (combobox.getSelectedItem() == null) {
                    WebUtils.showError("请选择版本期限");
                    return;
                }
                if (Strings.isBlank(textbox.getValue())) {
                    WebUtils.showError("请输入版本价格");
                    return;
                }
                for (Map<String, String> map : productVersion.getData()) {
                    if (map.get("time").equals(combobox.getSelectedItem().getLabel())) {
                        WebUtils.showError("一个版本不能重复添加相同的使用期限");
                        return;
                    }
                }
                Map<String, String> rowData = new HashMap<>();
                rowData.put("time", combobox.getSelectedItem().getLabel());
                rowData.put("price", textbox.getValue());
                productVersion.getData().add(rowData);

                Label label_time = new Label(combobox.getSelectedItem().getLabel());
                Label label_price = new Label(textbox.getValue());
                vlayout_time.insertBefore(label_time, combobox);
                vlayout_price.insertBefore(label_price, textbox);
                A delete = new A("删除");
                delete.setAttribute("rowData", rowData);
                delete.setAttribute("label_time", label_time);
                delete.setAttribute("label_price", label_price);
                delete.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        productVersion.getData().remove(event.getTarget().getAttribute("rowData"));
                        ((Component) event.getTarget().getAttribute("label_time")).detach();
                        ((Component) event.getTarget().getAttribute("label_price")).detach();
                        event.getTarget().detach();
                    }
                });
                vlayout_a.insertBefore(delete, add);
            }
        });
        vlayout_a.appendChild(add);
        vlayout_a.setParent(listcell_a);
        row.appendChild(listcell_a);

        Listcell listcell_text = new Listcell();
        Textbox textboxRow = new Textbox();
        textboxRow.setHflex("1");
        textboxRow.setRows(3);
        listcell_text.appendChild(textboxRow);
        row.appendChild(listcell_text);
        textboxRow.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                productVersion.setVerDesp(((InputEvent) event).getValue());
            }
        });

        row.setParent(product_listbox_verlist);
    }

    private class ProductVersion {
        private String version = "";
        private String userNum = "";
        private List<Map<String, String>> data = new ArrayList<>();
        private String verDesp = "";

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getUserNum() {
            return userNum;
        }

        public void setUserNum(String userNum) {
            this.userNum = userNum;
        }

        public List<Map<String, String>> getData() {
            return data;
        }

        public void setData(List<Map<String, String>> data) {
            this.data = data;
        }

        public String getVerDesp() {
            return verDesp;
        }

        public void setVerDesp(String verDesp) {
            this.verDesp = verDesp;
        }

    }

    //添加产品属性的配置行
    public void addProductPro() {
        String val = product_radiogroup_manual_pro.getSelectedItem().getLabel();
        Map<String, String> map = new HashMap<>();
        map.put("model", "");
        map.put("price", "");
        map.put("packPath", "");
        final Row row = new Row();
        row.setValue(map);
        Textbox textbox = new Textbox();
        textbox.setPlaceholder("请输入功能模块");
        textbox.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                Map<String, String> map = row.getValue();
                map.put("model", ((InputElement) event.getTarget()).getRawValue() + "");
            }
        });
        row.appendChild(textbox);
        textbox = new Textbox();
        if (val.equals("付费"))
            textbox.setVisible(true);
        else
            textbox.setVisible(false);
        textbox.setPlaceholder("请输入产品价格");
        textbox.setConstraint("/(^[1-9]\\d*(\\.\\d{1,2})?$)|(^0(\\.\\d{1,2})?$)/:格式不正确,请输入数字,仅能有两个小数位");
        textbox.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                Map<String, String> map = row.getValue();
                map.put("price", ((InputElement) event.getTarget()).getRawValue() + "");
            }
        });
        row.appendChild(textbox);
        final Button button = new Button();
        button.setUpload("true");
        button.setLabel("上传产品压缩包");
        button.addEventListener(Events.ON_UPLOAD, new EventListener<UploadEvent>() {
            @Override
            public void onEvent(UploadEvent event) throws Exception {
                org.zkoss.util.media.Media media = event.getMedia();
                //uploadFile(button, media, row);
                button.setLabel(media.getName());
                row.setAttribute("media", media);
            }
        });
        row.appendChild(button);
        Div div = new Div();
        A a = new A();
        a.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                if (rows.getChildren().size() > 1) {
                    row.detach();
                }
            }
        });
        a.setIconSclass("z-icon-minus");
        a.setSclass("btn btn-light btn-sm");
        div.appendChild(a);
        a = new A();
        a.setIconSclass("z-icon-plus");
        a.setSclass("btn btn-light btn-sm");
        a.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                addProductPro();
            }
        });
        div.appendChild(a);
        row.appendChild(div);
        rows.appendChild(row);
    }
    public void createModuleList() {
        List<ModuleVo> moduleVos = StudioApp.execute(new GetModuleCmd(null));
        List<Listitem> items = product_listbox_moduleList.getItems();
        for (ModuleVo vo : moduleVos) {
            Listitem ti = new Listitem();
            ti.setStyle("text-align:center");
            ti.appendChild(new Listcell(vo.getModuleId()));
            ti.appendChild(new Listcell(vo.getModuleName()));
            Listcell lc = new Listcell();
            Button detail = new Button();
            detail.setLabel(Labels.getLabel("admin.project.product.module.detail"));
            detail.addEventListener(Events.ON_CLICK, this);
            detail.setSclass("ml-2");
            lc.appendChild(detail);
            ti.appendChild(lc);
            ti.setValue(vo);
            ti.addEventListener(Events.ON_CLICK, this);
            items.add(ti);
        }
    }

    //选择版块添加新item
    private void addItem(Comboitem item) {
        List<Label> labelList = new ArrayList<>();
        Iterator iterator = product_hlayout_block.queryAll("label").iterator();
        while (iterator.hasNext()) {
            labelList.add((Label) iterator.next());
        }
        if (labelList.size() == 5) {
            return;
        }
        for (Label label : labelList) {
            if (label.getValue().equals(item.getLabel())) {
                return;
            }
        }
        final Hlayout hl = new Hlayout();
        hl.setSpacing("6px");
        hl.setClass("newFile");
        Label label = new Label(item.getLabel());
        label.setAttribute("detailno", item.getValue());
        hl.appendChild(label);
        A rm = new A();
        rm.setIconSclass("z-icon-remove");
        rm.addEventListener(Events.ON_CLICK, new EventListener() {
            public void onEvent(Event event) throws Exception {
                hl.detach();
            }
        });
        hl.appendChild(rm);
        product_hlayout_block.appendChild(hl);
    }

    public void initRequestApi() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "p");
        HttpUtils.getInstance().requestPostForm(HttpUtils.PART_INFO, map, new HttpUtils.OnMessageListener() {
            @Override
            public void onMessage(String result) {
                if (result == null) {
                    WebUtils.showError("分区信息请求失败");
                    return;
                }
                JSONObject jsonObject = JSON.parseObject(result);
                if (!jsonObject.getString("code").equals("0000")) {
                    WebUtils.showError("分区信息请求失败");
                    return;
                }
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                for (int i = 0; i < jsonArray.size(); i++) {
                    if (jsonArray.getJSONObject(i).getString("detailid").equals("开发者")) {
                        Comboitem comboitem = new Comboitem();
                        comboitem.setLabel(jsonArray.getJSONObject(i).getString("desc1"));
                        comboitem.setValue(jsonArray.getJSONObject(i).getString("detailno"));
                        JSONArray array = JSON.parseArray(jsonArray.getJSONObject(i).getString("block"));
                        comboitem.setAttribute("block", array);
                        product_combobox_partition.appendChild(comboitem);
                    }
                }
            }
        });
    }

    private boolean validate() {
        if (Strings.isBlank(product_textbox_name.getValue())) {
            WebUtils.showError("产品名称不能为空");
            return false;
        }
        if (product_textbox_name.getValue().length() > 20) {
            WebUtils.showError("产品名称不能超过20字");
            return false;
        }
        if (Strings.isBlank(product_textbox_introduce.getValue())) {
            WebUtils.showError("产品介绍不能为空");
            return false;
        }
        if (product_textbox_introduce.getValue().length() > 300) {
            WebUtils.showError("产品名称不能超过300字");
            return false;
        }
        List<Label> labelList = new ArrayList<>();
        Iterator iterator = product_hlayout_block.queryAll("label").iterator();
        while (iterator.hasNext()) {
            labelList.add((Label) iterator.next());
        }
        if (labelList.size() < 2) {
            WebUtils.showError("至少选择两个分类");
            return false;
        }
        if (product_image_uploadMainImg_one.getContent() == null || product_image_uploadMainImg_two.getContent() == null) {
            WebUtils.showError("请上传两张主图");
            return false;
        }
        if (product_image_uploadImg_one.getContent() == null || product_image_uploadImg_two.getContent() == null || product_image_uploadImg_three.getContent() == null || product_image_uploadImg_four.getContent() == null || product_image_uploadImg_five.getContent() == null) {
            WebUtils.showError("请上传5张产品展示图");
            return false;
        }
        /*if (product_image_uploadMainImg_one.getAttribute("fileUrl") == null || product_image_uploadMainImg_two.getAttribute("fileUrl") == null) {
            WebUtils.showError("请上传两张主图");
            return false;
        }
        if (product_image_uploadImg_one.getAttribute("fileUrl") == null || product_image_uploadImg_two.getAttribute("fileUrl") == null || product_image_uploadImg_three.getAttribute("fileUrl") == null || product_image_uploadImg_four.getAttribute("fileUrl") == null || product_image_uploadImg_five.getAttribute("fileUrl") == null) {
            WebUtils.showError("请上传5张产品展示图");
            return false;
        }*/
        if (Strings.isBlank(product_textbox_memo_one.getValue()) || Strings.isBlank(product_textbox_memo_two.getValue()) || Strings.isBlank(product_textbox_memo_three.getValue()) || Strings.isBlank(product_textbox_memo_four.getValue()) || Strings.isBlank(product_textbox_memo_five.getValue())) {
            WebUtils.showError("产品图片说明不能为空");
            return false;
        }
        if (product_textbox_memo_one.getValue().length() > 160 || product_textbox_memo_two.getValue().length() > 160 || product_textbox_memo_three.getValue().length() > 160 || product_textbox_memo_four.getValue().length() > 160 || product_textbox_memo_five.getValue().length() > 160) {
            WebUtils.showError("产品图片说明不能超过160字");
            return false;
        }
        if (Strings.isBlank(product_textbox_address.getValue())) {
            WebUtils.showError("请填写演示地址");
            return false;
        }
        if (!isHttpUrl(product_textbox_address.getValue())) {
            WebUtils.showError("请填写正确的演示地址");
            return false;
        }
        if (product_radiogroup_type.getSelectedItem().getLabel().equals("发布至软件超市")) {
            if(null==selectedModule.getModuleId()){
                WebUtils.showInfo(Labels.getLabel("admin.project.product.selected.error"));
                return false;
            }
            if(saveFlag==false){
                WebUtils.showInfo(Labels.getLabel("entity.api.preSave"));
                return false;
            }
            List<Row> rowList = rows.getChildren();
            for (Row row : rowList) {
                Map<String, String> map = row.getValue();
                if (Strings.isBlank(map.get("model"))) {
                    WebUtils.showError("模块名称不能为空");
                    return false;
                }
                if (Strings.isBlank(map.get("packPath"))) {
                    WebUtils.showError("请上传模块压缩包");
                    return false;
                }
                if (row.getAttribute("media") == null) {
                    WebUtils.showError("请上传模块压缩包");
                    return false;
                }
            }
        } else if (product_radiogroup_type.getSelectedItem().getLabel().equals("发布至软件租赁")) {
            if (product_listbox_verlist.getItems() == null || product_listbox_verlist.getItems().size() == 0) {
                WebUtils.showError("请添加产品属性");
                return false;
            }
        }
        return true;
    }

    private void disableBtn(boolean flag) {
        product_btn_uploadMainImg_one.setDisabled(flag);
        product_btn_uploadMainImg_two.setDisabled(flag);
        product_btn_uploadImg_one.setDisabled(flag);
        product_btn_uploadImg_two.setDisabled(flag);
        product_btn_uploadImg_three.setDisabled(flag);
        product_btn_uploadImg_four.setDisabled(flag);
        product_btn_uploadImg_five.setDisabled(flag);
        List<Row> rowList = rows.getChildren();
        for (Row row : rowList) {
            Button button = (Button) row.query("button");
            button.setDisabled(flag);
            Iterable<Component> iterable = row.queryAll("a");
            Iterator iterator = iterable.iterator();
            while (iterator.hasNext()) {
                ((A) iterator.next()).setDisabled(flag);
            }
        }
        product_button_add.setDisabled(flag);
        product_btn_submit.setDisabled(flag);
    }

    /**
     * 判断字符串是否为URL
     *
     * @param urls 需要判断的String类型url
     * @return true:是URL；false:不是URL
     */
    public boolean isHttpUrl(String urls) {
        boolean isurl = false;
        String regex = "(((https|http)?://)?([a-z0-9]+[.])|(www.))"
                + "\\w+[.|\\/]([a-z0-9]{0,})?[[.]([a-z0-9]{0,})]+((/[\\S&&[^,;\u4E00-\u9FA5]]+)+)?([.][a-z0-9]{0,}+|/?)";//设置正则表达式
        Pattern pat = Pattern.compile(regex.trim());//对比
        Matcher mat = pat.matcher(urls.trim());
        isurl = mat.matches();//判断是否匹配
        if (isurl) {
            isurl = true;
        }
        return isurl;
    }

    private void showValue() {
        product_textbox_module_name.setValue(selectedModule.getModuleName());
        product_textbox_module_price.setValue(selectedModule.getPrice());
        product_textbox_module_desp.setValue(selectedModule.getDesp());
        if(Strings.isBlank(selectedModule.getDicDetailID())){
            product_lable_module_parent.setValue(Labels.getLabel("wizard.none"));
        }else{
            product_lable_module_parent.setValue(selectedModule.getDicDetailID());
        }
    }

}
