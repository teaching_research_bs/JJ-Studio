/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.entities.beans.table.TableField;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TableInfo {

	private String table;
	
	private List<TableField> fields;

	/**
	 * @param table
	 * @param fields
	 */
	public TableInfo(String table, List<TableField> fields) {
		this.table = table;
		this.fields = fields;
	}

	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}

	/**
	 * @return the fields
	 */
	public List<TableField> getFields() {
		return fields;
	}
	
	
}
