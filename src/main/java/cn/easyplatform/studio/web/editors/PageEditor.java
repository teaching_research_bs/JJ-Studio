/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.web.ext.cmez.CMeditor;
import nu.xom.Element;
import org.zkoss.zk.ui.Component;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface PageEditor {

    void updateContent(String content);

    void updateProperties(Element selection,
                          List<Map<String, Component>> properties,
                          Map<String, Component> events);

    void showMsg(String msg, CMeditor editor);
}
