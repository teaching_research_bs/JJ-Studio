/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.support;

import cn.easyplatform.studio.utils.StudioUtil;
import nu.xom.Node;

import java.lang.reflect.Method;
import java.util.Map;

import static org.zkoss.lang.Generics.cast;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Element extends nu.xom.Element {
    public Element(String name, String uri) {
        super(name, uri.intern());
    }

    public Element(nu.xom.Element element) {
        super(element);
    }

    private static final Method m;

    static {
        m = StudioUtil.findMethod(nu.xom.Element.class, "getNamespacePrefixesInScope");
        m.setAccessible(true);
    }

    @Override
    public void addNamespaceDeclaration(String prefix, String uri) {
        super.addNamespaceDeclaration(prefix, uri.intern());
    }

    @SuppressWarnings("rawtypes")
	public Map<String, String> getNamespacePrefixInScope() {
        try {
            return cast((Map) m.invoke(this));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Node copy() {
        return new Element(this);
    }
}
