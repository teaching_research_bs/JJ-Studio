/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ComponentEditorCallback implements EditorCallback<String> {

	private Component base;

	private String title;

	/**
	 * @param base
	 * @param title
	 */
	public ComponentEditorCallback(Component base, String title) {
		this.base = base;
		this.title = title;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public void setValue(String value) {
		Events.postEvent(new Event(Events.ON_CHANGE, base, value));
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public Page getPage() {
		return base.getPage();
	}

	@Override
	public Editor getEditor() {
		return null;
	}

}
