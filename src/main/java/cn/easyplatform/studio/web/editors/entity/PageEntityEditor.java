/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.entity;

import cn.easyplatform.entities.beans.page.BindVariable;
import cn.easyplatform.entities.beans.page.DeviceBean;
import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.customWidget.GetAllWidgetCmd;
import cn.easyplatform.studio.cmd.identity.GetAccessInfoCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.AttributeCnUtil;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.AccessVo;
import cn.easyplatform.studio.vos.CustomWidgetGroupVo;
import cn.easyplatform.studio.vos.CustomWidgetVo;
import cn.easyplatform.studio.vos.DeviceVo;
import cn.easyplatform.studio.web.editors.*;
import cn.easyplatform.studio.web.editors.page.PageDesigner;
import cn.easyplatform.studio.web.editors.support.CodeFormatter;
import cn.easyplatform.studio.web.editors.support.NodeFactory;
import cn.easyplatform.studio.web.editors.support.ZulXsdUtil;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.studio.web.views.impl.PageTemplateView;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.web.ext.cmez.CMeditor;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Scrollview;
import org.zkoss.zul.*;
import org.zkoss.zul.Button;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PageEntityEditor extends AbstractEntityEditor<PageBean> implements
        CodeEditor, PageEditor {

    private Tabbox tabbox;

    private Idspace ajaxCanvas;

    private Idspace milCanvas;

    private Textbox entityId;

    private Textbox entityName;

    private Textbox entityDesp;

    private Bandbox entityTable;

    private Bandbox entityInherit;

    private Intbox entityWidth;

    private Intbox entityHeight;

    private Textbox entityDestination;

    private Button ajaxVariable;

    private Button milVariable;

    private Button cssContext;

    private Button jsContext;

    private Button milCssContext;

    private Button milJsContext;

    private Style ajaxStyle;

    private Style milStyle;

    private Button ajax;

    private Button mil;

    private Button script;

    private Button onVisible;

    private Button onMessage;

    private Button onShow;

    private Bandbox onLoad;

    private Bandbox onOk;

    private Bandbox onBack;

    private Bandbox onRefresh;

    private CMeditor ajaxEditor;

    private CMeditor milEditor;

    private PageDesigner ajaxDesigner;

    private PageDesigner milDesigner;

    private Rows properties;

    private Label milMsg;

    private Label ajaxMsg;

    private Image bgImage;

    private Div canvasDiv;
    //private Window ajaxTemplates;
    private Scrollview ajaxScrollview;

    private Label sizeLabel;

    private List<AccessVo> accessVos;
    /**
     * @param workbench
     * @param entity
     * @param code
     */
    public PageEntityEditor(WorkbenchController workbench, PageBean entity,
                            char code) {
        super(workbench, entity, code);
    }

    @Override
    public void create(Object... args) {
        super.create(args);
        if (args.length > 0)
            entity.setTable((String) args[0]);
        Tabpanel tabpanel = new Tabpanel();
        Idspace is = new Idspace();
        is.setHflex("1");
        is.setVflex("1");
        is.setParent(tabpanel);
        Executions.createComponents("~./include/editor/entity/page.zul", is,
                null);
        String theme = Contexts.getUser().getEditorTheme();
        for (Component comp : is.getFellows()) {
            if (comp.getId().equals("entityPage_tabbox_main")) {
                tabbox = (Tabbox) comp;
                tabbox.addEventListener(Events.ON_SELECT, this);
            } else if (comp.getId().equals("entityPage_idspace_source")) {
                source = (CMeditor) comp;
                source.setTheme(theme);
            } else if (comp.getId().equals("entityPage_textbox_property_id")) {
                entityId = (Textbox) comp;
                entityId.setValue(entity.getId());
                entityId.setReadonly(true);
            } else if (comp.getId().equals("entityPage_textbox_property_name")) {
                entityName = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityName.setValue(entity.getName());
            } else if (comp.getId().equals("entityPage_textbox_property_desp")) {
                entityDesp = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityDesp.setValue(entity.getDescription());
            } else if (comp.getId().equals("entityPage_textbox_property_table")) {
                entityTable = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                entityTable.setValue(entity.getTable());
            } else if (comp.getId().equals("entityPage_bandbox_property_inherit")) {
                entityInherit = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                entityInherit.setValue(entity.getInheritId());
            } else if (comp.getId().equals("entityPage_intbox_property_width")) {
                entityWidth = (Intbox) comp;
                entityWidth.setValue(entity.getWidth());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (comp.getId().equals("entityPage_intbox_property_height")) {
                entityHeight = (Intbox) comp;
                entityHeight.setValue(entity.getHeight());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (comp.getId().equals("entityPage_textbox_property_destination")) {
                entityDestination = (Textbox) comp;
                entityDestination.setValue(entity.getDestination());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (comp.getId().equals("entityPage_bandbox_event_onLoad")) {
                onLoad = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnLoad() != null) {
                    if (!Strings.isBlank(entity.getOnLoad().getId())) {
                        onLoad.setValue(entity.getOnLoad().getId());
                    } else if (!Strings
                            .isBlank(entity.getOnLoad().getContent())) {
                        ((Button) onLoad.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("entityPage_bandbox_event_onOk")) {
                onOk = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnOk() != null) {
                    if (!Strings.isBlank(entity.getOnOk().getId())) {
                        onOk.setValue(entity.getOnOk().getId());
                    } else if (!Strings.isBlank(entity.getOnOk().getContent())) {
                        ((Button) onOk.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("entityPage_bandbox_event_onBack")) {
                onBack = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnBack() != null) {
                    if (!Strings.isBlank(entity.getOnBack().getId())) {
                        onBack.setValue(entity.getOnBack().getId());
                    } else if (!Strings
                            .isBlank(entity.getOnBack().getContent())) {
                        ((Button) onBack.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("entityPage_bandbox_event_onRefresh")) {
                onRefresh = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnRefresh() != null) {
                    if (!Strings.isBlank(entity.getOnRefresh().getId())) {
                        onRefresh.setValue(entity.getOnRefresh().getId());
                    } else if (!Strings.isBlank(entity.getOnRefresh()
                            .getContent())) {
                        ((Button) onRefresh.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("ajax")) {
                ajax = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (!Strings.isBlank(entity.getAjax()))
                    ajax.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_event_script")) {
                script = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (!Strings.isBlank(entity.getScript()))
                    script.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("mil")) {
                mil = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (!Strings.isBlank(entity.getMil()))
                    mil.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_event_onVisible")) {
                onVisible = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (!Strings.isBlank(entity.getOnVisible()))
                    onVisible.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_event_onMessage")) {
                onMessage = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (!Strings.isBlank(entity.getOnMessage()))
                    onMessage.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_event_onShow")) {
                onShow = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (!Strings.isBlank(entity.getOnShow()))
                    onShow.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_property_ajax_variable")) {
                ajaxVariable = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (entity.getAjaxDevice() != null && entity.getAjaxDevice().getBindVariables() != null &&
                        entity.getAjaxDevice().getBindVariables().size() > 0)
                    ((Button)comp).setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_property_mil_variable")) {
                milVariable = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (entity.getMilDevice() != null && entity.getMilDevice().getBindVariables() != null &&
                        entity.getMilDevice().getBindVariables().size() > 0)
                    ((Button)comp).setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_idspace_desktop_ajaxCanvas")) {
                ajaxCanvas = (Idspace) comp;
                ajaxScrollview = (Scrollview) ajaxCanvas.getParent();
            } else if (comp.getId().equals("entityPage_idspace_mobile_milCanvas")) {
                milCanvas = (Idspace) comp;
            } else if (comp.getId().equals("entityPage_button_property_ajax_css")) {
                cssContext = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (entity.getAjaxDevice() != null && !Strings.isBlank(entity.getAjaxDevice().getStyle()))
                    cssContext.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_property_ajax_script")) {
                jsContext = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (entity.getAjaxDevice() != null && !Strings.isBlank(entity.getAjaxDevice().getJavascript()))
                    jsContext.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_property_mil_css")) {
                milCssContext = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (entity.getMilDevice() != null && !Strings.isBlank(entity.getMilDevice().getStyle()))
                    milCssContext.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_button_property_mil_script")) {
                milJsContext = (Button) comp;
                comp.addEventListener(Events.ON_CLICK, this);
                if (entity.getMilDevice() != null && !Strings.isBlank(entity.getMilDevice().getJavascript()))
                    milJsContext.setSclass("epeditor-btn-mark");
            } else if (comp.getId().equals("entityPage_idspace_desktop_ajaxEditor")) {
                ajaxEditor = (CMeditor) comp;
                ajaxEditor.setTheme(theme);
                if (!Strings.isBlank(entity.getAjax()))
                    ajaxEditor.setValue(CodeFormatter.formatXML(ZulXsdUtil
                            .buildDocument(entity.getAjax())));
                ajaxEditor.addEventListener(Events.ON_CHANGING, this);
            } else if (comp.getId().equals("entityPage_idspace_mobile_milEditor")) {
                milEditor = (CMeditor) comp;
                milEditor.setTheme(theme);
                if (!Strings.isBlank(entity.getMil()))
                    milEditor.setValue(CodeFormatter.formatXML(ZulXsdUtil
                            .buildDocument(entity.getMil())));
                milEditor.addEventListener(Events.ON_CHANGING, this);
            } /*else if (comp.getId().equals("entityPage_window_ajaxTemplates")
                    || comp.getId().equals("milTemplates")) {
                ajaxTemplates = (Window) comp;
                ajaxTemplates.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        ajaxTemplates.setVisible(false);
                        event.stopPropagation();
                    }
                });
                Iterator<Component> itr = comp.queryAll("toolbarbutton")
                        .iterator();
                while (itr.hasNext()) {
                    Button widget = (Button) itr.next();
                    widget.setDraggable(EntityType.TABLE.getName());
                    widget.setAttribute("t", "");
                }
            }*/ else if (comp.getId().equals("properties"))
                properties = (Rows) comp;
            else if (comp.getId().equals("milMsg"))
                milMsg = (Label) comp;
            else if (comp.getId().equals("ajaxMsg"))
                ajaxMsg = (Label) comp;
            else if (comp.getId().equals("entityPage_image_bg"))
                bgImage = (Image) comp;
            else if (comp.getId().equals("entityPage_div_mobile_milCanvas"))
                canvasDiv = (Div) comp;
            else if (comp.getId().equals("entityPage_a_minus") ||
                    comp.getId().equals("entityPage_a_plus"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_label_size"))
                sizeLabel = (Label) comp;
            else if (comp.getId().equals("entityPage_button_template"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_button_add_ajax_custom"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_button_add_mobile_custom"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_button_mobile_template"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_button_show_ajax_custom"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_button_show_mobile_custom"))
                comp.addEventListener(Events.ON_CLICK, this);
            else if (comp.getId().equals("entityPage_label_name")) {
                Label nameLabel = (Label) comp;
                nameLabel.setValue(Labels.getLabel("entity.id") + "：" + entity.getId() + " " +
                        Labels.getLabel("entity.name") + "：" + entity.getName());
            }
        }
        accessVos = StudioApp.execute(new GetAccessInfoCmd());

        final PageEntityEditor entityEditor = this;
        ajaxDesigner = new PageDesigner(workbench, this, ajaxCanvas, ajaxEditor, false, accessVos);
        ajaxDesigner.setDragListener(new PageDesigner.OnDragListener() {
            @Override
            public void getDevice(DeviceVo deviceVo) {
                if (entity.getAjaxDevice() == null)
                    entity.setAjaxDevice(new DeviceBean());
                if (deviceVo != null) {
                    boolean isChange = false;
                    if (Strings.isBlank(deviceVo.getStyle()) == false) {
                        String content = null;
                        if (Strings.isBlank(entity.getAjaxDevice().getStyle()) == false)
                            content = entity.getAjaxDevice().getStyle() + deviceVo.getStyle();
                        else
                            content =deviceVo.getStyle();
                        entity.getAjaxDevice().setStyle(content);
                        cssContext.setSclass("epeditor-btn-mark");
                        isChange = true;
                    }

                    if (Strings.isBlank(deviceVo.getJavascript()) == false) {
                        String content = null;
                        if (Strings.isBlank(entity.getAjaxDevice().getJavascript()) == false)
                            content = entity.getAjaxDevice().getJavascript() + deviceVo.getJavascript();
                        else
                            content =deviceVo.getJavascript();
                        entity.getAjaxDevice().setJavascript(content);
                        jsContext.setSclass("epeditor-btn-mark");
                        isChange = true;
                    }

                    if (deviceVo.getBindVariables() != null && deviceVo.getBindVariables().size() > 0) {
                        List<BindVariable> bindVariables = new ArrayList<>();
                        if (entity.getAjaxDevice().getBindVariables() != null &&
                                entity.getAjaxDevice().getBindVariables().size() > 0) {
                            bindVariables.addAll(entity.getAjaxDevice().getBindVariables());
                        }
                        bindVariables.addAll(deviceVo.getBindVariables());
                        entity.getAjaxDevice().setBindVariables(bindVariables);
                        ajaxVariable.setSclass("epeditor-btn-mark");
                        isChange = true;
                    }

                    if (isChange == true) {
                        entityEditor.setDirty();
                        entityEditor.redraw();
                    }
                }
            }
        });
        milDesigner = new PageDesigner(workbench, this, milCanvas, milEditor, true, accessVos);
        milDesigner.setDragListener(new PageDesigner.OnDragListener() {
            @Override
            public void getDevice(DeviceVo deviceVo) {
                if (entity.getMilDevice() == null)
                    entity.setMilDevice(new DeviceBean());
                if (deviceVo != null) {
                    boolean isChange = false;
                    if (Strings.isBlank(deviceVo.getStyle()) == false) {
                        String content = null;
                        if (Strings.isBlank(entity.getMilDevice().getStyle()) == false)
                            content = entity.getMilDevice().getStyle() + deviceVo.getStyle();
                        else
                            content =deviceVo.getStyle();
                        entity.getMilDevice().setStyle(content);
                        milCssContext.setSclass("epeditor-btn-mark");
                        isChange = true;
                    }

                    if (Strings.isBlank(deviceVo.getJavascript()) == false) {
                        String content = null;
                        if (Strings.isBlank(entity.getMilDevice().getJavascript()) == false)
                            content = entity.getMilDevice().getJavascript() + deviceVo.getJavascript();
                        else
                            content =deviceVo.getJavascript();
                        entity.getMilDevice().setJavascript(content);
                        milJsContext.setSclass("epeditor-btn-mark");
                        isChange = true;
                    }

                    if (deviceVo.getBindVariables() != null && deviceVo.getBindVariables().size() > 0) {
                        List<BindVariable> bindVariables = new ArrayList<>();
                        if (entity.getMilDevice().getBindVariables() != null &&
                                entity.getMilDevice().getBindVariables().size() > 0) {
                            bindVariables.addAll(entity.getMilDevice().getBindVariables());
                        }
                        bindVariables.addAll(deviceVo.getBindVariables());
                        entity.getMilDevice().setBindVariables(bindVariables);
                        milVariable.setSclass("epeditor-btn-mark");
                        isChange = true;
                    }

                    if (isChange == true) {
                        entityEditor.setDirty();
                        entityEditor.redraw();
                    }
                }
            }
        });

        workbench.addEditor(tab, tabpanel);
        redraw();
    }

    @Override
    protected void refreshSource() {
        if (tabbox.getSelectedIndex() == 2)
            super.refreshSource();
    }

    @Override
    protected boolean validate() {
        if (Strings.isBlank(entity.getName())) {
            WebUtils.notEmpty(Labels.getLabel("entity.name"));
            return false;
        }
        if (entity.getOnLoad() != null) {
            boolean notId = Strings.isBlank(entity.getOnLoad().getId());
            if (notId && Strings.isBlank(entity.getOnLoad().getContent()))
                entity.setOnLoad(null);
            else if (!notId)
                entity.getOnLoad().setContent(null);
        }
        if (entity.getOnOk() != null) {
            boolean notId = Strings.isBlank(entity.getOnOk().getId());
            if (notId && Strings.isBlank(entity.getOnOk().getContent()))
                entity.setOnOk(null);
            else if (!notId)
                entity.getOnOk().setContent(null);
        }
        if (entity.getOnBack() != null) {
            boolean notId = Strings.isBlank(entity.getOnBack().getId());
            if (notId && Strings.isBlank(entity.getOnBack().getContent()))
                entity.setOnBack(null);
            else if (!notId)
                entity.getOnBack().setContent(null);
        }
        if (entity.getOnRefresh() != null) {
            boolean notId = Strings.isBlank(entity.getOnRefresh().getId());
            if (notId && Strings.isBlank(entity.getOnRefresh().getContent()))
                entity.setOnRefresh(null);
            else if (!notId)
                entity.getOnRefresh().setContent(null);
        }
        if (!Strings.isBlank(entity.getAjax())) {
            if (!Strings.isBlank(ajaxMsg.getValue()) || !Strings.isBlank(milMsg.getValue())) {
                String errorString;
                if (!Strings.isBlank(ajaxMsg.getValue()))
                    errorString = ajaxMsg.getValue();
                else
                    errorString = milMsg.getValue();
                WebUtils.showError(Labels.getLabel(
                        "editor.parse.error",
                        new Object[]{Labels.getLabel("entity.page.ajax"),
                                errorString}));
                return false;
            } else {
                try {
                    Document doc = new Builder(false, new NodeFactory()).build(
                            entity.getAjax(), null);
                    doc.detach();
                    doc = null;
                } catch (Exception ex) {
                    WebUtils.showError(Labels.getLabel(
                            "editor.parse.error",
                            new Object[]{Labels.getLabel("entity.page.ajax"),
                                    ex.getMessage()}));
                    return false;
                }
            }
        }
        if (!Strings.isBlank(entity.getMil())) {
            try {
                Document doc = new Builder(false, new NodeFactory()).build(
                        entity.getMil(), null);
                doc.detach();
                doc = null;
            } catch (Exception ex) {
                WebUtils.showError(Labels.getLabel(
                        "editor.parse.error",
                        new Object[]{Labels.getLabel("entity.page.mil"),
                                ex.getMessage()}));
                return false;
            }
        }
        return true;
    }

    /*public void openWidgetWin(){
        ajaxTemplates.setVisible(true);
    }*/

    @Override
    protected void dispatch(final Event evt) {
        final PageEntityEditor entityEditor = this;
        if (evt.getTarget() == ajaxEditor || evt.getTarget() == milEditor) {
            changeCodeRedraw(evt.getTarget());
            /*try {
                if (evt.getTarget() == ajaxEditor) {
                    entity.setAjax((String) ajaxEditor.getValue());
                    ajaxDesigner.redraw();
                } else {
                    entity.setMil((String) milEditor.getValue());
                    milDesigner.redraw();
                }
                setDirty();
            } catch (Exception ex) {
                if ((ex instanceof UiException)
                        && ex.getMessage().startsWith("Not unique in ID"))
                    throw (UiException) ex;
            }*/
        } else if ("entityPage_button_event_script".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, script, "script", false),
                    "javascript", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_event_onVisible".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, onVisible, "onVisible",
                            false), "javascript", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_event_onMessage".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, onMessage, "onMessage",
                            false), "javascript", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_event_onShow".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, onShow, "onShow",
                            false), "javascript", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_property_ajax_script".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(new EditorCallback<String>() {
                @Override
                public String getValue() {
                    if (entity.getAjaxDevice() != null)
                        return entity.getAjaxDevice().getJavascript();
                    return null;
                }

                @Override
                public void setValue(String value) {
                    if (entity.getAjaxDevice() == null)
                        entity.setAjaxDevice(new DeviceBean());
                    entity.getAjaxDevice().setJavascript(value);
                    if (Strings.isBlank(value)) {
                        jsContext.setSclass("epeditor-btn");
                    } else {
                        jsContext.setSclass("epeditor-btn-mark");
                    }
                    entityEditor.setDirty();
                }

                @Override
                public String getTitle() {
                    return "javascript";
                }

                @Override
                public Page getPage() {
                    return jsContext.getPage();
                }

                @Override
                public Editor getEditor() {
                    return entityEditor;
                }
            },"javascript", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_property_ajax_css".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(new EditorCallback<String>() {
                @Override
                public String getValue() {
                    if (entity.getAjaxDevice() != null)
                        return entity.getAjaxDevice().getStyle();
                    return null;
                }

                @Override
                public void setValue(String value) {
                    if (entity.getAjaxDevice() == null)
                        entity.setAjaxDevice(new DeviceBean());
                    entity.getAjaxDevice().setStyle(value);
                    if (Strings.isBlank(value)) {
                        cssContext.setSclass("epeditor-btn");
                    } else {
                        cssContext.setSclass("epeditor-btn-mark");
                    }
                    entityEditor.setDirty();
                    entityEditor.redraw();
                }

                @Override
                public String getTitle() {
                    return "style";
                }

                @Override
                public Page getPage() {
                    return cssContext.getPage();
                }

                @Override
                public Editor getEditor() {
                    return entityEditor;
                }
            },"css", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_property_mil_script".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(new EditorCallback<String>() {
                @Override
                public String getValue() {
                    if (entity.getMilDevice() != null)
                        return entity.getMilDevice().getJavascript();
                    return null;
                }

                @Override
                public void setValue(String value) {
                    if (entity.getMilDevice() == null)
                        entity.setMilDevice(new DeviceBean());
                    entity.getMilDevice().setJavascript(value);
                    if (Strings.isBlank(value)) {
                        milJsContext.setSclass("epeditor-btn");
                    } else {
                        milJsContext.setSclass("epeditor-btn-mark");
                    }
                    entityEditor.setDirty();
                }

                @Override
                public String getTitle() {
                    return "javascript";
                }

                @Override
                public Page getPage() {
                    return milJsContext.getPage();
                }

                @Override
                public Editor getEditor() {
                    return entityEditor;
                }
            },"javascript", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_property_mil_css".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(new EditorCallback<String>() {
                @Override
                public String getValue() {
                    if (entity.getMilDevice() != null)
                        return entity.getMilDevice().getStyle();
                    return null;
                }

                @Override
                public void setValue(String value) {
                    if (entity.getMilDevice() == null)
                        entity.setMilDevice(new DeviceBean());
                    entity.getMilDevice().setStyle(value);
                    if (Strings.isBlank(value)) {
                        milCssContext.setSclass("epeditor-btn");
                    } else {
                        milCssContext.setSclass("epeditor-btn-mark");
                    }
                    entityEditor.setDirty();
                    entityEditor.redraw();
                }

                @Override
                public String getTitle() {
                    return "style";
                }

                @Override
                public Page getPage() {
                    return milCssContext.getPage();
                }

                @Override
                public Editor getEditor() {
                    return entityEditor;
                }
            },"css", evt.getTarget()).doOverlapped();
        } else if ("entityPage_button_property_ajax_variable".equals(evt.getTarget().getId()) ||
                "entityPage_button_property_mil_variable".equals(evt.getTarget().getId())) {
            boolean isAjax = "entityPage_button_property_ajax_variable".equals(evt.getTarget().getId());
            AbstractView.createBindVariable(new EditorCallback<List<BindVariable>>() {
                @Override
                public List<BindVariable> getValue() {
                    if (isAjax == true) {
                        if (entity.getAjaxDevice() != null)
                            return entity.getAjaxDevice().getBindVariables();
                    }
                    else {
                        if (entity.getMilDevice() != null)
                            return entity.getMilDevice().getBindVariables();
                    }
                    return null;
                }

                @Override
                public void setValue(List<BindVariable> value) {
                    if (isAjax == true) {
                        if (entity.getAjaxDevice() == null)
                            entity.setAjaxDevice(new DeviceBean());
                        entity.getAjaxDevice().setBindVariables(value);
                    }
                    else {
                        if (entity.getMilDevice() == null)
                            entity.setMilDevice(new DeviceBean());
                        entity.getMilDevice().setBindVariables(value);
                    }
                    Button btn = (Button) evt.getTarget();
                    if (value == null || value.size() == 0) {
                        btn.setSclass("epeditor-btn");
                    } else {
                        btn.setSclass("epeditor-btn-mark");
                    }
                    setDirty();
                }

                @Override
                public String getTitle() {
                    if (isAjax == true)
                        return Labels.getLabel("entity.page.ajax.variable");
                    else
                        return Labels.getLabel("entity.page.mil.variable");
                }

                @Override
                public Page getPage() {
                    return evt.getTarget().getPage();
                }

                @Override
                public Editor getEditor() {
                    return null;
                }
            }, evt.getTarget()).doOverlapped();
        } else if (evt.getName().equals(Events.ON_SELECT)
                && evt.getTarget() instanceof Tab) {
            if (tabbox.getSelectedIndex() == 2) {
                super.refreshSource();
                workbench.getHeader().setMenuitem(1);
                workbench.getFunctionBar().setMenuitem(1);
                workbench.getShortcutKeyBar().setMenuitem(1);
            } else {
                workbench.getHeader().setMenuitem(2);
                workbench.getFunctionBar().setMenuitem(2);
                workbench.getShortcutKeyBar().setMenuitem(2);
            }

            if (tabbox.getSelectedIndex() == 1) {
                Clients.evalJavaScript("setEntityBGImage('" + bgImage.getUuid() + "','"  + canvasDiv.getUuid() +"')");
            }
        } else if (Events.ON_CHANGE.equals(evt.getName())
                || Events.ON_CHECK.equals(evt.getName())) {// 变更数据
            Component c = evt.getTarget();
            if (c == entityName) {
                entity.setName(entityName.getValue());
            } else if (c == entityDesp) {
                entity.setDescription(entityDesp.getValue());
            } else if (c == entityTable) {
                entity.setTable(entityTable.getValue());
            } else if (c == entityInherit) {
                entity.setInheritId(entityInherit.getValue());
            } else if (c == entityWidth)
                entity.setWidth(entityWidth.getValue());
            else if (c == entityHeight)
                entity.setHeight(entityHeight.getValue());
            else if (c == entityDestination)
                entity.setDestination(entityDestination.getValue());
            else if (c == onLoad) {
                if (Strings.isBlank(onLoad.getValue())) {
                    if (entity.getOnLoad() != null) {
                        if (Strings.isBlank(entity.getOnLoad().getContent())) {
                            entity.setOnLoad(null);
                        } else
                            entity.getOnLoad().setId(null);
                    }
                } else {
                    if (entity.getOnLoad() == null)
                        entity.setOnLoad(new EventLogic());
                    entity.getOnLoad().setId(onLoad.getValue());
                }
            } else if (c == onOk) {
                if (Strings.isBlank(onOk.getValue())) {
                    if (entity.getOnOk() != null) {
                        if (Strings.isBlank(entity.getOnOk().getContent())) {
                            entity.setOnOk(null);
                        } else
                            entity.getOnOk().setId(null);
                    }
                } else {
                    if (entity.getOnOk() == null)
                        entity.setOnOk(new EventLogic());
                    entity.getOnOk().setId(onOk.getValue());
                }
            } else if (c == onBack) {
                if (Strings.isBlank(onBack.getValue())) {
                    if (entity.getOnBack() != null) {
                        if (Strings.isBlank(entity.getOnBack().getContent())) {
                            entity.setOnBack(null);
                        } else
                            entity.getOnBack().setId(null);
                    }
                } else {
                    if (entity.getOnBack() == null)
                        entity.setOnBack(new EventLogic());
                    entity.getOnBack().setId(onBack.getValue());
                }
            } else if (c == onRefresh) {
                if (Strings.isBlank(onRefresh.getValue())) {
                    if (entity.getOnRefresh() != null) {
                        if (Strings.isBlank(entity.getOnRefresh().getContent())) {
                            entity.setOnRefresh(null);
                        } else
                            entity.getOnRefresh().setId(null);
                    }
                } else {
                    if (entity.getOnRefresh() == null)
                        entity.setOnRefresh(new EventLogic());
                    entity.getOnRefresh().setId(onRefresh.getValue());
                }
            }
            setDirty();
        } else if (evt.getTarget().getId().equals("entityPage_a_minus") ||
                evt.getTarget().getId().equals("entityPage_a_plus")) {
            String numString = sizeLabel.getValue().substring(0, sizeLabel.getValue().length() - 1);
            Integer num = Integer.parseInt(numString);
            if ((num > 40 && evt.getTarget().getId().equals("entityPage_a_minus")) ||
                    (num < 200 && evt.getTarget().getId().equals("entityPage_a_plus"))) {
                if (evt.getTarget().getId().equals("entityPage_a_minus"))
                    num -= 20;
                else
                    num += 20;
                sizeLabel.setValue(num + "%");
                ajaxCanvas.setStyle("zoom: " + num/100.0 + ";");
            }
        } else if (evt.getTarget().getId().equals("entityPage_button_template") ||
                evt.getTarget().getId().equals("entityPage_button_mobile_template")) {
            PageTemplateView.PageTemplateType pageTemplateType = PageTemplateView.PageTemplateType.TEMPLATE_TYPE_PAGE_PC;
            if (evt.getTarget().getId().equals("entityPage_button_template"))
                pageTemplateType = PageTemplateView.PageTemplateType.TEMPLATE_TYPE_PAGE_PC;
            else
                pageTemplateType = PageTemplateView.PageTemplateType.TEMPLATE_TYPE_PAGE_MOBILE;
            final PageTemplateView.PageTemplateType finalPageTemplateType = pageTemplateType;
            AbstractView.createTemplate(new EditorCallback<PageBean>() {
                @Override
                public PageBean getValue() {
                    return null;
                }

                @Override
                public void setValue(PageBean pageBean) {
                    if (finalPageTemplateType == PageTemplateView.PageTemplateType.TEMPLATE_TYPE_PAGE_PC) {
                        ajaxEditor.setValue(pageBean.getAjax());
                        changeCodeRedraw(ajaxEditor);
                    } else {
                        milEditor.setValue(pageBean.getMil());
                        changeCodeRedraw(milEditor);
                    }
                    entity.setTable(pageBean.getTable());
                    entityTable.setValue(entity.getTable());
                }

                @Override
                public String getTitle() {
                    return Labels.getLabel("entity.select.template");
                }

                @Override
                public Page getPage() {
                    return tabbox.getPage();
                }

                @Override
                public Editor getEditor() {
                    return null;
                }
            }, pageTemplateType, evt.getTarget(), entity).doOverlapped();
        } else if (evt.getTarget().getId().equals("entityPage_button_add_ajax_custom")) {
            openCustomWidgetSetWin(true);
        } else if (evt.getTarget().getId().equals("entityPage_button_add_mobile_custom")) {
            openCustomWidgetSetWin(false);
        } else if (evt.getTarget().getId().equals("entityPage_button_show_ajax_custom") ||
                evt.getTarget().getId().equals("entityPage_button_show_mobile_custom")) {
            openCustomWidgetWin();
        } else {// 事件关联
            Component c = evt.getTarget();
            if (evt.getName().equals(Events.ON_OPEN)||evt instanceof KeyEvent) {// Bandbox
                String selector = EntityType.LOGIC.getName();
                if (c == entityInherit) {
                    selector = EntityType.PAGE.getName();
                } else if (c == entityTable)
                    selector = EntityType.TABLE.getName();
                if (evt instanceof OpenEvent){
                    ((Bandbox) c).setValue(((OpenEvent)evt).getValue()+"");
                }
                AbstractView.createEntityView(
                        new BandboxCallback(this, (Bandbox) c), selector, c)
                        .doOverlapped();
            } else {// 编辑按钮
                c = c.getPreviousSibling();
                if (c == entityTable) {
                    if (!Strings.isBlank(entityTable.getValue()))
                        openEditor(evt.getTarget(), entityTable.getValue());
                } else if (c == onLoad) {
                    if (Strings.isBlank(onLoad.getValue())) {
                        if (entity.getOnLoad() == null)
                            entity.setOnLoad(new EventLogic());
                        Button btn = (Button) onLoad.getNextSibling();
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnLoad(), entity.getTable(),
                                        "OnLoad"), btn).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onLoad.getValue());
                    }
                } else if (c == onOk) {
                    if (Strings.isBlank(onOk.getValue())) {
                        if (entity.getOnOk() == null)
                            entity.setOnOk(new EventLogic());
                        Button btn = (Button) onOk.getNextSibling();
                        AbstractView
                                .createLogicView(
                                        new EventLogicCallback(this,
                                                (Button) evt.getTarget(),
                                                entity.getOnOk(), entity.getTable(), "OnOk"), btn)
                                .doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onOk.getValue());
                    }
                } else if (c == onBack) {
                    if (Strings.isBlank(onBack.getValue())) {
                        if (entity.getOnBack() == null)
                            entity.setOnBack(new EventLogic());
                        Button btn = (Button) onBack.getNextSibling();
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnBack(), entity.getTable(),
                                        "OnBack"), btn).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onBack.getValue());
                    }
                } else if (c == onRefresh) {
                    if (Strings.isBlank(onRefresh.getValue())) {
                        if (entity.getOnRefresh() == null)
                            entity.setOnRefresh(new EventLogic());
                        Button btn = (Button) onRefresh.getNextSibling();
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnRefresh(), entity.getTable(),
                                        "OnRefresh"), btn).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onRefresh.getValue());
                    }
                }
            }
        }
    }

    private void openCustomWidgetWin() {
        List<CustomWidgetGroupVo> vos = StudioApp.execute(new GetAllWidgetCmd());
        final List<CustomWidgetGroupVo> voList = vos;
        AbstractView.createCustomWidget(new EditorCallback<List<CustomWidgetGroupVo>>() {
            @Override
            public List<CustomWidgetGroupVo> getValue() {
                return voList;
            }

            @Override
            public void setValue(List<CustomWidgetGroupVo> value) {
            }

            @Override
            public String getTitle() {
                return Labels.getLabel("entity.select.template");
            }

            @Override
            public Page getPage() {
                return tabbox.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }

    private void openCustomWidgetSetWin(boolean isAjax) {
        CustomWidgetVo customWidgetVo = new CustomWidgetVo();

        DeviceBean currentBean = null;
        if (isAjax) {
            currentBean = entity.getAjaxDevice();
        } else {
            currentBean = entity.getMilDevice();
        }

        if (currentBean != null) {
            DeviceVo deviceBean = new DeviceVo();
            deviceBean.setStyle(currentBean.getStyle());
            deviceBean.setJavascript(currentBean.getJavascript());
            List<BindVariable> bindVariables = new ArrayList<>();
            if (currentBean.getBindVariables() != null) {
                for (BindVariable bind :
                        currentBean.getBindVariables()) {
                    BindVariable abind = new BindVariable();
                    abind.setDsId(bind.getDsId());
                    abind.setRef(bind.getRef());
                    abind.setDesp(bind.getDesp());
                    abind.setName(bind.getName());
                    bindVariables.add(abind);
                }
            }
            deviceBean.setBindVariables(bindVariables);
            customWidgetVo.setDevice(deviceBean);
        }


        final CustomWidgetVo lastCustomWidgetVo = customWidgetVo;
        AbstractView.createCustomWidgetSet(new EditorCallback<CustomWidgetVo>() {
            @Override
            public CustomWidgetVo getValue() {
                return lastCustomWidgetVo;
            }

            @Override
            public void setValue(CustomWidgetVo value) {
            }

            @Override
            public String getTitle() {
                return Labels.getLabel("entity.select.template");
            }

            @Override
            public Page getPage() {
                return tabbox.getPage();
            }

            @Override
            public Editor getEditor() {
                return null;
            }
        }).doOverlapped();
    }
    private void changeCodeRedraw(Component component) {
        try {
            if (component == ajaxEditor) {
                entity.setAjax((String) ajaxEditor.getValue());
                ajaxDesigner.redraw(true);
            } else {
                entity.setMil((String) milEditor.getValue());
                milDesigner.redraw(true);
            }
            setDirty();
        } catch (Exception ex) {
            if ((ex instanceof UiException)
                    && ex.getMessage().startsWith("Not unique in ID"))
                throw (UiException) ex;
        }
    }


    @Override
    public void updateContent(String content) {
        if (tabbox.getSelectedIndex() == 0) {
            ajaxEditor.setValue(content);
            entity.setAjax(content);
        } else if (tabbox.getSelectedIndex() == 1) {
            milEditor.setValue(content);
            entity.setMil(content);
        }
        setDirty();
    }

    @Override
    public void showMsg(String msg, CMeditor editor) {
        if (editor == ajaxEditor)
            ajaxMsg.setValue(msg);
        else
            milMsg.setValue(msg);
    }

    @Override
    public void updateProperties(Element selection,
                                 List<Map<String, Component>> props, Map<String, Component> events) {
        Group baseGroup = properties.getGroups().get(0);
        properties.getChildren().removeAll(baseGroup.getItems());
        Group expertGroup = properties.getGroups().get(1);
        properties.getChildren().removeAll(expertGroup.getItems());
        Group eventGroup = properties.getGroups().get(2);
        properties.getChildren().removeAll(eventGroup.getItems());
        Tab tab = ((Tabpanel) properties.getGrid().getParent()).getLinkedTab();
        if (props != null) {
            tab.setLabel(selection.getLocalName());
            tab.setVisible(true);
            tab.setSelected(true);
            for (int i = 0; i < 2; i++) {
                for (Map.Entry<String, Component> entry : props.get(i)
                        .entrySet()) {
                    Row row = new Row();
                    Label label=new Label(entry.getKey());
                    if(selection.getLocalName().equals("actionbox")){
                        if(entry.getKey().equals("entry")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("actionbox."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    } else if(selection.getLocalName().equals("datalist")){
                        if(entry.getKey().equals("entry")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("datalist."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    } else if(selection.getLocalName().equals("barcodescanner")){
                        if(entry.getKey().equals("interval")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("barcodescanner."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    } else if(selection.getLocalName().equals("calendars")){
                        if(entry.getKey().equals("weekOfYear")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("calendars."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("charts")){
                        if(entry.getKey().equals("tooltip")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("charts."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("comboitem")){
                        if(entry.getKey().equals("content")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("comboitem."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("loader")){
                        if(entry.getKey().equals("entity")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("loader."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("movebox")){
                        if(entry.getKey().equals("target")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("movebox."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("report")){
                        if(entry.getKey().equals("entity")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("report."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("swifttag")){
                        if(entry.getKey().equals("tagName")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("swifttag."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("task")){
                        if(entry.getKey().equals("entity")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("task."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else{
                        label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                    }
                    row.appendChild(label);
                    row.appendChild(entry.getValue());
                    properties.insertBefore(row, i == 0 ? expertGroup
                            : eventGroup);
                }
            }
            Iterator<Map.Entry<String, Component>> itr = events.entrySet()
                    .iterator();
            while (itr.hasNext()) {
                Map.Entry<String, Component> entry = itr.next();
                if (!(entry.getValue() instanceof Button)) {
                    Row row = new Row();
                    Label label=new Label(entry.getKey());
                    if(selection.getLocalName().equals("import")){
                        if(entry.getKey().equals("after")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("import."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("import")){
                        if(entry.getKey().equals("before")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("import."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    }else if(selection.getLocalName().equals("datalist")){
                        if(entry.getKey().equals("after")){
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName("datalist."+entry.getKey()));
                        }else{
                            label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                        }
                    } else {
                        label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                    }
                    row.appendChild(label);
                   //row.appendChild(new Label(entry.getKey()));
                    row.appendChild(entry.getValue());
                    properties.appendChild(row);
                    itr.remove();
                }
            }
            for (Map.Entry<String, Component> entry : events.entrySet()) {
                Row row = new Row();
                Label label=new Label(entry.getKey());
                label.setTooltiptext(AttributeCnUtil.getAttrubuleName(entry.getKey()));
                row.appendChild(label);
               // row.appendChild(new Label(entry.getKey()));
                row.appendChild(entry.getValue());
                properties.appendChild(row);
            }
        } else {
            tab.setVisible(false);
            tab.getTabbox().setSelectedIndex(0);
        }
    }

    @Override
    protected void redraw() {
        ajaxDesigner.redraw(false);
        milDesigner.redraw(false);
        if (ajaxStyle == null) {
            ajaxStyle = new Style();
            ajaxScrollview.appendChild(ajaxStyle);
        }
        if (entity.getAjaxDevice() != null)
            ajaxStyle.setContent(entity.getAjaxDevice().getStyle());

        if (milStyle == null) {
            milStyle = new Style();
            canvasDiv.appendChild(milStyle);
        }
        if (entity.getMilDevice() != null)
            milStyle.setContent(entity.getMilDevice().getStyle());
    }

    @Override
    public void setTheme(String theme) {
        super.setTheme(theme);
        ajaxEditor.setTheme(theme);
        milEditor.setTheme(theme);
    }

    @Override
    public void insertTodo() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.insertText("<!-- TODO -->");
        else
            milEditor.insertText("<!-- TODO -->");
    }

    @Override
    public void insertComments() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.insertText("<!-- This is a comment -->");
        else
            milEditor.insertText("<!-- This is a comment -->");
    }

    @Override
    public void undo() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("undo");
        else
            milEditor.exeCmd("undo");
    }

    @Override
    public void redo() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("redo");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("redo");
    }

    @Override
    public void selectAll() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("selectAll");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("selectAll");
    }

    @Override
    public void format() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("format");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("format");
    }

    @Override
    public void formatAll() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("formatAll");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("formatAll");
    }

    @Override
    public void deleteLine() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("deleteLine");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("deleteLine");
    }

    @Override
    public void indentMore() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("indentMore");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("indentMore");
    }

    @Override
    public void indentLess() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("indentLess");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("indentLess");
    }

    @Override
    public void find() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("find");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("find");
    }

    @Override
    public void findNext() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("findNext");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("findNext");
    }

    @Override
    public void findPrev() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("findPrev");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("findPrev");

    }

    @Override
    public void replace() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("replace");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("replace");
    }

    @Override
    public void replaceAll() {
        if (tabbox.getSelectedIndex() == 0)
            ajaxEditor.exeCmd("replaceAll");
        else if (tabbox.getSelectedIndex() == 1)
            milEditor.exeCmd("replaceAll");
    }
}
