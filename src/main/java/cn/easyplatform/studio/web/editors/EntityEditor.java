/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors;

import cn.easyplatform.entities.BaseEntity;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface EntityEditor<T extends BaseEntity> extends Editor, Savable {

    /**
     * 删除
     */
    void delete();

    /**
     * @return
     */
    T getEntity();
}
