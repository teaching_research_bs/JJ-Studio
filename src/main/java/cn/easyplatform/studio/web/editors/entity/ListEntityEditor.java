/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.entity;

import cn.easyplatform.entities.beans.list.*;
import cn.easyplatform.entities.beans.list.Group;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.GetAccessInfoCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.AccessVo;
import cn.easyplatform.studio.web.editors.*;
import cn.easyplatform.studio.web.editors.page.PageDesigner;
import cn.easyplatform.studio.web.editors.support.CodeFormatter;
import cn.easyplatform.studio.web.editors.support.NodeFactory;
import cn.easyplatform.studio.web.editors.support.ZulXsdUtil;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.web.ext.cmez.CMeditor;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.*;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkmax.zul.Chosenbox;
import org.zkoss.zul.*;
import org.zkoss.zul.impl.LabelImageElement;

import java.util.*;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListEntityEditor extends AbstractEntityEditor<ListBean> implements
        CodeEditor, PageEditor {

    private Tabbox tabbox;

    private Tabbox propertytabbox;

    private Tree canvas;

    private Component panelcanvas;

    private CMeditor panelEditor;

    private Textbox entityId;

    private Textbox entityName;

    private Textbox entityDesp;

    private Bandbox entityTable;

    private Bandbox onRow;

    private Bandbox onInit;

    private Bandbox onBefore;

    private Bandbox onAfter;

    private Combobox entityType;

    private Intbox entityPageSize;

    private Textbox entityEditableColumns;

    private Textbox entityLevelBy;

    private Textbox entityGroup;

    private Bandbox entityPageId;

    private Checkbox entityShowTitle;

    private Checkbox entityShowPanel;

    private Checkbox entityShowRowNumbers;

    private Checkbox entitySizedByContent;

    private Checkbox entitySpan;

    private Checkbox entityCheckmark;

    private Checkbox entityMultiple;

    private A moveLeft;

    private A moveRight;

    private A addAuxHeader;

    private A delete;

    private CanvasHandler canvasHandler;

    private PageDesigner panelDesigner;

    private Rows properties;

    private Label canvasMsg;

    //private Window templates;

    private Chosenbox list_chosenbox_matrixColumns;

    private Chosenbox list_chosenbox_matrixRows;

    private List<AccessVo> accessVos;
    /**
     * @param workbench
     * @param entity
     * @param code
     */
    public ListEntityEditor(WorkbenchController workbench, ListBean entity,
                            char code) {
        super(workbench, entity, code);
    }

    @Override
    public void create(Object... args) {
        super.create(args);
        Tabpanel tabpanel = new Tabpanel();
        Idspace is = new Idspace();
        is.setHflex("1");
        is.setVflex("1");
        is.setParent(tabpanel);
        Executions.createComponents("~./include/editor/entity/list.zul", is,
                null);
        String theme = Contexts.getUser().getEditorTheme();
        canvasHandler = new CanvasHandler();
        for (Component comp : is.getFellows()) {
            String id = comp.getId();
            if (id.equals("list_tabbox_propertytabbox")) {
                propertytabbox = (Tabbox) comp;
            } else if (id.equals("list_chosenbox_matrixColumns")) {
                list_chosenbox_matrixColumns = (Chosenbox) comp;
                if (entity.getHeaders() != null && !entity.getHeaders().isEmpty()) {
                    ListModelList<String> model = new ListModelList<String>(entity
                            .getHeaders().size());
                    for (Header field : entity.getHeaders())
                        model.add(field.getName());
                    list_chosenbox_matrixColumns.setModel(model);
                    if (entity.getMatrixColumns() != null) {
                        try {
                            list_chosenbox_matrixColumns.setSelectedObjects(Arrays.asList(entity
                                    .getMatrixColumns()));
                        } catch (UiException ex) {
                        }
                    }
                }
                comp.addEventListener(Events.ON_SELECT,this);
            } else if (id.equals("list_chosenbox_matrixRows")) {
                list_chosenbox_matrixRows = (Chosenbox) comp;
                if (entity.getHeaders() != null && !entity.getHeaders().isEmpty()) {
                    ListModelList<String> model = new ListModelList<String>(entity
                            .getHeaders().size());
                    for (Header field : entity.getHeaders())
                        model.add(field.getName());
                    list_chosenbox_matrixRows.setModel(model);
                    if (entity.getMatrixRows() != null) {
                        try {
                            list_chosenbox_matrixRows.setSelectedObjects(Arrays.asList(entity
                                    .getMatrixRows()));
                        } catch (UiException ex) {
                        }
                    }
                }
                comp.addEventListener(Events.ON_SELECT,this);
            } else if (id.equals("list_tabbox_main")) {
                tabbox = (Tabbox) comp;
                tabbox.addEventListener(Events.ON_SELECT, this);
            } else if (id.equals("list_cmeditor_source")) {
                source = (CMeditor) comp;
                source.setTheme(theme);
            } else if (id.equals("list_textbox_id")) {
                entityId = (Textbox) comp;
                entityId.setValue(entity.getId());
            } else if (id.equals("list_combobox_type")) {
                entityType = (Combobox) comp;
                comp.addEventListener(Events.ON_SELECT, this);
                if (Constants.CATALOG.equals(entity.getShowType()))
                    entityType.setSelectedIndex(0);
                else if (Constants.DETAIL.equals(entity.getShowType()))
                    entityType.setSelectedIndex(1);
                else if (Constants.REPORT.equals(entity.getShowType()))
                    entityType.setSelectedIndex(2);
                else
                    entityType.setSelectedIndex(-1);
            } else if (id.equals("list_intbox_pageSize")) {
                entityPageSize = (Intbox) comp;
                entityPageSize.setValue(entity.getPageSize());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (id.equals("list_textbox_editableColumns")) {
                entityEditableColumns = (Textbox) comp;
                entityEditableColumns.setValue(entity.getEditableColumns());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (id.equals("list_textbox_levelBy")) {
                entityLevelBy = (Textbox) comp;
                entityLevelBy.setValue(entity.getLevelBy());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (id.equals("list_textbox_group")) {
                entityGroup = (Textbox) comp;
                entityGroup.setValue(entity.getGroupName());
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (id.equals("list_bandbox_pageId")) {
                entityPageId = (Bandbox) comp;
                entityPageId.setValue(entity.getPageId());
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_CHANGE, this);
            } else if (id.equals("list_switchbox_showTitle")) {
                entityShowTitle = (Checkbox) comp;
                entityShowTitle.setChecked(entity.isShowTitle());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_switchbox_showPanel")) {
                entityShowPanel = (Checkbox) comp;
                entityShowPanel.setChecked(entity.isShowPanel());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_switchbox_showRowNumbers")) {
                entityShowRowNumbers = (Checkbox) comp;
                entityShowRowNumbers.setChecked(entity.isShowRowNumbers());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_switchbox_sizedByContent")) {
                entitySizedByContent = (Checkbox) comp;
                entitySizedByContent.setChecked(entity.isSizedByContent());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_switchbox_span")) {
                entitySpan = (Checkbox) comp;
                entitySpan.setChecked(entity.isSpan());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_switchbox_checkmark")) {
                entityCheckmark = (Checkbox) comp;
                entityCheckmark.setChecked(entity.isCheckmark());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_switchbox_multiple")) {
                entityMultiple = (Checkbox) comp;
                entityMultiple.setChecked(entity.isMultiple());
                comp.addEventListener(Events.ON_CHECK, this);
            } else if (id.equals("list_textbox_name")) {
                entityName = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityName.setValue(entity.getName());
            } else if (id.equals("list_textbox_desp")) {
                entityDesp = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityDesp.setValue(entity.getDescription());
            } else if (id.equals("list_bandbox_table")) {
                entityTable = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                //comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                entityTable.setValue(entity.getTable());
            } else if (id.equals("list_tree_canvas")) {
                canvas = (Tree) comp;
                canvas.addEventListener(Events.ON_DROP, this);
            } else if (id.equals("list_idspace_panelcanvas")) {
                panelcanvas = comp;
            } else if (id.equals("list_cmeditor_panelEditor")) {
                panelEditor = (CMeditor) comp;
                panelEditor.setTheme(theme);
                if (!Strings.isBlank(entity.getPanel()))
                    panelEditor.setValue(CodeFormatter.formatXML(ZulXsdUtil
                            .buildDocument(entity.getPanel())));
                panelEditor.addEventListener(Events.ON_CHANGING, this);
            } else if (id.equals("list_textbox_headerTitle")) {
                canvasHandler.headerTitle = (Textbox) comp;
            } else if (id.equals("list_textbox_headerField")) {
                canvasHandler.headerField = (Textbox) comp;
            } else if (id.equals("list_textbox_headerName")) {
                canvasHandler.headerName = (Textbox) comp;
            } else if (id.equals("list_combobox_headerType")) {
                canvasHandler.headerType = (Combobox) comp;
            } else if (id.equals("list_textbox_headerDesp")) {
                canvasHandler.headerDesp = (Textbox) comp;
            } else if (id.equals("list_textbox_headerWidth")) {
                canvasHandler.headerWidth = (Textbox) comp;
            } else if (id.equals("list_combobox_headerTotalType")) {
                canvasHandler.headerTotalType = (Combobox) comp;
            } else if (id.equals("list_textbox_headerGroupName")) {
                canvasHandler.headerGroupName = (Textbox) comp;
            } else if (id.equals("list_textbox_headerTotalName")) {
                canvasHandler.headerTotalName = (Textbox) comp;
            } else if (id.equals("list_button_headerGroupStyle")) {
                canvasHandler.headerGroupStyle = (Button) comp;
            } else if (id.equals("list_button_headerTotalStyle")) {
                canvasHandler.headerTotalStyle = (Button) comp;
            } else if (id.equals("list_combobox_headerAlign")) {
                canvasHandler.headerAlign = (Combobox) comp;
            } else if (id.equals("list_combobox_headerValign")) {
                canvasHandler.headerValign = (Combobox) comp;
            } else if (id.equals("list_bandbox_headerImage")) {
                canvasHandler.headerImage = (Bandbox) comp;
            } else if (id.equals("list_bandbox_headerHoverimg")) {
                canvasHandler.headerHoverimg = (Bandbox) comp;
            } else if (id.equals("list_bandbox_headerIcon")) {
                canvasHandler.headerIcon = (Bandbox) comp;
            } else if (id.equals("list_button_headerStyle")) {
                canvasHandler.headerStyle = (Button) comp;
            } else if (id.equals("list_button_headerCellStyle")) {
                canvasHandler.headerCellStyle = (Button) comp;
            } else if (id.equals("list_switchbox_headerIsSort")) {
                canvasHandler.headerIsSort = (Checkbox) comp;
            } else if (id.equals("list_switchbox_headerIsVisible")) {
                canvasHandler.headerIsVisible = (Checkbox) comp;
            } else if (id.equals("list_textbox_headerFormat")) {
                canvasHandler.headerFormat = (Textbox) comp;
            } else if (id.equals("list_button_headerEvent")) {
                canvasHandler.headerEvent = (Button) comp;
            } else if (id.equals("list_button_headerSql")) {
                canvasHandler.headerSql = (Button) comp;
            } else if (id.equals("list_textbox_headerSqlSeparator")) {
                canvasHandler.headerSeparator = (Textbox) comp;
            } else if (id.equals("list_button_headerComponent")) {
                canvasHandler.headerComponent = (Button) comp;
            } else if (id.equals("list_textbox_auxTitle")) {
                canvasHandler.auxTitle = (Textbox) comp;
            } else if (id.equals("list_textbox_auxDesp")) {
                canvasHandler.auxDesp = (Textbox) comp;
            } else if (id.equals("list_button_auxStyle")) {
                canvasHandler.auxStyle = (Button) comp;
            } else if (id.equals("list_spinner_auxColspan")) {
                canvasHandler.auxColspan = (Spinner) comp;
            } else if (id.equals("list_spinner_auxRowspan")) {
                canvasHandler.auxRowspan = (Spinner) comp;
            } else if (id.equals("list_combobox_auxAlign")) {
                canvasHandler.auxAlign = (Combobox) comp;
            } else if (id.equals("list_combobox_auxValign")) {
                canvasHandler.auxValign = (Combobox) comp;
            } else if (id.equals("list_bandbox_auxImage")) {
                canvasHandler.auxImage = (Bandbox) comp;
            } else if (id.equals("list_bandbox_auxHoverimg")) {
                canvasHandler.auxHoverimg = (Bandbox) comp;
            } else if (id.equals("list_bandbox_auxIcon")) {
                canvasHandler.auxIcon = (Bandbox) comp;
            } else if (id.equals("list_textbox_groupName")) {
                canvasHandler.groupName = (Textbox) comp;
            } else if (id.equals("list_textbox_groupDesp")) {
                canvasHandler.groupDesp = (Textbox) comp;
            } else if (id.equals("list_textbox_groupTitle")) {
                canvasHandler.groupTitle = (Textbox) comp;
            } else if (id.equals("list_chosenbox_groupFields")) {
                canvasHandler.groupFields = (Chosenbox) comp;
            } else if (id.equals("list_button_groupOrderBy")) {
                canvasHandler.groupOrderBy = (Button) comp;
            } else if (id.equals("list_button_groupStyle")) {
                canvasHandler.groupStyle = (Button) comp;
            } else if (comp.getId().equals("list_bandbox_onLogic")) {
                onRow = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                //comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnRow() != null) {
                    if (!Strings.isBlank(entity.getOnRow().getId())) {
                        onRow.setValue(entity.getOnRow().getId());
                    } else if (!Strings.isBlank(entity.getOnRow().getContent())) {
                        ((Button) onRow.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("list_bandbox_onInit")) {
                onInit = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                //comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnInit() != null) {
                    if (!Strings.isBlank(entity.getOnInit().getId())) {
                        onInit.setValue(entity.getOnInit().getId());
                    } else if (!Strings
                            .isBlank(entity.getOnInit().getContent())) {
                        ((Button) onInit.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("list_bandbox_onBefore")) {
                onBefore = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                //comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnBefore() != null) {
                    if (!Strings.isBlank(entity.getOnBefore().getId())) {
                        onBefore.setValue(entity.getOnBefore().getId());
                    } else if (!Strings.isBlank(entity.getOnBefore()
                            .getContent())) {
                        ((Button) onBefore.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("list_bandbox_onAfter")) {
                onAfter = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                //comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnAfter() != null) {
                    if (!Strings.isBlank(entity.getOnAfter().getId())) {
                        onAfter.setValue(entity.getOnAfter().getId());
                    } else if (!Strings.isBlank(entity.getOnAfter()
                            .getContent())) {
                        ((Button) onAfter.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp instanceof Button || comp instanceof A) {
                comp.addEventListener(Events.ON_CLICK, this);
                if (id.equals("list_a_form_addAuxHeader"))
                    addAuxHeader = (A) comp;
                else if (id.equals("list_a_form_moveLeft"))
                    moveLeft = (A) comp;
                else if (id.equals("list_a_form_delete"))
                    delete = (A) comp;
                else if (id.equals("list_a_form_moveRight"))
                    moveRight = (A) comp;
                LabelImageElement xul = (LabelImageElement) comp;
                if (id.equals("panel") && !Strings.isBlank(entity.getPanel()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_query") && !Strings.isBlank(entity.getQuery()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_condition")
                        && !Strings.isBlank(entity.getCondition()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_orderBy")
                        && !Strings.isBlank(entity.getOrderBy()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_groupBy")
                        && !Strings.isBlank(entity.getGroupBy()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_headStyle")
                        && !Strings.isBlank(entity.getHeadStyle()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_rowStyle")
                        && !Strings.isBlank(entity.getRowStyle()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_footStyle")
                        && !Strings.isBlank(entity.getFootStyle()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_onHeader")
                        && !Strings.isBlank(entity.getOnHeaderScript()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_onRow")
                        && !Strings.isBlank(entity.getOnRowScript()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_onGroup")
                        && !Strings.isBlank(entity.getOnGroupScript()))
                    xul.setSclass("epeditor-btn-mark");
                if (id.equals("list_button_onFooter")
                        && !Strings.isBlank(entity.getOnFooterScript()))
                    xul.setSclass("epeditor-btn-mark");

            } else if (comp instanceof Bandbox) {
                comp.addEventListener(Events.ON_OPEN, this);
                //comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
            } /*else if (comp.getId().equals("list_window_templates")) {
                templates = (Window) comp;
                templates.addEventListener(Events.ON_CLOSE, new EventListener<Event>() {
                    @Override
                    public void onEvent(Event event) throws Exception {
                        templates.setVisible(false);
                        event.stopPropagation();
                    }
                });
                Iterator<Component> itr = comp.queryAll("toolbarbutton")
                        .iterator();
                while (itr.hasNext()) {
                    Button widget = (Button) itr.next();
                    widget.setDraggable(EntityType.TABLE.getName());
                    widget.setAttribute("t", "");
                }
            }*/ else if (comp.getId().equals("list_rows_properties"))
                properties = (Rows) comp;
            else if (comp.getId().equals("list_label_canvasMsg"))
                canvasMsg = (Label) comp;
            else if (comp.getId().equals("list_label_name")) {
                Label nameLabel = (Label) comp;
                nameLabel.setValue(Labels.getLabel("entity.id") + "：" + entity.getId() + " " +
                        Labels.getLabel("entity.name") + "：" + entity.getName());
            }
        }
        setCanvasToolbarStatus(true);
        accessVos = StudioApp.execute(new GetAccessInfoCmd());
        panelDesigner = new PageDesigner(workbench, this, panelcanvas,
                panelEditor, false, accessVos);
        canvasHandler.draw();
        canvasHandler.addEvent();
        workbench.addEditor(tab, tabpanel);
        redraw();
    }

    private void refreshChosenbox(){//刷新chosenbox
        if (entity.getHeaders() != null && !entity.getHeaders().isEmpty()) {
            ListModelList<String> modelColumns = new ListModelList<String>(entity
                    .getHeaders().size());
            ListModelList<String> modelRows = new ListModelList<String>(entity
                    .getHeaders().size());
            for (Header field : entity.getHeaders()){
                modelColumns.add(field.getName());
                modelRows.add(field.getName());
            }
            list_chosenbox_matrixColumns.setModel(modelColumns);
            list_chosenbox_matrixRows.setModel(modelRows);
            if (entity.getMatrixColumns() == null) {
                return;
            }
            if (entity.getMatrixRows() == null) {
                return;
            }
            List<String> matrixColumnsList = new ArrayList<>();
            List<String> matrixRowsList = new ArrayList<>();
            for (String var:entity.getMatrixColumns()){
                if (modelColumns.contains(var)){
                    matrixColumnsList.add(var);
                }
            }
            for (String var:entity.getMatrixRows()){
                if (modelRows.contains(var)){
                    matrixRowsList.add(var);
                }
            }
            entity.setMatrixColumns(matrixColumnsList.toArray(new String[matrixColumnsList.size()]));
            entity.setMatrixRows(matrixRowsList.toArray(new String[matrixRowsList.size()]));
            if (entity.getMatrixColumns() != null) {
                try {
                    list_chosenbox_matrixColumns.setSelectedObjects(Arrays.asList(entity
                            .getMatrixColumns()));
                } catch (UiException ex) {
                }
            }
            if (entity.getMatrixRows() != null) {
                try {
                    list_chosenbox_matrixRows.setSelectedObjects(Arrays.asList(entity
                            .getMatrixRows()));
                } catch (UiException ex) {
                }
            }
        }
    }

    @Override
    protected boolean validate() {
        if (Strings.isBlank(entity.getName())) {
            WebUtils.notEmpty(Labels.getLabel("entity.name"));
            return false;
        }
        if (entity.getHeaders() == null || entity.getHeaders().isEmpty()) {
            WebUtils.showError(Labels.getLabel("entity.list.header.empty"));
            return false;
        }
        if (!Strings.isBlank(entity.getPanel())) {
            if (!Strings.isBlank(canvasMsg.getValue()) ) {
                String errorString = canvasMsg.getValue();
                WebUtils.showError(Labels.getLabel(
                        "editor.parse.error",
                        new Object[]{Labels.getLabel("entity.page.ajax"),
                                errorString}));
                return false;
            } else {
                try {
                    Document doc = new Builder(false, new NodeFactory()).build(
                            entity.getPanel(), null);
                    doc.detach();
                    doc = null;
                } catch (Exception ex) {
                    WebUtils.showError(Labels.getLabel(
                            "editor.parse.error",
                            new Object[]{Labels.getLabel("entity.list.panel"),
                                    ex.getMessage()}));
                    return false;
                }
            }
        }
        for (Header header : entity.getHeaders()) {
            if (!Strings.isBlank(header.getField())) {
                if (header.getType() == null) {
                    WebUtils.showError(Labels.getLabel(
                            "entity.list.header.type.empty",
                            new String[]{header.getField()}));
                    return false;
                }
                if (Strings.isBlank(header.getName())) {
                    WebUtils.showError(Labels.getLabel(
                            "entity.list.header.name.empty",
                            new String[]{header.getField()}));
                    return false;
                }
            } else if (Strings.isBlank(header.getName())) {
                WebUtils.showError(Labels.getLabel(
                        "entity.list.header.name.empty",
                        new String[]{header.getField()}));
                return false;
            }
            if (!Strings.isBlank(header.getComponent())) {
                try {
                    Document doc = new Builder(false, new NodeFactory()).build(
                            header.getComponent(), null);
                    doc.detach();
                    doc = null;
                } catch (Exception ex) {
                    WebUtils.showError(Labels.getLabel(
                            "editor.parse.error",
                            new Object[]{
                                    header.getName()
                                            + ":"
                                            + Labels.getLabel("entity.list.header.component"),
                                    ex.getMessage()}));
                    return false;
                }
            }
        }
        if (entity.getGroups() != null) {
            for (Group group : entity.getGroups()) {
                if (Strings.isBlank(group.getName())) {
                    WebUtils.showError(Labels.getLabel(
                            "entity.list.group.name.empty",
                            new String[]{group.getTitle()}));
                    return false;
                }
                if (Strings.isBlank(group.getFields())) {
                    WebUtils.showError(Labels.getLabel(
                            "entity.list.group.fields.empty",
                            new String[]{group.getTitle()}));
                    return false;
                }
            }
        }
        if (entity.getOnRow() != null) {
            boolean notId = Strings.isBlank(entity.getOnRow().getId());
            if (notId && Strings.isBlank(entity.getOnRow().getContent()))
                entity.setOnRow(null);
            else if (!notId)
                entity.getOnRow().setContent(null);
        }
        if (entity.getOnInit() != null) {
            boolean notId = Strings.isBlank(entity.getOnInit().getId());
            if (notId && Strings.isBlank(entity.getOnInit().getContent()))
                entity.setOnInit(null);
            else if (!notId)
                entity.getOnInit().setContent(null);
        }
        if (entity.getOnBefore() != null) {
            boolean notId = Strings.isBlank(entity.getOnBefore().getId());
            if (notId && Strings.isBlank(entity.getOnBefore().getContent()))
                entity.setOnBefore(null);
            else if (!notId)
                entity.getOnBefore().setContent(null);
        }
        if (entity.getOnAfter() != null) {
            boolean notId = Strings.isBlank(entity.getOnAfter().getId());
            if (notId && Strings.isBlank(entity.getOnAfter().getContent()))
                entity.setOnAfter(null);
            else if (!notId)
                entity.getOnAfter().setContent(null);
        }
        if (entity.getPanel() != null && entity.getPanel().trim().equals(""))
            entity.setPanel(null);
        if (entity.getAuxHeads() != null && entity.getAuxHeads().isEmpty())
            entity.setAuxHeads(null);
        if (entity.getGroups() != null && entity.getGroups().isEmpty())
            entity.setGroups(null);
        return true;
    }

    /*public void openWidgetWin(){
        templates.setVisible(true);
    }*/

    @Override
    protected void dispatch(final Event evt) {
        if (evt.getTarget()==list_chosenbox_matrixColumns){
            List<String> matrixRowsList = entity.getMatrixRows()!=null?Arrays.asList(entity.getMatrixRows()):new ArrayList<String>();
            List<String> matrixColumnsList = new ArrayList<>();
            for (Object obj:list_chosenbox_matrixColumns.getSelectedObjects()){
                matrixColumnsList.add(obj+"");
            }
            boolean flag = false;
            for (String val:matrixRowsList){
                if (matrixColumnsList.contains(val)){
                    matrixColumnsList.remove(val);
                    flag = true;
                }
            }
            entity.setMatrixColumns(matrixColumnsList.toArray(new String[matrixColumnsList.size()]));
            if (flag){
                WebUtils.showError(Labels.getLabel("entity.list.layoutrepeat"));
                refreshChosenbox();
            }
            setDirty();
            return;
        }
        if (evt.getTarget()==list_chosenbox_matrixRows){
            List<String> matrixColumnsList = entity.getMatrixColumns()!=null?Arrays.asList(entity.getMatrixColumns()):new ArrayList<String>();
            List<String> matrixRowsList = new ArrayList<>();
            for (Object obj:list_chosenbox_matrixRows.getSelectedObjects()){
                matrixRowsList.add(obj+"");
            }
            boolean flag = false;
            for (String val:matrixColumnsList){
                if (matrixRowsList.contains(val)){
                    matrixRowsList.remove(val);
                    flag = true;
                }
            }
            entity.setMatrixRows(matrixRowsList.toArray(new String[matrixRowsList.size()]));
            if (flag){
                WebUtils.showError(Labels.getLabel("entity.list.dimensionrepeat"));
                refreshChosenbox();
            }
            setDirty();
            return;
        }
        if (evt.getTarget() == panelEditor) {
            try {
                entity.setPanel((String) panelEditor.getValue());
                redraw();
                setDirty();
            } catch (Exception ex) {
                if ((ex instanceof UiException)
                        && ex.getMessage().startsWith("Not unique in ID"))
                    throw (UiException) ex;
            }
        } else if ("list_a_form_table".equals(evt.getTarget().getId())) {
            AbstractView.createTableView(
                    new EditorCallback<TableInfo>() {
                        @Override
                        public TableInfo getValue() {
                            return null;
                        }

                        @Override
                        public void setValue(TableInfo value) {
                            entity.setTable(value.getTable());
                            List<Header> headers = new ArrayList<Header>();
                            for (TableField tf : value.getFields()) {
                                Header header = new Header();
                                header.setField(tf.getName());
                                header.setName(tf.getName());
                                header.setTitle(tf.getDescription());
                                header.setType(tf.getType());
                                headers.add(header);
                            }
                            entity.setAuxHeads(null);
                            entity.setHeaders(headers);
                            entity.setGroups(null);
                            canvas.getChildren().clear();
                            entityTable.setValue(entity.getTable());
                            entity.setMatrixColumns(null);
                            entity.setMatrixRows(null);
                            canvasHandler.draw();
                            propertytabbox.getTabs().getChildren().get(2).setVisible(false);
                            propertytabbox.getTabs().getChildren().get(3).setVisible(false);
                            propertytabbox.getTabs().getChildren().get(4).setVisible(false);
                            propertytabbox.getTabs().getChildren().get(6).setVisible(false);
                            propertytabbox.setSelectedIndex(0);
                        }

                        @Override
                        public String getTitle() {
                            return Labels.getLabel("wizard.table");
                        }

                        @Override
                        public Page getPage() {
                            return canvas.getPage();
                        }

                        @Override
                        public Editor getEditor() {
                            return null;
                        }
                    }, true, evt.getTarget())
                    .doOverlapped("right,center");
        } else if ("list_a_form_addHeader".equals(evt.getTarget().getId())) {
            addHeader(null);
            refreshChosenbox();
        } else if ("list_a_form_addGroup".equals(evt.getTarget().getId())) {
            addGroup();
        } else if ("list_a_form_addAuxHead".equals(evt.getTarget().getId())) {
            addAuxhead();
        } else if ("list_a_form_addAuxHeader".equals(evt.getTarget().getId())) {
            addAuxheader();
        } else if ("list_a_form_moveLeft".equals(evt.getTarget().getId())) {
            move(false);
        } else if ("list_a_form_moveRight".equals(evt.getTarget().getId())) {
            move(true);
        } else if ("list_a_form_delete".equals(evt.getTarget().getId())) {
            deleteHeader();
            refreshChosenbox();
        } else if ("panel".equals(evt.getTarget().getId())) {
            // AbstractView.createPageView(
            // new SimpleEditorCallback(this, (Button) evt.getTarget(),
            // "panel", true)).doView();
        } else if ("list_button_query".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "query", false), "text/x-plsql", evt.getTarget()).doOverlapped();
        } else if ("list_button_orderBy".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "orderBy", false), "text/x-plsql", evt.getTarget()).doOverlapped();
        } else if ("list_button_groupBy".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "groupBy", false), "text/x-plsql", evt.getTarget()).doOverlapped();
        } else if ("list_button_headStyle".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "headStyle", false), "text/css", evt.getTarget()).doOverlapped();
        } else if ("list_button_rowStyle".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "rowStyle", false), "text/css", evt.getTarget()).doOverlapped();
        } else if ("list_button_footStyle".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "footStyle", false), "text/css", evt.getTarget()).doOverlapped();
        } else if ("list_button_condition".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "condition", false), "text/x-plsql", evt.getTarget()).doOverlapped();
        } else if ("list_button_onHeader".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "onHeaderScript", false), "javascript", evt.getTarget()).doOverlapped();
        } else if ("list_button_onRow".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "onRowScript", false), "javascript", evt.getTarget()).doOverlapped();
        } else if ("list_button_onGroup".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "onGroupScript", false), "javascript", evt.getTarget()).doOverlapped();
        } else if ("list_button_onFooter".equals(evt.getTarget().getId())) {
            AbstractView.createCodeView(
                    new SimpleEditorCallback(this, (Button) evt.getTarget(),
                            "onFooterScript", false), "javascript", evt.getTarget()).doOverlapped();
        } else if (evt.getName().equals(Events.ON_SELECT)
                && evt.getTarget() instanceof Tab) {
            if (tabbox.getSelectedIndex() == 1) {
                super.refreshSource();
                workbench.getHeader().setMenuitem(1);
                workbench.getFunctionBar().setMenuitem(1);
                workbench.getShortcutKeyBar().setMenuitem(1);
            } else {
                workbench.getHeader().setMenuitem(2);
                workbench.getFunctionBar().setMenuitem(2);
                workbench.getShortcutKeyBar().setMenuitem(2);
            }
        } else if (Events.ON_CHANGE.equals(evt.getName())
                || Events.ON_CHECK.equals(evt.getName())
                || Events.ON_SELECT.equals(evt.getName())) {// 变更数据
            Component c = evt.getTarget();
            if (c == entityType) {
                if (entityType.getSelectedIndex() == 0)
                    entity.setShowType(Constants.CATALOG);
                else if (entityType.getSelectedIndex() == 1)
                    entity.setShowType(Constants.DETAIL);
                else if (entityType.getSelectedIndex() == 2)
                    entity.setShowType(Constants.REPORT);
                else
                    entity.setShowType(null);
            } else if (c == entityPageSize) {
                entity.setPageSize(entityPageSize.getValue());
            } else if (c == entityEditableColumns) {
                entity.setEditableColumns(entityEditableColumns.getValue());
            } else if (c == entityLevelBy) {
                entity.setLevelBy(entityLevelBy.getValue());
            } else if (c == entityGroup) {
                entity.setGroupName(entityGroup.getValue());
            } else if (c == entityPageId) {
                entity.setPageId(entityPageId.getValue());
            } else if (c == entityShowTitle) {
                entity.setShowTitle(entityShowTitle.isChecked());
            } else if (c == entityShowPanel) {
                entity.setShowPanel(entityShowPanel.isChecked());
            } else if (c == entityShowRowNumbers) {
                entity.setShowRowNumbers(entityShowRowNumbers.isChecked());
            } else if (c == entitySizedByContent) {
                entity.setSizedByContent(entitySizedByContent.isChecked());
            } else if (c == entitySpan) {
                entity.setSpan(entitySpan.isChecked());
            } else if (c == entityCheckmark) {
                entity.setCheckmark(entityCheckmark.isChecked());
            } else if (c == entityMultiple) {
                entity.setMultiple(entityMultiple.isChecked());
            } else if (c == entityName) {
                entity.setName(entityName.getValue());
            } else if (c == entityDesp) {
                entity.setDescription(entityDesp.getValue());
            } else if (c == entityTable) {
                entity.setTable(entityTable.getValue());
            } else if (c == onRow) {
                if (Strings.isBlank(onRow.getValue())) {
                    if (entity.getOnRow() != null) {
                        if (Strings.isBlank(entity.getOnRow().getContent())) {
                            entity.setOnRow(null);
                        } else
                            entity.getOnRow().setId(null);
                    }
                } else {
                    if (entity.getOnRow() == null)
                        entity.setOnRow(new EventLogic());
                    entity.getOnRow().setId(onRow.getValue());
                }
            } else if (c == onInit) {
                if (Strings.isBlank(onInit.getValue())) {
                    if (entity.getOnInit() != null) {
                        if (Strings.isBlank(entity.getOnInit().getContent())) {
                            entity.setOnInit(null);
                        } else
                            entity.getOnInit().setId(null);
                    }
                } else {
                    if (entity.getOnInit() == null)
                        entity.setOnInit(new EventLogic());
                    entity.getOnInit().setId(onInit.getValue());
                }
            } else if (c == onBefore) {
                if (Strings.isBlank(onBefore.getValue())) {
                    if (entity.getOnBefore() != null) {
                        if (Strings.isBlank(entity.getOnBefore().getContent())) {
                            entity.setOnBefore(null);
                        } else
                            entity.getOnBefore().setId(null);
                    }
                } else {
                    if (entity.getOnBefore() == null)
                        entity.setOnBefore(new EventLogic());
                    entity.getOnBefore().setId(onBefore.getValue());
                }
            } else if (c == onAfter) {
                if (Strings.isBlank(onAfter.getValue())) {
                    if (entity.getOnAfter() != null) {
                        if (Strings.isBlank(entity.getOnAfter().getContent())) {
                            entity.setOnAfter(null);
                        } else
                            entity.getOnAfter().setId(null);
                    }
                } else {
                    if (entity.getOnAfter() == null)
                        entity.setOnAfter(new EventLogic());
                    entity.getOnAfter().setId(onAfter.getValue());
                }
            }
            setDirty();
        } else if (evt.getName().equals(Events.ON_OPEN)||evt instanceof KeyEvent) {// Bandbox
            String selector = EntityType.LOGIC.getName();
            if (evt.getTarget() == entityTable)
                selector = EntityType.TABLE.getName();
            else if (evt.getTarget() == entityPageId)
                selector = EntityType.PAGE.getName();
            if (evt instanceof OpenEvent){
                ((Bandbox) evt.getTarget()).setValue(((OpenEvent)evt).getValue()+"");
            }
            AbstractView.createEntityView(
                    new BandboxCallback(this, (Bandbox) evt.getTarget()),
                    selector, evt.getTarget()).doOverlapped();
        } else if (evt.getName().equals(Events.ON_DROP)) {// 拖拉栏位
            DropEvent de = (DropEvent) evt;
            Row row = (Row) de.getDragged();
            TableField tf = row.getValue();
            addHeader(tf);
        } else {// Bandbox > button
            Component c = evt.getTarget();
            c = c.getPreviousSibling();
            if (c == entityTable) {
                if (!Strings.isBlank(entityTable.getValue()))
                    openEditor(evt.getTarget(), entityTable.getValue());
            } else if (c == onRow) {
                if (Strings.isBlank(onRow.getValue())) {
                    if (entity.getOnRow() == null)
                        entity.setOnRow(new EventLogic());
                    AbstractView.createLogicView(
                            new EventLogicCallback(this, (Button) evt
                                    .getTarget(), entity.getOnRow(), entity.getTable(), "OnRow"), evt
                                    .getTarget())
                            .doOverlapped();
                } else {
                    openEditor(evt.getTarget(), onRow.getValue());
                }
            } else if (c == onInit) {
                if (Strings.isBlank(onInit.getValue())) {
                    if (entity.getOnInit() == null)
                        entity.setOnInit(new EventLogic());
                    AbstractView
                            .createLogicView(
                                    new EventLogicCallback(this, (Button) evt
                                            .getTarget(), entity.getOnInit(), entity.getTable(),
                                            "OnInit"), c).doOverlapped();
                } else {
                    openEditor(evt.getTarget(), onInit.getValue());
                }
            } else if (c == onBefore) {
                if (Strings.isBlank(onBefore.getValue())) {
                    if (entity.getOnBefore() == null)
                        entity.setOnBefore(new EventLogic());
                    AbstractView.createLogicView(
                            new EventLogicCallback(this, (Button) evt
                                    .getTarget(), entity.getOnBefore(), entity.getTable(),
                                    "OnBefore"), evt
                                    .getTarget()).doOverlapped();
                } else {
                    openEditor(evt.getTarget(), onBefore.getValue());
                }
            } else if (c == onAfter) {
                if (Strings.isBlank(onAfter.getValue())) {
                    if (entity.getOnAfter() == null)
                        entity.setOnAfter(new EventLogic());
                    AbstractView.createLogicView(
                            new EventLogicCallback(this, (Button) evt
                                    .getTarget(), entity.getOnAfter(), entity.getTable(),
                                    "OnAfter"), evt
                                    .getTarget()).doOverlapped();
                } else {
                    openEditor(evt.getTarget(), onAfter.getValue());
                }
            }
        }
    }

    private void addHeader(TableField tf) {
        if (entity.getHeaders() == null)
            entity.setHeaders(new ArrayList<Header>());
        Header header = new Header();
        if (tf != null) {
            header.setTitle(tf.getDescription());
            header.setField(tf.getName());
            header.setName(tf.getName());
            header.setType(tf.getType());
            if (tf.getLength() > 0)
                header.setWidth(tf.getLength() + "px");
        } else
            header.setTitle(Labels.getLabel("entity.list.header.add"));
        Treecol ahd = new Treecol(header.getTitle());
        ahd.setAttribute("value", header);
        ahd.setWidgetListener(Events.ON_CLICK,
                "epstudio.selectEditorWidget(this)");
        ahd.addEventListener(Events.ON_CLICK, canvasHandler);
        if (canvas.getTreecols() == null)
            canvas.appendChild(new Treecols());
        canvas.getTreecols().appendChild(ahd);
        entity.getHeaders().add(header);
        setDirty();
    }

    private void addGroup() {
        Component parent = null;
        Group current = null;
        if (canvas.getTreechildren() == null) {
            parent = new Treechildren();
            canvas.appendChild(parent);
        } else if (canvasHandler.selectedItem != null
                && canvasHandler.selectedItem instanceof Treeitem) {
            parent = canvasHandler.selectedItem;
            current = ((Treeitem) canvasHandler.selectedItem).getValue();
            parent = new Treechildren();
            parent.setParent(canvas.getSelectedItem());
        } else
            parent = canvas.getTreechildren();
        if (entity.getGroups() == null)
            entity.setGroups(new ArrayList<Group>());
        Group group = new Group();
        group.setTitle(Labels.getLabel("entity.list.group.add"));
        Treeitem ti = new Treeitem(group.getTitle(), group);
        ti.setWidgetListener(Events.ON_CLICK,
                "epstudio.selectEditorWidget(this)");
        ti.addEventListener(Events.ON_CLICK, canvasHandler);
        ti.setParent(parent);
        if (current != null)
            current.setChild(group);
        else
            entity.getGroups().add(group);
        setDirty();
    }

    private void addAuxhead() {
        if (entity.getAuxHeads() == null)
            entity.setAuxHeads(new ArrayList<AuxHead>());
        AuxHead head = new AuxHead();
        head.setAuxHeaders(new ArrayList<AuxHeader>());
        Auxhead ah = new Auxhead();
        ah.setAttribute("value", head);
        AuxHeader header = new AuxHeader();
        header.setTitle(Labels.getLabel("entity.list.auxheader.add"));
        Auxheader ahd = new Auxheader(header.getTitle());
        ahd.setAttribute("value", header);
        ahd.setColspan(1);
        ahd.setRowspan(1);
        ahd.setWidgetListener(Events.ON_CLICK,
                "epstudio.selectEditorWidget(this)");
        ahd.addEventListener(Events.ON_CLICK, canvasHandler);
        ahd.setParent(ah);
        if (canvas.getFirstChild() instanceof Treecols) {
            canvas.insertBefore(ah, canvas.getFirstChild());
        } else if (canvas.getFirstChild() instanceof Auxhead) {
            canvas.insertBefore(ah, canvas.getFirstChild());
        } else {
            canvas.appendChild(ah);
        }
        head.getAuxHeaders().add(header);
        entity.getAuxHeads().add(0, head);
        //entity.getAuxHeads().add(head);
        setDirty();
    }

    private void addAuxheader() {
        if (canvasHandler.selectedItem == null
                || !(canvasHandler.selectedItem instanceof Auxheader))
            return;
        Auxhead ah = (Auxhead) canvasHandler.selectedItem.getParent();
        AuxHead head = (AuxHead) ah.getAttribute("value");
        AuxHeader header = new AuxHeader();
        header.setTitle(Labels.getLabel("entity.list.auxheader.add"));
        Auxheader ahd = new Auxheader(header.getTitle());
        ahd.setAttribute("value", header);
        ahd.setColspan(1);
        ahd.setRowspan(1);
        ahd.setWidgetListener(Events.ON_CLICK,
                "epstudio.selectEditorWidget(this)");
        ahd.addEventListener(Events.ON_CLICK, canvasHandler);
        ahd.setParent(ah);
        head.getAuxHeaders().add(header);
        setDirty();
    }

    private void move(boolean down) {
        if (canvasHandler.selectedItem == null)
            return;
        Component c = canvasHandler.selectedItem;
        Component parent = c.getParent();
        if (c instanceof Auxheader) {
            AuxHead head = (AuxHead) parent.getAttribute("value");
            AuxHeader header = (AuxHeader) c.getAttribute("value");
            int pos = head.getAuxHeaders().indexOf(header);
            if (down)
                pos++;
            else
                pos--;
            if (pos < 0)
                pos = 0;
            else if (pos >= head.getAuxHeaders().size())
                pos = head.getAuxHeaders().size() - 1;
            c.detach();
            parent.getChildren().add(pos, c);
            head.getAuxHeaders().remove(header);
            head.getAuxHeaders().add(pos, header);
        } else if (c instanceof Treecol) {
            Header header = (Header) c.getAttribute("value");
            int pos = entity.getHeaders().indexOf(header);
            if (down)
                pos++;
            else
                pos--;
            if (pos < 0)
                pos = 0;
            else if (pos >= entity.getHeaders().size())
                pos = entity.getHeaders().size() - 1;
            c.detach();
            parent.getChildren().add(pos, c);
            entity.getHeaders().remove(header);
            entity.getHeaders().add(pos, header);
        }
        setDirty();
    }

    private void deleteHeader() {
        if (canvasHandler.selectedItem == null)
            return;
        Component c = canvasHandler.selectedItem;
        if (c instanceof Auxheader) {// 删除表头
            Component ah = c.getParent();
            AuxHead head = (AuxHead) ah.getAttribute("value");
            AuxHeader header = (AuxHeader) c.getAttribute("value");
            head.getAuxHeaders().remove(header);
            c.detach();
            if (ah.getChildren().isEmpty()) {
                entity.getAuxHeads().remove(head);
                ah.detach();
            }
        } else if (c instanceof Treecol) {// 删除列
            Header header = (Header) c.getAttribute("value");
            entity.getHeaders().remove(header);
            c.detach();
        } else if (c instanceof Treeitem) {// 删除组
            Treeitem ti = (Treeitem) c;
            Group group = ti.getValue();
            if (entity.getGroups().contains(group))
                entity.getGroups().remove(group);
            else {
                Group pg = ti.getParentItem().getValue();
                pg.setChild(null);
            }
            c.detach();
        }
        setDirty();
        canvasHandler.selectedItem = null;
        setCanvasToolbarStatus(true);
    }

    /**
     * 重绘源码
     */
    protected void refreshSource() {
        if (tabbox.getSelectedIndex() == 1)
            super.refreshSource();
    }

    @Override
    protected void redraw() {
        panelDesigner.redraw(false);
        propertytabbox.getTabs().getChildren().get(6).setVisible(false);
        propertytabbox.setSelectedIndex(0);
    }

    private void setAuxheader(Auxheader ahd, AuxHeader header) {
        ahd.setLabel(header.getTitle());
        ahd.setStyle(header.getStyle());
        ahd.setIconSclass(header.getIconSclass());
        ahd.setImage(header.getImage());
        ahd.setAlign(header.getAlign());
        ahd.setHoverImage(header.getHoverimg());
        ahd.setValign(header.getValign());
        ahd.setColspan(header.getColspan() <= 0 ? 1 : header.getColspan());
        ahd.setRowspan(header.getRowspan() <= 0 ? 1 : header.getRowspan());
    }

    private void setHeader(Treecol ahd, Header header) {
        ahd.setLabel(header.getTitle());
        ahd.setStyle(header.getStyle());
        ahd.setIconSclass(header.getIconSclass());
        ahd.setImage(header.getImage());
        ahd.setAlign(header.getAlign());
        ahd.setHoverImage(header.getHoverimg());
        ahd.setValign(header.getValign());
    }

    private void setCanvasToolbarStatus(boolean disabled) {
        addAuxHeader.setDisabled(disabled);
        moveLeft.setDisabled(disabled);
        moveRight.setDisabled(disabled);
        delete.setDisabled(disabled);
    }

    @Override
    public void updateContent(String content) {
        panelEditor.setValue(content);
        entity.setPanel(content);
        setDirty();
    }

    @Override
    public void showMsg(String msg, CMeditor editor) {
        canvasMsg.setValue(msg);
    }

    @Override
    public void updateProperties(Element selection,
                                 List<Map<String, Component>> props, Map<String, Component> events) {
        org.zkoss.zul.Group baseGroup = properties.getGroups().get(0);
        properties.getChildren().removeAll(baseGroup.getItems());
        org.zkoss.zul.Group expertGroup = properties.getGroups().get(1);
        properties.getChildren().removeAll(expertGroup.getItems());
        org.zkoss.zul.Group eventGroup = properties.getGroups().get(2);
        properties.getChildren().removeAll(eventGroup.getItems());
        propertytabbox.getTabs().getChildren().get(2).setVisible(false);
        propertytabbox.getTabs().getChildren().get(3).setVisible(false);
        propertytabbox.getTabs().getChildren().get(4).setVisible(false);
        Tab tab = (Tab) propertytabbox.getTabs().getChildren().get(6);
        if (props != null) {
            tab.setLabel(selection.getLocalName());
            tab.setVisible(true);
            tab.setSelected(true);
            for (int i = 0; i < 2; i++) {
                for (Map.Entry<String, Component> entry : props.get(i)
                        .entrySet()) {
                    Row row = new Row();
                    row.appendChild(new Label(entry.getKey()));
                    row.appendChild(entry.getValue());
                    properties.insertBefore(row, i == 0 ? expertGroup
                            : eventGroup);
                }
            }
            Iterator<Map.Entry<String, Component>> itr = events.entrySet()
                    .iterator();
            while (itr.hasNext()) {
                Map.Entry<String, Component> entry = itr.next();
                if (!(entry.getValue() instanceof Button)) {
                    Row row = new Row();
                    row.appendChild(new Label(entry.getKey()));
                    row.appendChild(entry.getValue());
                    properties.appendChild(row);
                    itr.remove();
                }
            }
            for (Map.Entry<String, Component> entry : events.entrySet()) {
                Row row = new Row();
                row.appendChild(new Label(entry.getKey()));
                row.appendChild(entry.getValue());
                properties.appendChild(row);
            }
        } else {
            tab.setVisible(false);
            tab.getTabbox().setSelectedIndex(0);
        }
    }

    @Override
    public void setTheme(String theme) {
        super.setTheme(theme);
        panelEditor.setTheme(theme);
    }

    @Override
    public void insertTodo() {
        panelEditor.insertText("<!-- TODO -->");
    }

    @Override
    public void insertComments() {
        panelEditor.insertText("<!-- This is a comment -->");
    }

    @Override
    public void undo() {
        panelEditor.exeCmd("undo");
    }

    @Override
    public void redo() {
        panelEditor.exeCmd("redo");
    }

    @Override
    public void selectAll() {
        panelEditor.exeCmd("selectAll");
    }

    @Override
    public void format() {
        panelEditor.exeCmd("format");
    }

    @Override
    public void formatAll() {
        panelEditor.exeCmd("formatAll");
    }

    @Override
    public void deleteLine() {
        panelEditor.exeCmd("deleteLine");
    }

    @Override
    public void indentMore() {
        panelEditor.exeCmd("indentMore");
    }

    @Override
    public void indentLess() {
        panelEditor.exeCmd("indentLess");
    }

    @Override
    public void find() {
        panelEditor.exeCmd("find");
    }

    @Override
    public void findNext() {
        panelEditor.exeCmd("findNext");
    }

    @Override
    public void findPrev() {
        panelEditor.exeCmd("findPrev");

    }

    @Override
    public void replace() {
        panelEditor.exeCmd("replace");
    }

    @Override
    public void replaceAll() {
        panelEditor.exeCmd("replaceAll");
    }

    private class CanvasHandler implements EventListener<Event> {

        // header
        private Textbox headerTitle;

        private Textbox headerField;

        private Textbox headerName;

        private Textbox headerDesp;

        private Combobox headerType;

        private Textbox headerWidth;

        private Combobox headerTotalType;

        private Textbox headerGroupName;

        private Textbox headerTotalName;

        private Button headerGroupStyle;

        private Button headerTotalStyle;

        private Combobox headerAlign;

        private Combobox headerValign;

        private Bandbox headerImage;

        private Bandbox headerHoverimg;

        private Bandbox headerIcon;

        private Button headerStyle;

        private Button headerCellStyle;

        private Checkbox headerIsSort;

        private Checkbox headerIsVisible;

        private Textbox headerFormat;

        private Button headerEvent;

        private Button headerSql;

        private Textbox headerSeparator;

        private Button headerComponent;

        // auxheader
        private Textbox auxTitle;

        private Textbox auxDesp;

        private Button auxStyle;

        private Spinner auxColspan;

        private Spinner auxRowspan;

        private Combobox auxAlign;

        private Combobox auxValign;

        private Bandbox auxImage;

        private Bandbox auxHoverimg;

        private Bandbox auxIcon;

        // group
        private Textbox groupName;

        private Textbox groupTitle;

        private Textbox groupDesp;

        private Chosenbox groupFields;

        private Button groupOrderBy;

        private Button groupStyle;

        //
        private Component selectedItem;

        private HeaderEventListener headerEventListener;

        private AuxHeaderEventListener auxHeaderEventListener;

        private GroupEventListener groupEventListener;

        void createGroup(Component parent, Group group) {
            Treeitem ti = new Treeitem(group.getTitle(), group);
            ti.addEventListener(Events.ON_CLICK, this);
            ti.setWidgetListener(Events.ON_CLICK,
                    "epstudio.selectEditorWidget(this)");
            parent.appendChild(ti);
            if (group.getChild() != null) {
                Treechildren tc = new Treechildren();
                tc.setParent(ti);
                createGroup(tc, group.getChild());
            }
        }

        void draw() {
            // 创建多重表头
            if (entity.getAuxHeads() != null) {
                for (AuxHead head : entity.getAuxHeads()) {
                    Auxhead ah = new Auxhead();
                    ah.setAttribute("value", head);
                    for (AuxHeader header : head.getAuxHeaders()) {
                        Auxheader ahd = new Auxheader();
                        ahd.setAttribute("value", header);
                        setAuxheader(ahd, header);
                        ahd.setWidgetListener(Events.ON_CLICK,
                                "epstudio.selectEditorWidget(this)");
                        ahd.addEventListener(Events.ON_CLICK, this);
                        ahd.setParent(ah);
                    }
                    ah.setParent(canvas);
                }
            } else {
                if (canvas.getHeads() != null)
                    canvas.getHeads().clear();
            }
            // 创建表头
            if (entity.getHeaders() != null) {
                Treecols columns = new Treecols();
                if (canvas.getTreecols() != null)
                    columns = canvas.getTreecols();
                for (Header header : entity.getHeaders()) {
                    Treecol ahd = new Treecol();
                    ahd.setAttribute("value", header);
                    setHeader(ahd, header);
                    ahd.setWidgetListener(Events.ON_CLICK,
                            "epstudio.selectEditorWidget(this)");
                    ahd.addEventListener(Events.ON_CLICK, this);
                    ahd.setParent(columns);
                }
                columns.setParent(canvas);
            } else {
                if (canvas.getTreecols() != null)
                    canvas.getTreecols().getChildren().clear();
            }
            // 创建分组
            if (entity.getGroups() != null) {
                if (canvas.getTreechildren() == null)
                    canvas.appendChild(new Treechildren());
                for (Group group : entity.getGroups())
                    createGroup(canvas.getTreechildren(), group);
            } else {
                if (canvas.getTreechildren() != null)
                    canvas.getTreechildren().getChildren().clear();
            }
        }
        void addEvent() {
            // 初始化
            // Header
            headerEventListener = new HeaderEventListener();
            headerTitle.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerField.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerDesp.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerName.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerType.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerSeparator.addEventListener(Events.ON_CHANGE,
                    headerEventListener);
            Comboitem ci = new Comboitem("");
            ci.setParent(headerType);
            for (FieldType type : FieldType.values()) {
                ci = new Comboitem(type.name());
                ci.setParent(headerType);
            }
            headerWidth.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerTotalType.addEventListener(Events.ON_CHANGE,
                    headerEventListener);
            headerGroupName.addEventListener(Events.ON_CHANGE,
                    headerEventListener);
            headerTotalName.addEventListener(Events.ON_CHANGE,
                    headerEventListener);
            headerAlign.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerValign
                    .addEventListener(Events.ON_CHANGE, headerEventListener);
            headerFormat
                    .addEventListener(Events.ON_CHANGE, headerEventListener);

            headerImage.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerImage.addEventListener(Events.ON_OPEN, headerEventListener);

            headerHoverimg.addEventListener(Events.ON_CHANGE,
                    headerEventListener);
            headerHoverimg
                    .addEventListener(Events.ON_OPEN, headerEventListener);

            headerIcon.addEventListener(Events.ON_CHANGE, headerEventListener);
            headerIcon.addEventListener(Events.ON_OPEN, headerEventListener);

            headerIsSort.addEventListener(Events.ON_CHECK, headerEventListener);
            headerIsVisible.addEventListener(Events.ON_CHECK,
                    headerEventListener);

            headerGroupStyle.addEventListener(Events.ON_CLICK,
                    headerEventListener);
            headerTotalStyle.addEventListener(Events.ON_CLICK,
                    headerEventListener);
            headerStyle.addEventListener(Events.ON_CLICK, headerEventListener);
            headerCellStyle.addEventListener(Events.ON_CLICK,
                    headerEventListener);
            headerEvent.addEventListener(Events.ON_CLICK, headerEventListener);
            headerSql.addEventListener(Events.ON_CLICK, headerEventListener);
            headerComponent.addEventListener(Events.ON_CLICK,
                    headerEventListener);
            // Auxheader
            auxHeaderEventListener = new AuxHeaderEventListener();
            auxTitle.addEventListener(Events.ON_CHANGE, auxHeaderEventListener);
            auxDesp.addEventListener(Events.ON_CHANGE, auxHeaderEventListener);
            auxColspan.addEventListener(Events.ON_CHANGE,
                    auxHeaderEventListener);
            auxRowspan.addEventListener(Events.ON_CHANGE,
                    auxHeaderEventListener);
            auxAlign.addEventListener(Events.ON_CHANGE, auxHeaderEventListener);
            auxValign
                    .addEventListener(Events.ON_CHANGE, auxHeaderEventListener);

            auxImage.addEventListener(Events.ON_CHANGE, auxHeaderEventListener);
            auxHoverimg.addEventListener(Events.ON_CHANGE,
                    auxHeaderEventListener);
            auxIcon.addEventListener(Events.ON_CHANGE, auxHeaderEventListener);
            auxImage.addEventListener(Events.ON_OPEN, auxHeaderEventListener);
            auxHoverimg
                    .addEventListener(Events.ON_OPEN, auxHeaderEventListener);
            auxIcon.addEventListener(Events.ON_OPEN, auxHeaderEventListener);
            auxStyle.addEventListener(Events.ON_CLICK, auxHeaderEventListener);

            // group
            groupEventListener = new GroupEventListener();
            groupName.addEventListener(Events.ON_CHANGE, groupEventListener);
            groupDesp.addEventListener(Events.ON_CHANGE, groupEventListener);
            groupTitle.addEventListener(Events.ON_CHANGE, groupEventListener);
            groupFields.addEventListener(Events.ON_SELECT, groupEventListener);
            groupOrderBy.addEventListener(Events.ON_CLICK, groupEventListener);
            groupStyle.addEventListener(Events.ON_CLICK, groupEventListener);
        }
        @Override
        public void onEvent(Event event) throws Exception {
            Component c = event.getTarget();
            if (event.getName().equals(Events.ON_CLICK)) {
                if (c instanceof Treecol) {
                    selectedItem = c;
                    Header header = (Header) c.getAttribute("value");
                    setHeaderValue(header);
                } else if (c instanceof Auxheader) {
                    selectedItem = c;
                    AuxHeader header = (AuxHeader) c.getAttribute("value");
                    setAuxValue(header);
                } else if (c instanceof Treeitem) {
                    selectedItem = c;
                    Group group = ((Treeitem) c).getValue();
                    setGroupValue(group);
                }
                setCanvasToolbarStatus(false);
            }
        }

        private void setGroupValue(Group group) {
            groupTitle.setValue(group.getTitle());
            groupName.setValue(group.getName());
            groupDesp.setValue(group.getDescription());
            if (entity.getHeaders() != null && !entity.getHeaders().isEmpty()) {
                ListModelList<String> model = new ListModelList<String>(entity
                        .getHeaders().size());
                for (Header field : entity.getHeaders())
                    model.add(field.getName());
                groupFields.setModel(model);
                if (group.getFields() != null) {
                    try {
                        groupFields.setSelectedObjects(Arrays.asList(group
                                .getFields().split(",")));
                    } catch (UiException ex) {
                    }
                }
            }
            if (!Strings.isBlank(group.getOrderBy()))
                groupOrderBy.setSclass("epeditor-btn-mark");
            else
                groupOrderBy.setSclass("epeditor-btn");
            if (!Strings.isBlank(group.getStyle()))
                groupStyle.setSclass("epeditor-btn-mark");
            else
                groupStyle.setSclass("epeditor-btn");
            propertytabbox.getTabs().getChildren().get(2).setVisible(false);
            propertytabbox.getTabs().getChildren().get(3).setVisible(false);
            propertytabbox.getTabs().getChildren().get(4).setVisible(true);
            propertytabbox.getTabs().getChildren().get(6).setVisible(false);
            propertytabbox.setSelectedIndex(4);
        }

        private void setAuxValue(AuxHeader header) {
            auxTitle.setValue(header.getTitle());
            auxDesp.setValue(header.getDescription());
            auxRowspan.setValue(header.getRowspan());
            auxColspan.setValue(header.getColspan());
            auxAlign.setValue(header.getAlign());
            auxValign.setValue(header.getValign());
            auxImage.setValue(header.getImage());
            auxHoverimg.setValue(header.getHoverimg());
            auxIcon.setValue(header.getIconSclass());
            if (!Strings.isBlank(header.getStyle()))
                auxStyle.setSclass("epeditor-btn-mark");
            else
                auxStyle.setSclass("epeditor-btn");
            propertytabbox.getTabs().getChildren().get(2).setVisible(false);
            propertytabbox.getTabs().getChildren().get(3).setVisible(true);
            propertytabbox.getTabs().getChildren().get(4).setVisible(false);
            propertytabbox.getTabs().getChildren().get(6).setVisible(false);
            propertytabbox.setSelectedIndex(3);
        }

        private void setHeaderValue(Header header) {
            headerTitle.setValue(header.getTitle());
            headerField.setValue(header.getField());
            headerName.setValue(header.getName());
            headerDesp.setValue(header.getDescription());
            if (header.getType() != null)
                headerType.setValue(header.getType().name());
            else {
                headerType.setSelectedIndex(0);
                headerType.setValue("");
            }
            headerWidth.setValue(header.getWidth());
            headerTotalType.setSelectedIndex(header.getTotalType());
            headerGroupName.setValue(header.getGroupName());
            headerTotalName.setValue(header.getTotalName());
            headerAlign.setValue(header.getAlign());
            headerValign.setValue(header.getValign());
            headerFormat.setValue(header.getFormat());
            headerImage.setValue(header.getImage());
            headerHoverimg.setValue(header.getHoverimg());
            headerIcon.setValue(header.getIconSclass());

            headerIsSort.setChecked(header.isSort());
            headerIsVisible.setChecked(header.isVisible());

            headerSeparator.setValue(header.getSqlSeparator());

            if (!Strings.isBlank(header.getGroupStyle()))
                headerGroupStyle.setSclass("epeditor-btn-mark");
            else
                headerGroupStyle.setSclass("epeditor-btn");

            if (!Strings.isBlank(header.getTotalStyle()))
                headerTotalStyle.setSclass("epeditor-btn-mark");
            else
                headerTotalStyle.setSclass("epeditor-btn");

            if (!Strings.isBlank(header.getStyle()))
                headerStyle.setSclass("epeditor-btn-mark");
            else
                headerStyle.setSclass("epeditor-btn");

            if (!Strings.isBlank(header.getCellStyle()))
                headerCellStyle.setSclass("epeditor-btn-mark");
            else
                headerCellStyle.setSclass("epeditor-btn");

            if (!Strings.isBlank(header.getEvent()))
                headerEvent.setSclass("epeditor-btn-mark");
            else
                headerEvent.setSclass("epeditor-btn");

            if (!Strings.isBlank(header.getSql()))
                headerSql.setSclass("epeditor-btn-mark");
            else
                headerSql.setSclass("epeditor-btn");

            if (!Strings.isBlank(header.getComponent()))
                headerComponent.setSclass("epeditor-btn-mark");
            else
                headerComponent.setSclass("epeditor-btn");

            propertytabbox.getTabs().getChildren().get(2).setVisible(true);
            propertytabbox.getTabs().getChildren().get(3).setVisible(false);
            propertytabbox.getTabs().getChildren().get(4).setVisible(false);
            propertytabbox.getTabs().getChildren().get(6).setVisible(false);
            propertytabbox.setSelectedIndex(2);
        }

        class GroupEventListener implements EventListener<Event> {
            @Override
            public void onEvent(Event event) throws Exception {
                if (canvas.getSelectedItem() == null)
                    return;
                Group group = canvas.getSelectedItem().getValue();
                if (event.getName().equals(Events.ON_CHANGE)) {
                    if (event.getTarget() == groupTitle) {
                        group.setTitle(groupTitle.getValue());
                        canvas.getSelectedItem().setLabel(group.getTitle());
                    } else if (event.getTarget() == groupName)
                        group.setName(groupName.getValue());
                    else if (event.getTarget() == groupDesp)
                        group.setDescription(groupDesp.getValue());
                    setDirty();
                } else if (event.getName().equals(Events.ON_CLICK)) {
                    if (event.getTarget() == groupStyle)
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        group, groupStyle, "style", false),
                                "text/css", event.getTarget()).doOverlapped();
                    else
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        group, groupOrderBy, "orderBy", false),
                                "text/x-plsql", event.getTarget()).doOverlapped();
                } else {// chosenbox
                    List<String> model = new ArrayList<String>(groupFields
                            .getSelectedObjects().size());
                    for (Object field : groupFields.getSelectedObjects())
                        model.add((String) field);
                    group.setFields(Lang.concat(",", model).toString());
                    setDirty();
                }
            }
        }

        class AuxHeaderEventListener implements EventListener<Event> {

            @Override
            public void onEvent(Event event) throws Exception {
                if (selectedItem == null
                        || !(selectedItem instanceof Auxheader))
                    return;
                AuxHeader header = (AuxHeader) selectedItem
                        .getAttribute("value");
                Component c = event.getTarget();
                if (event.getName().equals(Events.ON_CHANGE)) {
                    if (c == auxTitle) {
                        header.setTitle(auxTitle.getValue());
                    } else if (c == auxDesp) {
                        header.setDescription(auxDesp.getValue());
                    } else if (c == auxColspan) {
                        header.setColspan(auxColspan.getValue());
                    } else if (c == auxRowspan) {
                        header.setRowspan(auxRowspan.getValue());
                    } else if (c == auxAlign) {
                        header.setAlign(auxAlign.getValue());
                    } else if (c == auxValign) {
                        header.setValign(auxValign.getValue());
                    } else if (c == auxImage) {
                        header.setImage(auxImage.getValue());
                    } else if (c == auxHoverimg) {
                        header.setHoverimg(auxHoverimg.getValue());
                    } else if (c == auxIcon) {
                        header.setIconSclass(auxIcon.getValue());
                    }
                    setAuxheader((Auxheader) selectedItem, header);
                    setDirty();
                } else if (event.getName().equals(Events.ON_CLICK)) {
                    AbstractView.createCodeView(
                            new SimpleEditorCallback(ListEntityEditor.this,
                                    header, auxStyle, "style", false),
                            "text/css", event.getTarget()).doOverlapped();
                } else {// ON_OPEN
                    String scope = "image";
                    if (c == auxIcon)
                        scope = "iconSclass";
                    AbstractView.createIconView(
                            new BandboxCallback(ListEntityEditor.this,
                                    (Bandbox) c), scope, event.getTarget()).doOverlapped();
                }
            }
        }

        class HeaderEventListener implements EventListener<Event> {

            @Override
            public void onEvent(Event event) throws Exception {
                if (selectedItem == null || !(selectedItem instanceof Treecol))
                    return;
                Header header = (Header) selectedItem.getAttribute("value");
                Component c = event.getTarget();
                if (event.getName().equals(Events.ON_CHANGE)
                        || event.getName().equals(Events.ON_CHECK)) {
                    if (c == headerTitle) {
                        header.setTitle(headerTitle.getValue());
                    } else if (c == headerDesp)
                        header.setDescription(headerDesp.getValue());
                    else if (c == headerField)
                        header.setField(headerField.getValue());
                    else if (c == headerName)
                        header.setName(headerName.getValue());
                    else if (c == headerType) {
                        if (headerType.getSelectedIndex() == 0
                                || Strings.isBlank(headerType.getValue()))
                            header.setType(null);
                        else
                            header.setType(FieldType.getType(headerType
                                    .getSelectedItem().getLabel()));
                    } else if (c == headerWidth)
                        header.setWidth(headerWidth.getValue());
                    else if (c == headerTotalType)
                        header.setTotalType(headerTotalType.getSelectedIndex());
                    else if (c == headerGroupName)
                        header.setGroupName(headerGroupName.getValue());
                    else if (c == headerTotalName)
                        header.setTotalName(headerTotalName.getValue());
                    if (c == headerAlign)
                        header.setAlign(headerAlign.getValue());
                    else if (c == headerValign)
                        header.setValign(headerValign.getValue());
                    else if (c == headerFormat)
                        header.setFormat(headerFormat.getValue());
                    else if (c == headerImage)
                        header.setImage(headerImage.getValue());
                    else if (c == headerHoverimg)
                        header.setHoverimg(headerHoverimg.getValue());
                    else if (c == headerIcon)
                        header.setIconSclass(headerIcon.getValue());
                    else if (c == headerIsSort)
                        header.setSort(headerIsSort.isChecked());
                    else if (c == headerIsVisible)
                        header.setVisible(headerIsVisible.isChecked());
                    else if (c == headerSeparator)
                        header.setSqlSeparator(headerSeparator.getValue());
                    setHeader((Treecol) selectedItem, header);
                    setDirty();
                } else if (event.getName().equals(Events.ON_CLICK)) {
                    if (c == headerGroupStyle) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerGroupStyle, "groupStyle",
                                        false), "text/css", event.getTarget()).doOverlapped();
                    } else if (c == headerTotalStyle) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerTotalStyle, "totalStyle",
                                        false), "text/css", event.getTarget()).doOverlapped();
                    } else if (c == headerStyle) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerStyle, "style", false),
                                "text/css", event.getTarget()).doOverlapped();
                    } else if (c == headerCellStyle) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerCellStyle, "cellStyle",
                                        false), "text/css", event.getTarget()).doOverlapped();
                    } else if (c == headerEvent) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerEvent, "event", false),
                                "javascript", event.getTarget()).doOverlapped();
                    } else if (c == headerSql) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerSql, "sql", false),
                                "text/x-plsql", event.getTarget()).doOverlapped();
                    } else if (c == headerComponent) {
                        AbstractView.createCodeView(
                                new SimpleEditorCallback(ListEntityEditor.this,
                                        header, headerComponent, "component",
                                        false), "eppage", event.getTarget()).doOverlapped();
                    }
                } else {// ON_OPEN
                    String scope = "image";
                    if (c == headerIcon)
                        scope = "iconSclass";
                    AbstractView.createIconView(
                            new BandboxCallback(ListEntityEditor.this,
                                    (Bandbox) c), scope, event.getTarget()).doOverlapped();
                }
                refreshChosenbox();
            }

        }
    }
}
