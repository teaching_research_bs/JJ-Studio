package cn.easyplatform.studio.web.editors.workbench;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.web.action.CreateAction;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.editors.Editor;
import cn.easyplatform.studio.web.editors.EditorCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.SubType;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Idspace;


import java.util.ArrayList;

import java.util.Iterator;
import java.util.List;

public class DefaultEditor extends AbstractPanelEditor {

    private List<A> aList;

    public DefaultEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(final Event evt) {
        if ("welcome_a_rule".equals(evt.getTarget().getId())) {
            AbstractView.createRule(new EditorCallback<String>() {
                @Override
                public String getValue() {
                    return null;
                }

                @Override
                public void setValue(String value) {

                }

                @Override
                public String getTitle() {
                    return Labels.getLabel("welcome.addRole.title");
                }

                @Override
                public Page getPage() {
                    return evt.getPage();
                }

                @Override
                public Editor getEditor() {
                    return null;
                }
            }, evt.getTarget()).doOverlapped();
        } else if ("welcome_a_mall".equals(evt.getTarget().getId())) {
            Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.mall"), "_black");
        } else if ("welcome_a_module".equals(evt.getTarget().getId())) {
            Executions.getCurrent().sendRedirect(StudioApp.getHelpConfig("help.baseUrl.module"), "_black");
        } else {
            EntityType entityType = null;
            SubType subType = null;
            if ("welcome_a_task".equals(evt.getTarget().getId())) {
                entityType = EntityType.TASK;
            } else if ("welcome_a_table".equals(evt.getTarget().getId())) {
                entityType = EntityType.TABLE;
            } else if ("welcome_a_view".equals(evt.getTarget().getId())) {
                entityType = EntityType.PAGE;
            } else if ("welcome_a_report".equals(evt.getTarget().getId())) {
                entityType = EntityType.REPORT;
            } else if ("welcome_a_api".equals(evt.getTarget().getId())) {
                entityType = EntityType.API;
            }

            workbench.getShortcutKeyBar().setDisabled("shortcutKey_div_create",true);
            for (A a : aList) {
                a.setDisabled(true);
            }
            new CreateAction(workbench, entityType, subType, new CreateAction.OnCreateListener() {
                @Override
                public void closeWindow() {
                    for (A a : aList) {
                        a.setDisabled(false);
                    }
                    workbench.getShortcutKeyBar().setDisabled("shortcutKey_div_create",false);
                }
            }).on();
        }
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("app.welcome"), null, "~./include/help/Welcome.zul");
        tab.setClosable(false);
        aList = new ArrayList<>();
        Iterator<Component> itr = is.queryAll("a")
                .iterator();
        while (itr.hasNext()){
            A component = (A) itr.next();
            component.addEventListener(Events.ON_CLICK, this);
            aList.add(component);
        }
    }
}
