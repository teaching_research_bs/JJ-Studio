package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.importAll.SaveMenuInfoCmd;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.utils.ZipUtil;
import cn.easyplatform.studio.vos.IXVo;
import cn.easyplatform.studio.vos.MenuInfoVo;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.*;


public class MenuImportEditor extends DictImportEditor {
    public MenuImportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getName().equals(Events.ON_UPLOAD)) {
            UploadEvent ue = (UploadEvent) evt;
            IXVo vo = (IXVo) ZipUtil.unpackZip(ue.getMedia().getByteData());
            redraw(vo);
        } else if (evt.getName().equals(Events.ON_DOUBLE_CLICK)) {
            Listitem treeitem = (Listitem) evt.getTarget();
            MenuInfoVo menuVo = treeitem.getValue();
            //AbstractView.createDicDetailView(new ComponentCallback(this.project, Labels.getLabel("dic.detail.title")),
                    //menuVo.getDicDetailVoList()).doOverlapped();
        } else if (evt.getTarget().getId().equals("menuImport_button_commit")) {
            if (StudioApp.execute(new SaveMenuInfoCmd(entity.getMenuInfoVos())) == true)
                WebUtils.showSuccess(Labels.getLabel("export.success"));
        }
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.import.menu"), "~./images/menu.gif",
                "~./include/editor/project/menuImport.zul");
        this.project = (Label) is.getFellow("menuImport_label_project");
        this.operator = (Label) is.getFellow("menuImport_label_operator");
        this.date = (Label) is.getFellow("menuImport_label_date");
        this.comment = (Label) is.getFellow("menuImport_label_comment");
        this.size = (Label) is.getFellow("menuImport_label_size");
        this.sources = (Listbox) is.getFellow("menuImport_listbox_sources");
        this.commit = (Button) is.getFellow("menuImport_button_commit");
        this.commit.addEventListener(Events.ON_CLICK, this);
        is.getFellow("menuImport_button_upload").addEventListener(Events.ON_UPLOAD, this);
    }

    private void redraw(IXVo vo) {
        entity = vo;
        project.setValue(vo.getProjectId());
        operator.setValue(vo.getUser());
        date.setValue(DateFormatUtils.format(vo.getDate(), "yyyy-MM-dd hh:mm:ss"));
        size.setValue((vo.getMenuInfoVos() != null ? vo.getMenuInfoVos().size(): "0") + "");
        comment.setValue(vo.getDesp());
        redrawTree(entity);
        commit.setDisabled(false);
    }

    private void redrawTree(IXVo vo) {
        sources.getItems().clear();
        if (vo.getMenuInfoVos() != null) {
            for (MenuInfoVo dicVo : vo.getMenuInfoVos()) {
                Listitem li = new Listitem();
                li.appendChild(new Listcell(dicVo.getMenuId()));
                li.appendChild(new Listcell(dicVo.getName()));
                li.appendChild(new Listcell(dicVo.getImage()));
                li.appendChild(new Listcell(dicVo.getOrderNo()));
                li.appendChild(new Listcell(dicVo.getParentMenuId()));
                li.appendChild(new Listcell(dicVo.getDesp()));
                li.appendChild(new Listcell(dicVo.getTasks()));
                li.setValue(dicVo);
                sources.appendChild(li);
            }
        }
    }
}
