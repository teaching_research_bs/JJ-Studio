package cn.easyplatform.studio.web.editors.admin;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.admin.LoginlogDeleteAllCmd;
import cn.easyplatform.studio.cmd.admin.LoginlogDeleteCmd;
import cn.easyplatform.studio.cmd.admin.LoginlogQueryAllCmd;
import cn.easyplatform.studio.cmd.admin.LoginlogQueryCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.dao.Page;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.LoginlogVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;
import org.zkoss.zul.event.ZulEvents;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author Zeta
 * @Version 1.0
 */
public class LoginLogEditor extends AbstractPanelEditor {

    public LoginLogEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    private Datebox qlogintime;
    private Textbox qusername;
    private Textbox qlogintn;
    private Combobox qlogintype;
    private Listbox  loglist;
    private Combobox pagesize;
    private Page page ;
    private Paging showPaging;
    private Button searchBtn;
    private Button deleteBtn;
    private Button deleteallBtn;

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.login.log"), "~./images/dictionary.png",
                "~./include/admin/log/loginlog.zul");

        this.qlogintime = (Datebox)is.getFellow("loginlog_datebox_qlogintime");
        this.qusername = (Textbox)is.getFellow("loginlog_textbox_qusername");
        this.qlogintn = (Textbox)is.getFellow("loginlog_combobox_qlogintn");
        this.qlogintype = (Combobox)is.getFellow("loginlog_combobox_qlogintype");
        this.loglist = (Listbox)is.getFellow("loginlog_grid_loglist");
        this.pagesize = (Combobox)is.getFellow("loginlog_combobox_pagesize");
        this.showPaging = (Paging)is.getFellow("module_paging_showPaging");
        searchBtn = (Button)is.getFellow("loginlog_button_query");
        searchBtn.addEventListener(Events.ON_CLICK, this);
        deleteBtn = (Button)is.getFellow("loginlog_button_delete");
        deleteBtn.addEventListener(Events.ON_CLICK, this);
        deleteallBtn = (Button)is.getFellow("loginlog_button_deleteall");
        deleteallBtn.addEventListener(Events.ON_CLICK, this);
        showPaging.addEventListener(ZulEvents.ON_PAGING, this);

        page=new Page(Integer.valueOf(pagesize.getValue()));
        Map<String,Object> queryallmap = StudioApp.execute(new LoginlogQueryAllCmd(page));
        showPaging.setPageSize(Integer.valueOf(pagesize.getValue()));
        showPaging.setTotalSize((int)queryallmap.get("loginlogcout"));
        loglist.setModel(new ListModelList<LoginlogVo>(this.changefield((List<LoginlogVo>)queryallmap.get("loginloglist"))));
        loglist.setCheckmark(true);
        loglist.setMultiple(true);
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getTarget() ==showPaging) {// 分页
            page=new Page(Integer.valueOf(pagesize.getValue()));
            showPaging.setPageSize(Integer.valueOf(pagesize.getValue()));
            page.setPageNo(showPaging.getActivePage()+1);
            LoginlogVo vo=new LoginlogVo();
            vo.setLoginTime(qlogintime.getValue());
            vo.setUserName(qusername.getValue());
            vo.setLoginTN(qlogintn.getValue());
            if(qlogintype.getSelectedItem()==null){
                vo.setType("");
            }else {
                vo.setType((String)qlogintype.getSelectedItem().getValue());
            }
            Map<String,Object> querymap = StudioApp.execute(new LoginlogQueryCmd(vo,page));
            loglist.setModel(new ListModelList<LoginlogVo>(this.changefield((List<LoginlogVo>) querymap.get("loginloglist"))));
            loglist.setMultiple(false);
            loglist.setCheckmark(true);
            loglist.setMultiple(true);
        }
        if (evt.getTarget()==searchBtn){
            LoginlogVo vo=new LoginlogVo();
            vo.setLoginTime(qlogintime.getValue());
            vo.setUserName(qusername.getValue());
            vo.setLoginTN(qlogintn.getValue());
            if(qlogintype.getSelectedItem()==null){
                vo.setType("");
            }else {
                vo.setType((String)qlogintype.getSelectedItem().getValue());
            }
            page=new Page(Integer.valueOf(pagesize.getValue()));
            Map<String,Object> querymap = StudioApp.execute(new LoginlogQueryCmd(vo,page));
            loglist.setModel(new ListModelList<LoginlogVo>(this.changefield((List<LoginlogVo>) querymap.get("loginloglist"))));
            showPaging.setPageSize(Integer.valueOf(pagesize.getValue()));
            showPaging.setTotalSize((int)querymap.get("loginlogcout"));
            loglist.setMultiple(false);
            loglist.setCheckmark(true);
            loglist.setMultiple(true);
        }
        if(evt.getTarget()==deleteBtn){
            WebUtils.showConfirm(Labels.getLabel("button.delete") , new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (event.getName().equals(Events.ON_OK)) {
                        String str = new String();
                        for (Listitem sel : loglist.getItems()) {
                            if (!sel.isSelected())
                                continue;
                            if (sel.getValue() != null) {
                                String s = sel.getId();
                                str += s + "/";
                            }
                        }
                        if (StudioApp.execute(new LoginlogDeleteCmd(str))) {
                            WebUtils.showInfo(Labels.getLabel("admin.user.delete.success"));
                            LoginlogVo vo = new LoginlogVo();
                            vo.setLoginTime(qlogintime.getValue());
                            vo.setUserName(qusername.getValue());
                            vo.setLoginTN(qlogintn.getValue());
                            if (qlogintype.getSelectedItem() == null) {
                                vo.setType("");
                            } else {
                                vo.setType((String) qlogintype.getSelectedItem().getValue());
                            }
                            page = new Page(Integer.valueOf(pagesize.getValue()));
                            page.setPageNo(showPaging.getActivePage() + 1);
                            Map<String, Object> querymap = StudioApp.execute(new LoginlogQueryCmd(vo, page));
                            loglist.setModel(new ListModelList<LoginlogVo>(changefield((List<LoginlogVo>) querymap.get("loginloglist"))));
                            showPaging.setTotalSize((int) querymap.get("loginlogcout"));
                            loglist.setMultiple(false);
                            loglist.setCheckmark(true);
                            loglist.setMultiple(true);
                            } else{
                            WebUtils.showInfo(Labels.getLabel("admin.user.delete.fail"));
                             }
                        }
                    }
                });
            }
        if(evt.getTarget()==deleteallBtn){
            WebUtils.showConfirm(Labels.getLabel("button.delete"),  new EventListener<Event>() {
                public void onEvent(Event event) throws Exception {
                    if (event.getName().equals(Events.ON_OK)) {
                        if(StudioApp.execute(new LoginlogDeleteAllCmd())){
                            WebUtils.showInfo(Labels.getLabel("admin.user.delete.success"));
                            page=new Page(Integer.valueOf(pagesize.getValue()));
                            Map<String,Object> queryallmap = StudioApp.execute(new LoginlogQueryAllCmd(page));
                            loglist.setModel(new ListModelList<LoginlogVo>(changefield((List<LoginlogVo>)queryallmap.get("loginloglist"))));
                            loglist.setMultiple(false);
                            loglist.setCheckmark(true);
                            loglist.setMultiple(true);
                        }else {
                            WebUtils.showInfo(Labels.getLabel("admin.user.delete.fail"));
                        }
                    }
                }
            });
        }
    }

    //改变集合字段
    public List<LoginlogVo> changefield(List<LoginlogVo> list){
        List<LoginlogVo> clist=new ArrayList<LoginlogVo>();
        if(list!=null && !list.isEmpty()){
            for(int i=0;i<list.size();i++){
                if(list.get(i).getType().equals("1")){
                    list.get(i).setType("登录");
                }else if(list.get(i).getType().equals("2")){
                    list.get(i).setType("正常退出");
                }else if(list.get(i).getType().equals("3")){
                    list.get(i).setType("超时退出");
                }else if(list.get(i).getType().equals("4")){
                    list.get(i).setType("管理员移除");
                }
                clist.add(list.get(i));
            }
        }
        return clist;
    }
}
