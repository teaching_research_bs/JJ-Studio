/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.entity;

import cn.easyplatform.entities.beans.batch.BatchBean;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.web.editors.AbstractEntityEditor;
import cn.easyplatform.studio.web.editors.BandboxCallback;
import cn.easyplatform.studio.web.editors.EventLogicCallback;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import cn.easyplatform.studio.web.views.impl.AbstractView;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.KeyEvent;
import org.zkoss.zk.ui.event.OpenEvent;
import org.zkoss.zul.*;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BatchEntityEditor extends AbstractEntityEditor<BatchBean> {

    private Textbox entityId;

    private Textbox entityName;

    private Textbox entityDesp;

    private Bandbox dsId;

    private Bandbox targetTable;

    private Combobox processCode;

    private Combobox transactionScope;

    private Bandbox nextId;

    private Bandbox onBegin;

    private Bandbox onRecord;

    private Bandbox onEnd;

    private Bandbox onException;

    private Checkbox batchUpdate;

    /**
     * @param workbench
     * @param entity
     * @param code
     */
    public BatchEntityEditor(WorkbenchController workbench, BatchBean entity,
                             char code) {
        super(workbench, entity, code);
    }

    @Override
    public void create(Object... args) {
        super.create(args);
        Tabpanel tabpanel = new Tabpanel();
        Idspace is = new Idspace();
        is.setHflex("1");
        is.setVflex("1");
        is.setParent(tabpanel);
        Executions.createComponents("~./include/editor/entity/batch.zul", is,
                null);
        for (Component comp : is.getFellows()) {
            if (comp.getId().equals("batch_textbox_id")) {
                entityId = (Textbox) comp;
                entityId.setValue(entity.getId());
                entityId.setReadonly(true);
            } else if (comp.getId().equals("batch_textbox_name")) {
                entityName = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityName.setValue(entity.getName());
            } else if (comp.getId().equals("batch_textbox_desp")) {
                entityDesp = (Textbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                entityDesp.setValue(entity.getDescription());
            } else if (comp.getId().equalsIgnoreCase("batch_switchbox_batchUpdate")) {
                batchUpdate = (Checkbox) comp;
                batchUpdate.addEventListener(Events.ON_CHECK, this);
                batchUpdate.setChecked(entity.isBatchUpdate());
            } else if (comp.getId().equals("batch_bandbox_dsId")) {
                dsId = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                dsId.setValue(entity.getSourceDsId());
            } else if (comp.getId().equals("batch_bandbox_targetTable")) {
                targetTable = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                targetTable.setValue(entity.getTargetTable());
            } else if (comp.getId().equals("batch_bandbox_nextId")) {
                nextId = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.addEventListener(Events.ON_OK, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                nextId.setValue(entity.getNextId());
            } else if (comp.getId().equals("batch_combobox_processCode")) {
                processCode = (Combobox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                if (Strings.isBlank(entity.getProcessCode()))
                    processCode.setSelectedIndex(0);
                else if (entity.getProcessCode().equals("C"))
                    processCode.setSelectedIndex(1);
                else if (entity.getProcessCode().equals("U"))
                    processCode.setSelectedIndex(2);
                else
                    processCode.setSelectedIndex(3);
            } else if (comp.getId().equals("batch_combobox_transactionScope")) {
                transactionScope = (Combobox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                transactionScope
                        .setSelectedIndex(entity.getTransactionScope() > 0 ? entity
                                .getTransactionScope() - 1 : 1);
            } else if (comp.getId().equals("batch_bandbox_onRecord")) {
                onRecord = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnRecord() != null) {
                    if (!Strings.isBlank(entity.getOnRecord().getId())) {
                        onRecord.setValue(entity.getOnRecord().getId());
                    } else if (!Strings.isBlank(entity.getOnRecord()
                            .getContent())) {
                        ((Button) onRecord.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("batch_bandbox_onBegin")) {
                onBegin = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnBegin() != null) {
                    if (!Strings.isBlank(entity.getOnBegin().getId())) {
                        onBegin.setValue(entity.getOnBegin().getId());
                    } else if (!Strings.isBlank(entity.getOnBegin()
                            .getContent())) {
                        ((Button) onBegin.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("batch_bandbox_onEnd")) {
                onEnd = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnEnd() != null) {
                    if (!Strings.isBlank(entity.getOnEnd().getId())) {
                        onEnd.setValue(entity.getOnEnd().getId());
                    } else if (!Strings.isBlank(entity.getOnEnd().getContent())) {
                        ((Button) onEnd.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("batch_bandbox_onException")) {
                onException = (Bandbox) comp;
                comp.addEventListener(Events.ON_CHANGE, this);
                comp.addEventListener(Events.ON_OPEN, this);
                comp.getNextSibling().addEventListener(Events.ON_CLICK, this);
                if (entity.getOnException() != null) {
                    if (!Strings.isBlank(entity.getOnException().getId())) {
                        onException.setValue(entity.getOnException().getId());
                    } else if (!Strings.isBlank(entity.getOnException()
                            .getContent())) {
                        ((Button) onException.getNextSibling())
                                .setSclass("epeditor-btn-mark");
                    }
                }
            } else if (comp.getId().equals("batch_cmeditor_source")) {
                source = (CMeditor) comp;
                source.setTheme(Contexts.getUser().getEditorTheme());
                source.setValue(entity.getSourceSql());
                source.addEventListener(Events.ON_CHANGING, this);
            }
        }
        workbench.addEditor(tab, tabpanel);
    }

    @Override
    protected boolean validate() {
        if (Strings.isBlank(entity.getName())) {
            WebUtils.notEmpty(Labels.getLabel("entity.name"));
            return false;
        }
        if (Strings.isBlank(entity.getSourceSql())) {
            WebUtils.notEmpty(Labels.getLabel("entity.report.content"));
            return false;
        }
        if (entity.getOnRecord() != null) {
            boolean notId = Strings.isBlank(entity.getOnRecord().getId());
            if (notId && Strings.isBlank(entity.getOnRecord().getContent()))
                entity.setOnRecord(null);
            else if (!notId)
                entity.getOnRecord().setContent(null);
        }
        if (entity.getOnBegin() != null) {
            boolean notId = Strings.isBlank(entity.getOnBegin().getId());
            if (notId && Strings.isBlank(entity.getOnBegin().getContent()))
                entity.setOnBegin(null);
            else if (!notId)
                entity.getOnBegin().setContent(null);
        }
        if (entity.getOnRecord() != null) {
            boolean notId = Strings.isBlank(entity.getOnRecord().getId());
            if (notId && Strings.isBlank(entity.getOnRecord().getContent()))
                entity.setOnRecord(null);
            else if (!notId)
                entity.getOnRecord().setContent(null);
        }
        if (entity.getOnEnd() != null) {
            boolean notId = Strings.isBlank(entity.getOnEnd().getId());
            if (notId && Strings.isBlank(entity.getOnEnd().getContent()))
                entity.setOnEnd(null);
            else if (!notId)
                entity.getOnEnd().setContent(null);
        }
        if (entity.getOnException() != null) {
            boolean notId = Strings.isBlank(entity.getOnException().getId());
            if (notId && Strings.isBlank(entity.getOnException().getContent()))
                entity.setOnException(null);
            else if (!notId)
                entity.getOnException().setContent(null);
        }
        return true;
    }

    @Override
    protected void dispatch(Event evt) {
        if (Events.ON_CHANGE.equals(evt.getName())
                || Events.ON_CHANGING.equals(evt.getName()) || Events.ON_CHECK.equals(evt.getName())) {
            Component c = evt.getTarget();
            if (c == entityName)
                entity.setName(entityName.getValue());
            else if (c == entityDesp)
                entity.setDescription(entityDesp.getValue());
            else if (c == dsId)
                entity.setSourceDsId(dsId.getValue());
            else if (c == source)
                entity.setSourceSql(source.getValue().toString());
            else if (c == targetTable)
                entity.setTargetTable(targetTable.getValue());
            else if (c == nextId)
                entity.setNextId(nextId.getValue());
            else if (c == processCode) {
                if (processCode.getSelectedItem() != null)
                    entity.setProcessCode((String) processCode.getSelectedItem().getValue());
            }
            else if (c == transactionScope)
                entity.setTransactionScope(transactionScope.getSelectedIndex() + 1);
            else if (c == batchUpdate)
                entity.setBatchUpdate(batchUpdate.isChecked());
            setDirty();
        } else {
            Component c = evt.getTarget();
            if (evt.getName().equals(Events.ON_OPEN) ||evt instanceof KeyEvent) {// Bandbox
                if (c == dsId) {
                    AbstractView.createDatasourceView(
                            new BandboxCallback(dsId, Labels
                                    .getLabel("entity.table.db")), evt.getTarget()).doOverlapped();
                } else if (c == targetTable) {
                    if (evt instanceof OpenEvent){
                        ((Bandbox) c).setValue(((OpenEvent)evt).getValue()+"");
                    }
                    AbstractView.createEntityView(
                            new BandboxCallback(this, (Bandbox) c),
                            EntityType.TABLE.getName(), c).doOverlapped();
                } else if (c == nextId) {
                    if (evt instanceof OpenEvent){
                        ((Bandbox) c).setValue(((OpenEvent)evt).getValue()+"");
                    }
                    AbstractView.createEntityView(
                            new BandboxCallback(this, (Bandbox) c),
                            EntityType.BATCH.getName(), c).doOverlapped();
                } else {
                    AbstractView.createEntityView(
                            new BandboxCallback(this, (Bandbox) c),
                            EntityType.LOGIC.getName(), c).doOverlapped();
                }
            } else {
                c = c.getPreviousSibling();
                if (c == targetTable) {
                    if (!Strings.isBlank(targetTable.getValue()))
                        openEditor(evt.getTarget(), targetTable.getValue());
                } else if (c == nextId) {
                    if (!Strings.isBlank(nextId.getValue()))
                        openEditor(evt.getTarget(), nextId.getValue());
                } else if (c == onRecord) {
                    if (Strings.isBlank(onRecord.getValue())) {
                        if (entity.getOnRecord() == null)
                            entity.setOnRecord(new EventLogic());
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnRecord(), entity.getTargetTable(),
                                        "OnRecord"), evt
                                        .getTarget()).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onRecord.getValue());
                    }
                } else if (c == onBegin) {
                    if (Strings.isBlank(onBegin.getValue())) {
                        if (entity.getOnBegin() == null)
                            entity.setOnBegin(new EventLogic());
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnBegin(), entity.getTargetTable(),
                                        "onBegin"), evt
                                        .getTarget()).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onBegin.getValue());
                    }
                } else if (c == onEnd) {
                    if (Strings.isBlank(onEnd.getValue())) {
                        if (entity.getOnEnd() == null)
                            entity.setOnEnd(new EventLogic());
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnEnd(), entity.getTargetTable(),
                                        "onEnd"), evt
                                        .getTarget()).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onEnd.getValue());
                    }
                } else if (c == onException) {
                    if (Strings.isBlank(onException.getValue())) {
                        if (entity.getOnException() == null)
                            entity.setOnException(new EventLogic());
                        AbstractView.createLogicView(
                                new EventLogicCallback(this, (Button) evt
                                        .getTarget(), entity.getOnException(), entity.getTargetTable(),
                                        "onException"), evt
                                        .getTarget()).doOverlapped();
                    } else {
                        openEditor(evt.getTarget(), onException.getValue());
                    }
                }
            }
        }
    }

    /**
     * 重绘源码
     */
    protected void refreshSource() {
    }
}
