package cn.easyplatform.studio.web.editors.page;


import cn.easyplatform.web.ext.zul.Datalist;
import cn.easyplatform.web.ext.zul.JReport;
import org.zkoss.pivot.Pivottable;
import org.zkoss.zk.ui.Component;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
abstract class ComponentBuilder<T extends Component> {

    protected abstract void build(T comp);

    static <T extends Component> void create(T comp) {
        if (comp instanceof Datalist)
            new DataListBuilder().build((Datalist) comp);
        else if (comp instanceof JReport)
            new ReportBuilder().build((JReport) comp);
        else if (comp instanceof Pivottable)
            new PivottableBuilder().build((Pivottable) comp);
    }
}
