package cn.easyplatform.studio.web.editors.project;

import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.importAll.SaveMessageInfoCmd;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.utils.ZipUtil;
import cn.easyplatform.studio.vos.IXVo;
import cn.easyplatform.studio.vos.MessageVo;
import cn.easyplatform.studio.web.editors.AbstractPanelEditor;
import cn.easyplatform.studio.web.layout.WorkbenchController;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.*;

import java.util.List;

public class LabelImportEditor extends AbstractPanelEditor {
    protected Label project;

    protected Label operator;

    protected Label date;

    protected Label size;

    protected Label comment;

    protected Listbox sources;

    protected Button commit;

    protected IXVo entity;
    public LabelImportEditor(WorkbenchController workbench, String id) {
        super(workbench, id);
    }

    @Override
    protected void dispatch(Event evt) {
        if (evt.getName().equals(Events.ON_UPLOAD)) {
            UploadEvent ue = (UploadEvent) evt;
            IXVo vo = (IXVo) ZipUtil.unpackZip(ue.getMedia().getByteData());
            redraw(vo);
        } else if (evt.getName().equals(Events.ON_CLICK)) {
            StudioApp.execute(new SaveMessageInfoCmd(entity.getMessageVos()));
            if (StudioApp.execute(new SaveMessageInfoCmd(entity.getMessageVos())) == true)
                WebUtils.showSuccess(Labels.getLabel("export.success"));
        }
    }

    @Override
    public void create(Object... args) {
        Idspace is = createPanel(Labels.getLabel("menu.repo.import.label"), "~./images/web.gif",
                "~./include/editor/project/labelImport.zul");
        this.project = (Label) is.getFellow("labelImport_label_project");
        this.operator = (Label) is.getFellow("labelImport_label_operator");
        this.date = (Label) is.getFellow("labelImport_label_date");
        this.comment = (Label) is.getFellow("labelImport_label_comment");
        this.size = (Label) is.getFellow("labelImport_label_size");
        this.sources = (Listbox) is.getFellow("labelImport_listbox_sources");
        this.commit = (Button) is.getFellow("labelImport_button_commit");
        this.commit.addEventListener(Events.ON_CLICK, this);
        is.getFellow("labelImport_button_upload").addEventListener(Events.ON_UPLOAD, this);
    }

    private void redraw(IXVo vo) {
        entity = vo;
        project.setValue(vo.getProjectId());
        operator.setValue(vo.getUser());
        date.setValue(DateFormatUtils.format(vo.getDate(), "yyyy-MM-dd hh:mm:ss"));
        size.setValue((vo.getMessageVos() != null ? vo.getMessageVos().size(): "0") + "");
        comment.setValue(vo.getDesp());
        redrawTree(vo.getMessageVos());
        commit.setDisabled(false);
    }

    protected void redrawTree(List<MessageVo> vos) {
        sources.getItems().clear();
        if (vos != null) {
            for (MessageVo vo: vos) {
                Listitem row = new Listitem();
                row.appendChild(new Listcell(vo.getCode()));
                row.appendChild(new Listcell(vo.getZh_cn()));
                row.appendChild(new Listcell(vo.getMSG_TYPE()));
                row.appendChild(new Listcell(vo.getStatus()));
                row.setValue(vo);
                sources.appendChild(row);
            }
        }
    }
}
