/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.editors.support;

import java.util.Map;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ChildDelegate<T> {

    void onChild(T child, Map<String, Object> params);
}
