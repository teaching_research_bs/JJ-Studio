/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.controller;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.ChangePasswordCmd;
import cn.easyplatform.studio.utils.WebUtils;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Captcha;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;


/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ChangePasswordController extends SelectorComposer<Window> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @Wire("textbox#oldPass")
    private Textbox oldPass;

    @Wire("textbox#newPass")
    private Textbox newPass;

    @Wire("textbox#newPassConfirm")
    private Textbox newPassConfirm;

    @Wire("textbox#captchabox")
    private Textbox captchabox;

    @Wire("captcha#captcha")
    private Captcha captcha;

    @Wire("button#submit")
    private Button submit;

    public void doAfterCompose(Window win) throws Exception {
        super.doAfterCompose(win);
        oldPass.setWidgetListener(Events.ON_OK,
                "if(jq(zk.$('$" +oldPass.getId() +"')).val().length!=0){jq(zk.$('$" +newPass.getId() +"')).focus();}");
        newPass.setWidgetListener(
                Events.ON_OK,
                "if(jq(zk.$('$" +newPass.getId() +"')).val().length>5){jq(zk.$('$" +newPassConfirm.getId() +"')).focus();}");
        newPassConfirm.setWidgetListener(Events.ON_OK,
                "if(jq(zk.$('$" +newPassConfirm.getId() +"')).val().length>5){jq(zk.$('$" +captchabox.getId() +"')).focus();}");
        captchabox.setWidgetListener(
                Events.ON_OK,
                "if(jq(zk.$('$" +captchabox.getId() +"')).val().length==5){zk.$('$" +submit.getId() +"').fire('onClick');}");
        win.doHighlighted();
    }

    @Listen("onClick=#submit")
    public void submit() {
        if (Strings.isBlank(oldPass.getValue())) {
            oldPass.setErrorMessage(Labels
                    .getLabel("user.old.password.placeholder"));
            oldPass.setFocus(true);
            return;
        }
        if (Strings.isBlank(newPass.getValue())) {
            newPass.setErrorMessage(Labels
                    .getLabel("user.new.password.placeholder"));
            newPass.setFocus(true);
            return;
        }
        if (newPass.getValue().trim().length() < 6) {
            newPass.setErrorMessage(Labels.getLabel("user.password.length"));
            newPass.setFocus(true);
            return;
        }
        if (Strings.isBlank(newPassConfirm.getValue())) {
            newPassConfirm.setErrorMessage(Labels
                    .getLabel("user.new.password.confirm.placeholder"));
            newPassConfirm.setFocus(true);
            return;
        }
        if (!newPass.getValue().equals(newPassConfirm.getValue())) {
            newPassConfirm.setErrorMessage(Labels
                    .getLabel("user.new.password.match"));
            newPassConfirm.setFocus(true);
            return;
        }
        if (Strings.isBlank(captchabox.getValue())) {
            captchabox.setErrorMessage(Labels
                    .getLabel("user.captcha.placeholder"));
            captchabox.setFocus(true);
            return;
        }
        if (!captchabox.getValue().toLowerCase()
                .equals(captcha.getValue().toLowerCase())) {
            captchabox.setErrorMessage(Labels.getLabel("user.captcha.match"));
            captchabox.setFocus(true);
            return;
        }
        String msg = StudioApp.execute(submit, new ChangePasswordCmd(oldPass.getValue(), newPass.getValue()));
        if (msg != null)
            WebUtils.showError(Labels.getLabel(msg));
        else {
            submit.getRoot().detach();
            WebUtils.showSuccess(Labels.getLabel("user.password.success"));
        }
    }
}
