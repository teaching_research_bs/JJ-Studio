package cn.easyplatform.studio.web.controller;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.cmd.identity.*;
import cn.easyplatform.studio.cmd.link.GetAllLinkCmd;
import cn.easyplatform.studio.cmd.taskLink.GetAllTaskLinkCmd;
import cn.easyplatform.studio.cmd.version.InitCmd;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.utils.WebUtils;
import cn.easyplatform.studio.vos.InviteVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.web.controller.admin.LoginLogListener;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.metainfo.ComponentInfo;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zk.ui.util.ComposerExt;
import org.zkoss.zul.Include;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;
import org.zkoss.zul.theme.Themes;



public class InviteController implements Composer<Component>, ComposerExt<Component>, EventListener<Event> {
    private Component parentComponent;

    private InviteVo inviteVo;
    @Override
    public void onEvent(Event event) throws Exception {
        if ("invite_button_add".equals(event.getTarget().getId())) {
            //加入项目
            Boolean isSuccess = StudioApp.execute(new InviteProjectCmd(Contexts.getUser().getUserId(), inviteVo.getProjectID(),
                    inviteVo.getRoleID()));
            String roleId = inviteVo.getRoleID();
            if (isSuccess && Strings.isBlank(roleId) == false) {
                changeProject(inviteVo.getProjectID(), roleId, parentComponent);
            }
        } else if ("invite_window_content".equals(event.getTarget().getId())) {
            WebUtils.showConfirm(Labels.getLabel("product.invite.exit"), new EventListener<Event>() {
                @Override
                public void onEvent(Event event) throws Exception {
                    if (event.getName().equals(Events.ON_OK)) {
                        Contexts.setInvite(null);
                        Include include = (Include) parentComponent.getRoot();
                        include.setSrc("~./include/login/login1.zul");
                    }
                }
            });
        }
    }

    @Override
    public void doAfterCompose(Component component) throws Exception {
        parentComponent = component;

        Window window = (Window) component.getChildren().get(0);
        inviteVo = Contexts.getInvite();
        Contexts.setInvite(null);
        LoginUserVo userVo = StudioApp.execute(new GetStudioUserCmd(inviteVo.getUserID()));
        ProjectBean projectBean = StudioApp.execute(new GetStudioProjectCmd(inviteVo.getProjectID()));
        Label titleLabel = (Label) window.getFellow("invite_label_title");
        if (inviteVo != null) {
            titleLabel.setValue(Labels.getLabel("product.invite.title",
                    new Object[] {" "+userVo.getName()+" ", " "+projectBean.getName()+" "}));
        }
        window.getFellow("invite_button_add").addEventListener(Events.ON_CLICK, this);
        window.getFellow("invite_window_content").addEventListener(Events.ON_CLOSE, this);
    }

    @Override
    public ComponentInfo doBeforeCompose(Page page, Component component, ComponentInfo componentInfo) throws Exception {
        return componentInfo;
    }

    @Override
    public void doBeforeComposeChildren(Component component) throws Exception {

    }

    @Override
    public boolean doCatch(Throwable throwable) throws Exception {
        return false;
    }

    @Override
    public void doFinally() throws Exception {

    }

    public void changeProject(String projectId, String roleId, Component com) {
        //设置项目
        StudioApp.execute(new InviteLoginCmd(Contexts.getUser().getUserId(), projectId,
                roleId.split(",")[0]));
        Contexts.setInvite(null);
        //记录用户登入
        LoginUserVo user = Contexts.getUser();
        user.setRoleId(roleId);
        Contexts.setUser(user);
        LoginLogListener.loginLog();
        //数据库验证
        try {
            Boolean success = StudioApp.execute(new VersionUpdateCmd());
        } catch (Exception ex) {
            StudioApp.execute(com, new LogoutCmd(true));
            WebUtils.showError(ex.getMessage());
            return;
        }
        //获取关系表
        if (GlobalVariableService.getInstance().getDataLinkVoList().size() == 0) {
            StudioApp.execute(new GetAllLinkCmd(Contexts.getProject().getId()));
            StudioApp.execute(new GetAllTaskLinkCmd(Contexts.getProject().getId()));
        }
        try {
            StudioApp.execute(new InitCmd());
        } catch (Exception ex) {
        }
        if (!Strings.isBlank(user.getTheme()))
            Themes.setTheme(Executions.getCurrent(), user.getTheme());
        Executions.sendRedirect("");
    }
}
