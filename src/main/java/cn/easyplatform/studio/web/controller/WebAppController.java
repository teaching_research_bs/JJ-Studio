/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.web.controller;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.StudioApp;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.vos.InviteVo;
import cn.easyplatform.studio.vos.LoginUserVo;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Composer;
import org.zkoss.zul.Include;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebAppController implements Composer<Include> {
    /*static {
        StudioApp.execute(new GetAllLinkCmd());
        StudioApp.execute(new GetAllTaskLinkCmd());
        //StudioApp.execute(new InitVersionCmd());
    }*/
    @Override
    public void doAfterCompose(Include comp) throws Exception {
        comp.getPage().setTitle(StudioApp.getAppName());
        LoginUserVo user = Contexts.getUser();
        ProjectBean pb = Contexts.getProject();
        InviteVo inviteVo = Contexts.getInvite();
        Boolean isIntoProject = Contexts.getIntoProjectCenter();
        //没有登陆打开邀请页面进入注册页面
        //已登陆进入邀请页面
        if (user == null) {
            if (inviteVo != null) {
                comp.setSrc("~./include/register/register1.zul");
            } else
                comp.setSrc("~./include/login/login1.zul");
        }
        else {
            if (LoginUserVo.UserType.ADMIN.getName().equals(Contexts.getUser().getRoleId()))
                comp.setSrc("~./include/admin/main.zul");
            else if (inviteVo != null) {
                comp.setSrc("~./include/invite/invite.zul");
            } else if (isIntoProject != null && isIntoProject == true) {
                comp.setSrc("~./include/normal/product.zul");
                Contexts.setIntoProjectCenter(null);
            } else if (pb == null){
                comp.setSrc("~./include/normal/product.zul");
            } else {
                comp.setSrc("~./include/normal/main.zul");
            }
            String userAgent = Executions.getCurrent().getUserAgent();
            if (userAgent.indexOf("Mac OS X") > 0) {
                comp.setCtrlKeys("^1^2^3^4^5^6^w^q");
            } else {
                comp.setCtrlKeys("@1@2@3@4@5@6@w@q");
            }
        }
    }

}
