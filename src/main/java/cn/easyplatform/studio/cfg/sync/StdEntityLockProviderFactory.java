/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg.sync;

import cn.easyplatform.studio.cfg.EntityLockProvider;
import cn.easyplatform.studio.cfg.EntityLockProviderFactory;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StdEntityLockProviderFactory implements EntityLockProviderFactory{

	public EntityLockProvider getEntityLockProvider(CommandContext cc) {
		return new StdEntityLockProvider(cc);
	}

}
