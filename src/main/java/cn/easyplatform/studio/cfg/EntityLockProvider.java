/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cfg;


/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface EntityLockProvider {


	/**
	 * 锁住指定的参数
	 * @param projectId
	 * @param entityId
	 */
	String lock(String projectId, String entityId);

	/**
	 * 解锁
	 * @param projectId
	 * @param entityId
	 */
	void unlock(String projectId, String entityId);

	/**
	 * 清空用户锁记录
	 * 
	 */
	void reset();
}
