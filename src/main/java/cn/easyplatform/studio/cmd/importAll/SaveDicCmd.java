package cn.easyplatform.studio.cmd.importAll;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.DaoException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.DicDetailVo;
import cn.easyplatform.studio.vos.DicVo;

import java.util.ArrayList;
import java.util.List;

public class SaveDicCmd implements Command<Boolean> {
    private List<DicVo> voList;
    private List<DicVo> updateDicVos = new ArrayList<>();
    private List<DicVo> addDicVos = new ArrayList<>();
    private List<DicDetailVo> updateDicDetailVos = new ArrayList<>();
    private List<DicDetailVo> addDicDetailVos = new ArrayList<>();
    public SaveDicCmd(List<DicVo> voList) {
        this.voList = voList;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        List<DicVo> list = cc.getDicDao(null).selectAllList();
        List<DicDetailVo> detailList = cc.getDicDetailDao(null).selectAll();
        List<String> dicKeyList = new ArrayList<>();
        List<String> dicDetailKeyList = new ArrayList<>();
        for (DicVo vo: list) {
            dicKeyList.add(vo.getDicCode());
        }
        for (DicDetailVo detailVo: detailList) {
            String keyString = detailVo.getDicCode() + "DETAILNO" + detailVo.getDetailNO();
            dicDetailKeyList.add(keyString);
        }
        parseData(voList, dicKeyList, dicDetailKeyList);

        try {
            cc.beginTx();
            cc.getDicDao(null).setDicList(addDicVos);
            cc.getDicDao(null).updateDicList(updateDicVos);
            cc.getDicDetailDao(null).setDicDetailList(addDicDetailVos);
            cc.getDicDetailDao(null).updateDicDetailList(updateDicDetailVos);
            cc.commitTx();
        } catch (Exception ex) {
            cc.rollbackTx();
            if (ex instanceof DaoException)
                throw (DaoException) ex;
            throw new StudioException("ExportCmd:" + ex.getMessage());
        } finally {
            cc.closeTx();
        }
        return Boolean.TRUE;
    }

    private void parseData(List<DicVo> vos, List<String> allDicVoList, List<String> allDicDetailVoList) {
        for (DicVo vo: vos) {
            if (allDicVoList.contains(vo.getDicCode()))
                updateDicVos.add(vo);
            else
                addDicVos.add(vo);

            if (vo.getDicDetailVoList() != null && vo.getDicDetailVoList().size() > 0) {
                for (DicDetailVo dicDetailVo: vo.getDicDetailVoList()) {
                    String keyString = dicDetailVo.getDicCode() + "DETAILNO" + dicDetailVo.getDetailNO();
                    if (allDicDetailVoList.contains(keyString))
                        updateDicDetailVos.add(dicDetailVo);
                    else
                        addDicDetailVos.add(dicDetailVo);
                }
            }

            if (vo.getChildrenList() != null && vo.getChildrenList().size() > 0)
                parseData(vo.getChildrenList(), allDicVoList, allDicDetailVoList);
        }
    }
}
