package cn.easyplatform.studio.cmd.module;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

public class BackupCmd implements Command<Boolean> {
    private String dir;

    public BackupCmd(String dir) {
        this.dir = dir;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        cc.getModule().backup(dir, cc.getProject().getId());
        return true;
    }
}
