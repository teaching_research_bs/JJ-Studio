/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectProjectCmd implements Command<Boolean> {

	private ProjectBean project;

	public SelectProjectCmd(ProjectBean project) {
		this.project = project;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		Contexts.setProject(project);

		return true;
	}

}
