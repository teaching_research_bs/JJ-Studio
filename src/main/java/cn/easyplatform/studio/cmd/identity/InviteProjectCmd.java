package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

public class InviteProjectCmd implements Command<Boolean> {

    private String userID;
    private String projectID;
    private String roleID;
    public InviteProjectCmd(String userID, String projectID, String roleID) {
        this.userID = userID;
        this.projectID = projectID;
        this.roleID = roleID;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        return cc.getIdentityDao().addProject(userID, projectID, roleID);
    }
}
