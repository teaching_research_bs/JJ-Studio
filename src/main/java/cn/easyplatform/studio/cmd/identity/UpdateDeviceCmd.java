/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.context.Contexts;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @Author Zeta
 * @Version 1.0
 */

public class UpdateDeviceCmd implements Command<Boolean> {

    private long id;

    private String xml;

    public UpdateDeviceCmd(long id ,String xml) {

        this.xml = xml;
        this.id = id;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        cc.getIdentityDao().updateDevice(id,Contexts.getProject().getId(),xml);
        return true;
    }

}
