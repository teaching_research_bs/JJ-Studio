/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.RoleVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetStudioRoleslikeCmd implements Command<List<RoleVo>> {

    private String value;

    public GetStudioRoleslikeCmd(String value) {
        this.value = value;
    }


    @Override
    public List<RoleVo> execute(CommandContext cc) {
        FieldVo params = new FieldVo(FieldType.VARCHAR, cc.getProject().getId());
        StringBuilder sb = new StringBuilder("select roleId,name,desp,permissions from ep_role_info").append(" where 1=1").append(" and projectId=?");
        sb.append(" and (roleId like ").append("'%" + value + "%'")
                .append(" or name like ").append("'%" + value + "%'")
                .append(" or desp like ").append("'%" + value + "%')").append(" order by roleId");
        List<Object[]> result = cc
                .getIdentityDao()
                .selectList(sb.toString(), params);
        List<RoleVo> roles = new ArrayList<RoleVo>(result.size());
        for (Object[] objs : result) {
            RoleVo vo = new RoleVo();
            vo.setId((String) objs[0]);
            vo.setName((String) objs[1]);
            vo.setDesp((String) objs[2]);
            //image作为授权使用
            vo.setImage((String) objs[3]);
            roles.add(vo);
        }
        return roles;

    }
}
