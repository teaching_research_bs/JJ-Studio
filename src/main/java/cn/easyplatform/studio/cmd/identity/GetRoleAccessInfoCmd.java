package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.RoleAccessVo;

import java.util.List;

public class GetRoleAccessInfoCmd implements Command<List<RoleAccessVo>> {
    private String roleId;

    public GetRoleAccessInfoCmd(String roleId) {
        this.roleId = roleId;
    }

    @Override
    public List<RoleAccessVo> execute(CommandContext cc) {
        return cc.getBizDao().getRoleAccess(roleId);
    }
}
