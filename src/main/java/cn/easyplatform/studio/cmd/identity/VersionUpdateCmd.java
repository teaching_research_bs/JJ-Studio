package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.ArrayList;
import java.util.List;

public class VersionUpdateCmd implements Command<Boolean> {
    @Override
    public Boolean execute(CommandContext cc) {
        //第2.1个版本
        if (cc.getEntityDao().exists(cc.getProject()
                .getEntityTableName() + "_link_info")) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createSecondOneVersion(cc.getProject().getEntityTableName(),
                        cc.getProject().getId(), cc.getEntityDao().exists("ep_mobile_conf_info"),
                        cc.getEntityDao().exists("ep_guide_info"));
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }
        //第2.2版本
        if (cc.getBizDao().exists("sys_guide_info") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createSecondSecondVersion(cc.getBizDao().getDataSource(),
                        cc.getBizDao().exists("sys_guide_info"));
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }
        //第2.3版本
        if (cc.getEntityDao().exists(cc.getProject()
                .getEntityTableName() + "_module") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createSecondThirdVersion(cc.getProject().getEntityTableName(),
                        cc.getEntityDao().exists(cc.getProject().getEntityTableName() + "_module"));
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }
        //第2.7版本
        List<String> list = cc.getEntityDao().getColumnNameList(cc.getProject()
                .getEntityTableName() + "_module");
        if (list.size() == 12) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createSecondSevenVersion(cc.getBizDao().getDataSource(),
                        cc.getProject().getEntityTableName(), cc.getUser().getUserId());
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第2.17版本
        /*List<String> result = cc.getBizDao().getColumnNameList("sys_role_info");
        if (result != null && result.size() > 0) {
            boolean isContainMobile = result.contains("tid");
            if (isContainMobile == false) {
                try {
                    cc.beginTx();
                    cc.getDatabaseUpdateDao().createSecondSeventeenVersion(cc.getBizDao().getDataSource());
                    cc.commitTx();
                } catch (Exception ex) {
                    cc.rollbackTx();
                    throw (RuntimeException) ex;
                } finally {
                    cc.closeTx();
                }
            }
        }*/

        //第2.18版本
        if (cc.getEntityDao().exists(cc.getProject()
                .getEntityTableName() + "_login_log") == false){
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createSecondEighteenVersion(cc.getProject().getEntityTableName()+ "_login_log");
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.0版本
        List<Object[]> res = cc.getBizDao().selectList(
                "SELECT * FROM sys_table_field_info WHERE table1id='sys_message_info'");
        if (res == null || res.size() == 0) {
            try {
                cc.beginTx();
                TableBean bean = cc.getEntityDao().getEntity(cc.getProject().getEntityTableName(), "sys_message_info");
                List<String> fieldNameList = new ArrayList<>();
                List<String> fieldTypeList = new ArrayList<>();
                List<String> fieldLengthList = new ArrayList<>();
                List<String> fieldDesList = new ArrayList<>();
                List<String> fieldNotNUllList = new ArrayList<>();
                for (TableField field : bean.getFields()) {
                    fieldNameList.add(field.getName());
                    fieldTypeList.add(field.getType().toString());
                    fieldLengthList.add(Integer.toString(field.getLength()));
                    fieldDesList.add(field.getDescription());
                    fieldNotNUllList.add(Boolean.toString(field.isNotNull()));
                }
                cc.getDatabaseUpdateDao().createThreeVersion(cc.getBizDao().getDataSource(), fieldNameList, fieldTypeList,
                        fieldLengthList, fieldDesList, fieldNotNUllList);
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.1版本
        List<String> detailSize = cc.getEntityDao().getColumnNameList("ep_import_detail_info");
        if (detailSize.size() == 2) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createThreeOneVersion();
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.2版本
        List<String> fieldList = cc.getBizDao().getColumnNameList("sys_message_info");
        List<Object[]> dbList = cc.getBizDao().selectList(
                "select field1name from sys_table_field_info where table1id = 'sys_message_info' order by (field1orderNo+0) asc");
        boolean isEqual = false;
        if (fieldList == null || dbList == null || fieldList.size() != dbList.size()) {
            isEqual = false;
        } else {
            for (int index = 0; index < fieldList.size(); index++) {
                boolean isNoNull = Strings.isBlank(fieldList.get(index)) != false &&
                        Strings.isBlank((String) dbList.get(index)[0]) != false;
                if (isNoNull && fieldList.get(index).equals((String) dbList.get(index)[0])) {
                    isEqual = true;
                } else {
                    isEqual = false;
                    break;
                }
            }
        }
        if (isEqual == false) {
            try {
                cc.beginTx();
                TableBean bean = cc.getEntityDao().getEntity(cc.getProject().getEntityTableName(), "sys_message_info");
                List<String> fieldNameList = new ArrayList<>();
                List<String> fieldTypeList = new ArrayList<>();
                List<String> fieldLengthList = new ArrayList<>();
                List<String> fieldDesList = new ArrayList<>();
                List<String> fieldNotNUllList = new ArrayList<>();
                for (TableField field : bean.getFields()) {
                    fieldNameList.add(field.getName());
                    fieldTypeList.add(field.getType().toString());
                    fieldLengthList.add(Integer.toString(field.getLength()));
                    fieldDesList.add(field.getDescription());
                    fieldNotNUllList.add(Boolean.toString(field.isNotNull()));
                }
                cc.getDatabaseUpdateDao().createThreeTwoVersion(cc.getBizDao().getDataSource(), fieldNameList, fieldTypeList,
                        fieldLengthList, fieldDesList, fieldNotNUllList);
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.3版本
        List<String> roleList = cc.getBizDao().getColumnNameList("sys_role_info");
        List<String> roleMenuList = cc.getBizDao().getColumnNameList("sys_role_menu_info");
        if (roleList != null && roleList.size() > 0 && roleList.contains("type") && roleMenuList != null
                && roleMenuList.size() > 0 && roleMenuList.contains("type") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createThreeThreeVersion(cc.getBizDao().getDataSource());
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.4版本
        if (cc.getEntityDao().exists( "ep_model_ext_info") == false){
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createThreeFourVersion();
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.5版本
        String tableString = cc.getProject().getEntityTableName();
        List<String> columnNameList = cc.getEntityDao().getColumnNameList(tableString);
        if (columnNameList != null && columnNameList.size() > 0 && columnNameList.contains("ext0") == false &&
                columnNameList.contains("ext1") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createThreeFiveVersion(tableString);
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.6版本
        if (cc.getBizDao().exists("sys_role_access_info") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createThreeSixVersion(cc.getBizDao().getDataSource());
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第3.7版本
        if (cc.getBizDao().exists("sys_access_info") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createThreeSevenVersion(cc.getBizDao().getDataSource());
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }

        //第4.1版本
        if (cc.getEntityDao().exists("ep_widget_info") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createFourOneVersion();
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }
        //第4.2版本
        String db = cc.getProject().getEntityTableName()+"_widget_info";
        if (cc.getEntityDao().exists(db) == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createFourTwoVersion(db);
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }
        //第4.3版本
        if (cc.getEntityDao().exists("ep_config_info") == false) {
            try {
                cc.beginTx();
                cc.getDatabaseUpdateDao().createFourThreeVersion();
                cc.commitTx();
            } catch (Exception ex) {
                cc.rollbackTx();
                throw (RuntimeException) ex;
            } finally {
                cc.closeTx();
            }
        }
        return Boolean.TRUE;
    }

}
