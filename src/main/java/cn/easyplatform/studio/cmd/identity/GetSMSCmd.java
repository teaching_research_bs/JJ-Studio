package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.utils.HttpUtil;
import cn.easyplatform.studio.utils.StringUtil;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.SMSVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.zkoss.util.resource.Labels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetSMSCmd implements Command<Boolean> {
    private String mobile;
    private SMSVo.SMSType smsType;

    public GetSMSCmd(String mobile, SMSVo.SMSType smsType){
        this.mobile = mobile;
        this.smsType = smsType;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (Strings.isBlank(mobile) == false) {
            //判断用户存在
            LoginUserVo userVo = cc.getIdentityDao().getUser(mobile);
            if (smsType == SMSVo.SMSType.SMSRegister) {
                if (userVo != null)
                    throw new StudioException(Labels.getLabel("register.mobile.register"));
            } else if (smsType == SMSVo.SMSType.SMSRetrieve) {
                if (userVo == null)
                    throw new StudioException(Labels.getLabel("forget.mobile.no.exist"));
            }
            //获取手机号获取验证码信息
            List<SMSVo> smsVoList = GlobalVariableService.getOneInstance().getSmsVos();
            if (smsVoList == null)
                smsVoList = new ArrayList<>();
            SMSVo currentSMSVo = null;
            long diff = 60000;
            for (SMSVo smsVo : smsVoList) {
                boolean isEqualType = false;
                if (smsType == SMSVo.SMSType.SMSRegister) {
                    isEqualType = SMSVo.SMSType.SMSRegister.getName().equals(smsVo.getType());
                } else {
                    isEqualType = SMSVo.SMSType.SMSRetrieve.getName().equals(smsVo.getType());
                }
                if (mobile.equals(smsVo.getPhoneNO()) && isEqualType) {
                    currentSMSVo = smsVo;
                    diff = new Date().getTime() -  currentSMSVo.getCreateDate().getTime();
                }
            }
            if (currentSMSVo == null) {
                currentSMSVo = new SMSVo();
                currentSMSVo.setPhoneNO(mobile);
                if (smsType == SMSVo.SMSType.SMSRegister) {
                    currentSMSVo.setType(SMSVo.SMSType.SMSRegister.getName());
                } else {
                    currentSMSVo.setType(SMSVo.SMSType.SMSRetrieve.getName());
                }
                smsVoList.add(currentSMSVo);
            }
            //判断创建时间间隔
            double second = diff / (1000.0 * 60.0);
            if (second > 0.9) {
                final String random = StringUtil.random();
                JSONObject json = new JSONObject();
                json.put("uid", "2734");
                json.put("pwd", "bhc12345");
                json.put("mobile", mobile);
                json.put("content", "【吉鼎科技】验证码："+random+",该验证码用于验证，10分钟内有效，为了您的账号信息安全，请勿把验证码提供给他人。");
                final SMSVo finalCurrentSMSVo = currentSMSVo;
                final List<SMSVo> finalSmsVoList = smsVoList;
                //请求接口
                HttpUtil.getInstance().requestPostJson("http://www.467890.com/Admin/index.php/Message/send", json.toJSONString(), new HttpUtil.OnMessageListener() {
                    @Override
                    public void onMessage(String result) {
                        if (Strings.isBlank(result)) {
                            throw new StudioException(Labels.getLabel("sms.get.error"));
                        } else {
                            JSONObject data = JSON.parseObject(result);
                            if ("success".equals(data.getString("status"))) {
                                finalCurrentSMSVo.setCreateDate(new Date());
                                finalCurrentSMSVo.setContent(random);
                                GlobalVariableService.getOneInstance().setSmsVos(finalSmsVoList);
                            } else {
                                throw new StudioException(data.getString("respMsg"));
                            }
                        }
                    }
                });
            } else {
                throw new StudioException(Labels.getLabel("sms.get.more.error"));
            }
        }
        return true;
    }
}
