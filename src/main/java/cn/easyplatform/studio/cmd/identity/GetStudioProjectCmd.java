package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

public class GetStudioProjectCmd implements Command<ProjectBean> {
    private String projectId;

    public GetStudioProjectCmd(String projectId) {
        this.projectId = projectId;
    }
    @Override
    public ProjectBean execute(CommandContext cc) {
        return cc.getIdentityDao().getProjectDetail(projectId);
    }
}
