/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.SecurityUtils;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StudioUserPasswordResetCmd implements Command<String> {

    private String userId;

    private String salt;

    /**
     * @param userId
     * @param salt
     */
    public StudioUserPasswordResetCmd(String userId, String salt) {
        this.userId = userId;
        this.salt = salt;
    }

    @Override
    public String execute(CommandContext cc) {
        String password = RandomStringUtils.randomAlphanumeric(6);
        cc.getIdentityDao().updatePassword(userId, SecurityUtils.getSecurePassword(password, salt));
        return password;
    }

}
