package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

public class GetStudioAllProjectCmd implements Command<ProjectBean[]> {
    @Override
    public ProjectBean[] execute(CommandContext cc) {
        return cc.getIdentityDao().getAllProject();
    }
}
