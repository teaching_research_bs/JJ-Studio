/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.UserVo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetUsersCmd implements Command<List<UserVo>> {

	@Override
	public List<UserVo> execute(CommandContext cc) {
		List<Object[]> result = cc
				.getBizDao()
				.selectList(
						"select userId,name,password,type,validDate,validDays,state from sys_user_info order by userId");
		List<UserVo> users = new ArrayList<UserVo>(result.size());
		for (Object[] objs : result) {
			UserVo vo = new UserVo();
			vo.setId((String) objs[0]);
			vo.setName((String) objs[1]);
			vo.setPassword((String) objs[2]);
			vo.setType(objs[3] == null ? 0 : ((Number) objs[3]).intValue());
			vo.setValidDate((Date) objs[4]);
			vo.setValidDays(objs[5] == null ? 0 : ((Number) objs[5]).intValue());
			vo.setState(objs[6] == null ? 0 : ((Number) objs[6]).intValue());
			users.add(vo);
		}
		return users;
	}
}
