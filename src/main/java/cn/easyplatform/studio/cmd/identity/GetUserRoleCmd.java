/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetUserRoleCmd implements Command<List<String>> {

	private String userId;

	/**
	 * @param roleId
	 */
	public GetUserRoleCmd(String userId) {
		this.userId = userId;
	}

	@Override
	public List<String> execute(CommandContext cc) {
		List<Object[]> result = cc
				.getBizDao()
				.selectList(
						"select roleId from sys_user_role_info where userId=? order by orderNo",
						new FieldVo(FieldType.VARCHAR, userId));
		List<String> menus = new ArrayList<String>(result.size());
		for (Object[] objs : result) {
			menus.add((String) objs[0]);
		}
		return menus;
	}
}
