/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;
import cn.easyplatform.studio.vos.RoleVo;
import cn.easyplatform.type.FieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetStudioRolesCmd implements Command<List<RoleVo>> {

    private String projectId;

    public GetStudioRolesCmd(String projectId) {
        this.projectId = projectId;
    }

    public GetStudioRolesCmd() {
    }

    @Override
    public List<RoleVo> execute(CommandContext cc) {
        FieldVo params = null;
        if (Strings.isBlank(projectId) == false) {
            params = new FieldVo(FieldType.VARCHAR, projectId);
        } else {
            params = new FieldVo(FieldType.VARCHAR, cc.getProject().getId());
        }
        List<Object[]> result = cc
                .getIdentityDao()
                .selectList(
                        "select roleId,name,desp,permissions from ep_role_info where projectId=? order by roleId", params);
        List<RoleVo> roles = new ArrayList<RoleVo>(result.size());
        for (Object[] objs : result) {
            RoleVo vo = new RoleVo();
            vo.setId((String) objs[0]);
            vo.setName((String) objs[1]);
            vo.setDesp((String) objs[2]);
            //image作为授权使用
            vo.setImage((String) objs[3]);
            roles.add(vo);
        }
        return roles;
    }
}
