/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.RoleVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetRolesCmd implements Command<List<RoleVo>> {

	@Override
	public List<RoleVo> execute(CommandContext cc) {
		List<Object[]> result = cc
				.getBizDao()
				.selectList(
						"select roleId,name,desp,image from sys_role_info order by roleId");
		List<RoleVo> roles = new ArrayList<RoleVo>(result.size());
		for (Object[] objs : result) {
			RoleVo vo = new RoleVo();
			vo.setId((String) objs[0]);
			//vo.setType((String) objs[1]);
			vo.setName((String) objs[1]);
			vo.setDesp((String) objs[2]);
			vo.setImage((String) objs[3]);
			roles.add(vo);
		}
		return roles;
	}
}
