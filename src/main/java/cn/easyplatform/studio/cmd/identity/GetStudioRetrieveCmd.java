package cn.easyplatform.studio.cmd.identity;

import cn.easyplatform.studio.StudioException;
import cn.easyplatform.studio.dao.IdentityDao;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.utils.SecurityUtils;
import cn.easyplatform.studio.vos.LoginUserVo;
import cn.easyplatform.studio.vos.SMSVo;
import org.zkoss.util.resource.Labels;

import java.util.Date;
import java.util.List;

public class GetStudioRetrieveCmd implements Command<Boolean> {

    private String userId;
    private String password;
    public GetStudioRetrieveCmd(String userId, String password) {
        this.userId = userId;
        this.password = password;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        IdentityDao dao = cc.getIdentityDao();
        LoginUserVo user = dao.getUser(userId);
        if (user == null)
            throw new StudioException(Labels.getLabel("forget.mobile.no.exist"));
        dao.updatePassword(userId, SecurityUtils.getSecurePassword(password, user.getSalt()));
        return true;
    }
}
