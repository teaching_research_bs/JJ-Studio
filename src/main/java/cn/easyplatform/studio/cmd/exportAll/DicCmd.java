package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.cmd.taskLink.TaskLinkCmd;
import cn.easyplatform.studio.vos.DicDetailVo;
import cn.easyplatform.studio.vos.DicVo;
import cn.easyplatform.studio.vos.QueryDicVo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DicCmd {
    protected QueryDicVo parentTree(List<DicVo> linkList, DicVo value, int pageSize, int pageNo)
    {
        List<DicVo> entityList = new ArrayList<>();

        // 1、获取第一级节点
        int fromIndex = (pageNo - 1) * pageSize;
        int toIndex = pageNo * pageSize;
        int addIndex = 0;
        if (linkList.size() <= (pageNo - 1) * pageSize) {
            return new QueryDicVo(1, entityList);
        }

        for (DicVo linkVo: linkList) {
            //分页
            boolean pageAdd = false;
            if (addIndex >= fromIndex && addIndex < toIndex)
                pageAdd = true;
            if (pageSize == TaskLinkCmd.NOLIMITPAGESIZE)
                pageAdd = true;
            //搜索
            boolean searchAdd = false;
            if (Strings.isBlank(linkVo.getUpCode()) || linkVo.getUpCode().equals(" "))
            {
                if (value == null || (Strings.isBlank(value.getDicCode()) && Strings.isBlank(value.getDesc1())
                        && Strings.isBlank(value.getUpCode()))) {
                    searchAdd = true;
                } else {
                    if (linkVo.getDicCode().contains(value.getDicCode()) && linkVo.getDesc1().contains(value.getDesc1())
                            && linkVo.getUpCode().contains(value.getUpCode()))
                        searchAdd = true;
                }
            }

            if (searchAdd == true)
                addIndex = addIndex + 1;
            if (searchAdd == true && pageAdd == true)
                entityList.add(linkVo);
        }
        return new QueryDicVo(addIndex, entityList);
    }
    protected List<DicVo> addDetail(List<DicVo> dicList, List<DicDetailVo> dicDetailList) {
        HashMap<String, List<DicDetailVo>> detailDic = new HashMap<>();
        for (DicDetailVo dicDetailVo: dicDetailList) {
            if (detailDic.get(dicDetailVo.getDicCode()) == null) {
                List<DicDetailVo> list = new ArrayList<>();
                list.add(dicDetailVo);
                detailDic.put(dicDetailVo.getDicCode(), list);
            } else {
                List<DicDetailVo> list = detailDic.get(dicDetailVo.getDicCode());
                list.add(dicDetailVo);
            }
        }

        for (DicVo dicVo: dicList) {
            if (detailDic.get(dicVo.getDicCode()) != null && detailDic.get(dicVo.getDicCode()).size() > 0)
                dicVo.setDicDetailVoList(detailDic.get(dicVo.getDicCode()));
        }
        return dicList;
    }

    protected QueryDicVo parseTree(List<DicVo> linkList, DicVo parentVo, QueryDicVo queryDicVo) {
        parentVo.getChildrenList().clear();
        // 缓存成字典
        HashMap<String, DicVo> entityDic = new HashMap<>();
        for (DicVo vo: linkList) {
            entityDic.put(vo.getDicCode(), (DicVo)vo.clone());
        }
        if (parentVo != null) {
            recursiveTree(parentVo, linkList);
            System.out.println(parentVo);
        }

        return queryDicVo;
    }

    private DicVo recursiveTree(DicVo parent, List<DicVo> linkList) {
        for (DicVo vo: linkList) {
            if (Strings.isBlank(vo.getUpCode()) == false) {
                if (vo.getUpCode().equals(parent.getDicCode())) {
                    parent.getChildrenList().add(vo);
                }
            }
        }
        for (DicVo vo: parent.getChildrenList()) {
            recursiveTree(vo,linkList);
        }
        return parent;
    }
}
