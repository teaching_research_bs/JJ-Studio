package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MessageVo;

import java.util.List;

public class QueryMessageInfoCmd implements Command<List<MessageVo>> {
    private MessageVo vo;
    private boolean isLabel;
    public QueryMessageInfoCmd(MessageVo vo, boolean isLabel) {
        this.vo = vo;
        this.isLabel = isLabel;
    }
    @Override
    public List<MessageVo> execute(CommandContext cc) {
        StringBuilder sb = new StringBuilder(
                "select * from sys_message_info where 1=1");
        boolean isNeedChooseStatus = true;
        if (vo != null) {
            if (!Strings.isBlank(vo.getStatus()))
                sb.append(" and status='").append(vo.getStatus()).append("'");
            if (!Strings.isBlank(vo.getMSG_TYPE())) {
                sb.append(" and MSG_TYPE='").append(vo.getMSG_TYPE()).append("'");
                isNeedChooseStatus = false;
            }
        }

        if (isNeedChooseStatus == true)
            if (isLabel == false)
                sb.append(" and MSG_TYPE in ('H','I','E','W','C','P')");
            else
                sb.append(" and MSG_TYPE not in ('H','I','E','W','C','P')");

        if (vo != null) {
            if (!Strings.isBlank(vo.getCode())) {
                String content = "%" + vo.getCode() + "%";
                sb.append(" and code like '").append(content).append("'");
            }
            if (!Strings.isBlank(vo.getZh_cn())) {
                String content = "%" + vo.getZh_cn() + "%";
                sb.append(" and zh_cn like '").append(content).append("'");
            }
        }
        List<MessageVo> data = cc.getMessageInfoDao(null).selectList(sb.toString(), null);
        return data;
    }
}
