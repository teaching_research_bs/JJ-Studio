package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.DicDetailVo;
import cn.easyplatform.studio.vos.DicVo;

import java.util.ArrayList;
import java.util.List;

public class GetDicCmd extends DicCmd implements Command<List<DicVo>> {
    private String dicCode;
    private List<DicVo> dicList;
    public  GetDicCmd(String dicCode, List<DicVo> dicList)
    {
        this.dicCode = dicCode;
        this.dicList = dicList;
    }
    @Override
    public List<DicVo> execute(CommandContext cc) {
        if (Strings.isBlank(dicCode) && (dicList == null || dicList.size() == 0)) {
            List<DicVo> list = cc.getDicDao(null).selectAllList();
            List<DicDetailVo> detailList = cc.getDicDetailDao(null).selectAll();
            list = addDetail(list, detailList);
            return list;
        } else {
            List<DicVo> changeList = new ArrayList<>();

            DicVo vo = null;
            if (dicList.size() > 0) {
                for (int i = 0; i < dicList.size(); i++) {
                    DicVo parentVo = dicList.get(i);
                    if (parentVo.getDicCode().equals(dicCode)) {
                        vo = parentVo;
                        break;
                    }
                }
            }
            changeList.add(vo);
            return changeList;
        }
    }
}
