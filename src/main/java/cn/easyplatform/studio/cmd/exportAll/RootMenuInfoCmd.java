package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MenuInfoVo;
import cn.easyplatform.studio.vos.QueryMenuInfoVo;

import java.util.List;

public class RootMenuInfoCmd extends MenuInfoCmd implements Command<QueryMenuInfoVo> {
    private QueryMenuInfoVo queryDicVo;
    private MenuInfoVo dicVo;
    private List<MenuInfoVo> dicVoList;
    public RootMenuInfoCmd(QueryMenuInfoVo queryDicVo, MenuInfoVo dicVo, List<MenuInfoVo> dicVoList) {
        this.queryDicVo = queryDicVo;
        this.dicVo = dicVo;
        this.dicVoList = dicVoList;
    }
    @Override
    public QueryMenuInfoVo execute(CommandContext cc) {
        if (dicVoList.size() == 0)
            return queryDicVo;
        return parseTree(dicVoList, dicVo, queryDicVo);
    }
}
