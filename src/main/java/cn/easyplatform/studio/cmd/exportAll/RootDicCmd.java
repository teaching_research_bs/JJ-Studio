package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.DicVo;
import cn.easyplatform.studio.vos.QueryDicVo;

import java.util.List;

public class RootDicCmd extends DicCmd implements Command<QueryDicVo> {
    private QueryDicVo queryDicVo;
    private DicVo dicVo;
    private List<DicVo> dicVoList;
    public RootDicCmd(QueryDicVo queryDicVo, DicVo dicVo, List<DicVo> dicVoList) {
        this.queryDicVo = queryDicVo;
        this.dicVo = dicVo;
        this.dicVoList = dicVoList;
    }
    @Override
    public QueryDicVo execute(CommandContext cc) {
        if (dicVoList.size() == 0)
            return queryDicVo;
        return parseTree(dicVoList, dicVo, queryDicVo);
    }
}
