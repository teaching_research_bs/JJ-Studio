package cn.easyplatform.studio.cmd.exportAll;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.DicDetailVo;

import java.util.ArrayList;
import java.util.List;

public class SaveDicDetailCmd implements Command<Boolean> {
    private DicDetailVo detailVo;
    public SaveDicDetailCmd(DicDetailVo detailVo) {
        this.detailVo = detailVo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        List<DicDetailVo> detailVos = new ArrayList<>();
        detailVos.add(detailVo);
        cc.getDicDetailDao(null).setDicDetailList(detailVos);
        return true;
    }
}
