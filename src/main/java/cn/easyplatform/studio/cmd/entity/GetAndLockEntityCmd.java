/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.GetEntityVo;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetAndLockEntityCmd implements Command<GetEntityVo> {

	private String id;

	/**
	 * @param id
	 */
	public GetAndLockEntityCmd(String id) {
		this.id = id;
	}

	@Override
	public GetEntityVo execute(CommandContext cc) {
		String lockUser = cc.getEntityLockProvider().lock(cc.getProject().getId(), id);
		BaseEntity entity = cc.getEntityDao().getEntity(
				cc.getProject().getEntityTableName(), id);
		if (entity != null)
			return new GetEntityVo(entity, lockUser);
		return null;
	}

}
