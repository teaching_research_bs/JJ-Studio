/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LockCmd implements Command<String> {

	private String entityId;

	/**
	 * @param entityId
	 */
	public LockCmd(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public String execute(CommandContext cc) {
		return cc.getEntityLockProvider().lock(cc.getProject().getId(),
				entityId);
	}

}
