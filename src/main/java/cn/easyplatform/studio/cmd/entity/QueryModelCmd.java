/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.EntityVo;
import cn.easyplatform.type.EntityType;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class QueryModelCmd implements Command<List<EntityVo>> {

	private String type;

	/**
	 * @param type
	 */
	public QueryModelCmd(String type) {
		this.type = type;
	}

	@Override
	public List<EntityVo> execute(CommandContext cc) {
		StringBuilder sb = new StringBuilder(
				"select modelId,name,desp from ep_model_info where type=?");
		if (type.equals(EntityType.DATASOURCE.getName()))
			sb.append(" and createUser='")
					.append(cc.getProject().getId()).append("'");
		return cc.getEntityDao().selectModel(sb.toString(), type);
	}

}
