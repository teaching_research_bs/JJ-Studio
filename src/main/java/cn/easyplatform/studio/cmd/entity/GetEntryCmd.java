/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.lang.stream.StringOutputStream;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetEntryCmd implements Command<EntityInfo> {

	private String entityId;

	/**
	 * @param entityId
	 */
	public GetEntryCmd(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public EntityInfo execute(CommandContext cc) {
		EntityInfo entityInfo = cc.getEntityDao().getEntry(cc.getProject().getEntityTableName(),
				entityId);
		if ("Page".equals(entityInfo.getType())) {
			if (Strings.isBlank(entityInfo.getContent()) == false) {
				PageBean e = TransformerFactory.newInstance().transformFromXml(PageBean.class, Lang.ins(entityInfo.getContent()));
				e.setMil(entityInfo.getExt1());
				e.setAjax(entityInfo.getExt0());
				StringBuilder sb = new StringBuilder();
				StringOutputStream os = new StringOutputStream(sb);
				TransformerFactory.newInstance().transformToXml(e, os);
				entityInfo.setContent(sb.toString());
			}
		}
		return entityInfo;
	}

}
