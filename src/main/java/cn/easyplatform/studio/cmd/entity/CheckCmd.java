/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CheckCmd implements Command<Boolean> {

	private String id;

	/**
	 * @param id
	 */
	public CheckCmd(String id) {
		this.id = id;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		boolean checked = cc.getEntityDao().exists(
				cc.getProject().getEntityTableName(), id);
		if (!checked) {
			String str = cc.getEntityLockProvider().lock(
					cc.getProject().getId(), id);
			if (!Strings.isBlank(str))
				return true;
		}
		return checked;
	}

}
