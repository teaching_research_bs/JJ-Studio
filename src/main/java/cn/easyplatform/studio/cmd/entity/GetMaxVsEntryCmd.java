/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetMaxVsEntryCmd implements Command<EntityInfo> {

	private String entityId;

	/**
	 * @param entityId
	 */
	public GetMaxVsEntryCmd(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public EntityInfo execute(CommandContext cc) {
		return cc.getHisEntityDao().getMaxVsEntry(cc.getProject().getEntityTableName(), entityId);
	}

}
