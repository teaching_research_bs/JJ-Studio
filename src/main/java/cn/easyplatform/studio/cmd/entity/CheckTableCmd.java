package cn.easyplatform.studio.cmd.entity;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

public class CheckTableCmd implements Command<Boolean> {
    private String id;

    /**
     * @param id
     */
    public CheckTableCmd(String id) {
        this.id = id;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        boolean checked = cc.getBizDao().exists(id);
        return checked;
    }
}
