/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.FieldVo;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UpdateCmd implements Command<Boolean> {

	private String dbId;

	private String sql;

	private List<FieldVo> params;

	/**
	 * @param sql
	 * @param params
	 */
	public UpdateCmd(String sql, List<FieldVo> params) {
		this.sql = sql;
		this.params = params;
	}

	/**
	 * @param dbId
	 * @param sql
	 * @param params
	 */
	public UpdateCmd(String dbId, String sql, List<FieldVo> params) {
		this.dbId = dbId;
		this.sql = sql;
		this.params = params;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		return cc.getBizDao(null).update(sql, params) == 1;
	}

}
