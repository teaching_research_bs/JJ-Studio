package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.List;

public class GetColumnNameCmd implements Command<List<String>> {
    private String tableName;

    public GetColumnNameCmd(String tableName) {
        this.tableName = tableName;
    }
    @Override
    public List<String> execute(CommandContext cc) {
        List<String> list = cc.getBizDao().getColumnNameList(tableName);
        return list;
    }
}
