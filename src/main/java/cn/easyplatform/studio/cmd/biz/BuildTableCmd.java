/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.TableVo;

import java.util.Date;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BuildTableCmd implements Command<Boolean> {

	private TableVo tv;

	public BuildTableCmd(TableVo tv) {
		this.tv = tv;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		try {
			cc.getUser().setBusying(true);
			cc.beginTx();
			cc.getBizDao(tv.getSource().getSubType()).buildTable(tv);
			cc.commitTx();
			return true;
		} catch (Exception e) {
			cc.rollbackTx();
			throw (RuntimeException) e;
		} finally {
			cc.closeTx();
			cc.getUser().setLastAccessTime(new Date());
			cc.getUser().setBusying(false);
		}
	}

}
