/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.TableVo;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BuildKeyCmd implements Command<Boolean> {

	private TableVo tv;

	public BuildKeyCmd(TableVo tv) {
		this.tv = tv;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		try {
			cc.beginTx();
			cc.getBizDao(tv.getSource().getSubType()).buildPrimaryKey(
					tv.getSource(), tv.getTarget());
			cc.commitTx();
			return true;
		} catch (Exception e) {
			cc.rollbackTx();
			throw (RuntimeException) e;
		} finally {
			cc.closeTx();
		}
	}

}
