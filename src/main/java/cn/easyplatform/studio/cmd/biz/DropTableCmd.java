/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.biz;

import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DropTableCmd implements Command<Boolean> {

	private TableBean tb;

	/**
	 * @param tb
	 */
	public DropTableCmd(TableBean tb) {
		this.tb = tb;
	}

	@Override
	public Boolean execute(CommandContext cc) {
		cc.getBizDao(tb.getSubType()).dropTable(tb);
		return true;
	}

}
