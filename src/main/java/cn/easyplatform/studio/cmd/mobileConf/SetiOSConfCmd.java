package cn.easyplatform.studio.cmd.mobileConf;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MobileConfVo;

public class SetiOSConfCmd implements Command<Boolean> {
    private MobileConfVo vo;
    public SetiOSConfCmd(MobileConfVo vo)
    {
        this.vo = vo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (vo == null)
            return false;
        vo.setCreateUserId(cc.getUser().getUserId());
        vo.setConfType(MobileConfVo.iOSCONF);
        vo.setProjectId(cc.getProject().getId());
        return cc.getMobileConf().addNewMobileConf(vo);
    }
}
