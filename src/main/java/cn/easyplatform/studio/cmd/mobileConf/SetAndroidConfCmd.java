package cn.easyplatform.studio.cmd.mobileConf;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MobileConfVo;

public class SetAndroidConfCmd implements Command<Boolean> {
    private MobileConfVo vo;
    public SetAndroidConfCmd(MobileConfVo vo)
    {
        this.vo = vo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (vo == null)
            return false;
        vo.setCreateUserId(cc.getUser().getUserId());
        vo.setConfType(MobileConfVo.ANDROIDCONF);
        vo.setProjectId(cc.getProject().getId());
        return cc.getMobileConf().addNewMobileConf(vo);
    }
}
