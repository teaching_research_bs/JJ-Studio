package cn.easyplatform.studio.cmd.mobileConf;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MobileConfVo;

public class UpdateMobileConfCmd implements Command<Boolean> {
    private MobileConfVo vo;
    public UpdateMobileConfCmd(MobileConfVo vo)
    {
        this.vo = vo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        vo.setUpdateUserId(cc.getUser().getUserId());
        return cc.getMobileConf().updateMobileConf(vo);
    }
}
