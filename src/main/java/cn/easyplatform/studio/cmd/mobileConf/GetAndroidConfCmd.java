package cn.easyplatform.studio.cmd.mobileConf;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.MobileConfVo;

import static cn.easyplatform.studio.vos.MobileConfVo.ANDROIDCONF;

public class GetAndroidConfCmd implements Command<MobileConfVo> {
    @Override
    public MobileConfVo execute(CommandContext cc) {
        MobileConfVo vo = cc.getMobileConf().getConf(ANDROIDCONF, cc.getProject().getId());
        return vo;
    }
}
