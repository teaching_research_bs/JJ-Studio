package cn.easyplatform.studio.cmd.link;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.utils.ListUtil;
import cn.easyplatform.studio.vos.LinkVo;
import cn.easyplatform.type.EntityType;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LinkCmd {
    private static Logger log = LoggerFactory.getLogger(LinkCmd.class);
    private int pathIndex;
    protected List<LinkVo> parseTree(List<LinkVo> linkList, String searchEntityId)
    {
        List<LinkVo> entityList = new ArrayList<>();
        // 缓存成字典
        HashMap<String, LinkVo> entityDic = new HashMap<>();
        for (LinkVo vo: linkList) {
            entityDic.put(vo.getEntityId(), (LinkVo)vo.clone());
        }
        // 1、获取第一级节点
        for (LinkVo linkVo: linkList) {
            if (Strings.isBlank(searchEntityId))
            {
                entityList.add((LinkVo)linkVo.clone());
            } else if(searchEntityId.equals(linkVo.getEntityId())) {
                entityList.add((LinkVo)linkVo.clone());
            }
        }

        // 2、递归获取子节点
        List<List<String>> allPathList = new ArrayList<>();
        for (LinkVo parent: entityList) {
            List<String> childrenEntityIdList = new ArrayList();
            if (Strings.isBlank(parent.getChildrenEntityId()) == false)
                childrenEntityIdList = Arrays.asList(parent.getChildrenEntityId().split(","));
            if (childrenEntityIdList.size() > 0)
            {
                pathIndex = -1;
                for (String entityId: childrenEntityIdList) {
                    pathIndex += 1;
                    List<String> pathList = new ArrayList<>();
                    pathList.add(entityId);
                    allPathList.add(pathList);
                    LinkVo childrenVo = null;
                    try{
                        childrenVo = recursiveTree((LinkVo)entityDic.get(entityId).clone(), entityDic, allPathList);
                    }catch (Exception e){
                        log.debug("parseTree:" + entityId);
                        log.debug("parseTree:" + entityDic.get(entityId)+"");
                        throw new RuntimeException(e.toString());
                    }
                    parent.getChildrenList().add(childrenVo);
                }
            }
        }
        return entityList;
    }

    /**
     * 生成树
     * @param parent 父节点
     * @param entityDic 所有数据
     * @param allPathList 树的所有路径
     * @return 节点
     */
    protected LinkVo recursiveTree(LinkVo parent, HashMap<String, LinkVo> entityDic, List<List<String>> allPathList) {
        List<String> childrenEntityIdList = new ArrayList();
        if (Strings.isBlank(parent.getChildrenEntityId()) == false) {
            childrenEntityIdList = Arrays.asList(parent.getChildrenEntityId().split(","));
            childrenEntityIdList = ListUtil.removeDuplicate(childrenEntityIdList);
        }
        //当数据遇到task类型停止下一轮添加
        if (EntityType.TASK.getName().equals(parent.getEntityType()) == false && childrenEntityIdList.size() > 0) {
            for (int index = 0; index < childrenEntityIdList.size(); index++) {
                //当前处理的id
                String entityId = childrenEntityIdList.get(index);
                //如果存在兄弟节点，在第一个节点的时候，copy这个节点的所有路径到新的路径中
                if (index == 0 && childrenEntityIdList.size() > 1) {
                    for (int count = 0; count < childrenEntityIdList.size() - 1; count++) {
                        List<String> newPathList = new ArrayList<>();
                        newPathList.addAll(allPathList.get(pathIndex));
                        allPathList.add(count + pathIndex,newPathList);
                    }
                }
                //当存在兄弟节点，并且不为第一个下标，则把当前路径链的下标+1，意味着在新的路径链下
                if (childrenEntityIdList.size() > 1 && index > 0) {
                    pathIndex = pathIndex + 1;
                }
                if (entityDic.get(entityId) != null) {
                    //判断当前父-子节点组成的两个元素存在于路径中则跳过该组合
                    List<String> currentPathList = allPathList.get(pathIndex);
                    boolean isDouble = false;
                    if (currentPathList != null && currentPathList.size() > 1) {
                        for (int i = 0; i < currentPathList.size() - 1; i++) {
                            String path = currentPathList.get(i);
                            if (path.equals(entityId)) {
                                isDouble = true;
                                break;
                            }
                        }
                    }
                    List<String> pathList = allPathList.get(pathIndex);
                    pathList.add(entityId);
                    //如果出现重复，添加当前节点后结束循环
                    if (isDouble == false) {
                        //添加节点到当前路径下面
                        LinkVo childrenVo = recursiveTree(entityDic.get(entityId), entityDic, allPathList);
                        parent.getChildrenList().add(childrenVo);
                    } else {
                        LinkVo addVo = (LinkVo) entityDic.get(entityId).clone();
                        addVo.setCanAdd(false);
                        parent.getChildrenList().add(addVo);
                    }
                }
            }
        }
        return parent;
    }

    /*protected List<LinkVo> needRefresh(List<LinkVo> linkVoList) {
        if (linkVoList == null || linkVoList.size() == 0)
            return linkVoList;
        for (LinkVo vo: linkVoList) {
            vo.setNeedRefresh(true);
        }
        return linkVoList;
    }*/

    protected List<LinkVo> getTableListWithTree(LinkVo parentVo, List<LinkVo> tableList) {
        boolean isContain = false;
        if (tableList.size() > 0) {
            for (LinkVo linkVo: tableList) {
                if (linkVo.getEntityId().equals(parentVo.getEntityId())) {
                    isContain = true;
                    break;
                }
            }
        }
        if (isContain == false && LinkVo.ENTITYSTATUS.equals(parentVo.getStatus()))
            tableList.add(parentVo);

        if (parentVo.getChildrenList() != null && parentVo.getChildrenList().size() > 0) {
            for (LinkVo linkVo: parentVo.getChildrenList()) {
                getTableListWithTree(linkVo, tableList);
            }
        }
        return tableList;
    }
}
