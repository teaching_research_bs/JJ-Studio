/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.uploadHist;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.UploadHistVo;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetUploadHistCmd implements Command<List<UploadHistVo>> {

	private UploadHistVo vo;

	/**
	 * @param vo
	 */
	public GetUploadHistCmd(UploadHistVo vo) {
		this.vo = vo;
	}

	@Override
	public List<UploadHistVo> execute(CommandContext cc) {
		return cc.getUploadHistDao().selectUploadHist("ep_upload_file_hist",vo);
	}

}
