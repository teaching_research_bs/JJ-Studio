/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetCmd implements Command<String> {

	private long versionNo;

	/**
	 * @param versionNo
	 */
	public GetCmd(long versionNo) {
		this.versionNo = versionNo;
	}

	@Override
	public String execute(CommandContext cc) {
		return cc.getVersionDao().getContent(
				cc.getProject().getEntityTableName(), versionNo);
	}

}
