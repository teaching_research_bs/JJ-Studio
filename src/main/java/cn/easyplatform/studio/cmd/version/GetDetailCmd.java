/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetDetailCmd implements Command<List<EntityInfo>> {

    private boolean isExport;

    private String txId;

    /**
     * @param isExport
     */
    public GetDetailCmd(boolean isExport, String txId) {
        this.isExport = isExport;
        this.txId = txId;
    }

    @Override
    public List<EntityInfo> execute(CommandContext cc) {
        String table = isExport ? "ep_export_detail_info" : "ep_import_detail_info";
        String versionSql = isExport ? "SELECT versionNo FROM " + table + " WHERE txId=?" : "SELECT targetVersionNo FROM " + table + " WHERE txId=?";
        String sql = "SELECT a.versionNo,a.entityId,a.name,a.desp,a.type,a.subType,a.content,a.updateDate,a.updateUser,a.status FROM "
                + cc.getProject().getEntityTableName() + "_repo a WHERE a.versionNo IN (" + versionSql + ")";
        return cc.getVersionDao().selectList(null, sql, this.txId);
    }

}
