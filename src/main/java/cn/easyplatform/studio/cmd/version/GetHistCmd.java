/**
 * www.easyplatform.cn ©2016
 */
package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.IXVo;

import java.util.List;

/**
 * @author <a href="mailto:shiny_vc@163.com">陈云亮</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetHistCmd implements Command<List<IXVo>> {

    private boolean isExport;

    /**
     * @param isExport
     */
    public GetHistCmd(boolean isExport) {
        this.isExport = isExport;
    }

    @Override
    public List<IXVo> execute(CommandContext cc) {
        String table = isExport ? "ep_export_info" : "ep_import_info";
        return cc.getVersionDao().selectHistory(table, cc.getProject().getId());
    }

}
