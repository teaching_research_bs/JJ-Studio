package cn.easyplatform.studio.cmd.version;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;

public class GetExportInfoContainCmd implements Command<Boolean> {
    private String txId;
    public GetExportInfoContainCmd(String txId) {
        this.txId = txId;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        return cc.getVersionDao().containExport(txId);
    }
}
