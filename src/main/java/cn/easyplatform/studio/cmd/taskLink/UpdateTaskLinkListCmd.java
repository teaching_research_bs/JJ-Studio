package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.ArrayList;
import java.util.List;

public class UpdateTaskLinkListCmd extends TaskLinkCmd implements Command<Boolean> {
    private List<TypeLinkVo> linkVos;
    private String deleteEntityId;
    public UpdateTaskLinkListCmd(List<TypeLinkVo> linkVos, String deleteEntityId)
    {
        this.linkVos = linkVos;
        this.deleteEntityId = deleteEntityId;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (linkVos == null || linkVos.size() == 0 || Strings.isBlank(deleteEntityId))
            return false;

        boolean isSuccess = cc.getTaskLink().updateTaskLinks("ep_" + cc.getProject().getId() + "_task_link", linkVos);
        if (isSuccess == false)
            return false;
        return updateValidation(cc);
    }

    protected Boolean updateValidation(CommandContext cc)
    {
        List<TypeLinkVo> updateVos = cc.getTaskLink().getLinkWithChildrenEntityId("ep_"+cc.getProject().getId()+"_link",
                "ep_"+cc.getProject().getId()+"_task_link", deleteEntityId);
        if (updateVos == null || updateVos.size() == 0)
            return false;
        synchronized (GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
            //List<TypeLinkVo> dataList = GlobalVariableService.getInstance().getDataTypeLinkVoList();
            //更换数据表缓存
            List<Integer> updateList = new ArrayList<>();
            for (TypeLinkVo updateVo: updateVos) {
                boolean isSearch = false;
                for (int i = 0; i < GlobalVariableService.getInstance().getDataTypeLinkVoList().size(); i++) {
                    TypeLinkVo vo = GlobalVariableService.getInstance().getDataTypeLinkVoList().get(i);
                    if (updateVo.getEntityId().equals(vo.getEntityId())) {
                        updateList.add(i);
                        isSearch = true;
                        break;
                    }
                }
                if (isSearch == false)
                    updateList.add(-1);
            }

            if (updateList != null && updateList.size() != 0)
            {
                for (int i = 0; i < updateList.size(); i++) {
                    GlobalVariableService.getInstance().getDataTypeLinkVoList().set(updateList.get(i), updateVos.get(i));
                }
            } else
                return false;
            return true;
        }
    }
}
