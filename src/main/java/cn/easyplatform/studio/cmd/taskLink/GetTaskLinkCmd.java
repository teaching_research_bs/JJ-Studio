package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.ArrayList;
import java.util.List;

public class GetTaskLinkCmd extends TaskLinkCmd implements Command<List<TypeLinkVo>> {
    private TypeLinkVo linkVo;
    public GetTaskLinkCmd(TypeLinkVo linkVo) {
        this.linkVo = linkVo;
    }
    @Override
    public List<TypeLinkVo> execute(CommandContext cc) {
        synchronized (GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
            //List<TypeLinkVo> typeLinkVos = GlobalVariableService.getInstance().getDataTypeLinkVoList();
            if (linkVo == null) {
                List<TypeLinkVo> list = cc.getTaskLink().getAllTaskLink("ep_"+cc.getProject().getId()+"_link",
                        "ep_"+cc.getProject().getId()+"_task_link");
                if (list.size() == 0)
                    return list;
                if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() == 0) {
                    GlobalVariableService.getInstance().getDataTypeLinkVoList().addAll(list);

                }
                return list;
            } else {
                List<TypeLinkVo> result = new ArrayList<>();
                if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() == 0)
                    return result;
                else {
                    //更新单条数据
                    TypeLinkVo updateVo = cc.getTaskLink().getTaskLink("ep_"+cc.getProject().getId()+"_link",
                            "ep_"+cc.getProject().getId()+"_task_link", linkVo.getEntityId());
                    int index = -1;
                    if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() > 0) {
                        for (int i = 0; i < GlobalVariableService.getInstance().getDataTypeLinkVoList().size(); i++) {
                            TypeLinkVo parentVo = GlobalVariableService.getInstance().getDataTypeLinkVoList().get(i);
                            if (parentVo.getEntityId().equals(linkVo.getEntityId())) {
                                index = i;
                                break;
                            }
                        }
                    }
                    if (updateVo == null) {
                        if (index >= 0) {
                            GlobalVariableService.getInstance().getDataTypeLinkVoList().remove(index);
                        }
                    } else {
                        if (index == -1) {
                            GlobalVariableService.getInstance().getDataTypeLinkVoList().add(updateVo);
                        } else {
                            GlobalVariableService.getInstance().getDataTypeLinkVoList().set(index, updateVo);
                        }
                    }
                    //查询数据
                    if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() == 0)
                        return result;
                    if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() > 0)
                    {
                        for (TypeLinkVo vo: GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
                            if (vo.getEntityId().equals(linkVo.getEntityId())) {
                                result.add((TypeLinkVo) vo.clone());
                                break;
                            }
                        }
                    }
                    return result;
                }
            }
        }
    }
}
