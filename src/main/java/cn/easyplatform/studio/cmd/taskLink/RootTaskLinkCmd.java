package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.QueryTypeLinkVo;
import cn.easyplatform.studio.vos.TypeLinkVo;

public class RootTaskLinkCmd extends TaskLinkCmd implements Command<QueryTypeLinkVo> {
    private QueryTypeLinkVo typeLinkVo;
    private TypeLinkVo linkVo;
    public RootTaskLinkCmd(QueryTypeLinkVo typeLinkVo, TypeLinkVo linkVo) {
        this.typeLinkVo = typeLinkVo;
        this.linkVo = linkVo;
    }
    @Override
    public QueryTypeLinkVo execute(CommandContext cc) {
        if (GlobalVariableService.getInstance().getDataTypeLinkVoList().size() == 0)
            return typeLinkVo;
        synchronized (GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
            return parseTree(GlobalVariableService.getInstance().getDataTypeLinkVoList(), linkVo, typeLinkVo);
        }
    }
}
