package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.entities.beans.project.ProjectBean;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetAllTaskLinkCmd implements Command<Boolean> {
    private String beanId;

    public GetAllTaskLinkCmd(String beanId) {
        this.beanId = beanId;
    }

    public GetAllTaskLinkCmd() {

    }
    @Override
    public Boolean execute(CommandContext cc) {
        Map<String, List<TypeLinkVo>> lists = new HashMap<>();
        if (Strings.isBlank(beanId)) {
            ProjectBean[] projects = cc.getIdentityDao().getAllProject();
            for (ProjectBean bean : projects) {
                if (cc.getEntityDao().exists("ep_"+bean.getId()+"_task_link")) {
                    List<TypeLinkVo> list = cc.getTaskLink().getAllTaskLink("ep_"+bean.getId()+"_link",
                            "ep_"+bean.getId()+"_task_link");
                    lists.put(bean.getId(), list);
                }
            }
        } else {
            List<TypeLinkVo> list = cc.getTaskLink().getAllTaskLink("ep_"+beanId+"_link",
                    "ep_"+beanId+"_task_link");
            lists.put(beanId, list);
        }
        GlobalVariableService.getOneInstance().setAllDataTypeLinkVoMap(lists);
        return true;
    }
}
