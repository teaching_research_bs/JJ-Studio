package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.List;

public class DeleteTaskLinkCmd extends TaskLinkCmd implements Command<Boolean> {
    private TypeLinkVo linkVo;
    public DeleteTaskLinkCmd(TypeLinkVo linkVo)
    {

        this.linkVo = linkVo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        if (linkVo == null)
            return false;
        boolean deleteSuccess = cc.getTaskLink().deleteTaskLink("ep_" + cc.getProject().getId() + "_task_link", linkVo.getEntityId());
        if (deleteSuccess == false)
            return false;
        return deleteValidation(cc);
    }

    protected Boolean deleteValidation(CommandContext cc) {
        TypeLinkVo updateVo = cc.getTaskLink().getTaskLink("ep_"+cc.getProject().getId()+"_link",
                "ep_" + cc.getProject().getId() + "_task_link", linkVo.getEntityId());
        if (updateVo != null)
            return false;
        synchronized (GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
            List<TypeLinkVo> dataList = GlobalVariableService.getInstance().getDataTypeLinkVoList();
            //更换数据表缓存
            int index = -1;
            for (int i = 0; i < dataList.size(); i++) {
                TypeLinkVo vo = dataList.get(i);
                if (vo.getEntityId().equals(linkVo.getEntityId())) {
                    index = i;
                    break;
                }
            }
            if (index >= 0)
            {
                dataList.remove(index);
            } else
                return false;
            return true;
        }
    }
}
