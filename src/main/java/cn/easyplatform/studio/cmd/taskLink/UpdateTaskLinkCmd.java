package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.utils.GlobalVariableService;
import cn.easyplatform.studio.vos.TypeLinkVo;

public class UpdateTaskLinkCmd implements Command<Boolean> {
    private TypeLinkVo vo;

    public UpdateTaskLinkCmd(TypeLinkVo vo)
    {
        this.vo = vo;
    }
    @Override
    public Boolean execute(CommandContext cc) {
        boolean isSuccess = cc.getTaskLink().updateTaskLink("ep_"+cc.getProject().getId()+"_task_link", vo);
        if (isSuccess == false)
            return false;
        return updateValidation(cc);
    }

    protected Boolean updateValidation(CommandContext cc)
    {
        TypeLinkVo updateVo = cc.getTaskLink().getTaskLink("ep_"+cc.getProject().getId()+"_link",
                "ep_"+cc.getProject().getId()+"_task_link", vo.getEntityId());
        if (updateVo == null)
            return false;
        //更换数据表缓存
        synchronized (GlobalVariableService.getInstance().getDataTypeLinkVoList()) {
            //List<TypeLinkVo> dataList = GlobalVariableService.getInstance().getDataTypeLinkVoList();
            int index = -1;
            for (int i = 0; i < GlobalVariableService.getInstance().getDataTypeLinkVoList().size(); i++) {
                TypeLinkVo vo = GlobalVariableService.getInstance().getDataTypeLinkVoList().get(i);
                if (vo.getEntityId().equals(updateVo.getEntityId())) {
                    index = i;
                    break;
                }
            }
            if (index >= 0)
            {
                GlobalVariableService.getInstance().getDataTypeLinkVoList().set(index, (TypeLinkVo) updateVo.clone());
            } else
                return false;
            return true;
        }
    }
}
