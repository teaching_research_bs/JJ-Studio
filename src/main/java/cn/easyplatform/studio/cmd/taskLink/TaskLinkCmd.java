package cn.easyplatform.studio.cmd.taskLink;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.utils.ListUtil;
import cn.easyplatform.studio.vos.QueryTypeLinkVo;
import cn.easyplatform.studio.vos.TypeLinkVo;

import java.util.*;

public class TaskLinkCmd {
    private String parentEntityId;
    private int pathIndex;
    public final static int NOLIMITPAGESIZE = 12315;
    protected QueryTypeLinkVo parentTree(List<TypeLinkVo> linkList, String field, String value, int pageSize, int pageNo)
    {
        List<TypeLinkVo> entityList = new ArrayList<TypeLinkVo>();

        // 1、获取第一级节点
        int fromIndex = (pageNo - 1) * pageSize;
        int toIndex = pageNo * pageSize;
        int addIndex = 0;
        if (linkList.size() <= (pageNo - 1) * pageSize) {
            return new QueryTypeLinkVo(1, entityList);
        }

        for (TypeLinkVo linkVo: linkList) {
            //分页
            boolean pageAdd = false;
            if (addIndex >= fromIndex && addIndex < toIndex)
                pageAdd = true;
            if (pageSize == NOLIMITPAGESIZE)
                pageAdd = true;
            //搜索
            boolean searchAdd = false;
            if (linkVo.isRoot().equals(TypeLinkVo.ROOTENTITYID))
            {
                if (!Strings.isBlank(value)) {
                    if (field.equals("*")) {
                        if (!Strings.isBlank(linkVo.getEntityId()) && linkVo.getEntityId().contains(value))
                            searchAdd = true;
                        else if (!Strings.isBlank(linkVo.getEntityName()) && linkVo.getEntityName().contains(value))
                            searchAdd = true;
                        else if (!Strings.isBlank(linkVo.getEntityDesp()) && linkVo.getEntityDesp().contains(value))
                            searchAdd = true;
                    } else if (field.indexOf(",") > 0 && value.indexOf(",") > 0) {
                        List<String> fieldList = Arrays.asList(field.split(","));
                        List<String> valueList = Arrays.asList(value.split(","));
                        if (fieldList.size() == 3 && valueList.size() == 3) {
                            if (fieldList.indexOf("entityId") > -1 && fieldList.indexOf("entityName") > -1 && fieldList.indexOf("entityDesp") > -1) {
                                int entityIdIndex = fieldList.indexOf("entityId");
                                int entityNameIndex = fieldList.indexOf("entityName");
                                int entityDespIndex = fieldList.indexOf("entityDesp");
                                boolean idContain = linkVo.getEntityId().contains(valueList.get(entityIdIndex)) ||
                                        valueList.get(entityIdIndex).equals(" ");
                                boolean nameContain = linkVo.getEntityName().contains(valueList.get(entityNameIndex)) ||
                                        valueList.get(entityNameIndex).equals(" ");
                                boolean despContain = linkVo.getEntityDesp().contains(valueList.get(entityDespIndex)) ||
                                        valueList.get(entityDespIndex).equals(" ");
                                if (idContain == true && nameContain == true && despContain == true)
                                    searchAdd = true;
                            }
                        }
                    } else {

                    }
                } else
                    searchAdd = true;
            }

            if (searchAdd == true)
                addIndex = addIndex + 1;
            if (searchAdd == true && pageAdd == true)
                entityList.add((TypeLinkVo) linkVo.clone());
        }
        sort(entityList);
        return new QueryTypeLinkVo(addIndex, entityList);
    }

    protected QueryTypeLinkVo parseTree(List<TypeLinkVo> linkList, TypeLinkVo parentVo, QueryTypeLinkVo typeLinkVo)
    {
        parentEntityId = parentVo.getEntityId();
        parentVo.getChildrenList().clear();
        // 缓存成字典
        HashMap<String, TypeLinkVo> entityDic = new HashMap<>();
        for (TypeLinkVo vo: linkList) {
            entityDic.put(vo.getEntityId(), (TypeLinkVo)vo.clone());
        }

        // 递归获取子节点
        List<String> childrenEntityIdList = new ArrayList();
        List<List<String>> allPathList = new ArrayList<>();
        TypeLinkVo newParentVo = entityDic.get(parentVo.getEntityId());
        if (newParentVo != null) {
            if (Strings.isBlank(newParentVo.getChildrenEntityId()) == false)
                childrenEntityIdList = Arrays.asList(newParentVo.getChildrenEntityId().split(","));
            if (childrenEntityIdList.size() > 0)
            {
                pathIndex = -1;
                for (String entityId: childrenEntityIdList) {
                    pathIndex += 1;
                    List<String> pathList = new ArrayList<>();
                    pathList.add(entityId);
                    allPathList.add(pathList);

                    TypeLinkVo vo =(TypeLinkVo)entityDic.get(entityId).clone();

                    TypeLinkVo childrenVo = recursiveTree(vo, entityDic, allPathList);
                    parentVo.getChildrenList().add(childrenVo);
                }
            }
        }
        return typeLinkVo;
    }

    protected TypeLinkVo recursiveTree(TypeLinkVo parent, HashMap<String, TypeLinkVo> entityDic, List<List<String>> allPathList) {
        List<String> childrenEntityIdList = new ArrayList();
        if (parent.getChildrenEntityId() != null && parent.getChildrenEntityId().length() > 0) {
            childrenEntityIdList = Arrays.asList(parent.getChildrenEntityId().split(","));
            childrenEntityIdList = ListUtil.removeDuplicate(childrenEntityIdList);
        }
        if(childrenEntityIdList.size() > 0) {
            for (int index = 0; index < childrenEntityIdList.size(); index++) {
                //当前处理的id
                String entityId = childrenEntityIdList.get(index);
                //如果存在兄弟节点，在第一个节点的时候，copy这个节点的所有路径到新的路径中
                if (index == 0 && childrenEntityIdList.size() > 1) {
                    for (int count = 0; count < childrenEntityIdList.size() - 1; count++) {
                        List<String> newPathList = new ArrayList<>();
                        newPathList.addAll(allPathList.get(pathIndex));
                        allPathList.add(count + pathIndex,newPathList);
                    }
                }
                //当存在兄弟节点，并且不为第一个下标，则把当前路径链的下标+1，意味着在新的路径链下
                if (childrenEntityIdList.size() > 1 && index > 0) {
                    pathIndex = pathIndex + 1;
                }
                if (entityDic.get(entityId) != null) {
                    //判断当前父-子节点组成的两个元素存在于路径中则跳过该组合
                    List<String> currentPathList = allPathList.get(pathIndex);
                    boolean isDouble = false;
                    if (currentPathList != null && currentPathList.size() > 1) {
                        for (int i = 0; i < currentPathList.size() - 1; i++) {
                            String path = currentPathList.get(i);
                            if (path.equals(entityId)) {
                                isDouble = true;
                                break;
                            }
                        }
                    }
                    List<String> pathList = allPathList.get(pathIndex);
                    pathList.add(entityId);
                    //如果出现重复，添加当前节点后结束循环
                    if (isDouble == false) {
                        System.out.println(parent.getEntityId()+":"+entityId);
                        TypeLinkVo childrenVo = recursiveTree(entityDic.get(entityId), entityDic, allPathList);
                        parent.getChildrenList().add(childrenVo);
                    } else {
                        TypeLinkVo addVo = (TypeLinkVo) entityDic.get(entityId).clone();
                        parent.getChildrenList().add(addVo);
                    }
                }
            }
        }
        return parent;
    }

    protected void sort(List<TypeLinkVo> list) {
        Collections.sort(list, new Comparator<TypeLinkVo>() {
            @Override
            public int compare(TypeLinkVo o1, TypeLinkVo o2) {
                return o1.getEntityId().compareTo(o2.getEntityId());
            }
        });
    }
}
