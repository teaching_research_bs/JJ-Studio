package cn.easyplatform.studio.cmd.customWidget;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.CustomWidgetVo;

public class SaveWidgetCmd implements Command<Boolean> {
    private CustomWidgetVo customWidgetVo;
    public SaveWidgetCmd(CustomWidgetVo customWidgetVo) {
        this.customWidgetVo = customWidgetVo;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        customWidgetVo.setCreateUser(cc.getUser().getUserId());
        return cc.getCustomWidgetDao().addWidget(cc.getProject().getEntityTableName() + "_widget_info",
                customWidgetVo);
    }
}
