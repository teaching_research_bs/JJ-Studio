package cn.easyplatform.studio.cmd.help;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
import cn.easyplatform.studio.vos.GuideConfigVo;
import cn.easyplatform.studio.web.editors.help.GuideConfigEditor;
import cn.easyplatform.web.ext.introJs.Step;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetGuideConfigCmd implements Command<List<GuideConfigVo>> {
    private GuideConfigEditor.GuideConfigType guideConfigType;

    public GetGuideConfigCmd(GuideConfigEditor.GuideConfigType guideConfigType) {

        this.guideConfigType = guideConfigType;
    }

    @Override
    public List<GuideConfigVo> execute(CommandContext cc) {
        List<GuideConfigVo> list = cc.getGuideConfig(guideConfigType).getGuideConfig(guideConfigType);
        return createStep(list);
    }


    private List<GuideConfigVo> createStep(List<GuideConfigVo> guideConfigVos) {
        if (guideConfigVos != null && guideConfigVos.size() > 0) {
            Map<Integer, String> vos = new HashMap<>();
            for (GuideConfigVo guideVo : guideConfigVos) {
                vos.put(Integer.valueOf(guideVo.getGuideID()), guideVo.getGuideName());
            }
            for (GuideConfigVo guideVo : guideConfigVos) {
                if (Strings.isBlank(guideVo.getGuideStep()) == false) {
                    List stepList = JSON.parseArray(guideVo.getGuideStep(), Step.class);
                    if (Strings.isBlank(guideVo.getGuideNextID()) == false)
                        guideVo.setGuideNextName(vos.get(Integer.valueOf(guideVo.getGuideNextID())));
                    guideVo.setStepList(stepList);
                }
            }
        }
        return guideConfigVos;
    }

    public static List<List<GuideConfigVo>> allTree(List<GuideConfigVo> allGuideConfigVos) {
        List<GuideConfigVo> allTree = new ArrayList<>();
        Map<Integer, GuideConfigVo> vos = new HashMap<>();
        for (GuideConfigVo vo : allGuideConfigVos) {
            vos.put(vo.getGuideID(), vo);
        }

        for (GuideConfigVo guideVo : allGuideConfigVos) {
            if (guideVo.getIsRoot().equals("R")) {
                allTree.add(guideVo);
            }
        }

        // 2、递归获取子节点
        List<List<GuideConfigVo>> allPathList = new ArrayList<>();
        for (GuideConfigVo parent: allTree) {
            List<GuideConfigVo> pathList = new ArrayList<>();
            allPathList.add(pathList);
            recursiveTree((GuideConfigVo) parent.clone(), vos, allPathList);
        }
        return allPathList;
    }

    private static void recursiveTree(GuideConfigVo nextID, Map<Integer, GuideConfigVo> vos, List<List<GuideConfigVo>> allPathList) {
        List<GuideConfigVo> pathList = allPathList.get(allPathList.size() - 1);
        pathList.add((GuideConfigVo) nextID.clone());
        if (Strings.isBlank(nextID.getGuideNextID()) == false) {
            if (vos.keySet().contains(Integer.valueOf(nextID.getGuideNextID()))) {
                GuideConfigVo configVo = vos.get(Integer.valueOf(nextID.getGuideNextID()));
                recursiveTree(configVo, vos, allPathList);
            }
        }
    }
}
