package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;


/**
 * @Author Zeta
 * @Version 1.0
 */
public class LoginlogDeleteAllCmd implements Command<Boolean> {

    public LoginlogDeleteAllCmd() {
    }

    @Override
    public Boolean execute(CommandContext cc) {
        return cc.getLoginlogDao().deleteLoginlogAll(cc.getProject().getEntityTableName()+"_login_log");
    }

}
