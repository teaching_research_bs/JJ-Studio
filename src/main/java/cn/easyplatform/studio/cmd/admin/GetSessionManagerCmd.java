package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.cfg.SessionManager;
import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;
/**
 * @Author Zeta
 * @Version 1.0
 */

public class GetSessionManagerCmd implements Command<SessionManager> {

    public GetSessionManagerCmd() {
    }

    @Override
    public SessionManager execute(CommandContext cc) {
        return cc.getSessionManager();
    }
}
