package cn.easyplatform.studio.cmd.admin;

import cn.easyplatform.studio.interceptor.Command;
import cn.easyplatform.studio.interceptor.CommandContext;


/**
 * @Author Zeta
 * @Version 1.0
 */
public class LoginlogDeleteCmd implements Command<Boolean> {

    private String str;

    public LoginlogDeleteCmd(String str) {
        this.str = str;
    }

    @Override
    public Boolean execute(CommandContext cc) {
        return cc.getLoginlogDao().deleteLoginlogById(cc.getProject().getEntityTableName()+"_login_log",str);
    }

}
