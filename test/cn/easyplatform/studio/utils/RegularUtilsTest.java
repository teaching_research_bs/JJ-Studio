package cn.easyplatform.studio.utils;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RegularUtilsTest {

    @Test
    public void getLogicGrammar() {
        List<String> list = RegularUtils.getLogicGrammar("#include \'asd\';" +
                "#APP.refresh(\"String id\");" +
                "#db.selectObject(\"select * from abc\");" +
                "#db.selectObject(\"String dsId\", \"select * from def\");" +
                "#db.selectOne(\"select * from fgh\");" +
                "#db.selectOne(\"String dsId\", \"select * from ijk\");" +
                "#db.selectList(\"String dsId\", \"select * from opq\", 5,5, \"String orderBy\");" +
                "#db.selectList(\"select * from rst\", 5, 8,\"String orderBy\");" +
                "#db.transfer(\"StringtableId\", \"#include \'uvw\';\");" +
                "#db.transfer(\"StringtableId\", \"a\", true);" +
                "#db.update(\'select * from b where id = \"ses\"\');" +
                "#db.update(\"StringtableId\",\'select * from c where id = \"ses\"\');" +
                "#db.update(\'select * from d where id = \"ses\"\', false);" +
                "#db.update(\"StringtableId\",\'select * from e where id = \"ses\"\', false);" +
                "#db.init(\"StringtableId1\");" +
                "#db.getEntity(\"StringtableId2\");" +
                "#db.lock(\"StringtableId3\", \"\", \"\");" +
                "#db.unlock(\"StringtableId4\", \"\", \"\");" +
                "listID.load(\'select * from f where id = \"ses\"\', \'sdw_\');" +
                "listID.load(5,\"select\",\'one\',\"#include \'g\';\");" +
                "listID.load(5,\'two\',\"#include \'h\';\");" +
                "go(\'taskid\', \"c\")" +
                "go(\'taskid2\')" +
                "go(\'taskid3\', \"D\", true)" +
                "invoke(\'taskid4\');" +
                "load(\"select * from i \");" +
                "load(\"select * from j \", \"k\");" +
                "load(9, \"l\");" +
                "load(9, \"m\",\'#include \"n\";\');" +
                "#bpm.getInstanceTask(\"StringprocessId\")" +
                "#bpm.startAndExecute(\"StringprocessId2\")" +
                "#bpm.startAndExecute(\"StringprocessId3\", \"String nodeName\")" +
                "#bpm.execute(\"StringtaskId4\")" +
                "#bpm.take(\"StringtaskId5\")" +
                "#bpm.withdraw(\"String taskId6\")" +
                "#bpm.executeAndJumpTask(\"String taskId7\",\"name\")" +
                "#bpm.reject(\"String taskId8\")" +
                "#bpm.resignTo(\"String taskId9\", \"String... actorIds\", \"String... actorIds\", \"String... actorIds\")" +
                "#bpm.resignTo(\"String taskId10\", 10,\"String... actorIds\")" +
                "#bpm.addTaskActor(\"String taskId11\",\"String... actorIds\")" +
                "#bpm.addTaskActor(\"String taskId12\", 12,\"String... actorIds\")" +
                "#bpm.removeTaskActor(\"String taskId13\",\"String... actorIds\")" +
                "#bpm.terminate(\"String taskId14\")" +
                "#bpm.getNodes(\"String taskId15\")" +
                "#bpm.getNextNodes(\"String taskId16\",\"name\")" +
                "#bpm.getActors(\"String taskId17\",\"name\")" +
                "#db.selectList(\"select * from lmn\")");
        System.out.println(list);
    }

    @Test
    public void getSQLGrammar() {
        RegularUtils.getSQLGrammar("select orgId,name from sys_org_info order by orgId");
    }

    @Test
    public void getLabelGrammar() {
        List<String> list = RegularUtils.getLabelGrammar("<datalist entity = \"000SYSORG01QY01\" showPanel=\"true\" " +
                "showRowNumbers=\"false\" sizedByContent=\"true\" event=\"open('U');selectObject('select * from s1')\"" +
                "span=\"true\" pageSize=\"30\" durable=\"true\" levelBy=\"orgId,parent\" pageId=\"orgId\" " +
                        "init=\"#app.refresh('213')\" before=\"#app.refresh('223')\" " +
                        "after=\"#app.refresh('233')\" dbId=\"sd1\" " +
                        "query=\"select * from s243\" condition=\"select * from s253\" " +
                        "logic=\"#app.refresh('263')\" orderId=\"sd2\" processId=\"sd3\" " +
                        "onSave=\"selectObject('2331','select * from s2');selectOne('select * from s3');" +
                        "selectOne('2331','select * from s4');selectList('select * from s5');" +
                        "selectList('2331','select * from s6');selectList('select * from s7',2,2,'asdasd')\" " +
                        "onNode=\"executeUpdate('select * from s8')\" taskEvent=\"executeUpdate('ad12','select * from s9')\" " +
                        "assigneeEvent=\"executeUpdate('select * from s10',true)\" " +
                        "assigneeTypeEvent=\"executeUpdate('ad13','select * from s11',true)\" " +
                        "assigneeRoleEvent=\"evalList('listid','con1');evalList('listid2','listid3','con2');\" " +
                        "decisionEvent=\"evalLogic('con3');evalExpr('con4');go('tasked1');go('tasked2','C');" +
                        "go('tasked3',4);go('tasked4',4,'c');go('tasked5',4,'c','dsa');\" autoCreateExpression=\"#app.refresh('273')\" " +
                        "expSql=\"select * from s12\" taskSql=\"select * from s13\" ></datalist><include src=\"page1\"/>" +
                        "<button id=\"mysave\" label=\"#{'B_save'}\" event=\"save();userList.reload();showCode('I001');executeUpdate('select * from I001',true)\"/>"
                );
        System.out.println(list);
    }
}